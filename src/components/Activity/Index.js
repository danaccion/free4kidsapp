import React, { Component } from 'react';
import { ScrollView, StyleSheet,View,ImageBackground,Image,TouchableOpacity,Platform,Dimensions,Text,AsyncStorage} from 'react-native';
import Ionicons from 'react-native-vector-icons/Ionicons'
import { Picker } from 'react-native-picker-dropdown';
import BackButton from '../../BackButton';
import HamburgerIcon from '../../icons/HamburgerIcon';
import HeartBagBurger from '../../icons/HeartBagBurger';
import { Container, Content, List, ListItem, Thumbnail,Icon,Button,InputGroup,Input,Body,Right,Badge } from 'native-base';
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';
import Dialog, {DialogFooter,DialogButton,SlideAnimation,DialogContent } from 'react-native-popup-dialog';
import { PH } from 'react-native-dotenv'
import {all_notifications} from '../actions/index'
import { connect } from 'react-redux';

class Index extends Component {
  static navigationOptions = () => {
    return {
        headerLeft: <HamburgerIcon/>,
        headerTitle: (
            <Image
            resizeMode="contain"
            source={require('../../../assets/images/slice2.png')}
            style={{ width: 10, height:10 , flex:4,justifyContent: 'space-around', alignItems: 'center', padding: 25,}}
          />
          ),
        headerRight: <HeartBagBurger/>
    };
};

   
    state = {
      socket:null,
      username: '',
      password: '',
      ButtonStateHolder : true,
      loading: false,
      fontLoaded:false,
      preferences:'',
      popupVisible:false,
      notificationList:null,
      noData:false,
      refreshing:false

    };
  
    back = async ()=>{
    this.props.navigation.navigate('Home');
    }
    submit =()=>{
      //this.props.navigation.navigate('Home');
    }
    messages =()=>{
      this.props.navigation.navigate('ActivityChat');
     }

     async componentDidMount(){
      this.getNotification();
    }

    getNotification = async () =>{
      let notificationList=await AsyncStorage.getItem('notificationList');
      if(notificationList!==null){
        this.props.loadNotifications(JSON.parse(notificationList))
      }
      let userID=await AsyncStorage.getItem('user_id');
      fetch(PH+'/api/v1/users/notification?user_id='+userID, {
        method: 'POST',
        headers: {
          'Accept':'application/json',
          'Content-Type': 'multipart/form-data',
          'Authorization': 'Bearer '+await AsyncStorage.getItem('token')
        },
        }).then((response) => response.json())
            .then(async (responseJson) => {
              // this.setState({notificationList:responseJson.notification})
              console.log(responseJson.meta.notification);
              if(responseJson.meta.notification.length>0){
                this.props.loadNotifications(responseJson.meta.notification)
                this.setState({refresh:false,refreshing:false})
                await AsyncStorage.setItem('notificationList', JSON.stringify(responseJson.meta.notification) )
                .then( ()=>{
                console.log("succesfully Saved")
                } )
                .catch( ()=>{
                console.log('‘There was an error saving the product’')
                } )
              } else {
                console.log("ayay")
                this.props.loadNotifications(null)
                this.setState({noData:true,refreshing:false})
              }
        })
            .catch((error) => {
                console.log(error)
        });
    }

    
  render(){
    const resizeMode = 'cover';
    return (
  
      <View style={styles.container}>
       
      <ImageBackground
         style={{
           backgroundColor: '#ccc',
           flex: 1,
           resizeMode,
           position: 'absolute',
           width: '120%', 
           height: '100%',
           justifyContent: 'center',
           
         }}
         source={require('../../../assets/images/slice81-min.png')}
         />
       
       <View style={{flex: 1, flexDirection: 'row',justifyContent:'space-between', margin:15,marginTop:Dimensions.get('screen').height/50}}>
       <Dialog
          visible={this.state.popupVisible}
          onHardwareBackPress={() => { this.setState({ popupVisible: false }); } }
          onTouchOutside={() => {
            this.setState({ popupVisible: false });
          }}
        >
          
          <DialogContent style={{width:wp('90%')}}>
          {/* <Image  resizeMode="contain" style={{width:wp('90%') ,height:hp('20%'),alignSelf:'center'}} source={require('../../../assets/images/slice70-min.png')}/> */}
          <Text adjustsFontSizeToFit={true} style={{color:'black',fontSize:20,alignSelf:'center',justifyContent:'center',top:3}}>stadig på udvikling..</Text>
          </DialogContent>
        </Dialog>
           <View>
            
         </View>
         <View>
          <Text  style={{ fontSize: 25 ,alignSelf:'center',color:'#2db9b9',fontFamily:'KGPrimaryWhimsy'}}> Notifikationer</Text>
          </View>
          <View>
          <TouchableOpacity onPress={this.back.bind(this)} loading={true}>
            <Image resizeMode='contain' source={require('../../../assets/images/slice50-min.png')} style={{width: 20,height: 20,}}></Image>
            
         </TouchableOpacity>
         </View>
      </View>
      <View style={styles.container3}>
      <Container style={{backgroundColor:'transparent'}} >
      <Content>
      {this.props.allnotifications==null?null:this.props.allnotifications.map((prop, key) => {
         return (
           
              <List style={{backgroundColor:'white',borderColor:'grey',borderWidth:.5,borderRadius: 15,marginBottom:7,margin:5,shadowColor: '#000',
              shadowOffset: { width: 0, height: 2 },
              shadowOpacity: 0.5,
              shadowRadius: 1.5,
              elevation: 1}} key={prop.id}>
                  <ListItem button onPress={this.back}>
                   <Body style={{alignContent:'center'}}>
                    {/* <Thumbnail large square source={{ uri: prop.image}} style={{marginRight:30}}  /> */}
                      <Text style={{fontFamily:'KGPrimaryWhimsy',fontSize: 20,color:'grey' }}>{prop.description}</Text>
                      <Text style={{fontFamily:'KGPrimaryWhimsy',fontSize: 12,color:'grey' }}>{prop.message}</Text>
                    </Body> 
                    <Right>
                    {prop.identifier === "followed" ?
                        <View>
                          <TouchableOpacity onPress={this.messages.bind(this)} loading={true}>
                            {/* <Badge style={{position:'absolute',  height:20, backgroundColor: 'orange', top: -5,right:1,zIndex:100}}>
                            <Text style={{color:'white'}}>{prop.notif_count}</Text>
                          </Badge> */}
                         {/* <Thumbnail resizeMode='contain' square small source={require('../../../assets/images/slice52-min.png')} style={{marginRight:5}}/> */}
                         <Thumbnail  small source={{ uri: prop.image}} style={{marginRight:5}}/>
                         </TouchableOpacity>
                         </View>
                            :
                            <Icon name="ios-arrow-forward" />
                      }
                   
                     
                    </Right>
                     
                  </ListItem>
                  {/* <Icon ios='ios-arrow-forward' android='md-arrow-dropright' iconRight/> */}
             </List>           
         );
         
      })}
        </Content>
       </Container>
     
      </View>
            
  </View>
   
    );
  }
  }

  const mapStateToProps = (state) =>{
    console.log("redux",state.count.allnotifications);
    return{
      allnotifications:state.count.allnotifications,
    }
  }

  const mapDispatchToProps = (dispatch) =>{
    return{
      loadNotifications:(data)=>dispatch(all_notifications(data))
    }
  }
  
export default connect(mapStateToProps,mapDispatchToProps)(Index)
   
   

const styles = StyleSheet.create({
  container: {
    flex: 1,
    //alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: 'transparent'
  },
  container2: {
    flex: 1,
    //alignItems: 'center',
    //justifyContent: 'flex-start',
    
    backgroundColor: 'transparent'
    //bottom: Dimensions.get('screen').height /5
  },
  textstyle:{
    fontFamily:'KGPrimaryWhimsy',
    fontSize:40
  },
  container3: {
    position:'absolute',
    backgroundColor: 'transparent',
    flex: 1,
    height:hp('79%'),
    width:wp('100%'),
    bottom:0,
    top:45
    //justifyContent: "flex-end",
    //alignItems: "center"
  },
  dropdown: {
    height: Dimensions.get('screen').width /7,
    padding: 5,
    position:'relative',
    margin:5,
    borderWidth: .2,
    borderRadius: 12,
    textShadowColor: 'black',
    borderColor: 'black',
    marginBottom: 10,
    backgroundColor: 'white',
    opacity: 0.9,
    fontFamily:'KGPrimaryWhimsy',
    fontSize:20,
    alignContent:'center'
  },
  wrpper: {
    opacity: 0.9,
    fontSize:20,
    fontFamily:'KGPrimaryWhimsy',
    flexDirection: 'row', justifyContent: 'space-around',   
  },
  button: {
    backgroundColor: '#54969c',
    borderColor: 'white',
    borderWidth: .5,
    borderRadius: 12,
    color: 'white',
    overflow: 'hidden',
    padding: 12,
    textAlign:'center',
    width: 300,
    height: 48,
    opacity: 0.9,
    fontSize:20,
    fontFamily:'KGPrimaryWhimsy'
  },
});

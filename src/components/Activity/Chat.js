import React, { Component } from 'react';
import {StyleSheet,View,ImageBackground,Image,TouchableOpacity,TextInput,Platform,Dimensions,AsyncStorage,Keyboard,StatusBar,TouchableWithoutFeedback,KeyboardAvoidingView} from 'react-native';
import BackButton from '../../BackButton';
import HamburgerIcon from '../../icons/HamburgerIcon';
import UUIDGenerator from 'react-native-uuid-generator';
import HeartBagBurger from '../../icons/HeartBagBurger';
import { Container, Content, List, ListItem, Thumbnail,Icon,Button,InputGroup,Input,Body,Right,Badge,Text,Left,Textarea,Form,Item } from 'native-base';
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view'
import SwipeableRating from 'react-native-swipeable-rating';
import Modal from 'react-native-modal';
import ModalBox  from 'react-native-modalbox'
import io from 'socket.io-client'
import moment from 'moment'
import { GiftedChat } from 'react-native-gifted-chat'
import KeyboardSpacer from 'react-native-keyboard-spacer';
import {COMMUNITY_CHAT,PRIVATE_MESSAGE,USER_DISCONNECTED,VERIFY_USER,MESSAGE_RECEIVED,MESSAGE_SENT,TYPING} from '../chatEvents/events'
const Fsize2 = Platform.OS === 'ios' ?25 : 18;
const Fsize = Platform.OS === 'ios' ?22 : 16;
const { width, height } = Dimensions.get('window');
const TAB_BAR_HEIGHT = 49;
const  socketURL= 'http://192.168.1.157:3231'
export default class Chat extends Component {
  static navigationOptions = () => {
    return {
        headerLeft: <HamburgerIcon/>,
        headerTitle: (
            <Image
            resizeMode="contain"
            source={require('../../../assets/images/slice2.png')}
            style={{ width: 10, height:10 , flex:4,justifyContent: 'space-around', alignItems: 'center', padding: 25,}}
          />
          ),
        headerRight: <HeartBagBurger/>
    };
};

 
    state = {
      socket:null,
      user:null,
      username: 'MAONI',
      password: '',
      ButtonStateHolder : true,
      loading: false,
      fontLoaded:false,
      preferences:'',
      panelMargin:0,
      chatMessages:[],
      chats:[],
      messages: '',
      userid:'',
      userImage:'',
      activeChat:null,
      isModalVisible:false,
      reservationModal:false,
      rating:2,
      swipeablePanelActive: false,
      reportModal:false
    };


  async componentDidMount() {
    // const {socket}=this.state
    
    // this.initSocket()
    // socket.emit(COMMUNITY_CHAT,this.resetChat)
     this.setState({userid:await AsyncStorage.getItem('user_id'),userImage:await AsyncStorage.getItem('image_url')})
  //  this.connect()
      // this.socket= io("http://192.168.1.157:3000");
      // this.socket.on("chat message", msg => {
      //   chatData.unshift(msg)
      //   this.setState({ chatMessages:chatData  });
      // });
     
    }


  
  toggleModal = () => {
    this.refs.slideUp.close()
    this.setState({isModalVisible: !this.state.isModalVisible});
  };
  toggleModalReservation = () => {
    this.refs.options.close()
    this.setState({reservationModal: !this.state.reservationModal});
  };
  toggleModalReport = () => {
    this.refs.options.close()
    this.setState({reportModal: !this.state.reportModal});
  };
 
  initSocket =() =>{
    const socket= io(socketURL)
    socket.on('connect',()=>{
      console.log("connected")
    })
    this.setState({socket})
  
  }

  // sendMessage=(chatId,message)=>{

  //  const { navigation } = this.props;
  // const item = navigation.getParam('item');
  // const user_id=await AsyncStorage.getItem('user_id')
  // const username=await AsyncStorage.getItem('name')
  //   // messages[0]._id=this.randomStr(24, this.state.user_id);
  //   // console.log(messages[0])
  //   this.socket.emit("sendmessage",messages[0])
  //   // this.setState(previousState => ({
  //   //   messages: GiftedChat.append(previousState.messages, messages),
  //   // }))
  //    this.setState({ chatMessages: [...this.state.chatMessages, messages[0]] });

  // }

  sendTyping = (chatId,isTyping)=>{
      const {socket} = this.state
      socket.emit(TYPING, {chatId,isTyping})
  }

  setActiveChat = (activeChat) =>{
      this.setState({activeChat})
  }

  disconnect = () => {
    const {socket} = this.state
    socket.emit(USER_DISCONNECTED)
    this.setState({user:null})
  }
  
 connect = () => {
  var chatData=[]
    this.socket = io("http://192.168.1.157:3000", {
      autoConnect: false,
    });
     this.socket.on("chat message", msg => {
      chatData.unshift(msg)
      this.setState({ chatMessages:chatData  });
    });
  
    // this.socket.on('connect', () => {
    //   console.log('Connected');
    //   // statusInput.value = 'Connected';
    // });
  
    this.socket.on('disconnect', (reason) => {
      console.log(`Disconnected: ${reason}`);
      // statusInput.value = `Disconnected: ${reason}`;
    })
  
    this.socket.open();
  };
  
 
   
 
 
  back = async ()=>{
  this.props.navigation.navigate('ReservationChat');
  }

   
   

 async submitMesssage(messages){
  const { navigation } = this.props;
  const item = navigation.getParam('item');
  console.log(messages)
    // this.textInput.clear()
    // const messages ={id:this.randomid(24,'123456abcdef'),sender_id:this.state.userid,
    // text:this.state.messages,time:this.getDateNow(),image_url:this.state.userImage,receiver_id:null}
    // console.log(messages)
    // this.socket.emit("chat message",messages)
    // this.setState({messages:''})
  
  }

  getDateNow(){
       
    var date = moment()
    .format(' hh:mm a');
    return date;
        
  }
  randomid(len, arr) { 
    var ans = ""; 
    for (var i = len; i > 0; i--) { 
        ans +=  
          arr[Math.floor(Math.random() * arr.length)]; 
    } 
    return ans; 
  } 
  

    buyshipping = async () => {
      this.props.navigation.navigate('BuyShipping');
    }
    basket_overview = async () => {
      this.props.navigation.navigate('BasketOverview');
    }
    messages =()=>{
      this.props.navigation.navigate('ActivityChat');
     }
    isCloseToBottom = ({ layoutMeasurement, contentOffset, contentSize }) => {
      return  contentOffset.y == 0;
    };

  render(){
    console.log(this.state.userid)
    const resizeMode = 'cover';
    const config = {
      velocityThreshold: 0.3,
      directionalOffsetThreshold: 80
    };
    const extraHeight = Platform.OS === 'ios' ? 50 : 100
    
    const renderMessageText = (props) => {
      const {
        currentMessage,
      } = props;
      const { user: currUser } = currentMessage;
      if (currUser._id+"" === this.state.userid+"") {
                return <View  style={{backgroundColor:'white',borderColor:'#bfbfbf',borderWidth:.5,marginBottom:7,borderRadius: 15,padding:10}}>

                <MessageText {...props} style={{flex: 1}} textStyle={{
                    left: {
                      color: 'black',
                      fontSize:chatText
                    },
                    right: {
                      color: 'black',
                      fontSize:chatText
                    },
                  }}/>
                <View style={{bottom: -5,right:-2,zIndex:100,position:'absolute'}}>
                <ProgressiveImage  resizeMode='contain'  source={{ uri: this.state.userImage}} style={{width:Fsize2,height:Fsize2,borderRadius:Fsize2/2}}/>
                </View>
        </View>
      }
      return (  <View  style={{backgroundColor:'white',borderColor:'#bfbfbf',borderWidth:.5,borderRadius: 15,marginBottom:7,padding:10,left:-40}}>

                    <MessageText {...props} style={{flex: 1}} textStyle={{
                        left: {
                          color: 'black',
                          fontSize:chatText

                        },
                        right: {
                          color: 'black',
                          fontSize:chatText
                        },
                      }}/>
                    <View style={{bottom: -5,left:-2,zIndex:100,position:'absolute'}}>
                    <ProgressiveImage  resizeMode='contain'  source={{ uri: item.users.image_url}} style={{width:Fsize2,height:Fsize2,borderRadius:Fsize2/2}}/>
                    </View>
        </View>)
            
      
    };
    const renderBubble = (props) => { 
   
  
      return <Bubble {...props} 
        wrapperStyle={{
            left: {
              backgroundColor: 'transparent',
            },
            right: {
              backgroundColor: 'transparent'
            }
          }} 
  
        timeTextStyle={{
          left: {
            color: '#000',
          },
          right: {
            color: '#000',marginBottom:20
          },
        }}
      />
    }
    const  renderInputToolbar = (props) => { 

     return (
     <View style={{alignContent:'center',alignItems:'center',left:5,bottom:-20}}>
     <InputToolbar {...props} containerStyle={{position:'relative',justifyContent:'center',
     alignItems: 'center',backgroundColor:'white',width:wp('98%'),bottom:3,borderColor: '#bfbfbf',
     borderWidth: .5,
     borderRadius: 12,shadowColor: '#000',
     shadowOffset: { width: 0, height: 2 },
     shadowOpacity: 0.5,
     shadowRadius: 1.5,
     elevation: 1}}/>
     </View>
     )
   }
   const  sendButton = (props) => { 
   return <Send
   {...props}
   containerStyle={{
     height: 40,
     width: 40,
     justifyContent: 'center',
     alignItems: 'center',
   }}
 >
    <Icon style={{padding:10,  color:"grey",bottom:10}} name="md-send" />
 </Send>
 }
    return (
      
      <View style={styles.container}>

     <StatusBar barStyle = "dark-content" hidden = {false} backgroundColor = "#00BCD4" translucent = {true}/>
      <ImageBackground
         style={{
           backgroundColor: '#ccc',
           flex: 1,
           resizeMode,
           position: 'absolute',
           width: '120%', 
           height: '100%',
           justifyContent: 'center',
           
         }}
         source={require('../../../assets/images/slice81-min.png')}
         />
              <ModalBox  position={"bottom"} style={{height:hp('70%'),zIndex:999,borderTopLeftRadius: 20, 
        borderTopRightRadius: 20,
        overflow: "hidden"}} transparent={false} ref={"slideUp"} >
        <View style={{flex:1,flexDirection:'column',alignContent:'center',alignItems:'center',justifyContent:'flex-start'}}>
        <Body style={{width:wp('85%'),alignItems:'center',top:-20}}>
        <Image resizeMode='contain' style={{width:wp('50%'),height:hp('50%')}} source={require('../../../assets/girls_info/slice4.png')} />        
        <TouchableOpacity onPress={()=>console.log("kob")} style={{marginBottom:20,marginTop:-30}}>
          <Text style={styles.button2}>Køb porto</Text>
        </TouchableOpacity>
        <TouchableOpacity onPress={()=>console.log("ayay")} style={{marginBottom:20}}>
          <Text style={styles.button2}>Afstand på kort</Text>
        </TouchableOpacity>
        <TouchableOpacity onPress={()=>this.toggleModal()}>
          <Text style={styles.button5}>Afslut vores aftale</Text>
        </TouchableOpacity>
        </Body>
        </View>       
            </ModalBox>   
            <ModalBox  position={"bottom"} style={{height:hp('25%'),zIndex:999,borderTopLeftRadius: 20, 
        borderTopRightRadius: 20,
        overflow: "hidden"}} transparent={false} ref={"options"} >
        <View style={{flex:1,flexDirection:'column',alignContent:'center',alignItems:'center',justifyContent:'flex-end'}}>
        <Body style={{width:wp('85%'),alignItems:'center',marginTop:30}}>
        <TouchableOpacity onPress={()=>console.log("to profile")}
          // this.props.navigation.navigate('UserProfile')} 
          style={{marginBottom:10}}>
          <Text style={styles.button3}>Se profil</Text>
        </TouchableOpacity>
        <TouchableOpacity onPress={()=>this.toggleModalReservation()} style={{marginBottom:10}}>
          <Text style={styles.button3}>Fortryd reservation</Text>
        </TouchableOpacity>
        <TouchableOpacity onPress={()=>this.toggleModalReport()} style={{marginBottom:10}}>
          <Text style={styles.button3}>Rapport</Text>
        </TouchableOpacity>
        </Body>
        </View>       
            </ModalBox> 
       <View style={{flex: 1, flexDirection: 'row',justifyContent:'space-between', margin:15,marginTop:Dimensions.get('screen').height/50}}>
       <Modal isVisible={this.state.reservationModal} style={{height:hp('50%')}} onBackdropPress={()=>this.setState({reservationModal:false})} animationOut="fadeOutLeft" onSwipeStart={()=>console.log("ayay")}>
       <TouchableOpacity style={{position:'relative',alignSelf:'flex-end',height:60,width:60,zIndex:99,right:25,top:-85}} onPress={()=>this.toggleModalReservation()}>
       </TouchableOpacity>
       <TouchableOpacity style={{position:'relative',alignSelf:'center',height:60,width:80,zIndex:99,top:70}} onPress={()=>console.log("Undo Reserve")}>
          
       </TouchableOpacity>
       <Image
         style={{
           zIndex:10,
           resizeMode:'contain',
           position: 'absolute',
           alignSelf:'center',
          width:wp('70%'),
          height:wp('70%'),
           justifyContent: 'center',
           
         }}
         source={require('../../../assets/icons/slice12.png')}
         />
         
       </Modal>
       <Modal isVisible={this.state.reportModal} style={{height:hp('50%')}} onBackdropPress={()=>this.setState({reportModal:false})} animationOut="fadeOutLeft" onSwipeStart={()=>console.log("ayay")}>
       <TouchableOpacity style={{position:'relative',alignSelf:'flex-end',height:60,width:60,zIndex:99,right:25,top:-85}} onPress={()=>this.toggleModalReport()}>
       </TouchableOpacity>
       <TouchableOpacity style={{position:'relative',alignSelf:'center',height:60,width:80,zIndex:99,top:60}} onPress={()=>console.log("Reported")}>
          
       </TouchableOpacity>
       <Image
         style={{
           zIndex:10,
           resizeMode:'contain',
           position: 'absolute',
           alignSelf:'center',
          width:wp('70%'),
          height:wp('70%'),
           justifyContent: 'center',
           
         }}
         source={require('../../../assets/icons/slice14.png')}
         />
         
       </Modal>
       <Modal isVisible={this.state.isModalVisible} style={{height:hp('50%')}} onBackdropPress={()=>this.setState({isModalVisible:false})} animationOut="fadeOutLeft" onSwipeStart={()=>console.log("ayay")}>
       
        <TouchableWithoutFeedback onPress={()=>Keyboard.dismiss()}>
       
          <View style={{backgroundColor:'white',alignContent:'center',alignItems:'center',height:hp('50%'),borderRadius: hp('10%')/1.2}}>
          <Text style={{alignSelf:'center',top:20,fontSize:Fsize2,fontFamily:'KGPrimaryWhimsy',color:'#d47d7d'}}>Bedøm familien</Text>
            <View style={{top:40}}>
             
            <SwipeableRating
            rating={this.state.rating}
            size={28}
            maxRating={6}
            gap={2}
            color='#e8b14a'
            emptyColor='#e8b14a'
            onPress={this.handleRating}
            allowHalves={true}
            style={{position:'absolute',alignSelf:'center'}}
        />
        </View>
        <View style={{top:80}}>
          
    
    <TextInput multiline  style={{maxHeight: 80}} rounded style={{backgroundColor:'white',height:100,width:wp('80%'),borderColor: '#bfbfbf',
    borderWidth: .5,
    borderRadius: 12,padding:15,paddingBottom:5,paddingTop:10,shadowColor: '#000',
    shadowOffset: { width: 0, height: 2 },
    shadowOpacity: 0.5,
    shadowRadius: 1.5,
    elevation: 1,fontFamily:'KGPrimaryWhimsy',fontSize:Fsize,marginBottom:20}} 
    // onChangeText={text => this.setState({messages:text})}
    autoCorrect={true}
  //   onEndEditing={(e) => 
  //   {
  //       )
  //   }
  // }
   
    ref={input => { this.textInput = input }}
    />
   
   <TouchableOpacity onPress={() =>console.log("ayay")} >
      <Text style={styles.button5}>Afslut aftalen</Text>           
    </TouchableOpacity>
      </View>
      
          </View>
          </TouchableWithoutFeedback>
          
        </Modal>
           <View>
           <BackButton/>
         </View>
         <View>
         <Image source={require('../../../assets/images/slice73-min.png')} style={{resizeMode,width:40,height:hp('65%'),}}></Image>
          {/* <Text  style={{ fontSize: 25 ,alignSelf:'center',color:'#2db9b9',fontFamily:'KGPrimaryWhimsy'}}>Chat</Text> */}
          </View>
          <View>
          <TouchableOpacity onPress={()=>this.refs.options.open()}>
            <Image source={require('../../../assets/images/img-inf.png')} style={{width: 20,height: 20,}}></Image>
            
         </TouchableOpacity>
         
         </View>
         
      </View>
      
      {/* <KeyboardAwareScrollView keyboardShouldPersistTaps={'always'} extraScrollHeight={extraHeight} scrollEnabled={false} enableOnAndroid={true}>
      <View style={styles.container3}>
     
      <Container style={{backgroundColor:'transparent'}} >
      <Content  style={{  transform: [{ scaleY: -1 }] }}
        >
      {this.state.chatMessages.map((prop, key) => {
        if(prop.user_id==this.state.userid){
          return (
            <View key={prop.id} style={{width:wp('45%'),right:-wp('53%'), transform: [{ scaleY: -1 }]}}>
            <List style={{backgroundColor:'white',borderColor:'#bfbfbf',borderWidth:.5,borderRadius: 15,marginBottom:7,}}>
                <ListItem >
                 <Body style={{flexDirection:'column',alignContent:'center',alignSelf:'center',alignItems:'flex-start'}}>
                    <Text style={{fontFamily:'KGPrimaryWhimsy',fontSize: 20,marginRight:15 }}>{prop.name}</Text>
                    <Text note style={{fontSize:12,marginRight:25,color:'#737373',fontWeight:'500',marginTop:-10 }}>{prop.text}</Text>
                    <Thumbnail source={{ uri: prop.image_url}} style={{position:"absolute", bottom: -5,right:-6,zIndex:100,width:30,height:30}} />
                  </Body>  
                </ListItem>
           </List> 
           <Text note style={{left:100,marginBottom:10}}>{prop.time}</Text>
           </View>
       );   
        }else{
          return (
      <View key={prop.id} style={{width:wp('50%'), transform: [{ scaleY: -1 }]}}> 
            <List style={{backgroundColor:'white',borderColor:'#bfbfbf',borderWidth:.5,borderRadius: 15,marginBottom:7,margin:5}}>
                <ListItem>
                 <Body style={{flexDirection:'column',alignContent:'center',alignSelf:'center',alignItems:'flex-start'}}>
                   <Text style={{fontFamily:'KGPrimaryWhimsy',fontSize: 20,marginLeft:30 }}>{prop.name}</Text>
                    <Text note style={{fontSize: 12,marginLeft:30,color:'#737373',fontWeight:'500' }}>{prop.text}</Text>
                    <Thumbnail source={{ uri: prop.image_url}} style={{position:"absolute", bottom: -5,left:-6,zIndex:100,width:30,height:30}} />
                  </Body>      
                </ListItem>   
           </List> 
            <Text  note style={{marginLeft:10, marginBottom:10}}>{prop.time}</Text>   
           </View>
       );   
        }
       
      })}      
                    
        </Content>  
       </Container>
       
      </View>
       
      <View >
      <View style={styles.searchSection}>
    
    <TextInput multiline  style={{maxHeight: 80}} rounded style={{backgroundColor:'white',height:50,width:wp('80%'),borderColor: '#bfbfbf',
    borderWidth: .5,
    borderRadius: 12,padding:15,paddingBottom:5,paddingTop:10,shadowColor: '#000',
    shadowOffset: { width: 0, height: 2 },
    shadowOpacity: 0.5,
    shadowRadius: 1.5,
    elevation: 1}} 
    onChangeText={text => this.setState({messages:text})}
    autoCorrect={true}
  //   onEndEditing={(e) => 
  //   {
  //       )
  //   }
  // }
   
    ref={input => { this.textInput = input }}
    />
   
        <TouchableOpacity disabled={this.state.messages==""? true:false} onPress={()=>{this.submitMesssage()}}>
        <Icon style={{padding:10,  color:"grey"}} name="md-send" />
        </TouchableOpacity>
        
      </View>
      <TouchableOpacity  onPress={()=>this.refs.slideUp.open()}>
        <Icon style={{alignSelf:'center',bottom:-15,paddingTop:-5,color:"#707070",fontSize:25}} name="ios-arrow-up" />
        </TouchableOpacity>
        <KeyboardSpacer topSpacing={Platform.OS=='ios'? 0:-5}/>
        
     
       
            </View>
        
            </KeyboardAwareScrollView> */}
            <GiftedChat
              messages={this.state.messages}
              onSend={messages => this.submitMesssage(messages)}
              user={{
                _id:this.state.user_id,
                avatar: this.state.userAvatar,
              }}
              inverted={false}
              alwaysShowSend={false}
              showUserAvatar={false}
              avatar={true}
              reset
            />
            
       
  </View>
  
    );
    
  }
  }
   

const styles = StyleSheet.create({
  container: {
    flex: 1,
    //alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: 'transparent'
  },
  button5: {
    marginTop:5,
    backgroundColor: '#54969c',
    borderColor: 'white',
    borderWidth: .5,
    borderRadius: 12,
    color: 'white',
    overflow: 'hidden',
    padding: 12,
    textAlign:'center',
    width: wp('80%'),
    height: 48,
    opacity: 0.9,
    fontFamily:'KGPrimaryWhimsy',
    fontSize:20,
    alignSelf:'center',
    alignItems:'center'
  },
  container2: {
    flex: 1,
    //alignItems: 'center',
    //justifyContent: 'flex-start',
    
    backgroundColor: 'transparent'
    //bottom: Dimensions.get('screen').height /5
  },
  textstyle:{
    fontFamily:'KGPrimaryWhimsy',
    fontSize:40
  },
  container3: {
    position:'relative',
    backgroundColor: 'transparent',
    height:hp('65%'),
    width:wp('100%'),
    bottom:-10
    // bottom:hp('15%'),
    //justifyContent: "flex-end",
    //alignItems: "center"
  },
  searchIcon: {
    padding: 10,
},
searchSection: {
  flex: 1,
  flexDirection: 'row',
  justifyContent: 'center',
  alignItems: 'center',
  backgroundColor: 'transparent',
},
  dropdown: {
    height: Dimensions.get('screen').width /7,
    padding: 5,
    position:'relative',
    margin:5,
    borderWidth: .2,
    borderRadius: 12,
    textShadowColor: 'black',
    borderColor: 'black',
    marginBottom: 10,
    backgroundColor: 'white',
    opacity: 0.9,
    fontFamily:'KGPrimaryWhimsy',
    fontSize:20,
    alignContent:'center'
  },
  wrpper: {
    opacity: 0.9,
    fontSize:20,
    fontFamily:'KGPrimaryWhimsy',
    flexDirection: 'row', justifyContent: 'space-around',   
  },
  button: {
    backgroundColor: '#54969c',
    borderColor: 'white',
    borderWidth: .5,
    borderRadius: 12,
    color: 'white',
    overflow: 'hidden',
    padding: 12,
    textAlign:'center',
    width: 300,
    height: 48,
    opacity: 0.9,
    fontSize:20,
    fontFamily:'KGPrimaryWhimsy'
  },
  button2: {
    backgroundColor: 'white',
    borderColor: 'grey',
    borderWidth: .5,
    borderRadius: 12,
    color: '#54969c',
    overflow: 'hidden',
    padding: 12,
    textAlign:'center',
    width: wp('80%'),
    height: 48,
    opacity: 0.9,
    fontSize:20,
    fontFamily:'KGPrimaryWhimsy'
  },
  button3: {
    backgroundColor: 'white',
    borderColor: 'grey',
    borderWidth: .5,
    borderRadius: 12,
    color: '#54969c',
    overflow: 'hidden',
    padding: 10,
    textAlign:'center',
    width: wp('80%'),
    height: hp('5%'),
    opacity: 0.9,
    fontSize:Fsize,
    fontFamily:'KGPrimaryWhimsy'
  },
  bodyViewStyle: {
    flex: 1,
    justifyContent: 'center', 
    alignItems: 'center',
  },
  headerLayoutStyle: {
    width, 
    height: 20, 
    backgroundColor: '#54969c', 
    justifyContent: 'center', 
    alignItems: 'center',
  },
  slidingPanelLayoutStyle: {
   
    alignContent:'center',
    height:hp('100%'), 
    backgroundColor: 'transparent', 
    alignItems: 'center',
  },
  commonTextStyle: {
    color: 'white', 
    fontSize: 18,
  },
});

const { v4: uuidv4 } = require('uuid');



const createUser =({name = "",socketId = null}= {})=>({
    id:uuidv4(),
    name,
    socketId
})


const createMessage =({message = " ", sender = " ",time= " "})=>(
    {
        id:uuidv4(),
        time,
        sender,
        message
    }
)

const createChat = ({messages=[], name=" ",users=[] } ={})=>({
    id: uuidv4(),
    name,
    users,
    messages,
    typingUser:[]
})

module.exports = {
    createChat,createMessage,createUser
}
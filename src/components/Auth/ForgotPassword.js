import React, { Component } from 'react';
import { Alert, Button, TextInput, View, StyleSheet, ImageBackground, TouchableOpacity, Text, AsyncStorage,Image,StatusBar,Keyboard,TouchableWithoutFeedback,Dimensions } from 'react-native';
import Loader from '../../icons/Loader';
import * as Font from 'expo-font';
import Dialog, {DialogFooter,DialogButton,SlideAnimation,DialogContent } from 'react-native-popup-dialog';
import { Permissions, Contacts } from 'expo';
import { PH,SS } from 'react-native-dotenv'
// import AwesomeAlert from '../../icons/Alert';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view'
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';
import {
  Block,Input
} from 'galio-framework';
const { width, height } = Dimensions.get('window');
export default class Login extends Component {
  _isMounted = false;
    static navigationOptions = {
        //To hide the NavigationBar from current Screen
        header: null
      };
  constructor(props) {
    super(props);
    
    this.state = {
      username: '',
      ButtonStateHolder : true,
      loading: false,
      fontLoaded:false,
      popupVisible:false,
      popupErVisible:false,
      showDialogMessage:false,
        dialog_message:''
    };
  }
  permissionFlow = async () => {
    const { status } = await Permissions.askAsync(Permissions.CONTACTS);

    this.setState({ status });

    if (status !== 'granted') {
      Linking.openURL('app-settings:');
      return;
    }

    /*
      Get some data
    */
    const { data } = await Contacts.getContactsAsync({ pageSize: 1 });
    console.log(data[0]);
  };
  register() {
    //alert("maoni");
     this.props.navigation.navigate('SignUp');
    // this.props.navigation.navigate('Home');  
}
forgotPassword() {
  //alert("maoni");
   this.props.navigation.navigate('ForgotPassword');
  // this.props.navigation.navigate('Home');  
}
async componentDidMount() {
  console.log(PH)
  this._isMounted = true;
    await Font.loadAsync({
      'KGPrimaryWhimsy': require('../../../assets/fonts/KGPrimaryWhimsy.ttf'),
    });
  
    this.setState({ fontLoaded: true });
  }

  getProductsCount(){

  }

  onLogin() {
    this._isMounted = true;
    Keyboard.dismiss()
    this.setState({
      ButtonStateHolder : true,
      loading: true
    })
    console.log("email",this.state.username)
    console.log("email",this.state.password)
    const { username } = this.state;
      fetch(PH+'/forgot', {
        method: 'POST',
        headers: {
          'Accept':'application/json',
          'Content-Type': 'application/json',
        },
        body: JSON.stringify({
          email: this.state.username
        }),
        }).then((response) => response.json())
            .then((responseJson) => {
            
                
              if(responseJson.code!=200){
                console.log(responseJson.title)
                this.setState({
                  
                  loading: false,popupErVisible:true
                })
                Keyboard.dismiss();
              }else{
                console.log(responseJson)
                  if (this._isMounted) {
                    this.setState({
                      ButtonStateHolder : false,
                      loading: false,
                      popupVisible:true
                    })
                  }
                 
              }
        })
            .catch((error) => {
            
                this.setState({
                  ButtonStateHolder : false,
                  loading: false
                })
              console.log("ayay",error)
              this.setState({
                    showDialogMessage:true,
                    dialog_message:error
                })
            //alert(error); 
        });
}
componentWillUnmount() {
    this._isMounted = false;
  }
  render() {
    const resizeMode = 'cover';
      return (
        <KeyboardAwareScrollView>
       
        <Block flex={1} center middle safe fluid style={{width:width,height:height}}>
     
        <StatusBar barStyle = "dark-content" hidden = {false} backgroundColor = "#00BCD4" translucent = {true}/>
        <ImageBackground
        style={{
         backgroundColor: '#ccc',
          flex: 1,
          resizeMode:'stretch',
          position: 'absolute',
          width: wp('100%'),
          height: hp('120%'),
          top:-10
        }}
        source={require('../../../assets/images/slice81-min.png')}
      >
              
      </ImageBackground>
      
        <Block center  middle>
      <Text  style={styles.BestilTopText}>Bestil nyt kodeord</Text> 
      <Text style={styles.BestilTopTextchild}>Kan du ikke huske dit kodeord?</Text> 
      <Text style={styles.BestilTopTextchild}>Indtast din e-mail og vi sender dig et nyt</Text> 
      <Text style={styles.BestilTopTextchild}>kodeord.</Text> 
      

       <TextInput
         value={this.state.username}
         onChangeText={(username) => this.setState({ username, ButtonStateHolder:false })}
         placeholder={'Indtast din e-mail'}
         secureTextEntry={true} 
         placeholderTextColor='black'
         style={styles.input}
         secureTextEntry
         keyboardType='visible-password'
         keyboardAppearance='light'
        
       />

   
      <TouchableOpacity onPress={this.onLogin.bind(this)} disabled={this.state.ButtonStateHolder} loading={true}>
          <Text style={styles.button}>Bestil</Text>
        </TouchableOpacity>
        <View style={{flexDirection:'row',alignContent:'space-between'}}>
        <Text style={styles.register} onPress={()=>this.props.navigation.navigate('SignIn')}>Log på</Text>
        </View>

      <Text style={{alignSelf:'center',color:'#525252',fontSize:15,fontWeight: 'bold' }}>HUSK...</Text> 
      <Text style={styles.BestilTopTextchild}>at tjekke spam</Text> 
      <Text style={styles.BestilTopTextchild}>at angive din korrekte e-mail</Text>
      
       
        <Loader
          loading={this.state.loading} />




               <Dialog
    visible={this.state.popupVisible}
    onHardwareBackPress={() => { this.setState({ popupVisible: false }); } }
    onTouchOutside={() => {
      this.setState({ popupVisible: false });
    }}
    dialogStyle={{backgroundColor:'transparent'}}
  >
    <DialogContent>
     <View styles={{alignItems:'center'}}>
     <TouchableOpacity onPress={()=>this.setState({ popupVisible: false })} style={{position:'absolute',right:5,top:50,height:60,width:60,zIndex:10}}>
        {/* <Text style={{color:'black',right:0}}>x</Text> */}
      </TouchableOpacity> 
      <Text  style={{position:'absolute',right:80,bottom:100,zIndex:10,fontSize:16,color:'grey'}}>Email sendt
</Text>
     <Image  resizeMode="contain" style={{width:wp('60%') ,height:hp('40%'),alignSelf:'center'}} source={require('../../../assets/icons/errorLogin.png')}/> 
     </View>
    </DialogContent>
  </Dialog>



       <Dialog
    visible={this.state.popupErVisible}
    onHardwareBackPress={() => { this.setState({ popupErVisible: false }); } }
    onTouchOutside={() => {
      this.setState({ popupErVisible: false });
    }}
    dialogStyle={{backgroundColor:'transparent'}}
  >
    <DialogContent>
     <View styles={{alignItems:'center'}}>
     <TouchableOpacity onPress={()=>this.setState({ popupErVisible: false })} style={{position:'absolute',right:5,top:50,height:60,width:60,zIndex:10}}>
        {/* <Text style={{color:'black',right:0}}>x</Text> */}
      </TouchableOpacity> 
      <Text  style={styles.DialogErContent}>E-mail ikke tilknyttet
</Text>
     <Image  resizeMode="contain" style={{width:wp('60%') ,height:hp('40%'),alignSelf:'center'}} source={require('../../../assets/icons/errorLogin.png')}/> 
     </View>
    </DialogContent>
  </Dialog>  

  
    
          <View style={{flexDirection:'row'}}>
            <Image
                            style={{
                              width: 160,
                              height: 250,
                            }}
                            source={require('../../../assets/images/slice14-inverted.png')}
                          />
              <ImageBackground
                style={{
                  width: 185,
                  height: 135,
                }}
                source={require('../../../assets/images/slice8.png')}
              ><Text adjustsFontSizeToFit numberOfLines={5} style={{ position: 'absolute', backgroundColor: 'transparent',
              left:15, top:38, right: 0, zIndex: 100, alignItems: 'flex-end',fontFamily:'KGPrimaryWhimsy',fontSize:16}}> Vidste du godt, at vi lige nu har 5202 børneting, som koster helt GRATIS?

              </Text></ImageBackground>
          </View>
      </Block>
     </Block>
     
    <Dialog
        visible={this.state.showDialogMessage}
        onTouchOutside={() => { this.setState({ showDialogMessage: false }); }}
        onHardwareBackPress={() => {this.setState({ showDialogMessage: false });}}
        dialogStyle={{backgroundColor:'white',width:wp('90%'),alignItems:'center',alignContent:'center'}}>
        <DialogContent>
            <View styles={{alignItems:'center', }}>
                <Text  style={{fontSize:28, color:'#eca6a1', padding: 12, textAlign:'center', fontFamily:'KGPrimaryWhimsy',}}>{this.state.dialog_message}</Text>
            </View>
        </DialogContent>
    </Dialog>
     
    </KeyboardAwareScrollView>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: 'transparent'
  },
  input: {
    width: 300,
    height: 44,
    padding: 10,
    borderWidth: .5,
    borderRadius: 12,
    borderColor: 'black',
    marginBottom: 10,
    marginTop:25,
    backgroundColor: 'white',
    fontFamily:'KGPrimaryWhimsy',
    opacity: 0.9,
    fontSize:20
  },
  button: {
    backgroundColor: '#54969c',
    borderColor: 'white',
    borderWidth: .5,
    borderRadius: 12,
    color: 'white',
    overflow: 'hidden',
    padding: 12,
    textAlign:'center',
    width: 300,
    height: 44,
    opacity: 0.9,
    fontFamily:'KGPrimaryWhimsy',
    fontSize:20
  },
  //Forgot Page Top text
  BestilTopText: {    
    fontFamily:'KGPrimaryWhimsy',
    alignSelf:'center',
    color:'#F08080',
    fontSize:15,
    fontWeight: 'bold' 
  },
  //Forgot Page Child text
  BestilTopTextchild: {
    alignSelf:'center'
    ,color:'#525252'
    ,fontSize:12  
  },
  DialogErContent: {
    position:'absolute',
    right:50,
    bottom:100,
    zIndex:10,
    fontSize:16,
    color:'#F08080'
  },
  register: {
    color: 'black',
    overflow: 'hidden',
    padding: 12,
    textAlign:'center',
    fontSize:20,
    fontFamily:'KGPrimaryWhimsy',
  },
  forgot: {
    color: 'grey',
    overflow: 'hidden',
    padding: 12,
    textAlign:'center',
    fontSize:20,
    fontFamily:'KGPrimaryWhimsy',
  },
});

import React, {Component} from 'react'
import {
  View,
  Button,
  TextInput,
  StyleSheet,
  TouchableWithoutFeedback,
  Image,
  Text,
  TouchableOpacity,
  Platform,
  ImageBackground,
  Dimensions,
  SectionList,AsyncStorage,
  Keyboard,
  Alert
} from 'react-native'
import TextField from 'react-native-text-field';
import CameraScreen from './CameraScreenRegister';
import DropDownPicker from 'react-native-dropdown-picker';
import Information from '../InformationScreen';
//import pickerInfo from '../pickerInfo';
import Loader from '../../icons/Loader';
import CheckboxFormX from 'react-native-checkbox-form';
import Modal from 'react-native-modalbox';
//import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons'
import Ionicons from 'react-native-vector-icons/Ionicons'
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';
import { Container, Header, Content, Form, Item, Picker,Icon,Input  } from 'native-base';
import { ThemeProvider } from 'react-native-elements';
import Dialog, {SlideAnimation,DialogContent } from 'react-native-popup-dialog';
//import { Font } from 'expo';
import {
   Block
} from 'galio-framework';
import { PH,SS } from 'react-native-dotenv'
const Fsize = Platform.OS === 'ios' ?20 : 17;
const checkB = [
  {
      label: '',
  },
];
const sleep = ms => new Promise(resolve => setTimeout(resolve, ms))
const { width, height } = Dimensions.get('window');
export default class Register extends Component {
  static navigationOptions = {
    //To hide the NavigationBar from current Screen
    header: null,
    gesturesEnabled: false,
  };  
  state = {
    fullname:null, username:null, email:null, password:null, usernameError:null,
    picture:null,continue:false,  pages:0,isChecked:false,
    question1:null,question2:null,question3:null,text:'options',
    selected2: undefined,ButtonStateHolder:false,loading:false,inputError:1,emailError:false,passwordError:1,
    showDialogMessage:false,
    dialog_message:''
  }

  login() {
    this.props.navigation.navigate('SignIn');
  
    //alert("maoni");
    // this.props.navigation.navigate('FamProfil');
}

_menu = null;
 
  setMenuRef = ref => {
    this._menu = ref;
  };
 
  hideMenu = () => {
    this._menu.hide();
  };
 
  showMenu = () => {
    this._menu.show();
  };
  
 
_onSelect = ( item ) => {
  if(this.state.isChecked==true){
    this.setState({isChecked:false})
  }else{
    this.setState({isChecked:true})
  }
};
  onUpdate = (val) => {
    this.setState({
      picture: val
    })
  };

  onChangeText = (key, val) => {
    this.setState({ [key]: val })
  }

  callCheckers = () => {
    console.log('usernamechecker'+this.usernameChecker());
    console.log('passwordchecker'+this.passwordChecker());
    console.log("Call Checkers");
    // this.usernameChecker() ? this.passwordChecker() : null;
  }

  
  usernameChecker = () => {
    console.log('checking username');
    console.log("usernameerror" +this.state.usernameError);
    if(this.state.usernameError == 0)
    {
      //Alert.alert('','Username Must be 5 characters or greater');
      this.setState({
	showDialogMessage:true,
	dialog_message:'Username Must be 5 characters or greater'
        })
      return true;
    }
    return false;
  };
  
  passwordChecker = () => {
    console.log('checking');
    if(this.state.passwordError == 0)
    {
      //Alert.alert('','Password Must be 4 characters or greater');
      this.setState({
	showDialogMessage:true,
	dialog_message:'Password Must be 4 characters or greater'
        })
      this.setState({pages:this.state.pages})
    }
    if(this.state.usernameError == 0)
    {
        this.setState({
	showDialogMessage:true,
	dialog_message:'Password Must be 4 characters or greater'
        })
      //Alert.alert('','Password Must be 4 characters or greater');
      this.setState({pages:this.state.pages})
    }
    else if( this.state.usernameError == 1 & this.state.passwordError == 1)
    {
       this.setState({pages:this.state.pages+1});
    }
    
    return false;
  };


  
  onInputChange(text) {
    console.log(text)
  }

  goRegister = async () => {

    this.setState({pages:1})
  }

  nextPage = async () => {
    this.setState({pages:this.state.pages+1})
  }

 clearAppData = async ()=> {
    try {
        const keys = await AsyncStorage.getAllKeys();
        await AsyncStorage.multiRemove(keys);
    } catch (error) {
        console.error('Error clearing app data.');
    }
}

validate = (text) => {
  console.log(text);
  let reg = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
  if (reg.test(text) === false) {
    console.log("Email is Not Correct");
    this.setState({ email: text,emailError:true })
    return false;
  }
  else {
    this.setState({ email: text,emailError:false })
    this.state.email = text;
    console.log("Email is Correct");
  }
}

validatePassword = (password) => {
  console.log("validate length "+password);
  
  if (password.length < 4) {
    // Return error message as string if the text is not valid.
     this.state.passwordError = 0;
    return 'Password need to be at least 4 digits.' 
  }
  else{
    this.state.passwordError = 1;
  }
  if (!/[a-zA-Z]/.test(password)) {
    // Return a different error message if the text doesn't match certain criteria. 
    return 'Password need to contain letters.'
  }
  this.state.password = password;
  
  return true // Return 'true' to indicate the text is valid.
  
}

validateUsername = (username) => {
  console.log("validate length username "+username);
  if (username.length < 4) {
    // Return error message as string if the text is not valid.
    this.state.usernameError = 0;
    console.log("username Error"+this.state.usernameError);
  }
  else{
    this.state.usernameError = 1;
    console.log("password Error"+this.state.usernameError)
    this.state.username = username;
  }
  return true // Return 'true' to indicate the text is valid.
  
}



  SubmitDetails = async () => {
    console.log("Submit trigger");
    let type='png'
   
    this.setState({
      loading: true
    })
  
    if(this.state.picture!=null){
      type = "image/" + (this.state.picture.split(".").pop() || "jpg");
    }
    
    if(this.state.email==null||this.state.password==null||this.state.fullname==null||this.state.username==null){
        console.log("email"+this.state.email);

        console.log("fullname"+this.state.fullname);

        console.log("password"+this.state.password);

        console.log("username"+this.state.username);

        this.setState({pages:1,inputError:3})
       
    }else{
    const data = new FormData();
        data.append('email', this.state.email); // you can append anyone.
        data.append('password', this.state.password);
        data.append('name', this.state.fullname);
        data.append('user_name', this.state.username);
        if(this.state.picture!=null){
          data.append("image", {
            uri: this.state.picture,
            type: type,
            name: "DP"
          });
        }
    let password = this.state.password   
    let username = this.state.username   
    fetch(PH+'/auth/register', {
      method: 'POST',
      headers: {
        'Accept':'application/json',
        // 'Content-Type': 'multipart/form-data; boundary=${data._boundary}',
      },
      body:data,
      }).then((response) => response.json())
          .then(async (responseJson) => {
            console.log(responseJson)
            console.log("picture:",this.state.picture)
            if(responseJson.code==400){
                this.setState({
                showDialogMessage:true,
                dialog_message:responseJson.title
                })
              //Alert.alert(responseJson.title)
              await sleep(1500)
              this.setState({
                loading: false,pages:1,inputError:3
              })
              
            }if(responseJson.code==404){
              //Alert.alert(responseJson.title)
              this.setState({
                showDialogMessage:true,
                dialog_message:responseJson.title
                })
              await sleep(1500)
              this.setState({
                loading: false,pages:1,inputError:3
              })
              
            }else{
              console.log("logging IN")
              AsyncStorage.getAllKeys()
              .then(keys => AsyncStorage.multiRemove(keys))
              .then(() => console.log('success'));

              fetch(PH+'/auth/login', {
                  method: 'POST',
                  headers: {
                    'Accept':'application/json',
                    'Content-Type': 'application/json',
                  },
                  body: JSON.stringify({
                    email: username,
                    password: password

                  }),
                  }).then((response) => response.json())
                      .then((responseJson) => {
                        console.log(responseJson)
                        if(responseJson.code==400){
                          this.setState({
                            loading: false
                          })
                          console.log(responseJson.title)
                        }else{
                          this.setState({
                            loading: false
                          })
                          AsyncStorage.getAllKeys()
                          .then(keys => AsyncStorage.multiRemove(keys))
                          .then(() => console.log('success'));
                          console.log(responseJson)
                          AsyncStorage.setItem('token',responseJson.meta.token.access_token);
                          if(responseJson.meta.user.image_url!=null){
                            //AsyncStorage.setItem('image_url',responseJson.meta.user.image_url);
                            AsyncStorage.setItem('image_url',this.state.picture);
                          }
                          AsyncStorage.setItem('name',responseJson.meta.user.name);
                          AsyncStorage.setItem('user_id',responseJson.meta.user.id.toString());
                          AsyncStorage.setItem('isLoggedIn','1');
                            this.props.navigation.navigate('Home');
                           
                        }
                  })
                      .catch((error) => {
                        this.setState({
                          loading: false
                        })
                        console.log("ayay")
                        this.setState({
                          loading: false
                        })
                  });
            }
      })
          .catch((error) => {
            this.setState({
              ButtonStateHolder : false,
              loading: false
            })
            console.log("sa ubos")
          // alert(error); 
      });
         
    }
  }

  back = async ()=>{
    this.setState({pages:this.state.pages-1})
  }

  signUp = async () => {
    const { fullname, username, email, password } = this.state
    try {
      // here place your signup logic
      console.log('user successfully signed up!: ', success)
    } catch (err) {
      console.log('error signing up: ', err)
    }
  }
  render() {
    console.log(this.state)
    const resizeMode = 'cover';
    var data = [["Vi bruger parfumefri vaske- og skyllemiddel", "Vi bruger vaske-og skyllemiddel med parfume", "Vi bruge både parfumefri og ikke parfumefri \n vaske- og skyllemiddel"],["Vi holder dyr", "Vi holder dyr,men er allergivenlige", "Vi bruge både parfumefri og ikke parfumefri vaske- og skyllemiddel"]];
    var data2 = [["Vi holder dyr", "Vi holder dyr,men er allergivenlige", "Vi bruge både parfumefri og ikke parfumefri vaske- og skyllemiddel"]];
    var data3 = [["Vi bruger parfumefri vaske- og skyllemiddel", "Vi bruger vaske-og skyllemiddel med parfume", "Vi bruge både parfumefri og ikke parfumefri vaske- og skyllemiddel"]];
    return (
      <TouchableWithoutFeedback onPress={()=>Keyboard.dismiss()}>
        <View style={styles.container}>
           <ImageBackground
        style={{
          backgroundColor: '#ccc',
          flex: 1,
          resizeMode:'stretch',
          position: 'absolute',
          width: wp('100%'),
          height: hp('120%'),
          top:-10
        }}
        source={require('../../../assets/images/slice81-min.png')}
      >
      
      </ImageBackground>
      <Modal style={{height:hp('100%')}} position={"bottom"} ref={"modal4"}>
      <View style={{top:40}}>
      <TouchableOpacity onPress={()=>this.refs.modal4.close()}>
      <Text style={{position:'absolute',right:20,fontSize:30}}>X</Text>
      </TouchableOpacity>
      <View style={{justifyContent:'center',alignContent:'center',marginTop:50}}>
      <Text style={{alignSelf:'center',fontSize:20}}>Betingelser og vilkår</Text>
      </View>
        
      </View>
           
      
    
     </Modal>    
         {this.state.pages==0 ?(
            <View style={styles.container2}>
                <View style={{top:10}}>
                  <Text style={styles.phrase4}>
                    Dyt, dyt ska´ vi byt?
                  </Text>
                  <Text adjustsFontSizeToFit numberOfLines={5} style={styles.phrase1}>Velkommen til et bæredygtigt børneunivers med alt hvad hjertet kan begære. Fyld dit barns garderobe op,med bl.a. børnetøj,sko,legetøj eller hvad med små herligheder til børneværelset? Alle børneting koster GRATIS.
                  </Text>
                  </View>
                  <Text style={styles.phrase2}>
                  Rigtig god fornøjelse!
                  </Text>
                  <Text style={styles.phrase3}>
                  Kh Søstrene
                  </Text>
                  <Image  resizeMode='contain'
                          style={{
                            width: 200,
                            height: 200,
                          }}
                          source={require('../../../assets/images/slice11.png')}
                        />
          <View style={{marginTop:40}}>          
          <TouchableOpacity onPress={this.goRegister.bind(this)}>
          <Text style={styles.button}>Kom i gang</Text>
          </TouchableOpacity>
          <TouchableOpacity onPress={this.login.bind(this)}>
          <Text style={styles.buttonRed}>Log ind</Text>
          </TouchableOpacity>  
          </View>              
          <Text></Text> 
          <Text></Text>
          <Text></Text>
         
           </View>
        ):null}
        {this.state.pages==3 ?(    
           <View  style={styles.container}>
             <Image resizeMode="contain"
                        style={{width:120,height:90}}
                        source={require('../../../assets/images/slice2.png')}
                        />         
            <CameraScreen nextPage={this.nextPage} onUpdate={this.onUpdate}/>  
         </View>
          ):null}
           {this.state.pages==2 ?(    
           <View  style={styles.container}>
             <TouchableOpacity style={{right:1,top:hp('5%'),position:'absolute',height:50}} onPress={()=>this.setState({pages:this.state.pages+2})}>
               <Text style={{color:'grey',fontWeight:'600',fontSize:15}}>Spring over</Text>
             </TouchableOpacity>
              <Ionicons name={Platform.OS === "ios" ? "ios-camera" : "md-camera"}
                      
                         style={{color: '#54969c', fontSize: 90 ,
                          position: 'absolute', backgroundColor: 'transparent',
                        zIndex: 200, alignItems: 'center', top: 265
                      }}
                      onPress={this.nextPage.bind(this)} 
              >
              </Ionicons>
              <View style={{flexDirection: 'row',
              flexWrap: 'wrap',alignContent:'center', backgroundColor: 'transparent',
              top: 400,zIndex: 100, alignItems: 'center'}}>
                <Image resizeMode="contain"
                        style={{width:350}}
                        source={require('../../../assets/images/slice70-min.png')}
                        />
                <Image resizeMode="contain"
                        style={{width:40,position:'absolute',zIndex:100,top: -395,left:35}}
                        source={require('../../../assets/images/slice34-min.png')}
                        />  
                <Image resizeMode="contain"
                        style={{transform: [{ rotate: '40deg' }],width:80,position:'absolute',top: -400,left:230}}
                        source={require('../../../assets/images/slice5-inverted.png')}
                        />             
                 <Image resizeMode="contain"
                        style={{width:180,position:'absolute',top: -400,left:90}}
                        source={require('../../../assets/images/slice72-min.png')}
                        />     
                    
                   {/* <Image resizeMode="contain"
                        style={{width:90,position:'absolute',top: -180,left:135}}
                        source={require('../../../assets/images/slice65-min.png')}
                        />          */}
                </View>
               
              
              <Text  style={{color: '#cbd1cd', fontSize: 20 ,
                          position: 'absolute', backgroundColor: 'transparent',
                        top: 370,zIndex: 100, alignItems: 'center',fontFamily:'KGPrimaryWhimsy',
                      }}>Vælg profilbillede
                      </Text>
         </View>
          ):null}
         {this.state.pages==1 ?(
          <View style={styles.container}>
          <View style={{marginTop:hp('10%'),alignContent:'center',alignSelf:'center',alignItems:'center'}}>
            <TextInput
             style={this.state.fullname==null&&this.state.inputError==3?styles.errorInput:styles.input}
            placeholder='Fornavn&efternavn'
            placeholderTextColor='black'
            onChangeText={val => this.onChangeText('fullname', val)}
            value={this.state.fullname!==null?this.state.fullname:null}
          />
          <TextInput
            style={this.state.username==null&&this.state.inputError==3?styles.errorInput:styles.input}
            placeholder='Brugernavn'
            autoCapitalize="none"
            placeholderTextColor='black'
            onChangeText={val => this.validateUsername(val)}
            value={this.state.username!==null?this.state.username:null}
          />

          <TextInput
            style={(this.state.email==null&&this.state.inputError==3)||this.state.emailError==true?styles.errorInput:styles.input}
            placeholder='E-mail'
            autoCapitalize="none"
            placeholderTextColor='black'
            onChangeText={val => this.validate(val)}
            value={this.state.email!==null?this.state.email:null}
            keyboardType='email-address'
          />
         <TextInput
            style={this.state.password==null&&this.state.inputError==3?styles.errorInput:styles.input}
            // placeholder='Adgangskode'
             secureTextEntry={true}
            // autoCapitalize="none"
            // placeholderTextColor='black'
            // value={this.state.password!==null?this.state.password:null}
            // onValidate={val => this.validatePassword(val)}
            placeholder='E-Adgangskode'
            placeholderTextColor='black'
            onChangeText={val => this.validatePassword(val)}
            value={this.state.password!==null?this.state.password:null}
          />
         
          <View  style={styles.checkBox}>
          <View style={{ flexDirection: 'row', justifyContent: 'space-between', paddingHorizontal: 0, marginBottom: 15, alignItems: 'flex-end'  ,marginRight:60}}> 
          <CheckboxFormX
                  dataSource={checkB}
                  itemShowKey="label" 
                  style={{marginLeft:-40}}
                  itemCheckedKey="RNchecked"
                  iconSize={30}
                  iconColor='#d4264f'
                  formHorizontal={false}
                  labelHorizontal={true}
                  onChecked={(item) => this._onSelect(item)}
              />
            
            <Text>Ja tak,til skønne tips,tilbud & {"\n"} inspiration for hele familien</Text>
          </View>
          </View>
          {/* disabled={!this.state.isChecked} */}
          <TouchableOpacity onPress={ this.callCheckers} > 
          <Text style={styles.button}>Opret mig</Text>
          </TouchableOpacity>
          <View style={{width:wp('80%'),marginBottom:20,marginTop:10,alignItems:'center',alignContent:'center'}}> 
          <Text adjustsFontSizeToFit numberOfLines={2}  style={{fontSize:17,alignContent:'center'}}>Ved oprettelse accepterer du,at du har læst vores <Text onPress={()=> this.refs.modal4.open()} style={{fontWeight:'900',fontSize:17}}>brugervilkår</Text> og <Text onPress={()=> this.refs.modal4.open()} style={{fontWeight: 'bold',fontSize:17}}>fortrolighedspolitik</Text>.</Text>
          </View>
          <View style={{flexDirection:'row'}}>
            <Image
                            style={{
                              width: 150,
                              height: 250,
                            }}
                            source={require('../../../assets/images/slice7.png')}
                          />
              <ImageBackground
                style={{
                  width: 185,
                  height: 135,
                }}
                source={require('../../../assets/images/slice8.png')}
              ><Text style={{ transform: [{ rotate: '20deg' }], position: 'absolute', backgroundColor: 'transparent',
              left: 25, top:hp('5%'), right: 0, zIndex: 100, alignItems: 'flex-end', fontFamily:'KGPrimaryWhimsy',fontSize:15}}> Dejlige du vil støtte genbrug og være en del af vores bæredygtige børneunivers.
              </Text></ImageBackground>
          </View>
          </View>
          </View>):null}
          {this.state.pages==4 ?(
      <Block flex={1} safe style={{marginTop:100}}>
            
            <DropDownPicker
             zIndex={5000}
            items={[ {label: 'Vi bruger parfumefri vaske- og skyllemiddel', value: 'Vi bruger parfumefri vaske- og skyllemiddel'},
            {label: 'Vi bruger vaske-og skyllemiddel med parfume ', value: 'Vi bruger vaske-og skyllemiddel med parfume '},
            {label: 'Vi bruge både parfumefri og ikke parfumefri vaske- og skyllemiddel', value: 'Vi bruge både parfumefri og ikke parfumefri vaske- og skyllemiddel'}]}
            labelStyle={{
                color: 'grey'
            }}
            arrowStyle={{color: 'grey'}}
            defaultNull
            dropDownStyle={{zIndex:5000}}
            placeholder="Hvordan vasker du børnetøj?"
            containerStyle={{height: 50,width:width*0.95,borderRadius:8}}
            onChangeItem={item =>  this.setState({ question1: item.value})}
        />
        <Text style={{marginTop:10}}></Text>
        <DropDownPicker
        zIndex={4000}
                    items={[ {label: 'Vi holder dyr', value: 'Vi holder dyr'},
                    {label: 'Vi holder dyr,men er allergivenlige', value: 'Vi holder dyr,men er allergivenlige'},
                    {label: 'Vi holder dyr,men er allergivenlige', value: 'Vi holder dyr,men er allergivenlige'}]}
                    labelStyle={{
                        color: 'grey'
                    }}
                    arrowStyle={{color: 'grey'}}
                    defaultNull
                    dropDownStyle={{zIndex:4000}}
                    placeholder="Holder du husdyr?"
                    containerStyle={{height: 50,width:width*0.95,borderRadius:8}}
                    onChangeItem={item =>  this.setState({ question2: item.value})}
                />
                 <Text style={{marginTop:10}}></Text>
              <DropDownPicker
              zIndex={3000}
                    items={[ {label: 'Vi er en rygerfamilie og ryger indenfor', value: 'Vi er en rygerfamilie og ryger indenfor'},
                    {label: 'Vi er en rygerfamilie,men ryger udenfor', value: 'Vi er en rygerfamilie,men ryger udenfor'},
                    {label: 'Vi er en røgfrifamilie', value: 'Vi er en røgfrifamilie'}]}
                    labelStyle={{
                        color: 'grey'
                    }}
                    arrowStyle={{color: 'grey'}}
                    defaultNull
                    dropDownStyle={{zIndex:3000}}
                    placeholder="Ryger du/I?"
                    containerStyle={{height: 50,width:width*0.95,borderRadius:8}}
                    onChangeItem={item =>  this.setState({ question3: item.value})}
                />
       
         
            <View style={styles.container2}>
            <Image resizeMode="contain"
                        style={{width:250,bottom:50,height:250}}
                        source={require('../../../assets/girls_info/slice3.png')}
                        /> 
                                {/*disabled={!this.state.isChecked}  */}
          <TouchableOpacity onPress={this.nextPage.bind(this)} >
          <Text style={styles.button}>Opret mig</Text>
          </TouchableOpacity>
          </View>
         </Block>
         ):null}
          {this.state.pages==5?( 
           
            <View >
               <Loader
                  loading={this.state.loading} />
           
              <Information nextPage={this.nextPage} submit={this.SubmitDetails} />  
            
            </View>
           ):null}
          {this.state.pages>0?( 
            
           <TouchableOpacity onPress={this.back.bind(this)} loading={true} style={{top:hp('5%'),left:20,position:'absolute',height:50}}>
           <Ionicons name={Platform.OS === "ios" ? "ios-arrow-back" : "md-arrow-back"}  size={30} style={{color:'grey'}}></Ionicons>
          </TouchableOpacity>
         
           ):null} 
            {this.state.pages>4?( 
            
            <TouchableOpacity style={{right:10,top:hp('5%'),position:'absolute',height:50}} onPress={()=>this.SubmitDetails}>
            <Text style={{color:'grey',fontWeight:'600',fontSize:15}}>Spring over</Text>
           </TouchableOpacity>
          
            ):null} 
      </View>
              
        <Dialog
            visible={this.state.showDialogMessage}
            onTouchOutside={() => { this.setState({ showDialogMessage: false }); }}
            onHardwareBackPress={() => {this.setState({ showDialogMessage: false });}}
            dialogStyle={{backgroundColor:'white',width:wp('90%'),alignItems:'center',alignContent:'center'}}>
            <DialogContent>
                <View styles={{alignItems:'center', }}>
                    <Text  style={{fontSize:28, color:'#eca6a1', padding: 12, textAlign:'center', fontFamily:'KGPrimaryWhimsy',}}>{this.state.dialog_message}</Text>
                </View>
            </DialogContent>
        </Dialog>
    
      </TouchableWithoutFeedback>
    
     
    )
  }

 
    
}

const styles = StyleSheet.create({
  input: {
    width: 300,
    height: 44,
    padding: 10,
    borderWidth: .5,
    borderRadius: 12,
    textShadowColor: 'black',
    borderColor: 'black',
    marginBottom: 10,
    fontSize:Fsize,
    //backgroundColor: 'green',
    opacity: 0.9,

    fontFamily:'KGPrimaryWhimsy'
  },
  errorInput: {
    width: 300,
    height: 44,
    padding: 10,
    borderWidth: .5,
    borderRadius: 12,
    textShadowColor: 'black',
    borderColor: 'red',
    marginBottom: 10,
    fontSize:Fsize,
    //backgroundColor: 'green',
    opacity: 0.9,

    fontFamily:'KGPrimaryWhimsy'
  },
  dropdown: {
    height: Dimensions.get('screen').height /18,
    padding: 5,
    position:'relative',
    margin:10,
    borderWidth: .2,
    borderRadius: 12,
    textShadowColor: 'black',
    borderColor: 'black',
    marginBottom: 10,
    backgroundColor: 'white',
    opacity: 0.9,
    fontFamily:'KGPrimaryWhimsy',
    fontSize:Fsize,
    alignContent:'center'
  },
  checkBox: {
    width: 300,
    height: 45,
    marginLeft:80,
  },
  phrase: {
    width: 350,
    height: Dimensions.get('screen').width /3,
    padding: 12,
    textAlign:'center',
    fontSize:Fsize,
    fontFamily:'KGPrimaryWhimsy'
  },
  phrase1: {
    width: 330,
    height: 180,
    padding: 12,
    borderRadius: 12,
    textAlign:'center',
    fontSize:Platform.OS === 'ios' ?30 : 25,
    fontFamily:'KGPrimaryWhimsy'
  },
  phrase2: {
    width: 200,
    height: 50,
    padding: 12,
    textAlign:'center',
    fontSize:Fsize,
    fontFamily:'KGPrimaryWhimsy'
  },
  phrase3: {
    width: 200,
    height: 50,
    padding: 12,
    textAlign:'center',
    marginBottom: 10,
    fontSize:Fsize,
    fontFamily:'KGPrimaryWhimsy'
  },
  phrase4: {
    padding: 12,
    textAlign:'center',
    color:'#54969c',
    fontSize:Fsize,
    fontFamily:'KGPrimaryWhimsy'
  },
  button: {
    backgroundColor: '#54969c',
    borderColor: 'white',
    borderWidth: .5,
    borderRadius: 12,
    color: 'white',
    overflow: 'hidden',
    padding: 12,
    textAlign:'center',
    width: 300,
    height: 48,
    opacity: 0.9,
    fontSize:20,
    fontFamily:'KGPrimaryWhimsy'
  },
  buttonRed: {
    backgroundColor: '#d4264f',
    borderColor: 'white',
    borderWidth: .5,
    borderRadius: 12,
    color: 'white',
    overflow: 'hidden',
    padding: 12,
    textAlign:'center',
    width: 300,
    height: 48,
    opacity: 0.9,
    fontSize:20,
    fontFamily:'KGPrimaryWhimsy'
  },
  container: {
    flex: 1,
    justifyContent: 'flex-start',
    alignItems: 'center'
  },
  container2: {
    top:hp('15%'),
    flex: 1,
    justifyContent: "flex-start",
    alignItems: "center",
    // bottom:5,
    zIndex:10
  },
  item: {
    padding: 10,
    fontSize: Fsize,
    height: 44,
  },
  sectionHeader: {
    paddingTop: 2,
    paddingLeft: 10,
    paddingRight: 10,
    paddingBottom: 2,
    fontSize: Fsize,
    fontWeight: 'bold',
    backgroundColor: 'rgba(247,247,247,1.0)',
  },
})

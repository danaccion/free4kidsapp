import React, { Component } from "react";
import {
    View,
    Text,
    StyleSheet,
    ScrollView,
    Image,TouchableOpacity,
    Platform
} from "react-native";

import { Camera} from 'expo-camera'
import * as Permissions from 'expo-permissions'
import * as ImagePicker from 'expo-image-picker';
import Constants from 'expo-constants'
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';
import { Container, Content, Header, Item, Icon, Input, Button } from 'native-base'
import Ionicons from 'react-native-vector-icons/Ionicons'
import * as ImageManipulator from "expo-image-manipulator";
import * as FileSystem from 'expo-file-system';
import Dialog, {SlideAnimation,DialogContent } from 'react-native-popup-dialog';
export default class CameraScreenRegister extends Component {

    state = {
        hasCameraPermission: null,
        type: Camera.Constants.Type.back,
        hasAccesstoRoll:null,
        image: null,
        imageArray:[],
        flash:'off',
        showDialogMessage:false,
        dialog_message:''
    }

    async componentDidMount() {
        const { status } = await Permissions.askAsync(Permissions.CAMERA);
        this.setState({ hasCameraPermission: status === 'granted' })
        console.log(this.props);
    }

    async snapPhoto() {       
      console.log('Button Pressed');
      if (this.camera) {
         console.log('Taking photo');
         const options = { quality: 1, base64: false, fixOrientation: false, 
         exif: true,};
         await this.camera.takePictureAsync(options).then(photo => {
          //  photo.exif.Orientation = photo.exif.Orientation*2;            
             console.log(photo);
             this.setState({
              image: photo.uri
            });  
            // this.props.onUpdate(photo.uri);          
             });     
       }
      }
     getPermissionAsync = async () => {
    if (Constants.platform.ios) {
      const { status } = await Permissions.askAsync(Permissions.CAMERA_ROLL);
      this.setState({ hasAccesstoRoll: status === 'granted' })
      if (status !== 'granted') {
        //alert('Sorry, we need camera roll permissions to make this work!');
        this.setState({
                showDialogMessage:true,
                dialog_message:'Sorry, we need camera roll permissions to make this work!'
        })
      }
    }else{
      const { status } = await Permissions.askAsync(Permissions.CAMERA_ROLL);
     // this.setState({ hasAccesstoRoll: status === 'granted' })
      if (status !== 'granted') {
        //alert('Sorry, we need camera roll permissions to make this work!');
        this.setState({
                showDialogMessage:true,
                dialog_message:'Sorry, we need camera roll permissions to make this work!'
        })
      }
      // alert(status)
    }
   
  }
  onFlashTrue(){
    this.setState({flash:'on'})
  }
  onFlashFalse(){
    console.log("ayay")
    this.setState({flash:'off'})
  }
  onBack(){
    this.props.onBack({back:true});     
  }
  onCancel(){
    this.setState({image:null})
  }
  async onDone(){
    const manipResult = await ImageManipulator.manipulateAsync(
      this.state.image,
      [{ resize: { width: 500, height: 600 } }],
      { compress: 1, format: ImageManipulator.SaveFormat.PNG }
    );
    let fileInfo = await FileSystem.getInfoAsync(manipResult.uri);
    console.log("size",fileInfo.size)
    let orig = await FileSystem.getInfoAsync(this.state.image);
    console.log("orig",orig.size)
    this.props.onUpdate(manipResult.uri); 
    this.props.nextPage(); 
  }
  addMore(){
    var joined = this.state.imageArray.concat(this.state.image);
    this.setState({imageArray: joined,image:null})
  }
  pickImage = async () => {
    this.getPermissionAsync()
    let result = await ImagePicker.launchImageLibraryAsync({
      mediaTypes: ImagePicker.MediaTypeOptions.Images,
      allowsEditing: true,
      aspect: [6, 6],
      quality:1
    });

    console.log(result);

    if (result.cancelled==false) {
      this.setState({
        image: result.uri
      });  
      
    }
  };
    render() {
      
        const { hasCameraPermission } = this.state
        if (hasCameraPermission === null) {
            return <View />
        }
        else if (hasCameraPermission === false) {
            return <Text> No access to camera</Text>
        }
        else {
            return (
                <View style={styles.container}>
                 {this.state.image!==null?( 
                  <View style={styles.container}>
                     
                        <Image
                        resizeMode="contain"
                          style={{
                            width: wp('100%'),
                            height: hp('72%'),
                          }}
                          source={{ uri: this.state.image }}
                        />
                        <Text></Text>
                    <View style={{flexDirection:'row',alignContent:'space-between'}}>
                    <TouchableOpacity onPress={()=>this.onCancel()}>
                    <Text style={styles.button2}>Afbestille</Text>
                    </TouchableOpacity>
                      <TouchableOpacity onPress={this.onDone.bind(this)}>
                      <Text style={styles.button3}>Færdig</Text>
                      </TouchableOpacity>
                    </View>
                     
                    </View>
                  
                 ): <Camera autoFocus="on" flashMode={this.state.flash}  style={{ flex: 1, justifyContent: 'space-between' , width: wp('100%'),height:hp('90%')}} type={this.state.type}   ref={ (ref) => {this.camera = ref} }>
                  
                
                 <Header searchBar rounded
                     style={{
                         position: 'absolute', backgroundColor: 'transparent',
                         left: 0, top: 0, right: 0, zIndex: 0, alignItems: 'center',opacity:0
                     }}
                 >
                    
                 </Header>       
                 {this.state.flash=="on"?(
                   <TouchableOpacity  style={{position:'absolute' ,right:10,top:10,zIndex:100}}  onPress={() => {
                    this.onFlashFalse()    
                }}>
                         <Ionicons name={Platform.OS === "ios" ? "ios-flash" : "md-flash"} style={{color: 'white', fontSize: 36,}}
                        ></Ionicons>
                        </TouchableOpacity>
                  ):
                  <TouchableOpacity  style={{position:'absolute' ,right:10,top:10,zIndex:100}}  onPress={() => {
                    this.onFlashTrue()    
                }}>
                  <Ionicons name={Platform.OS === "ios" ? "ios-flash-off" : "md-flash-off"} style={{color: 'white', fontSize: 36,}}></Ionicons>
                    </TouchableOpacity>}
                  
                 <View style={{ flexDirection: 'row', justifyContent: 'space-between', paddingHorizontal: 0, marginBottom: 15, alignItems: 'flex-end' ,marginLeft:10 ,marginRight:10}}>
                   
                   <TouchableOpacity onPress={() => {
                       this.pickImage()    
                   }}>
                   <Ionicons name={Platform.OS === "ios" ? "ios-images" : "md-images"}
                      
                         style={{ color: 'white', fontSize: 36,left:20 }}
                     ></Ionicons>

                   </TouchableOpacity>
                   <TouchableOpacity   onPress={this.snapPhoto.bind(this)}>
                     <View style={{ alignItems: 'center' }}>
                         <Ionicons name={Platform.OS === "ios" ? "ios-radio-button-off" : "md-radio-button-off"}
                       
                             style={{ color: 'white', fontSize: 80 }}
                         ></Ionicons>
                        
                     </View>
                     </TouchableOpacity>
                     <TouchableOpacity  onPress={() => {
                              
                              this.setState({
                                  type: this.state.type === Camera.Constants.Type.back ?
                                      Camera.Constants.Type.front :
                                      Camera.Constants.Type.back
                              })
                               
                          }}>
                     <Ionicons name={Platform.OS === "ios" ? "ios-camera" : "md-camera"}
                         style={{ color: 'white', fontSize: 36 }}
                     ></Ionicons>
                     </TouchableOpacity>

                 </View>
             </Camera>}
                   
            <Dialog
                visible={this.state.showDialogMessage}
                onTouchOutside={() => { this.setState({ showDialogMessage: false }); }}
                onHardwareBackPress={() => {this.setState({ showDialogMessage: false });}}
                dialogStyle={{backgroundColor:'white',width:wp('90%'),alignItems:'center',alignContent:'center'}}>
                <DialogContent>
                    <View styles={{alignItems:'center', }}>
                        <Text  style={{fontSize:28, color:'#eca6a1', padding: 12, textAlign:'center', fontFamily:'KGPrimaryWhimsy',}}>{this.state.dialog_message}</Text>
                    </View>
                </DialogContent>
            </Dialog>
                   
                </View>
            )
        }
    }
}


const styles = StyleSheet.create({
    container: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center'
    },
    button: {
      backgroundColor: '#54969c',
      borderColor: 'white',
      borderWidth: .5,
      borderRadius: 12,
      color: 'white',
      overflow: 'hidden',
      padding: 12,
      textAlign:'center',
      width: wp('31%'),
      height: 44,
      opacity: 0.9,
    },
    button2: {
      backgroundColor: '#d4675f',
      borderColor: 'white',
      borderWidth: .5,
      borderRadius: 12,
      color: 'white',
      overflow: 'hidden',
      padding: 12,
      textAlign:'center',
      width: wp('31%'),
      height: 44,
      opacity: 0.9,
    },
    button3: {
      backgroundColor: '#266336',
      borderColor: 'white',
      borderWidth: .5,
      borderRadius: 12,
      color: 'white',
      overflow: 'hidden',
      padding: 12,
      textAlign:'center',
      width: wp('31%'),
      height: 44,
      opacity: 0.9,
    },
});


import React, { Component } from 'react';
import { Alert, Button, TextInput, View, StyleSheet, ImageBackground, TouchableOpacity, Text, AsyncStorage,Image,StatusBar,Keyboard,TouchableWithoutFeedback,Dimensions } from 'react-native';
import Loader from '../../icons/Loader';
import * as Font from 'expo-font';
import Dialog, {DialogFooter,DialogButton,SlideAnimation,DialogContent } from 'react-native-popup-dialog';

import { Permissions, Contacts } from 'expo';
import { PH,SS } from 'react-native-dotenv'
// import AwesomeAlert from '../../icons/Alert';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view'
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';
import {
  Block,Input
} from 'galio-framework';
import { Ionicons, FontAwesome } from "@expo/vector-icons";
import * as Facebook from 'expo-facebook';
const { width, height } = Dimensions.get('window');
export default class Login extends Component {
  _isMounted = false;
    static navigationOptions = {
        //To hide the NavigationBar from current Screen
        header: null
      };
  constructor(props) {
    super(props);
    
    this.state = {
      username: '',
      password: '',
      name:'',
      email:'',
      ButtonStateHolder : true,
      loading: false,
      fontLoaded:false,
      popupVisible:false,
      showDialogMessage:false,
        dialog_message:''
    };
  }
  permissionFlow = async () => {
    const { status } = await Permissions.askAsync(Permissions.CONTACTS);

    this.setState({ status });

    if (status !== 'granted') {
      Linking.openURL('app-settings:');
      return;
    }

    /*
      Get some data
    */
    const { data } = await Contacts.getContactsAsync({ pageSize: 1 });
    console.log(data[0]);
  };
  register() {
    //alert("maoni");
     this.props.navigation.navigate('SignUp');
    // this.props.navigation.navigate('Home');  
}
forgotPassword() {
  //alert("maoni");
   this.props.navigation.navigate('ForgotPassword');
  // this.props.navigation.navigate('Home');  
}
async componentDidMount() {
  console.log(PH)
  this._isMounted = true;
    await Font.loadAsync({
      'KGPrimaryWhimsy': require('../../../assets/fonts/KGPrimaryWhimsy.ttf'),
    });
  
    this.setState({ fontLoaded: true });
  }

  getProductsCount(){

  }

  

  onLogin() {
    this._isMounted = true;
    Keyboard.dismiss()
    this.setState({
      ButtonStateHolder : true,
      loading: true
    })
    console.log("email",this.state.username)
    console.log("email",this.state.password)
    const { username, password } = this.state;
      fetch(PH+'/auth/login', {
        method: 'POST',
        headers: {
          'Accept':'application/json',
          'Content-Type': 'application/json',
        },
        body: JSON.stringify({
          email: this.state.username,
          password: this.state.password
        }),
        }).then((response) => response.json())
            .then((responseJson) => {
            
                
              if(responseJson.code!=200){
                console.log(responseJson.title)
                this.setState({
                  
                  loading: false,popupVisible:true
                })
                Keyboard.dismiss();
              }else{
                console.log(responseJson)
                AsyncStorage.setItem('user',responseJson.meta.user.email);
                AsyncStorage.setItem('token',responseJson.meta.token.access_token);
                if(responseJson.meta.user.image_url!==null){
                  AsyncStorage.setItem('image_url',responseJson.meta.user.image_url);
                }
                AsyncStorage.setItem('name',responseJson.meta.user.name);
                AsyncStorage.setItem('user_id',responseJson.meta.user.id.toString());
                console.log(responseJson.meta.user.id.toString())
                AsyncStorage.setItem('isLoggedIn','1');
                  this.props.navigation.navigate('Home');
                  // if (this._isMounted) {
                  //   this.setState({
                  //     ButtonStateHolder : false,
                  //     loading: false
                  //   })
                  // }
              }
        })
        .catch((error) => {
             this.setState({
               showDialogMessage:true,
                dialog_message:error
             })
            console.log("ayay",error)
            //alert(error); 
        })
        .finally((info)=>{
          this.setState({
            ButtonStateHolder : false,
            loading: false
          });
        });
}


FBlogIn = async () => {
 
  Facebook.initializeAsync ("1144404209331528");
  try {
    const {
      type,
      token,
      expires,
      permissions,
      declinedPermissions
    } = await Facebook.logInWithReadPermissionsAsync("1144404209331528", {
      permissions: ["public_profile"]
    });
    console.log(token);
    console.log("facebook.");
    if (type === "success") {
     const response = await  fetch('https://graph.facebook.com/v2.5/me?fields=name,email,picture,friends&access_token=' + token);
     const { picture, name, birthday, email } = await response.json();
     console.log(email);
    const data = new FormData();
    data.append('email', email); // you can append anyone.
    data.append('password', email);
    data.append('name', name);
    data.append('user_name', name);
    data.append('image' , picture.data.url);

    this.setState({ email:email , password: email});
    // if(this.state.picture!=null){
    //   data.append("image", {
    //     uri: this.state.picture,
    //     type: type,
    //     name: "DP"
    //   });
    // }
 if(email==undefined){
      //Alert.alert("No associated email");
      this.setState({
	showDialogMessage:true,
	dialog_message:'No associated email'
        })
      await sleep(1500)
      this.setState({
        loading: false,pages:1,inputError:3
      })
    }
fetch(PH+'/auth/register', {
  method: 'POST',
  headers: {
    'Accept':'application/json',
    'Content-Type': 'multipart/form-data',
  },
  body:data,
  }).then((response) => response.json())
      .then(async (responseJson) => {
        if(responseJson.code==400){
          //Alert.alert(responseJson.title)
        this.setState({
            showDialogMessage:true,
            dialog_message:responseJson.title
        })
          await sleep(1500)
          this.setState({
            loading: false,pages:1,inputError:3
          })
          
        }if(responseJson.code==404){
          // /Alert.alert(responseJson.title)
          // await sleep(1500)

          this.setState({
            loading: false,pages:1
          })
            console.log(this.state.email +"<   >")
            console.log("logging IN");
        fetch(PH+'/auth/login', {
          method: 'POST',
          headers: {
            'Accept':'application/json',
            'Content-Type': 'application/json',
          },
          body: JSON.stringify({
            email: this.state.email,
            password: this.state.email
          }),
          }).then((response) => response.json())
              .then((responseJson) => {
              
               
                if(responseJson.code!=200){
                  console.log(responseJson.title)
                  this.setState({
                    
                    loading: false,popupVisible:true
                  })
                  Keyboard.dismiss();
                }else{
                  console.log(responseJson)
                  AsyncStorage.setItem('user',responseJson.meta.user.email);
                  AsyncStorage.setItem('token',responseJson.meta.token.access_token);
                  if(responseJson.meta.user.image_url!==null){
                    AsyncStorage.setItem('image_url',responseJson.meta.user.image_url);
                  }
                  AsyncStorage.setItem('name',responseJson.meta.user.name);
                  AsyncStorage.setItem('user_id',responseJson.meta.user.id.toString());
                  console.log(responseJson.meta.user.id.toString())
                  AsyncStorage.setItem('isLoggedIn','1');
                    this.props.navigation.navigate('Home');
                    if (this._isMounted) {
                      this.setState({
                        ButtonStateHolder : false,
                        loading: false
                      })
                    }
                   
                }
          })
              .catch((error) => {
              
                  this.setState({
                    ButtonStateHolder : false,
                    loading: false
                  })
                console.log("ayay")
              //alert(error); 
              this.setState({
                showDialogMessage:true,
                dialog_message:error
            })
          });
          
        }
  })
      .catch((error) => {
        this.setState({
          ButtonStateHolder : false,
          loading: false
        })
        console.log("sa ubos")
      
        
      
  });

} else {
   //alert(`Facebook Login Error: Cancelled`);
   this.setState({ showDialogMessage:true, dialog_message:`Facebook Login Error: Cancelled` });
}
} catch ({ message }) {
 //alert(`Facebook Login Error: ${message}`);
 this.setState({ showDialogMessage:true, dialog_message:`Facebook Login Error: ${message}` })
}
};



componentWillUnmount() {
    this._isMounted = false;
  }
  render() {
    const resizeMode = 'cover';
      return (
        <KeyboardAwareScrollView>
       
        <Block flex={1} center middle safe fluid style={{width:width,height:height}}>
     
        <StatusBar barStyle = "dark-content" hidden = {false} backgroundColor = "#00BCD4" translucent = {true}/>
        <ImageBackground
        style={{
         backgroundColor: '#ccc',
          flex: 1,
          resizeMode:'stretch',
          position: 'absolute',
          width: wp('100%'),
          height: hp('120%'),
          top:-10
        }}
        source={require('../../../assets/images/slice81-min.png')}
      >
              
      </ImageBackground>
      
        <Block center  middle>
      <Text style={{alignSelf:'center',color:'#525252',fontSize:13 }}>Version 21 Development Mode</Text> 
      
       <TextInput
         value={this.state.username}
         onChangeText={(username) => this.setState({ username })}
         placeholder={'E-mail / brugernavn'}
         style={styles.input}
         placeholderTextColor='black'
         autoCapitalize='none'
         keyboardType='email-address'
         keyboardAppearance='light'
       />
       <TextInput
         value={this.state.password}
         onChangeText={(password) => this.setState({ password, ButtonStateHolder:false })}
         placeholder={'Adgangskode'}
         secureTextEntry={true} 
         placeholderTextColor='black'
         style={styles.input}
         secureTextEntry
         keyboardType='visible-password'
         keyboardAppearance='light'
        
       />
      
       <TouchableOpacity onPress={this.onLogin.bind(this)} disabled={this.state.ButtonStateHolder} loading={true}>
          <Text style={styles.button}>Log Ind</Text>
        </TouchableOpacity>
        {/* <Text style={{fontWeight: 'bold', color: '#fff', fontSize:30, justifyContent:'center', marginVertical:10}}>
           Login With Facebook

    </Text> */}

        <TouchableOpacity
              style={{
                backgroundColor: "",
                alignItems: "center",
                justifyContent: "center",
                width: "100%",
                height: 40,
              }}
              onPress={this.FBlogIn}
            >
              <Text style={styles.fbbutton}>Eller log ind med facebook? <FontAwesome name="facebook-f" size={15} color="grey" /></Text>
            </TouchableOpacity>

        <View style={{flexDirection:'row',alignContent:'space-between'}}>
        <Text style={styles.register} onPress={this.register.bind(this)}>Tilmelde</Text>
        <Text style={styles.forgot} onPress={this.forgotPassword.bind(this)}>Har du glemt dit log ind?</Text>
        </View>
       
        <Loader
          loading={this.state.loading} />
               <Dialog
    visible={this.state.popupVisible}
    onHardwareBackPress={() => { this.setState({ popupVisible: false }); } }
    onTouchOutside={() => {
      this.setState({ popupVisible: false });
    }}
    dialogStyle={{backgroundColor:'transparent'}}
  >
    <DialogContent>
     <View styles={{alignItems:'center'}}>
     <TouchableOpacity onPress={()=>this.setState({ popupVisible: false })} style={{position:'absolute',right:5,top:50,height:60,width:60,zIndex:10}}>
        {/* <Text style={{color:'black',right:0}}>x</Text> */}
      </TouchableOpacity> 
      <Text  style={{position:'absolute',right:60,bottom:100,zIndex:10,fontSize:16,color:'grey'}}>Invalid Credentials</Text>
     <Image  resizeMode="contain" style={{width:wp('60%') ,height:hp('40%'),alignSelf:'center'}} source={require('../../../assets/icons/errorLogin.png')}/> 
     </View>
   
     
    </DialogContent>
  </Dialog>
          <View style={{flexDirection:'row'}}>
            <Image
                            style={{
                              width: 160,
                              height: 250,
                            }}
                            source={require('../../../assets/images/slice14-inverted.png')}
                          />
              <ImageBackground
                style={{
                  width: 185,
                  height: 135,
                }}
                source={require('../../../assets/images/slice8.png')}
              ><Text adjustsFontSizeToFit numberOfLines={5} style={{ position: 'absolute', backgroundColor: 'transparent',
              left:15, top:38, right: 0, zIndex: 100, alignItems: 'flex-end',fontFamily:'KGPrimaryWhimsy',fontSize:16}}> Vidste du godt, at vi lige nu har 5202 børneting, som koster helt GRATIS?

              </Text></ImageBackground>
          </View>
      </Block>

     </Block>
     
     <Dialog
        visible={this.state.showDialogMessage}
        onTouchOutside={() => { this.setState({ showDialogMessage: false }); }}
        onHardwareBackPress={() => {this.setState({ showDialogMessage: false });}}
        dialogStyle={{backgroundColor:'white',width:wp('90%'),alignItems:'center',alignContent:'center'}}>
        <DialogContent>
            <View styles={{alignItems:'center', }}>
                <Text  style={{fontSize:28, color:'#eca6a1', padding: 12, textAlign:'center', fontFamily:'KGPrimaryWhimsy',}}>{this.state.dialog_message}</Text>
            </View>
        </DialogContent>
    </Dialog>
     
    </KeyboardAwareScrollView>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: 'transparent'
  },
  input: {
    width: 300,
    height: 44,
    padding: 10,
    borderWidth: .5,
    borderRadius: 12,
    borderColor: 'black',
    marginBottom: 10,
    backgroundColor: 'white',
    fontFamily:'KGPrimaryWhimsy',
    opacity: 0.9,
    fontSize:20
  },
  button: {
    backgroundColor: '#54969c',
    borderColor: 'white',
    borderWidth: .5,
    borderRadius: 12,
    color: 'white',
    overflow: 'hidden',
    padding: 12,
    textAlign:'center',
    width: 300,
    height: 44,
    opacity: 0.9,
    fontFamily:'KGPrimaryWhimsy',
    fontSize:20
  },
 fbbutton: {
    color: 'grey',
    overflow: 'hidden',
    padding: 12,
    textAlign:'center',
    width: 300,
    height: 44,
    opacity: 0.9,
    fontFamily:'KGPrimaryWhimsy',
    fontSize:20
  },
  register: {
    color: 'black',
    overflow: 'hidden',
    padding: 12,
    textAlign:'center',
    fontSize:20,
    fontFamily:'KGPrimaryWhimsy',
  },
  forgot: {
    color: 'grey',
    overflow: 'hidden',
    padding: 12,
    textAlign:'center',
    fontSize:20,
    fontFamily:'KGPrimaryWhimsy',
  },
});

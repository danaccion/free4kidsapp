import React from 'react';
import { Image, ImageBackground } from 'react-native';
import shorthash from 'shorthash';
import * as FileSystem from 'expo-file-system'

export default class CacheImageWC extends React.Component {
  state = {
    source: null,
  };

  componentDidMount = async () => {
    const { uri } = this.props;
    if(uri!==null){
    const name = shorthash.unique(uri.uri);
    
    const path = `${FileSystem.cacheDirectory}${name}`;
    const image = await FileSystem.getInfoAsync(path);
    if (image.exists) {
      this.setState({
        source: {
          uri: image.uri,
        },
      });
      return;
    }
    console.log("downloading image")  
    const newImage = await FileSystem.downloadAsync(
                        uri.uri,
                        FileSystem.cacheDirectory + name)
                        .then(({ uri }) => {
                          
                            this.setState({
                                source: {
                                  uri: uri,
                                },
                              });
                        })
                        .catch(error => {
                        console.error(error);
                        });
                      }
  };

  render() {
    return (
    <ImageBackground resizeMode='cover' source={require('../../assets/images/slice72-min.png')} style={this.props.style}>
    <Image resizeMode='cover' style={this.props.style} source={this.state.source} />
    </ImageBackground>
    
    );
  }
}
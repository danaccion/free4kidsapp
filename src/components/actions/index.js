import  {RESERVE_PRODUCT,
    MINUS_APPROVAL_COUNT,
    MINUS_RESERVE_PRODUCT,
    APPROVAL_COUNT,
    LOAD_RESERVATION,
    LOAD_APPROVAL,
    DELETE_RESERVATION_PRODUCT,
    DELETE_APPROVAL_PRODUCT,
    ADD_PRODUCT_DATA,
    LOAD_PRODUCTS,
    LOAD_BASKET_ITEMS,
    HAS_PRODUCTS,
    HEART_BAGNOTIF,
    ALL_NOTIFICATIONS,
    MESSAGES_NOTIF

} from './types';

export const messages_notif = (data) => ({
    type: MESSAGES_NOTIF,
    data: data
})

export const all_notifications = (data) => ({
    type: ALL_NOTIFICATIONS,
    data: data
})

export const heartBagNotifCount = (data) => ({
    type: HEART_BAGNOTIF,
    data: data
})

export const getHasProducts = (data) => ({
    type: HAS_PRODUCTS,
    data: data
})

export const loadBasketItems = (data) => ({
    type: LOAD_BASKET_ITEMS,
    data: data
})

export const loadProducts = (data) => ({
    type: LOAD_PRODUCTS,
    data: data
})


export const addProductData = (data) => ({
    type: ADD_PRODUCT_DATA,
    data: data
})

export const reserveCount = (badgeCount) => ({
    type: RESERVE_PRODUCT,
    data: badgeCount
})

export const loadReservation = (data) => ({
    type: LOAD_RESERVATION,
    data: data
})
export const deleteReservation = (data) => ({
    type: DELETE_RESERVATION_PRODUCT,
    data: data
})
export const deleteApproval = (data) => ({
    type: DELETE_APPROVAL_PRODUCT,
    data: data
})
export const loadApproval = (data) => ({
    type: LOAD_APPROVAL,
    data: data
})
export const minusreserveCount = (badgeCount) => ({
    type: MINUS_RESERVE_PRODUCT,
    data: badgeCount
})

export const approvalCount = (badgeCount) => ({
    type: APPROVAL_COUNT,
    data: badgeCount
})

export const minusapprovalCount = (badgeCount) => ({
    type: MINUS_APPROVAL_COUNT,
    data: badgeCount
})
 
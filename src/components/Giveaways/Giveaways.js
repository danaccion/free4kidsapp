import React, { Component } from 'react';
import {StyleSheet,Text,View,ImageBackground,Dimensions,Image,TouchableOpacity,Vibration,AppState,AsyncStorage,StatusBar,FlatList,Platform, Keyboard,LogBox } from 'react-native';
import * as Permissions from 'expo-permissions'
import Constants from 'expo-constants'
import { Notifications } from 'expo';
// import * as Notifications from 'expo-notifications';
import * as Location from 'expo-location';
import HamburgerIcon from '../../icons/HamburgerIcon';
import HeartBagBurger from '../../icons/HeartBagBurger';
import Gestures from 'react-native-easy-gestures';
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';
import {Icon } from 'native-base';
import { PH } from 'react-native-dotenv'
import AnimatedLoader from "react-native-animated-loader";
import {horizontalTabs} from '../../icons/horizontalTabIcons';
import styles2 from './styles';
import ProgressiveImage from '../ProgressiveImage';
import Modal from 'react-native-modalbox';
import BouncingPreloader from 'react-native-bouncing-preloader';
import HomeNavigation from '../HomeNav';
import { Pulse } from 'react-native-animated-spinkit'
import { MAX_HEADER_HEIGHT, MIN_HEADER_HEIGHT,
} from "../Model";
import {connect} from 'react-redux'
import {reserveCount,approvalCount,loadProducts,getHasProducts,heartBagNotifCount,messages_notif} from '../actions/index'
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view'
import DropDownPicker from 'react-native-dropdown-picker';
import Dialog, {SlideAnimation,DialogContent } from 'react-native-popup-dialog';
import {
  Block,Input
} from 'galio-framework';
const { width, height } = Dimensions.get('window');
import { Searchbar } from 'react-native-paper';
 const Fsize = Platform.OS === 'ios' ?14 : 11;
 const Fsize2 = Platform.OS === 'ios' ?20 : 16;
 var gridHeight;
 var count=1;
if(hp('100%')>820){
  gridHeight=hp('70%')
}else if(hp('100%')<800&&hp('100%')>700){
  gridHeight=hp('62%')
}else if(hp('100%')<700&&hp('100%')>600){
  gridHeight=hp('57%')
}else if(hp('100%')<600&&hp('100%')>500){
  gridHeight=hp('48%')
}else{
  gridHeight=hp('43%')
}
 
class HorizontalMenuList extends Component{
  
 
  state = {
      type: '',
    
    };

  onSelectType =(e,id) =>{

    this.props.SelectType(e,id); 
     this.setState({type:e})
    // this.setState(this.state);
    
  }

  render(){
    return(
      <Block >
      {this.props.props.selectedType===this.props.item.description?(
      <Block center style={{
        width:95,
        margin:4,
        borderWidth: 1,
        borderColor:'#cc586f',
        padding:-1,
        borderRadius:8,
        borderStyle: 'dashed',
      }}>
        <TouchableOpacity onPress={()=>{
           if(this.props.props.selectedType===this.props.item.description){
            this.onSelectType(null)
           }else{
            this.onSelectType(this.props.item.description,this.props.item.id)
           }
        }}>
      
          <View style={{margin:4}}>
          <Image  resizeMode="contain" style={{width:wp('9.5%') ,height:hp('3%'),alignSelf:'center'}} source={this.props.item.image}/> 
          <Text adjustsFontSizeToFit numberOfLines={1}  style={{color:'#cc586f',fontFamily:'KGPrimaryWhimsy', fontSize:Fsize}}>
            {this.props.item.description}
          </Text>
          </View>
        </TouchableOpacity>
      </Block>): <Block center style={{
        width:95,
        margin:4,
       
      }}> 
        <TouchableOpacity onPress={()=>{

           if(this.props.props.selectedType===this.props.item.description){
            this.onSelectType(null)
           }else{
            this.onSelectType(this.props.item.description,this.props.item.id)
           }
        }}>
        <View style={{margin:4}}>
        <Image  resizeMode="contain" style={{width:wp('9.5%') ,height:hp('3%'),alignSelf:'center'}} source={this.props.item.image}/> 
        <Text adjustsFontSizeToFit numberOfLines={1}  style={{color:'grey',fontFamily:'KGPrimaryWhimsy', fontSize:Fsize}}>
          {this.props.item.description}
        </Text>
        </View>
       
        </TouchableOpacity>
      </Block>}
      </Block>
    )
  }
}

  class Giveaway extends Component {

    static navigationOptions = () => {
        return {
            headerLeft: <HamburgerIcon/>, 
            headerTitle: (
                <Image
                resizeMode="contain"
                source={require('../../../assets/images/slice2.png')}
                style={{ width: 10, height:10 , flex:4,justifyContent: 'space-around', alignItems: 'center', padding: 25,}}
              />
              ),
            headerRight:<HeartBagBurger/>
        };
    };
 
    state = 
    {
          hasproduct:false,
          Onload:false,
          hasLocationPermission:null,
          github:0,
          dropuppages:0,
          pickerSelectOption:null,
          pickerDisplayed: false,
          allData:undefined,
          location:undefined,selectedType:null,
          searchValue:null,
          evenCount:0,
          currentLocation:null,
          refresh:false,
          refreshing:false,
          category_id:null,
          kilometer:0,
          selectedTypeId:null,
          showDialogMessage:false,
          dialog_message:''
    }

    getPushNotificationPermissions = async () => {
      const { status: existingStatus } = await Permissions.getAsync(Permissions.NOTIFICATIONS);
      let finalStatus = existingStatus;
      
      // only ask if permissions have not already been determined, because
      // iOS won't necessarily prompt the user a second time.
      if (existingStatus !== 'granted') {
        // Android remote notification permissions are granted during the app
        // install, so this will only ask on iOS
        const { status } = await Permissions.askAsync(Permissions.NOTIFICATIONS);
        finalStatus = status;
      }
  
      // Stop here if the user did not grant permissions
      if (finalStatus !== 'granted') {
        return;
      }
      console.log(finalStatus)
  
      // Get the token that uniquely identifies this device
      let notifToken = await Notifications.getExpoPushTokenAsync();
      var matches = notifToken.match(/\[(.*?)\]/);
      console.log("Notification Token: ", await Notifications.getExpoPushTokenAsync());
      let userID=await AsyncStorage.getItem('user_id');

      fetch(PH+'/api/v1/users/update?id='+parseInt(userID)+'&notification_token='+matches[1], {
        method: 'POST',
        headers: {
          'Accept':'application/json',
          'Content-Type': 'multipart/form-data',
          'Authorization': 'Bearer '+await AsyncStorage.getItem('token')
        },
        }).then((response) => response.json())
            .then(async (responseJson) => {
              AsyncStorage.setItem('notif_token',responseJson.meta.status.notification_token);
               console.log("ayay:",responseJson);
               
        }) 
            .catch((error) => {
                console.log(error)
        });
      this.sendPushNotification(notifToken);
      if (Platform.OS === 'android') {
        Notifications.setNotificationChannelAsync('default', {
          name: 'default',
          importance: Notifications.AndroidImportance.MAX,
          vibrationPattern: [0, 250, 250, 250],
          lightColor: '#FF231F7C',
        });
      }
    }

    async  sendPushNotification(expoPushToken) {
      const message = {
        to: expoPushToken,
        sound: 'default',
        title: 'Free4kids',
        body: 'Welcome Back!',
        // data: { data: 'goes here' },
      };
    
      await fetch('https://exp.host/--/api/v2/push/send', {
        method: 'POST',
        headers: {
          Accept: 'application/json',
          'Accept-encoding': 'gzip, deflate',
          'Content-Type': 'application/json',
        },
        body: JSON.stringify(message),
      });
    }

    SelectType = (value,id) => {
      this.setState({
        selectedType: value,refresh:true,selectedTypeId:id
      })
      if(value!=null){
        this.getAllDataByCategory(id)
      }else{
        this.getAllData()
      }
      updateSearch = search => {
        this.setState({ searchValue:search });
      };  
    
    }; 

    dropdown=()=>{ 
      //alert('Bed');
      this.setState({
	showDialogMessage:true,
	dialog_message:'Bed'
        })
    }

    searchClose(){
      Keyboard.dismiss()
    }

    togglePicker() {
      this.setState({
        pickerDisplayed: !this.state.pickerDisplayed
      })
    }
    
    _hideModal() {
      this.setState({ pickerDisplayed: false })
    }

    setPickerValue=(value)=>{ 
      this.setState({
        pickerDisplayed: !this.state.pickerDisplayed,
        dropuppages:value
      })
      this.refs.modal4.close()
      if(value=='1'){
        this.props.navigation.navigate('Srequest');
      }
      if(value=='2'){
        this.props.navigation.navigate('Clocations');
      }
      if(value=='3'){
        this.props.navigation.navigate('ShowFollowers');
      }
      if(value=='4'){
        this.props.navigation.navigate('Donate');
      }
      if(value=='5'){
        this.props.navigation.navigate('SaveBirth');
      }
      
    }

    close=()=>{ 
      this.setState({
        pickerDisplayed: !this.state.pickerDisplayed
      })
    }

    searchData(){
      this.refs.searchModal.open()
      // this._input.focus();
    }

    async componentDidMount() {
      this.didBlurSubscription = this.props.navigation.addListener(
        'didFocus',
        payload => {
          if(this.props.category_id==null){
            this.getAllData();
          }
        }
      )

    await this.locationPermission()
    let notifToken=await AsyncStorage.getItem('notif_token');
    //  Notifications.addNotificationReceivedListener(this._handleNotificationResponse);
      
    //  Notifications.addNotificationResponseReceivedListener(this._handleNotificationResponse);
    Notifications.addListener(this._handleNotification);
      if(notifToken==null){
        this.getPushNotificationPermissions();
    }
    
    }

    _handleNotification = notification => {
      // Vibration.vibrate();
      if(notification.data.type=="buyer"){
        if (AppState.currentState !== 'active') {
          this.props.navigation.navigate('Reservation');
          console.log("gikan",notification);
          this.props.update_message_notif([{id:parseInt(notification.data.sender_id),count:1}]);
          }else{
            setTimeout(function (){

              this.props.reserve(1);
            
            }, 1000);
            
          }
          
      }else{
        if (AppState.currentState !== 'active') {
          this.props.navigation.navigate('Approve');
          console.log("gikan",notification);
          this.props.update_message_notif([{id:parseInt(notification.data.sender_id),count:1}]);
          }else{
            setTimeout(function (){
              //this.props.approval(1);
            }, 1000);
            
          
          }
        
      }
      
      // this.setState({ notification: notification });
    };

    _handleNotificationResponse = response => {
      console.log("unod",response);
    };

    scrolltoTop = (clicks) => {
      
      if(clicks>0){
        this.flatListRef.scrollToOffset({animated: true, offset:10 });
      }
    
    }

    checkProduct = async () =>{
      console.log("checking")
      let hasProduct=await AsyncStorage.getItem('hasProduct');
      console.log("bus",hasProduct);
        if(hasProduct!==null){
          console.log("sulod");
          this.props.getHasProducts(false)
        }else{
          fetch(PH+'/api/v1/users/hasproduct', {
            method: 'GET',
            headers: {
              'Accept':'application/json',
              'Content-Type': 'multipart/form-data',
              'Authorization': 'Bearer '+await AsyncStorage.getItem('token')
            },
            }).then((response) => response.json())
                .then(async (responseJson) => {
                  console.log("ayay",responseJson.meta.productexist)
                  if(responseJson.meta.productexist === true){
                    // this.setState({hasproduct:true})
                    await AsyncStorage.setItem('hasProduct', JSON.stringify(true) )
                    this.props.getHasProducts(true)
                  }else{
                    this.props.getHasProducts(false)
                  }
            })
                .catch((error) => {
                    console.log(error)
            });
        }
    
    }

    onSubmitSearch = async (value) =>{
      if(value.length>0){
    
      fetch(PH+'/api/v1/users/search?offset=0&target[]=name&query='+value, {
        method: 'POST',
        headers: {
          'Accept':'application/json',
          'Content-Type': 'multipart/form-data',
          'Authorization': 'Bearer '+await AsyncStorage.getItem('token')
        },
        }).then((response) => response.json())
            .then((responseJson) => {
            if(responseJson.code!==500){
              this.props.loadProducts({data:responseJson.meta.product,category_id:null})
            }else{
              this.getAllData()
            }
            
        })
            .catch((error) => {
                console.log(error)
        });
      }else{
        this.getAllData()
      }
    }

    async getAllDataByCategory(id){
      this.setState({refresh:true})
      let data = new FormData();
        data.append('category_id', id);
    fetch(PH+'/api/v1/users/products', {
      method: 'POST',
      headers: {
        'Accept':'application/json',
        'Content-Type': 'multipart/form-data',
        'Authorization': 'Bearer '+await AsyncStorage.getItem('token')
      },
      body:data
      }).then((response) => response.json())
          .then((responseJson) => {
              if(responseJson.code==404){
                this.props.loadProducts({data:[],category_id:id})
                this.setState({refresh:false})
              }else if(responseJson.meta.product.length==0){
                this.props.navigation.navigate('Shop');
                this.setState({refresh:false})
              }else{ 
                this.props.loadProducts({data:responseJson.meta.product,category_id:id})
                this.setState({refresh:false})
              }
              
            
      })
          .catch((error) => {
            console.log(error)  
      });
    }
    
    async getAllData(){
      let token=await AsyncStorage.getItem('token');
      let productList=await AsyncStorage.getItem('productList');
      if(productList!==null){
        this.setState({refresh:false})
        this.props.loadProducts({data:JSON.parse(productList),category_id:null})
      }
    fetch(PH+'/api/v1/users/products', {
      method: 'POST',
      headers: {
        'Accept':'application/json',
        'Content-Type': 'multipart/form-data',
        'Authorization': 'Bearer '+token
      },
      }).then((response) => response.json())
          .then(async (responseJson) => {
          //  if(responseJson.code==401){
          //   fetch(PH+'/auth/logout', {
          //     method: 'GET',
          //     headers: {
          //       'Accept':'application/json',
          //       'Content-Type': 'application/json',
          //       'Authorization': 'Bearer '+token
          //     },
          //     }).then((response) => response.json())
          //         .then((responseJson) => {
          //             AsyncStorage.getAllKeys()
          //             .then(keys => AsyncStorage.multiRemove(keys))
          //             .then(() => console.log('success'));
          //             AsyncStorage.setItem('isLoggedIn','0');
                      
          //             this.props.navigation.navigate('Auth');
          //     })
          //         .catch((error) => {
                    
          //           this.props.navigation.navigate('Auth');
          //     })
          //  }
      
            if(responseJson.meta.product.length==0){
              this.props.navigation.navigate('Shop');
              this.setState({refresh:false})
            }else{ 
              this.props.loadProducts({data:responseJson.meta.product,category_id:null})
              await AsyncStorage.setItem('productList', JSON.stringify(responseJson.meta.product))
              this.setState({refresh:false})
            }
              
            
      })
          .catch((error) => {
          console.log("ayay daut",error)
      });
    }

    _getLocationAsync = async () => {
      let location = await Location.getCurrentPositionAsync({});
      this.setState({ currentLocation:location });
    };

    getPermissionAsync = async () => {
      // this.setState({refresh:true})
      
      if (Constants.platform.ios) {
        const { status } = await Permissions.askAsync(Permissions.LOCATION);
        // let loca = await Location.getCurrentPositionAsync();
        
        if (status !== 'granted') {
          //alert('Sorry, we need Location permissions to serve you better!');
          this.setState({ showDialogMessage:true, dialog_message:'Sorry, we need Location permissions to serve you better!' })
          const { status } = await Permissions.askAsync(Permissions.LOCATION);
        }else{
          this._getLocationAsync();
          this.setState({ hasLocationPermission: status === 'granted' })
        }
      }else{
        const { status } = await Permissions.askAsync(Permissions.LOCATION);
        this.setState({ hasLocationPermission: status === 'granted' })
        if (status !== 'granted') {
          //alert('Sorry, we need Location permissions to serve you better!');
          this.setState({ showDialogMessage:true, dialog_message:'Sorry, we need Location permissions to serve you better!' })
        }else{
        this._getLocationAsync();
        this.setState({ hasLocationPermission: status === 'granted' })
      }
    
      
      }
      await  this.checkProduct();
      this.getAllData();
  
    
    }

    distance(lat1,lon1,lat2,lon2) {
      var R = 6371; // km (change this constant to get miles)
      var dLat = (lat2-lat1) * Math.PI / 180;
      var dLon = (lon2-lon1) * Math.PI / 180;
      var a = Math.sin(dLat/2) * Math.sin(dLat/2) +
        Math.cos(lat1 * Math.PI / 180 ) * Math.cos(lat2 * Math.PI / 180 ) *
        Math.sin(dLon/2) * Math.sin(dLon/2);
      var c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1-a));
      var d = R * c;
      if (d>1) return Math.round(d)+"km";
      else if (d<=1) return Math.round(d*1000)+"m";
      return d;
    }

    locationPermission =  () =>{
      this.getPermissionAsync();
    }

    openBotNav =  () =>{
      this.refs.modal4.open()
      
    }

    _renderItem = (data,count) => (
    
      <View style={{alignSelf:'center',paddingBottom:10}}> 
          {count%2==0?(
            <Image resizeMode='contain' style={{position:'absolute',width:wp('94%'),bottom:-hp('4%'),zIndex:0,left:-8}} source={require('../../../assets/images/slice70-min.png')}/>)
            :null}
            {/* <Image resizeMode='contain' style={{width:wp('40%'),height:hp('26.5%'),position:'absolute',alignSelf:'center',top:13}}  source={require('../../../assets/images/slice72-min.png')} /> */}
          
          
            {/* {data.delivery_type==1?( 
            <Image style={{width:40,height:40,position:'absolute',left:3,bottom:3,zIndex:10}}  source={require('../../../assets/images/slice32-min.png')} /> 
          )
          :null}
          {data.delivery_type==2?(
            <Image style={{width:40,height:40,position:'absolute',right:3,bottom:3,zIndex:10}}  source={require('../../../assets/images/slice33-min.png')} />
          )
          :null} */}
          {/* <Image
            style={styles2.container2}
            source={require('../../../assets/images/slice72-min.png')}
            resizeMode='cover'
            />   */}
        <View style={{zIndex:10}}>
        <TouchableOpacity  onPress={() => this.props.navigation.navigate('ProductDetails', { 'product':data,'location': this.distance(data.product_lat,data.product_lng,this.state.currentLocation.coords.latitude,this.state.currentLocation.coords.longitude)})}>
        {/* <View style={styles2.container}> */}
        <ImageBackground
          style={styles2.container}
          source={require('../../../assets/images/slice72-min.png')}
          resizeMode='contain'
        >
            
        <ProgressiveImage
            source={{  uri: data.images[0] }}
            thumbnailSource={require('../../../assets/images/slice3.png')}
            style={styles2.photo}
            resizeMode="cover"
          />
          {/* <Image  style={styles2.photo} source={{ uri: data.images[0] }}  /> */}
          <Text adjustsFontSizeToFit numberOfLines={1}  style={styles2.title}>{data.name}</Text>
          <Text adjustsFontSizeToFit numberOfLines={1}  style={{fontSize:Platform.OS === 'ios' ?11 : 9}}>{this.state.currentLocation!=null?this.distance(data.product_lat,data.product_lng,this.state.currentLocation.coords.latitude,this.state.currentLocation.coords.longitude)+' fra dig':'0 km fra dig'}</Text>
          {/* <Text style={styles.category}>{item.category_name}</Text> */}
          </ImageBackground>
        {/* </View> */}
      </TouchableOpacity>
        {/* <CardViewWithImage
                  key={data.id}
                  width={ wp('40%')}
                  content={ this.state.currentLocation!=null?this.distance(data.product_lat,data.product_lng,this.state.currentLocation.coords.latitude,this.state.currentLocation.coords.longitude)+' fra dig':'0 km fra dig'}
                  source={{uri : data.images[0]}}
                  title={ data.name }
                  imageWidth={wp('40%') }
                  imageHeight={ hp('20%') }
                  contentPadding={{}}
                  // imageMargin={{top:hp('10%')}}
                  // titlePadding={{top:hp('0.41%'),bottom:-hp('8.14%')}}
                  onPress={() =>  this.props.navigation.navigate('ProductDetails', { 'product':data,'location': this.distance(data.product_lat,data.product_lng,this.state.currentLocation.coords.latitude,this.state.currentLocation.coords.longitude)})}
                  roundedImage={ false }
                  contentFontFamily	={'KGPrimaryWhimsy'}
                  titleFontFamily={'KGPrimaryWhimsy'}
                  titleFontSize={Fsize}
                  
                />    */}
              </View> 
              {data.delivery_type==1?(
                <Image style={{width:30,height:30,position:'absolute',right:10,bottom:-2,zIndex:100}}  source={require('../../../assets/images/slice33-min.png')} />
              ):null}     
              {data.delivery_type==2?(
            <Image style={{width:30,height:30,position:'absolute',left:10,bottom:10,zIndex:100}}  source={require('../../../assets/images/slice32-min.png')} /> 
              ):null} 
              {data.delivery_type==3||data.delivery_type==null||data.delivery_type==0?(
                <View style={{zIndex:100}}>
              <Image style={{width:30,height:30,position:'absolute',right:10,bottom:10}}  source={require('../../../assets/images/slice33-min.png')} />
            <Image style={{width:30,height:30,position:'absolute',left:10,bottom:12}}  source={require('../../../assets/images/slice32-min.png')} /> 
            </View>
              ):null} 
      </View>        
    );

   _renderPlaceholder = i => (<Image  resizeMode="contain" style={{width:wp('80%'),alignSelf:'center'}} source={require('../../../assets/images/slice70-min.png')} key={i}/>);

    render(){
      const {searchValue} = this.state
      //  LogBox.ignoreAllLogs()
      const pickerValues = [
        {
          title: 'Lav en specifik søgen',
          value: '1'
        },
        {
          title: 'Ændre din placering',
          value: '2'
        },
        {
          title: 'Vis dem jeg følger',
          value: '3'
        },
        {
          title: 'Vis gemte børneting',
          value: '4'
        },
        {
          title: 'Donér børneting',
          value: '5'
        }
      ]
      
      const resizeMode = 'contain';
      const  { hasLocationPermission } = this.state
    
      if(hasLocationPermission==true){
        return (
        <View>
            
            <Dialog
                visible={this.state.showDialogMessage}
                onTouchOutside={() => { this.setState({ showDialogMessage: false }); }}
                onHardwareBackPress={() => {this.setState({ showDialogMessage: false });}}
                dialogStyle={{backgroundColor:'white',width:wp('90%'),alignItems:'center',alignContent:'center'}}>
                <DialogContent>
                    <View styles={{alignItems:'center', }}>
                        <Text  style={{fontSize:28, color:'#eca6a1', padding: 12, textAlign:'center', fontFamily:'KGPrimaryWhimsy',}}>{this.state.dialog_message}</Text>
                    </View>
                </DialogContent>
            </Dialog>

          <Block  center width={width}>
            <StatusBar barStyle = "dark-content" hidden = {false} backgroundColor = "#00BCD4" translucent = {true}/>
          <ImageBackground
          resizeMode='stretch'
            style={{
              //  flex: 1,
              position: 'absolute',
              width: wp('105%'),
              height: hp('100%'),
              justifyContent: 'center',
            }}
            source={require('../../../assets/images/slice81-min.png')}
            />
            <Block center>
          
            <HomeNavigation openBotNav={this.openBotNav} onSearch={this.onSubmitSearch}/>
          
            <Block center style={{marginTop:15}}>
              
                <View >
                <FlatList style={{backgroundColor:'transparent'}} 
                            horizontal={true} 
                            data={horizontalTabs}
                            showsHorizontalScrollIndicator={false}
                            renderItem={({item,index})=>{
                              return(
                                
                                <HorizontalMenuList SelectType={this.SelectType} props={this.state} item={item} index={index} parentFlatlist={this}/>
                                
                              );
                            }}
                            keyExtractor={(item,index)=>item.type}
            />    
                  
                      
                    
            </View>
                <Block style={{position:'absolute',top:60,zIndex:100}}>
              
                    <Gestures
                      draggable={{
                        y: false,
                        x:true
                      }}
                      scalable={{
                        min: -200,
                        max: 20,
                      }}
                        rotatable={false}
                        onEnd={(event, styles) => {

                          this.setState({kilometer:((wp('95%')/2)+styles.left)/4.7})
                        }}
                        // onEnd={()=>console.log("ayay")}
                        // onChange={(event,styles)=>{this.setState({kilometer:((wp('95%')/2)+styles.left)/4.7})}}
                        style={{zIndex:500}}
                              >
                      <Image  resizeMode="contain" style={{height:50,width:50}} source={require('../../../assets/images/slice84-bil-min_flipped.png')}/>
                          </Gestures>  
                    
                      <Image  resizeMode="contain" style={{width:width,alignSelf:'center',zIndex:10,top:-75}} source={require('../../../assets/images/slice82-ny-min.png')}/>  
                      <Text style={{ position: 'absolute',alignSelf:'center', fontFamily:'KGPrimaryWhimsy',alignSelf:'center' ,fontSize: Platform.OS === 'ios' ?13 : 11,color:'grey',alignItems:'center'}}> {Math.round(this.state.kilometer)} km omkring Vordingborg</Text>
                      <Text style={{ position: 'absolute',alignSelf:'center', fontFamily:'KGPrimaryWhimsy',left:10,top:40, fontSize: Platform.OS === 'ios' ?13 : 11,color:'grey',zIndex:10}}>0 km</Text>
                  
                      {this.props.onrefresh==true||this.state.refresh==true?(
                    <Block center middle >
                      <Pulse style={{marginTop:10}} size={60} color="#c97190"/>
                    </Block>
                  ):null}    
                  <Text style={{ position: 'absolute',alignSelf:'center', fontFamily:'KGPrimaryWhimsy', right:10,top:40, fontSize: Platform.OS === 'ios' ?13 : 11,color:'grey',zIndex:10}}>Hele DK</Text>
                      {this.props.allProducts!=undefined&&this.state.refresh!==true?(
                //  <View style={{flex:1,position:'absolute',bottom:1,flexDirection:"row",alignItems:'center',zIndex:1,alignSelf:'center'}}>
            <Block  style={{flex:1,height:gridHeight,top:-120,zIndex:100,justifyContent:'center',bottom:-140}}>

              
                  {/* <View resizeMode="contain" style={{position:'absolute',flexDirection:'row',height:50,width:30,alignSelf:'center',zIndex:10000,bottom:-140,justifyContent:'center'}}>
                      <BouncingPreloader
                            icons={[
                              require('../../../assets/images/slice78-min.png')
                            ]}
                            
                            leftRotation="0deg"
                            rightRotation="0deg"
                            // leftDistance={-180}
                            // rightDistance={-250}
                            speed={2000}
                            size={50}
                            resizeMode='contain' />
                    </View> */}
  
          
                  
          
                  <FlatList style={styles.list}
                    contentContainerStyle={styles.listContainer}
                    ref={(ref) => { this.flatListRef = ref; }}
                    data={this.props.allProducts}
                    horizontal={false}
                    numColumns={2}
                    keyExtractor= {(item) => {
                      return item.id;
                    }}
                    refreshing={this.state.refreshing}

                    onRefresh={() => {
                      if(this.state.selectedType==null){
                      this.getAllData()
                      }else{
                      this.getAllDataByCategory(this.state.selectedTypeId)
                      }
                      
                    }}

                    extraData={extra=1,datalenth=this.props.allProducts.length}
                    renderItem={({item,index}) => {
                  
                      console.log(extra++,datalenth,index+1,item.name)
                  
                      return (
                        <Block center style={{alignSelf:'center',paddingBottom:20}}>
                          <ImageBackground
                              style={styles.frame}
                              source={require('../../../assets/images/slice72-min.png')}
                              resizeMode='contain'
                            >
                               {/* this.distance(item.product_lat,item.product_lng,this.state.currentLocation.coords.latitude,this.state.currentLocation.coords.longitude) */}
                          <TouchableOpacity style={{alignItems:'center',alignContent:'center',alignSelf:'center'}}  onPress={() => this.props.navigation.navigate('ProductDetails', { 'product':item,'location':console.log("lokasyon")})}>
                        
                          <ProgressiveImage
                              source={{uri: item.images[0]}} 
                              thumbnailSource={require('../../../assets/images/slice3.png')}
                              style={styles.userImage}
                              // resizeMode="cover"
                            />
                        
                          </TouchableOpacity>


                          <View style={styles.cardFooter}>
                            <View style={{alignItems:"center", justifyContent:"center"}}>
                              <Text adjustsFontSizeToFit numberOfLines={1}  style={styles.name}>{item.name}</Text>
                              <Text adjustsFontSizeToFit numberOfLines={1}  style={styles.position}>{this.state.currentLocation!=null?this.distance(item.product_lat,item.product_lng,this.state.currentLocation.coords.latitude,this.state.currentLocation.coords.longitude)+' fra dig':'0 km fra dig'}</Text>
                            </View>
                            
                            {item.delivery_type==2?(
                              <TouchableOpacity onPress={()=>console.log("Test")} style={{position:'absolute',right:10,bottom:-20,zIndex:100}}>
                                <Image style={{width:40,height:40}}  source={require('../../../assets/images/slice33-min.png')} />
                                </TouchableOpacity>
                              ):null}     
                              {item.delivery_type==1?(
                            <TouchableOpacity onPress={()=>this.props.notif(1)} style={{position:'absolute',left:10,bottom:-20,zIndex:100}}>
                            <Image style={{width:40,height:40}}  source={require('../../../assets/images/slice32-min.png')} /> 
                            </TouchableOpacity>
                              ):null} 
                              {item.delivery_type==3||item.delivery_type==null||item.delivery_type==0?(
                                <View style={{zIndex:100,position:'absolute',bottom:20}}>
                              <Image style={{width:40,height:40,position:'absolute',left:40}}  source={require('../../../assets/images/slice33-min.png')} />
                            <Image style={{width:40,height:40,position:'absolute',right:40}}  source={require('../../../assets/images/slice32-min.png')} /> 
                            </View>
                              ):null} 
                            
                          </View>


                          {extra%2==0&&index+1!=datalenth?(
                        
                              <Image resizeMode='contain' style={{position:'absolute',width:wp('94%'),marginTop:Platform.OS=='ios'?148:143,zIndex:-60,left:-20}} source={require('../../../assets/images/slice70-min.png')}/>
                            )
                              :null}
                              {datalenth%2!=0&&index+1==datalenth?(
                                <Block center style={{position:'absolute',width:wp('94%'),marginTop:Platform.OS=='ios'?148:143,zIndex:-60}} >
                                <Image resizeMode='contain' style={{width:wp('94%')}} source={require('../../../assets/images/slice70-min.png')}/>
                                </Block>
                              )
                                :null}
                                
                          </ImageBackground>
                          </Block>
                        
                      )
                    }}/>
                    </Block>
                    ):  null} 
          
                      </Block>

                
            </Block> 
          
          
                
              
              </Block>
              
            
          
        
                {/* <Image  resizeMode="contain"  source={require('../../../assets/images/slice78-min.png')}/> */}
                <Modal backButtonClose={true} coverScreen={true} position={"bottom"} style={{height:hp('30%'),zIndex:4000,borderTopLeftRadius: 20, 
          borderTopRightRadius: 20,
          overflow: "hidden"}} transparent={true} ref={"modal4"} >
                  <Block style={{
                    flex:1,
                    backgroundColor: 'transparent',
                    bottom: 10,
                    zIndex:200,
                    width:  wp('98%'),
                    alignItems: 'center',
                    position: 'absolute',
                    height:hp('28%'),
                    zIndex:5000
                  }}>
                    { pickerValues.map((value, index) => {
                      return <TouchableOpacity key={index} onPress={() => this.setPickerValue(value.value)} style={{ paddingTop: hp('0.54%'), paddingBottom: hp('0.54%'),borderColor: 'grey',borderWidth: .2,
                      borderRadius: 12,marginBottom:hp('0.27%'),width:  Dimensions.get('screen').width/1.1, marginTop:hp('0.27%'),shadowColor: '#000',
                      shadowOffset: { width: 0, height: 2 },
                      shadowOpacity: 0.5,
                      shadowRadius: 1,
                      elevation: 1,}}>
                          <Text style={{
                              margin:1,
                              textAlign:'center',
                              fontSize: Platform.OS === 'ios' ?20 : 18,
                              color:'#e87289',
                              alignSelf:'center',
                              fontFamily:'KGPrimaryWhimsy',
                              }}>{ value.title }</Text>
                        </TouchableOpacity>
                    })}
                  </Block>
                
                </Modal>   

                <Modal backButtonClose={true} entry={"top"} backdropOpacity={.1} onClosed={()=>Keyboard.dismiss()}  position={"top"} style={{position:'absolute',backgroundColor:'transparent',top:10,borderRadius:10,height:hp('13%'),zIndex:3000,
                width:wp('95%')}} transparent={true} ref={"searchModal"} >
                <Block flex={1}>
                <Searchbar
                    onIconPress={()=>this.onSubmitSearch()}
                    placeholder="Søg"
                    ref={(c) => this._input = c}
                    searchAccessibilityLabel={'Søg'}
                    onChangeText={query => { this.setState({ searchValue: query }); }}
                    value={searchValue}
                    // onFocus={()=>this.setState({topSpace:-10})}
                    onSubmitEditing={()=>this.onSubmitSearch()}
                    onTouchCancel={()=>console.log("ayay")}
                    inputStyle={{fontFamily:'KGPrimaryWhimsy',width:wp('90%'),color:'grey'}}
                  />
                  </Block>
                </Modal>
          
            </Block>
          
            </View>
        )
          
    
      }else if (hasLocationPermission==null){
        return (
          <View style={styles.container}>
          <ImageBackground
            style={{
              backgroundColor: '#ccc',
              flex: 1,
              resizeMode,
              position: 'absolute',
              width: '100%',
              height: '100%',
              justifyContent: 'center',
            }}
            source={require('../../../assets/images/slice81-min.png')}
            />  
            
            <View > 
            <Image resizeMode="contain"
                            style={{ width: wp('76.39%'),
                              height: hp('31.19%'),alignSelf:'center'}}
                            source={require('../../../assets/images/slice26-min.png')}
                            />  
              <View style={{position:'absolute', top:hp('24.41%'), alignSelf:'center'}}>
              <TouchableOpacity onPress={this.locationPermission.bind(this)}>
              <Text style={styles.button}>Opret mig</Text>
              </TouchableOpacity>   
              </View>
            </View>
                  
            
      </View>
      
        )
      }else if(hasLocationPermission=='denied'){
        return (
          <View style={styles.container}>
          <ImageBackground
            style={{
              backgroundColor: '#ccc',
              flex: 1,
              resizeMode,
              position: 'absolute',
              width: '100%',
              height: '100%',
              justifyContent: 'center',
            }}
            source={require('../../../assets/images/slice81-min.png')}
            />  
            
            <View > 
            <Image resizeMode="contain"
                            style={{ width: wp('91.67%'),
                              height: hp('31.19%'),alignSelf:'center'}}
                            source={require('../../../assets/images/slice26-min.png')}
                            />  
              <View style={{position:'absolute', top: hp('24.41%'), alignSelf:'center'}}>
              <TouchableOpacity onPress={this.locationPermission.bind(this)}>
              <Text style={styles.button}>Opret mig</Text>
              </TouchableOpacity>   
              </View>
            </View>
                  
            
      </View>
        )
      }
      
    }
    }

    const mapStateToProps = (state) =>{
      console.log("prod ni",state.count.hasProducts)
      return{
        category_id:state.count.category_id,
        onrefresh:state.count.onrefresh,
        count:state.count.approvalBadgeCount,
        allProducts: state.count.allProducts,
        hasproducts: state.count.hasProducts
      }
    }

    const mapDispatchToProps = (dispatch) =>{
      return{
        reserve:(count)=>dispatch(reserveCount(count)),
        notif:(count)=>dispatch(heartBagNotifCount(count)),
        approval:(count)=>dispatch(approvalCount(count)),
        loadProducts:(data)=>dispatch(loadProducts(data)),
        getHasProducts:(data)=>dispatch(getHasProducts(data)),
        update_message_notif:(data)=>dispatch(messages_notif(data))
      }
    }
    
    export default connect(mapStateToProps,mapDispatchToProps)(Giveaway);
  const styles = StyleSheet.create({
    item: {
      flex: 1,
      height: 160,
      margin: 1,
    },
    lottie: {
      width: 150,
      height: 150
    },
    container: {
      flex: 1,
      alignItems: 'center',
      justifyContent: 'center',
      backgroundColor: 'transparent'
    },
    container3: {
      position:'absolute',
      backgroundColor: 'transparent',
      flex: 1,
      height:hp('79%'),
      width:wp('100%'),
      bottom:0,
      top:45
      
      //justifyContent: "flex-end",
      //alignItems: "center"
    },
    textstyle:{
      fontFamily:'KGPrimaryWhimsy',
      fontSize:hp('5.42%')
    },
    wrpper: {
      backgroundColor: '#c97190',
      borderColor: 'white',
      borderWidth: .5,
      borderRadius: 10,
      overflow: 'hidden',
      top:hp('2%'),
      shadowColor:'black',
      width:  wp('95%'),
      height: Dimensions.get('screen').height /17,
      opacity: 0.9,
      // fontSize:hp('2.71%'),
      // fontFamily:'KGPrimaryWhimsy',
      flexDirection: 'row', justifyContent:'space-between',alignItems:'center'
    },
    wrpper2: {
      borderColor: 'white',
      borderWidth: .5,
      borderRadius: 10,
      justifyContent: "space-around",
      alignItems: "center",
      
      textAlign:'center',
      fontFamily:'KGPrimaryWhimsy',
      flexDirection: 'row',   
    },
    button: {
      backgroundColor: '#54969c',
      borderColor: 'white',
      borderWidth: .5,
      borderRadius: 12,
      color: 'white',
      overflow: 'hidden',
      padding: wp('3.06%'),
      textAlign:'center',
      width: wp('30.56%'),
      height: hp('3.8%'),
      opacity: 0.9,
      fontSize:hp('1.36%'),
      alignSelf:'center',
      fontFamily:'KGPrimaryWhimsy',
      opacity:0
    },
    container4:{
      flex:1,
      backgroundColor:'transparent',
      // marginTop:20,
      zIndex:99
    },
    list: {
      paddingHorizontal: 5,
      backgroundColor:"transparent",
      zIndex:99,
      marginBottom:90
    },
    listContainer:{
    alignItems:'center',

    zIndex:99
    },
    /******** card **************/
    card:{
      shadowColor: '#00000021',
      shadowOffset: {
        width: 0,
        height: 6,
      },
      shadowOpacity: 0.37,
      shadowRadius: 7.49,
      elevation: 12,
      alignSelf:'center',
      marginVertical: 5,
      backgroundColor:"transparent",
      flexBasis: '46%',
      zIndex:99
      
    },
    frame:{
      backgroundColor:"transparent",
      flexBasis: '45%',
      marginHorizontal: 5,
      width:wp('40%'),
      zIndex:999
    },
    cardFooter: {
      paddingVertical: 17,
      paddingHorizontal: 16,
      borderTopLeftRadius: 1,
      borderTopRightRadius: 1,
      flexDirection: 'row',
      alignItems:"center", 
      justifyContent:"center",
      zIndex:99
    },
    cardContent: {
      paddingVertical: 12.5,
      paddingHorizontal: 16,
      zIndex:99
    },
    cardHeader:{
      flexDirection: 'row',
      justifyContent: 'space-between',
      paddingTop: 12.5,
      paddingBottom: 25,
      paddingHorizontal: 16,
      borderBottomLeftRadius: 1,
      borderBottomRightRadius: 1,
    },
    userImage:{
      height: Platform.OS=='ios'?125:120,
      width: Platform.OS=='ios'?125:120,
      // borderRadius:60,
      marginTop:Platform.OS=='ios'?10:12,
      alignSelf:'center',
      // borderColor:"#DCDCDC",
      // borderWidth:3,
      zIndex:99
    },
    name:{
      fontSize:Fsize,
      flex:1,
      alignSelf:'center',
      color: '#444444',
      fontWeight:'bold',
      fontFamily:'KGPrimaryWhimsy',
      zIndex:99,
      padding:2
    },
    position:{
      fontSize: Platform.OS === 'ios' ?11 : 9,
      flex:1,
      alignSelf:'center',
      color:"#696969",
      zIndex:99
    },
    followButton: {
      marginTop:10,
      height:35,
      width:100,
      flexDirection: 'row',
      justifyContent: 'center',
      alignItems: 'center',
      borderRadius:30,
      backgroundColor: "#00BFFF",
    },
    followButtonText:{
      color: "#FFFFFF",
      fontSize:20,
    },
    icon:{
      height: 20,
      width: 20, 
    }
  });

import { StyleSheet } from 'react-native';
import { RecipeCard } from '../../components/AppStyles';

const styles = StyleSheet.create({
  container: RecipeCard.container,
  photo: RecipeCard.photo,
  title: RecipeCard.title,
  category: RecipeCard.category,
  type: RecipeCard.type
});

export default styles;

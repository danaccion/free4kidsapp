import React, { Component } from 'react';
import { ScrollView, StyleSheet,Text,View,ImageBackground,Image,TouchableOpacity,Platform,Dimensions } from 'react-native';
import Ionicons from 'react-native-vector-icons/Ionicons'
// import { Picker } from 'react-native-picker-dropdown';
import BackButton from '../../../BackButton';
import { Container, Header, Content, Form, Item, Picker,Icon  } from 'native-base';
import HamburgerIcon from '../../../icons/HamburgerIcon';
import HeartBagBurger from '../../../icons/HeartBagBurger';
import RNPickerSelect from 'react-native-picker-select';
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';
import DropDownPicker from 'react-native-dropdown-picker';
import {
  Block,Input
} from 'galio-framework';
const { width, height } = Dimensions.get('window');
const Fsize3 = Platform.OS === 'ios' ?25 : 20;
export default class Srequest extends Component {
  static navigationOptions = () => {
    return {
        headerLeft: <BackButton/>,
        headerTitle: (
            <Image
            resizeMode="contain"
            source={require('../../../../assets/images/slice2.png')}
            style={{ width: 10, height:10 , flex:4,justifyContent: 'space-around', alignItems: 'center', padding: 25,}}
          />
          ),
        headerRight: <HeartBagBurger/>
    };
};
// constructor(props) {
//   super(props);
//   this.state = {
//     selected2: undefined
//   };
// }
// onValueChange2(value: string) {
//   this.setState({
//     selected2: value
//   });
// }
    back = async ()=>{
    this.props.navigation.navigate('Home');
    }
    submit =()=>{
      //this.props.navigation.navigate('Home');
    }
   
     state = {
        selected2: undefined,
        Hvad:null,
        Mærke:null,
        Stand:null,
        Forsendelse:null,
      };
 
    onValueChange2(value) {
      this.setState({
        selected2: value
      });
    }
    hvadSelect(value) {
      this.setState({
        Hvad: value
      });
    }
  render(){
    const items = [{
      label: '0 md / 40',
      value:'0 md / 40',
    },
    {
      label: '0 md / 50',
      value: '0 md / 50',
    },
    {
      label: '2 mdr / 56',
      value: '2 mdr / 56',
    },
    {
      label: '3 mdr / 62',
      value: '3 mdr / 62',
    },
    {
      label: '6 mdr / 68',
      value: '6 mdr / 68',
    },
    {
      label: '9 mdr / 74',
      value: '9 mdr / 74',
    },
    {
      label: '12 mdr / 80',
      value: '12 mdr / 80',
    },
    {
      label: '12-18 mdr / 86',
      value: '12-18 mdr / 86',
    },

    {
      label: '18 mdr - 2 år / 92',
      value: '18 mdr - 2 år / 92',
    },
    {
      label: '2-3 år / 98',
      value: '2-3 år / 98',
    },
    {
      label: '3-4 år 104',
      value: '3-4 år 104',
    },
    {
      label: '4-5 år / 110',
      value: '4-5 år / 110',
    },
    {
      label: '5-6 år / 116',
      value: '5-6 år / 116',
    },
    {
      label: '6-7 år / 122',
      value: '6-7 år / 122',
    },
    {
      label: '7-8 år / 128',
      value: '7-8 år / 128',
    },
    {
      label: '8-9 år / 134',
      value: '8-9 år / 134',
    },
    {
      label: '9-10 år / 140',
      value: '9-10 år / 140',
    },
    {
      label: '10-11 år / 146',
      value: '10-11 år / 146',
    },
    {
      label: '11-12 år / 152',
      value: '11-12 år / 152',
    },
    {
      label: '12-13 år / 158',
      value: '12-13 år / 158',
    },
    {
      label: '13-14 år / 164',
      value: '13-14 år / 164',
    },
    {
      label: '14 år+ / 170',
      value: '14 år+ / 170',
    }
];
    const resizeMode = 'cover';
    return (
      <View style={styles.container}>
       
      <ImageBackground
         style={{
           backgroundColor: '#ccc',
           flex: 1,
           resizeMode,
           position: 'absolute',
           width: '100%', 
           height: '100%',
           justifyContent: 'center',
           
         }}
         source={require('../../../../assets/images/slice81-min.png')}
         />
       
       <View style={{flex: 1,marginTop:Dimensions.get('screen').height/50}}>
           <View>
         </View>
         <View>
          <Text  style={{ fontSize: Fsize3 ,alignSelf:'center',color:'#2db9b9',fontFamily:'KGPrimaryWhimsy'}}>Specifik søgen</Text>
          </View>
      </View>
      <View style={{ position:'absolute',
    backgroundColor: 'transparent',
    flex: 1,
    height:hp('79%'),
    width:wp('100%'),
    bottom:0,
    top:50}}>
      <Container style={{backgroundColor:'transparent'}} >
        <Content scrollEnabled={false}>
          <Form>
            <Block center style={{marginBottom:10}}>
          <DropDownPicker
                    zIndex={9000}
                    arrowColor={'grey'}
                    items={items}
                    labelStyle={{
                        color: 'grey'
                    }}
                    style={{
                      borderTopLeftRadius: 10, borderTopRightRadius: 10,
                      borderBottomLeftRadius: 10, borderBottomRightRadius: 10
                  }}
                    arrowStyle={{color: 'grey'}}
                    defaultNull
                    dropDownStyle={{zIndex:6000}}
                    placeholder="Størrelse"
                    containerStyle={{height: 50,width:width*0.95,borderRadius:8}}
                    onChangeItem={item =>  this.setState({ selectedOption: item.value})}
            />
            </Block>
          <Block center style={{marginBottom:10}}>
            <DropDownPicker
                    zIndex={8000}
                    arrowColor={'grey'}
                    items={items}
                    labelStyle={{
                        color: 'grey'
                    }}
                    style={{
                      borderTopLeftRadius: 10, borderTopRightRadius: 10,
                      borderBottomLeftRadius: 10, borderBottomRightRadius: 10
                  }}
                    arrowStyle={{color: 'grey'}}
                    defaultNull
                    dropDownStyle={{zIndex:6000}}
                    placeholder="Størrelse"
                    containerStyle={{height: 50,width:width*0.95,borderRadius:8}}
                    onChangeItem={item =>  this.setState({ selectedOption: item.value})}
              />
            </Block>
          <Block center style={{marginBottom:10}}>
            <DropDownPicker
                    zIndex={7000}
                    arrowColor={'grey'}
                    items={[{label: 'Helt ny (ubrugt)', value: 'Helt ny (ubrugt)'},{label: 'Næsten som ny', value: 'Næsten som ny'},{label: 'Brugt, men fin', value: 'Brugt, men fin'}]}
                    labelStyle={{
                        color: 'grey'
                    }}
                    style={{
                      borderTopLeftRadius: 10, borderTopRightRadius: 10,
                      borderBottomLeftRadius: 10, borderBottomRightRadius: 10
                  }}
                    arrowStyle={{color: 'grey'}}
                    defaultNull
                    dropDownStyle={{zIndex:6000}}
                    placeholder="Stand"
                    containerStyle={{height: 50,width:width*0.95,borderRadius:8}}
                    onChangeItem={item =>  this.setState({ selectedOption: item.value})}
              />
            </Block>
            <Block center style={{marginBottom:10}}>
            <DropDownPicker
                    zIndex={6000}
                    arrowColor={'grey'}
                    items={[{label: 'Pige', value: 'Pige'},{label: 'Dreng', value: 'Dreng'},{label: 'Unisex', value: 'Unisex'}]}
                    labelStyle={{
                        color: 'grey'
                    }}
                    style={{
                      borderTopLeftRadius: 10, borderTopRightRadius: 10,
                      borderBottomLeftRadius: 10, borderBottomRightRadius: 10
                  }}
                    arrowStyle={{color: 'grey'}}
                    defaultNull
                    dropDownStyle={{zIndex:6000}}
                    placeholder="Mærke"
                    containerStyle={{height: 50,width:width*0.95,borderRadius:8}}
                    onChangeItem={item =>  this.setState({ selectedOption: item.value})}
              />
            </Block>
             
            <Block center style={{marginBottom:15}}>
            <DropDownPicker
                    zIndex={5000}
                    arrowColor={'grey'}
                    items={[{label: 'Sender gerne', value: '1'},{label: 'Kan afhentes', value: '2'},{label: 'Tilbyder begge dele', value: '3'}]}
                    labelStyle={{
                        color: 'grey'
                    }}
                    style={{
                      borderTopLeftRadius: 10, borderTopRightRadius: 10,
                      borderBottomLeftRadius: 10, borderBottomRightRadius: 10
                  }}
                    arrowStyle={{color: 'grey'}}
                    defaultNull
                    // dropDownStyle={{zIndex:6000}}
                    placeholder="Forsendelse / fragt"
                    containerStyle={{height: 50,width:width*0.95,borderRadius:8}}
                    onChangeItem={item =>  this.setState({ selectedOption: item.value})}
              />
            </Block>
            <Block center style={{marginBottom:20}}>
            <TouchableOpacity onPress={this.submit.bind(this)}>
                  <Text style={styles.button2}>Søg</Text>
              </TouchableOpacity>
            </Block>
             
             
          </Form>
        </Content>
        
      
      </Container>
        
      </View>
    
  </View>
   
    );
  }
  }
   
const pickerSelectStyles = StyleSheet.create({
  inputIOS: {
      fontSize: 20,
      paddingTop: 13,
      paddingHorizontal: 10,
      paddingBottom: 12,
      borderWidth: 1,
      borderColor: 'transparent',
      borderRadius: 4,
      backgroundColor: 'white',
      color: 'black',
  },
});
const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    backgroundColor: 'transparent'
  },
  container2: {
    backgroundColor: 'transparent',
  
    height:hp('100%'),
    width:wp('100%'),
    
  },
  textstyle:{
    fontFamily:'KGPrimaryWhimsy',
    fontSize:40
  },
  container3: {
    flex: 1,
    position:'absolute',
    alignItems: "center",
    alignContent:'center',
    justifyContent: 'flex-end',
  },
  dropdown: {
    height: Dimensions.get('screen').width /7,
    padding: 5,
    position:'relative',
    margin:5,
    borderWidth: .2,
    borderRadius: 12,
    textShadowColor: 'black',
    borderColor: 'black',
    marginBottom: 10,
    backgroundColor: 'white',
    opacity: 0.9,
    fontFamily:'KGPrimaryWhimsy',
    fontSize:20,
    alignContent:'center'
  },
  wrpper: {
    opacity: 0.9,
    fontSize:20,
    fontFamily:'KGPrimaryWhimsy',
    flexDirection: 'row', justifyContent: 'space-around',   
  },
  button: {
    alignSelf:'center',
    marginLeft:10,
    marginRight:20,
    margin:10,
    backgroundColor: 'white',
    borderColor: 'black',
    borderWidth: 3,
    borderTopColor:'red',
    borderRadius: 12,
    color: 'white',
    overflow: 'hidden',
    textAlign:'center',
    width: 350,
    height: 48,
    // opacity: 0.9,
    fontSize:20,
    fontFamily:'KGPrimaryWhimsy'
  },
  button2: {
    backgroundColor: '#54969c',
    borderColor: 'white',
    borderWidth: .5,
    borderRadius: 12,
    color: 'white',
    overflow: 'hidden',
    padding: 12,
    textAlign:'center',
    width:wp('95%'),
    height: 48,
    opacity: 0.9,
    fontSize:20,
    fontFamily:'KGPrimaryWhimsy'
  },
  loginFormTextInput: {
    height: 48,
    fontSize: 14,
    borderRadius: 5,
    borderWidth: 1,
    borderColor: '#eaeaea',
    backgroundColor: '#fafafa',
    paddingLeft: 10,
    marginLeft: 15,
    marginRight: 15,
    marginTop: 5,
    marginBottom: 5,
  
  },
});

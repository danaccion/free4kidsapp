import React, { Component } from 'react';
import { ScrollView, StyleSheet,Text,View,ImageBackground,TouchableOpacity,Platform,Dimensions,Image,AsyncStorage} from 'react-native';
import Ionicons from 'react-native-vector-icons/Ionicons'
import MapView from 'react-native-maps';
import bootsMarker from '../../../../assets/images/slice9.png'
import BackButton from '../../../BackButton'
import ProgressiveImage from '../../ProgressiveImage'
import HamburgerIcon from '../../../icons/HamburgerIcon';
import HeartBagBurger from '../../../icons/HeartBagBurger';
import Gestures from 'react-native-easy-gestures';
import * as Location from 'expo-location';
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';
import { PH,SS } from 'react-native-dotenv'
const Fsize3 = Platform.OS === 'ios' ?25 : 20;
export default class Clocations extends Component {
  static navigationOptions = () => {
    return {
        headerLeft: <BackButton/>,
        headerTitle: (
            <Image
            resizeMode="contain"
            source={require('../../../../assets/images/slice2.png')}
            style={{ width: 10, height:10 , flex:4,justifyContent: 'space-around', alignItems: 'center', padding: 25,}}
          />
          ),
        headerRight: <HeartBagBurger/>
    };
};

   state = {
      username: '',
      password: '',
      allData: null,
      singleCoord:null,
      currentLocation:null,
      ButtonStateHolder : true,
      loading: false,
      fontLoaded:false,
      preferences:'',
      kilometer:0,
      mapRegion: {
        latitude: 55.676098,
        longitude: 12.568337,
        latitudeDelta: 0.0922,
        longitudeDelta: 0.0421,
        },
      markers: [],
      userLocation:null

    };
 
  componentDidMount() {
    this._getLocationAsync()
    const { navigation } = this.props;
  const item = navigation.getParam('item');
  console.log("item ni",item)

  

  if(item!==undefined){
        this.setState({singleCoord:[{images:item.images,name:item.name,description:item.description,product_lat:parseFloat(item.product_lat),product_lng:parseFloat(item.product_lng),latitudeDelta:0.0922,
          longitudeDelta:0.0421,}],mapRegion:{latitude:parseFloat(item.product_lat),longitude:parseFloat(item.product_lng),longitudeDelta:0.05,latitudeDelta:0.05}})
  }else{
    this.getAllData()
  }
  
}

_getLocationAsync = async () => {
  let location = await Location.getCurrentPositionAsync({});
    let coodinates={ latitude: parseFloat(location.coords.latitude),
      longitude: parseFloat(location.coords.longitude),latitudeDelta: 0.0922,
      longitudeDelta: 0.0421,}
  this.setState({ currentLocation:location,mapRegion:coodinates });
};
    back = async ()=>{
    this.props.navigation.navigate('Home');
    }
    submit =()=>{
      //this.props.navigation.navigate('Home');
    }
    _handleMapRegionChange = mapRegion => {
    //  this.setState({ mapRegion });
    };
   
    async getAllData(){
      let token=await AsyncStorage.getItem('token');
    fetch(PH+'/api/v1/users/products', {
      method: 'POST',
      headers: {
        'Accept':'application/json',
        'Content-Type': 'multipart/form-data',
        'Authorization': 'Bearer '+token
      },
      }).then((response) => response.json())
          .then((responseJson) => {
           if(responseJson.code==401){
            fetch(PH+'/auth/logout', {
              method: 'GET',
              headers: {
                'Accept':'application/json',
                'Content-Type': 'application/json',
                'Authorization': 'Bearer '+token
              },
              }).then((response) => response.json())
                  .then((responseJson) => {
                      AsyncStorage.getAllKeys()
                      .then(keys => AsyncStorage.multiRemove(keys))
                      .then(() => console.log('success'));
                      AsyncStorage.setItem('isLoggedIn','0');
                      
                      this.props.navigation.navigate('Auth');
              })
                  .catch((error) => {
                    
                    this.props.navigation.navigate('Auth');
              })
           }
      
            if(responseJson.meta.product.length==0){
              this.props.navigation.navigate('Shop');
            }else{ 
              this.setState({allData:responseJson.meta.product,refresh:false})
            }
              
            
      })
          .catch((error) => {
            // this.setState({
            //   ButtonStateHolder : false,
            //   loading: false
            // })
            // console.log("ayay",error)
          // alert(error); 
      });
    }
    
  render(){
    const resizeMode = 'cover';
    const { navigation } = this.props;
    const item = navigation.getParam('item');
    return (
      <View style={styles.container}>
       
      <ImageBackground
         style={{
           backgroundColor: '#ccc',
           flex: 1,
           resizeMode,
           position: 'absolute',
           width: '100%', 
           height: '100%',
           justifyContent: 'center',
           
         }}
         source={require('../../../../assets/images/slice81-min.png')}
         />
       
       <View style={{position:'absolute',alignSelf:'center',top:10,marginBottom:10,zIndex:200}}>
           <View>
         </View>
         <View>
          <Text  style={{ fontSize: Fsize3 ,alignSelf:'center',color:'#2db9b9',fontFamily:'KGPrimaryWhimsy'}}>Din placering</Text>
          </View>
      </View>   
      <View style={styles.container2}>
      <View style={{flexDirection: 'row',zIndex:200}}>
            
            <View style={{flexDirection: 'column'}}>
        
            <Gestures
                  draggable={{
                    y: false,
                    x:true
                  }}
                  scalable={{
                    min: -192,
                    max: 7,
                  }}
                  rotatable={false}
                  onEnd={(event, styles) => {
                    console.log(((wp('95%')/2)+styles.left)/4.7);
                    this.setState({kilometer:((wp('95%')/2)+styles.left)/4.7})
                  }}
                  onChange={(event,styles)=>{this.setState({kilometer:((wp('95%')/2)+styles.left)/4.7})}}
                  style={{zIndex:400}}
                >
                  <Image  resizeMode="contain" style={{zIndex:400,width:50,alignSelf:'center', top:-50, position: 'absolute'}} source={require('../../../../assets/images/slice84-bil-min_flipped.png')}/>
                </Gestures>  
            <Image  resizeMode="contain" style={{width:wp('100%'),alignSelf:'center',zIndex:10}} source={require('../../../../assets/images/slice82-ny-min.png')}/>  
            <Text style={{ position: 'absolute', fontFamily:'KGPrimaryWhimsy',top:50,left:5, fontSize:18,color:'grey',zIndex:10}}>0 km</Text>
            <Text style={{ position: 'absolute', fontFamily:'KGPrimaryWhimsy',top:50, right:5, fontSize:18,color:'grey',zIndex:10}}>Hele DK</Text>
            <Text style={{ position: 'absolute', fontFamily:'KGPrimaryWhimsy',top:65,alignSelf:'center' ,fontSize:20,color:'grey'}}> {Math.round(this.state.kilometer)} km omkring Vordingborg</Text>
            
              </View>
              {/* <Image  resizeMode="contain" style={{position:'absolute',width:350,height:100,alignSelf:'center',top:228,right:20}} source={require('../../assets/images/slice70-min.png')}/>  
              <Image  resizeMode="contain" style={{position:'absolute',width:350,height:100,alignSelf:'center',top:417,right:20}} source={require('../../assets/images/slice70-min.png')}/> 
              <Image  resizeMode="contain" style={{position:'absolute',width:60,height:60,alignSelf:'center',top:430,right:168,zIndex:100}} source={require('../../assets/images/slice78-min.png')}/>  */}
              </View> 
         
       
         <View style={styles.container3}>
         <MapView
          ref={(ref) => { this.mapRef = ref }}
          style={{height:hp('100%')}}
          region={this.state.mapRegion}
          onRegionChange={this._handleMapRegionChange}
          // onUserLocationChange={{coordinate:this.state.mapRegion}}
        >
          {this.state.allData==null ? null : this.state.allData.map((marker, index) => {
            const coords = {
                latitude: parseFloat(marker.product_lat),
                longitude: parseFloat(marker.product_lng),
            };

            const metadata = `Status: ${marker.statusValue}`;

            return (
                <MapView.Marker
                    key={index}
                    coordinate={coords}
                    title={marker.name}
                    description={marker.description}
                   
                >
                   <View style={{flexDirection:'row'}}>
                  <Image
                    resizeMode="contain"
                    source={require('../../../../assets/images/slice14-inverted.png')}
                    style={{ width: 70, height:70 , flex:4,justifyContent: 'center', alignItems: 'center', padding: 25,top:0,right:1}}
                  />
                      <ProgressiveImage
                      source={{  uri: marker.images.length>0?marker.images[0]:null }}
                      thumbnailSource={require('../../../../assets/images/slice3.png')}
                      style={{left:-5,height:30,width:30,top:10}}
                      resizeMode="cover"
                    />

                  <Image
                    resizeMode="contain"
                    source={require('../../../../assets/images/slice14-inverted2.png')}
                    style={{ left:-14, width: 70, height:70 , flex:4,justifyContent: 'center', alignItems: 'center', padding: 25,top:0}}
                  />
              </View>
              </MapView.Marker>
            );
  })}

{this.state.singleCoord==null ? null : this.state.singleCoord.map((marker, index) => {
            const coords = {
                latitude: parseFloat(marker.product_lat),
                longitude: parseFloat(marker.product_lng),
            };

            const metadata = `Status: ${marker.statusValue}`;

            return (
                <MapView.Marker
                    key={index}
                    coordinate={coords}
                    title={marker.name}
                    description={marker.description}
                   
                >  
              <View style={{flexDirection:'row'}}>
                <Image
                  resizeMode="contain"
                  source={require('../../../../assets/images/slice14-inverted.png')}
                  style={{ width: 70, height:70 , flex:4,justifyContent: 'center', alignItems: 'center', padding: 25,top:40}}
                />
                    <ProgressiveImage
                    source={{  uri: marker.images.length>0?marker.images[0]:null }}
                    thumbnailSource={require('../../../../assets/images/slice3.png')}
                    style={{height:70,width:70,top:10}}
                    resizeMode="cover"
                  />
            </View>
              </MapView.Marker>
            );
  })}
        </MapView>
       
          </View>
      </View>
            
  </View>
   
    );
  }
  }
  

const styles = StyleSheet.create({
  container: {
    flex: 1,
    //alignItems: 'center',
    // justifyContent: 'flex-end',
    backgroundColor: 'transparent'
  },
  container2: {
    
  },
  textstyle:{
    fontFamily:'KGPrimaryWhimsy',
    fontSize:40
  },
  container3: {
    height:hp('100%'),
    width:wp('100%'),
    position:'absolute'
  },
  dropdown: {
    height: Dimensions.get('screen').width /7,
    padding: 5,
    position:'relative',
    margin:5,
    borderWidth: .2,
    borderRadius: 12,
    textShadowColor: 'black',
    borderColor: 'black',
    marginBottom: 10,
    backgroundColor: 'white',
    opacity: 0.9,
    fontFamily:'KGPrimaryWhimsy',
    fontSize:20,
    alignContent:'center'
  },
  wrpper: {
    opacity: 0.9,
    fontSize:20,
    fontFamily:'KGPrimaryWhimsy',
    flexDirection: 'row', justifyContent: 'space-around',   
  },
  button: {
    backgroundColor: '#54969c',
    borderColor: 'white',
    borderWidth: .5,
    borderRadius: 12,
    color: 'white',
    overflow: 'hidden',
    padding: 12,
    textAlign:'center',
    width: 300,
    height: 48,
    opacity: 0.9,
    fontSize:20,
    fontFamily:'KGPrimaryWhimsy'
  },
});

import React, { Component } from 'react';
import { ScrollView, StyleSheet,Text,View,ImageBackground,TouchableOpacity,Platform,Dimensions,Image,TextInput,Keyboard,TouchableWithoutFeedback} from 'react-native';
import { Container, Header, Content, Form, Item, Picker,Icon,Input  } from 'native-base';
import Ionicons from 'react-native-vector-icons/Ionicons'
import BackButton from '../../../BackButton'
import HamburgerIcon from '../../../icons/HamburgerIcon';
import HeartBagBurger from '../../../icons/HeartBagBurger';
import RNPickerSelect from 'react-native-picker-select';
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';
import { PH,SS } from 'react-native-dotenv'
const Fsize3 = Platform.OS === 'ios' ?25 : 20;
export default class SaveBirth extends Component {
  static navigationOptions = () => {
    return {
      headerLeft: <BackButton/>,
        headerTitle: (
            <Image
            resizeMode="contain"
            source={require('../../../../assets/images/slice2.png')}
            style={{ width: 10, height:10 , flex:4,justifyContent: 'space-around', alignItems: 'center', padding: 25,}}
          />
          ),
        headerRight: <HeartBagBurger/>
    };
};
  
 state = {
      username: '',
      password: '',
      ButtonStateHolder : true,
      loading: false,
      fontLoaded:false,
      selected2:undefined,
      preferences:'',
      drop1:undefined,
        drop1items: [
          {
              label: '1 sæk',
              value: '1sack',
          },
          {
              label: '2 sække',
              value: '2sacks',
          },
      ],
      drop2:undefined,
      drop2items: [
          {
              label: 'Some Value',
              value: 'some value',
          },
          {
              label: 'Some Value',
              value: 'some value',
          },
      ],
    };
  componentDidMount() {
   
  }
    back = async ()=>{
    this.props.navigation.navigate('Home');
    }
    submit =()=>{
      //this.props.navigation.navigate('Home');
    }
    _handleMapRegionChange = mapRegion => {
     // this.setState({ mapRegion });
    };
    onValueChange2(value) {
      this.setState({
        selected2: value
      });
    }
    hvadSelect(value) {
      this.setState({
        Hvad: value
      });
    }
  render(){
   
    const resizeMode = 'cover';
    return (
      <View style={styles.container}>
       
      <ImageBackground
         style={{
           backgroundColor: '#ccc',
           flex: 1,
           resizeMode,
           position: 'absolute',
           width: '100%', 
           height: '100%',
           justifyContent: 'center',
           
         }}
         source={require('../../../../assets/images/slice81-min.png')}
         />
       
       <View style={{flex: 1, flexDirection: 'row',justifyContent:'center', margin:15,marginTop:Dimensions.get('screen').height/50}}>
       
         <View>
          <Text  style={{ fontSize: Fsize3 ,alignSelf:'center',color:'#2db9b9',fontFamily:'KGPrimaryWhimsy'}}> Book et  besøg</Text>
          </View>
         
      </View>
     
      <TouchableWithoutFeedback onPress={()=>Keyboard.dismiss()}>
       <View style={{ position:'absolute',
    backgroundColor: 'transparent',
    flex: 1,
    height:hp('79%'),
    width:wp('100%'),
    bottom:0,
    top:50}}>
       
      <View style={{ flexDirection: 'row'}}>
            
            <View style={{flex: 1, flexDirection: 'column'}}>
            <Image  resizeMode="contain" style={{width:50,alignSelf:'center', top:-Dimensions.get('screen').height/2.2, position: 'absolute',}} source={require('../../../../assets/images/slice13.png')}/>  
            <Image  resizeMode="contain" style={{width:400,alignSelf:'center'}} source={require('../../../../assets/images/slice82-ny-min.png')}/>  
            <Text style={{ position: 'absolute', fontFamily:'KGPrimaryWhimsy',top:Dimensions.get('screen').height/13, fontSize:18}}>0 km</Text>
            <Text style={{ position: 'absolute', fontFamily:'KGPrimaryWhimsy',top:Dimensions.get('screen').height/13, right:0, fontSize:18}}>Hele DK</Text>
            <Text style={{ position: 'absolute', fontFamily:'KGPrimaryWhimsy',top:Dimensions.get('screen').height/10, left:85 ,fontSize:20}}> 80 km omkring Vordingborg</Text>
            
              </View>
              {/* <Image  resizeMode="contain" style={{position:'absolute',width:350,height:100,alignSelf:'center',top:228,right:20}} source={require('../../assets/images/slice70-min.png')}/>  
              <Image  resizeMode="contain" style={{position:'absolute',width:350,height:100,alignSelf:'center',top:417,right:20}} source={require('../../assets/images/slice70-min.png')}/> 
              <Image  resizeMode="contain" style={{position:'absolute',width:60,height:60,alignSelf:'center',top:430,right:168,zIndex:100}} source={require('../../assets/images/slice78-min.png')}/>  */}
      </View> 
   
      <Container style={{backgroundColor:'transparent'}} >
        <Content>
          <Form>
          <Item rounded regular style={{backgroundColor:'white',marginBottom:10,borderColor:'grey',border:.5,borderRadius:8,width:wp('95%'),alignSelf:'center',alignContent:'center'}}>
             <Input onChangeText={(productBrand) => this.setState({ productBrand })} placeholder='Dit navn på postkassen?'/>
           </Item>
            
           <Item rounded regular style={{backgroundColor:'white',marginBottom:10,borderColor:'grey',border:.5,borderRadius:8,width:wp('95%'),alignSelf:'center',alignContent:'center'}}>
             <Input onChangeText={(productBrand) => this.setState({ productBrand })} placeholder='Hvor skal vi hente det? (adresse)'/>
           </Item>
            
           <Item rounded regular style={{backgroundColor:'white',marginBottom:10,borderColor:'grey',border:.5,borderRadius:8,width:wp('95%'),alignSelf:'center',alignContent:'center'}}>
             <Input onChangeText={(productBrand) => this.setState({ productBrand })} placeholder='Mobil nummer'/>
           </Item>
           <Item rounded regular style={{backgroundColor:'white',marginBottom:10,borderColor:'grey',border:.5,borderRadius:8,width:wp('95%'),alignSelf:'center',alignContent:'center'}}>
             <Input onChangeText={(productBrand) => this.setState({ productBrand })} placeholder='Din e-mail'/>
           </Item>
            <Item picker regular style={{backgroundColor:'white',marginBottom:10,borderColor:'grey',border:.5,borderRadius:8,width:wp('95%'),alignSelf:'center',alignContent:'center'}}>
              <Picker
                mode="dropdown"
                iosIcon={<Icon name="arrow-down" style={{color:'grey'}} />}
                headerBackButtonText="tilbage"
                iosHeader="Vælg En"
                placeholder="Hvor mange børneting skal vi hente?"
                style={{width:wp('95%')}}
                placeholderStyle={{ color: "#4f4e4d" }}
                placeholderIconColor="#4f4e4d"
                selectedValue={this.state.Hvad}
                onValueChange={this.hvadSelect.bind(this)}
              >
                <Picker.Item label="Pige" value="Pige" />
                <Picker.Item label="Dreng" value="Dreng" />
                <Picker.Item label="Unisex" value="Unisex" />
              </Picker>
            </Item>
            <Item picker regular style={{backgroundColor:'white',marginBottom:10,borderColor:'grey',border:.5,borderRadius:8,width:wp('95%'),alignSelf:'center',alignContent:'center'}}>
              <Picker
                mode="dropdown"
                iosIcon={<Icon name="arrow-down" style={{color:'grey'}} />}
                headerBackButtonText="tilbage"
                iosHeader="Vælg En"
                placeholder="Hvor sætter du de thenne?"
                style={{width:wp('95%')}}
                placeholderStyle={{ color: "#4f4e4d" }}
                placeholderIconColor="#4f4e4d"
                selectedValue={this.state.Hvad}
                onValueChange={this.hvadSelect.bind(this)}
              >
                <Picker.Item label="Sender gerne" value="1" />
                <Picker.Item label="Kan afhentes" value="2" />
                <Picker.Item label="Tilbyder begge dele" value="3"/>
              </Picker>
            </Item>
           
            <View style={{alignContent:'center',alignSelf:'center'}}>
            <TouchableOpacity onPress={this.submit.bind(this)}>
              <Text style={styles.button}>Bestil afhentning</Text>
              </TouchableOpacity>
          
            </View>
             
          </Form>
        </Content>
        
      
      </Container>
     
      </View>
{/*          
    <View style={styles.container3}>
     <TouchableOpacity onPress={this.submit.bind(this)}>
     <Text style={styles.button}>Bestil afhentning</Text>
     </TouchableOpacity>
     </View> */}

   {/* </View> */}
   </TouchableWithoutFeedback>  
  </View>
   
    );
  }
  }
  
  const pickerSelectStyles = StyleSheet.create({
    inputIOS: {
        fontSize: 20,
        paddingTop: 13,
        paddingHorizontal: 10,
        paddingBottom: 12,
        borderWidth: 1,
        borderColor: 'transparent',
        borderRadius: 4,
        backgroundColor: 'white',
        color: 'black',
        borderWidth: .5,
        borderRadius: 12,

    },
  });
const styles = StyleSheet.create({
  container: {
    flex: 1,
    //alignItems: 'center',
     
    justifyContent: 'flex-end',
    backgroundColor: 'transparent'
  },
  input: {
    height: Dimensions.get('screen').height /18,
    padding: 10,
    borderWidth: .5,
    borderRadius: 12,
    borderColor: 'black',
    marginBottom: 10,
    marginLeft:10,
    marginRight:10,
    backgroundColor: 'white',
    fontFamily:'KGPrimaryWhimsy',
    opacity: 0.9,
    fontSize:20
  },
  container4: {
    flex: 1,
    //alignItems: 'center',
    justifyContent: 'center',
    //backgroundColor: 'transparent'
    bottom: Dimensions.get('screen').height /5
  },
  container2: {
    flex: 1,
    //alignItems: 'center',
    justifyContent: 'center',
    //backgroundColor: 'transparent'
    bottom: Dimensions.get('screen').height /5
  },
  textstyle:{
    fontFamily:'KGPrimaryWhimsy',
    fontSize:40
  },
  container3: {
    flex: 5,
    justifyContent: "flex-end",
    alignItems: "center"
  },
  dropdown: {
    height: Dimensions.get('screen').height /18,
    padding: 5,
    position:'relative',
    margin:10,
    borderWidth: .2,
    borderRadius: 12,
    textShadowColor: 'black',
    borderColor: 'black',
    marginBottom: 10,
    backgroundColor: 'white',
    opacity: 0.9,
    fontFamily:'KGPrimaryWhimsy',
    fontSize:20,
    alignContent:'center'
  },
  wrpper: {
    opacity: 0.9,
    fontSize:20,
    fontFamily:'KGPrimaryWhimsy',
    flexDirection: 'row', justifyContent: 'space-around',   
  },
  button: {
    backgroundColor: '#54969c',
    borderColor: 'white',
    borderWidth: .5,
    borderRadius: 12,
    color: 'white',
    overflow: 'hidden',
    padding: 12,
    textAlign:'center',
    width: wp('95%'),
    height: 48,
    opacity: 0.9,
    fontSize:25,
    fontFamily:'KGPrimaryWhimsy'
  },
  loginFormTextInput: {
    height: 48,
    fontSize: 14,
    borderRadius: .5,
    borderWidth: 1,
    borderColor: '#eaeaea',
    backgroundColor: '#fafafa',
    paddingLeft: 10,
    marginLeft: 15,
    marginRight: 15,
    marginTop: 5,
    marginBottom: 5,
    
  },
});

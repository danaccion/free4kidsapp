import React, { Component } from 'react';
import { ScrollView, StyleSheet,View,ImageBackground,Image,TouchableOpacity,Platform,Dimensions,Text,AsyncStorage} from 'react-native';
import Ionicons from 'react-native-vector-icons/Ionicons'
import { Picker } from 'react-native-picker-dropdown';
import * as Permissions from 'expo-permissions'
import Constants from 'expo-constants'
import * as Location from 'expo-location';
import BackButton from '../../../BackButton';
import HamburgerIcon from '../../../icons/HamburgerIcon';
import HeartBagBurger from '../../../icons/HeartBagBurger';
import LottieView from 'lottie-react-native';
import ProgressiveImage from '../../ProgressiveImage';
import { Container, Content, List, ListItem, Thumbnail,Button,InputGroup,Input,Body,Right,Badge  } from 'native-base';
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';
import { Snackbar } from 'react-native-paper';
import Dialog, {SlideAnimation,DialogContent } from 'react-native-popup-dialog';
import { PH,SS } from 'react-native-dotenv'
const Fsize3 = Platform.OS === 'ios' ?25 : 20;
import {
  Block,Icon
} from 'galio-framework';
export default class Donate extends Component {
  static navigationOptions = () => {
    return {
        headerLeft: <HamburgerIcon/>,
        headerTitle: (
            <Image
            resizeMode="contain"
            source={require('../../../../assets/images/slice2.png')}
            style={{ width: 10, height:10 , flex:4,justifyContent: 'space-around', alignItems: 'center', padding: 25,}}
          />
          ),
        headerRight: <HeartBagBurger/>
    };
};

 
    state = {
      username: '',
      password: '',
      ButtonStateHolder : true,
      loading: false,
      fontLoaded:false,
      preferences:'',
      deleteItem:false,
      saveData:null,
      noData:false,
      color:'white',
      visibleSnack:false,
      hasLocationPermission:null,
      currentLocation:null,
      showDialogMessage:false,
      dialog_message:''
    };
    baseState = this.state 
 

  async componentDidMount(){
    this.didBlurSubscription = this.props.navigation.addListener(
    'didBlur',
    payload => {
      // this.getSavedProducts()
    }  
  );
  await this.locationPermission()
  
}
    componentWillUnmount() {
      // Remove the event listener
      this.didBlurSubscription.remove();
    }

    distance(lat1,lon1,lat2,lon2) {
      var R = 6371; // km (change this constant to get miles)
      var dLat = (lat2-lat1) * Math.PI / 180;
      var dLon = (lon2-lon1) * Math.PI / 180;
      var a = Math.sin(dLat/2) * Math.sin(dLat/2) +
        Math.cos(lat1 * Math.PI / 180 ) * Math.cos(lat2 * Math.PI / 180 ) *
        Math.sin(dLon/2) * Math.sin(dLon/2);
      var c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1-a));
      var d = R * c;
      if (d>1) return Math.round(d)+"km";
      else if (d<=1) return Math.round(d*1000)+"m";
      return d;
    }
    locationPermission =  () =>{
      this.getPermissionAsync();
      this.getSavedProducts()
    }

    _getLocationAsync = async () => {
      let location = await Location.getCurrentPositionAsync({});
      this.setState({ currentLocation:location });
    };

    getPermissionAsync = async () => {
      if (Constants.platform.ios) {
        const { status } = await Permissions.askAsync(Permissions.LOCATION);
        // let loca = await Location.getCurrentPositionAsync();
        
        if (status !== 'granted') {
            this.setState({ showDialogMessage:true, dialog_message:'Sorry, we need Location permissions to serve you better!' })
          //alert('Sorry, we need Location permissions to serve you better!');
          const { status } = await Permissions.askAsync(Permissions.LOCATION);
        }else{
          this._getLocationAsync();
          this.setState({ hasLocationPermission: status === 'granted' })
        }
      }else{
        const { status } = await Permissions.askAsync(Permissions.LOCATION);
        this.setState({ hasLocationPermission: status === 'granted' })
        if (status !== 'granted') {
          //alert('Sorry, we need Location permissions to serve you better!');
          this.setState({ showDialogMessage:true, dialog_message:'Sorry, we need Location permissions to serve you better!' })
        }else{
        this._getLocationAsync();
        this.setState({ hasLocationPermission: status === 'granted' })
        
      }
     
      
      }
     
    }
  
    async getSavedProducts() {
        fetch(PH+'/api/v1/users/saved_products', {
          method: 'POST',
          headers: {
            'Accept':'application/json',
            'Content-Type': 'multipart/form-data',
            'Authorization': 'Bearer '+await AsyncStorage.getItem('token')
          },
          }).then((response) => response.json())
              .then((responseJson) => {
                  if(responseJson.code==404){
                    this.setState({noData:true,refresh:false})
                  }else if(responseJson.meta.product.length>0){
                    this.setState({saveData:responseJson.meta.product,refresh:false})
                  }else{ 
                    this.setState({refresh:false})
                  }
                  
                
          })
              .catch((error) => {
                console.log("ayay",error)  
          });
      };    

      goToProduct = async (prod)=>{
        if(this.state.deleteItem==true){
          this.setState({deleteItem:false,visibleSnack:true,color:'white'})
          let data = new FormData();
          data.append('product_id', prod.id);
          fetch(PH+'/api/v1/saved_products/delete', {
            method: 'POST',
            headers: {
              'Accept':'application/json',
              'Content-Type': 'multipart/form-data',
              'Authorization': 'Bearer '+await AsyncStorage.getItem('token')
            },
            body:data
            }).then((response) => response.json())
                .then((responseJson) => {
                  this.setState({visibleSnack:true})
                  this.getSavedProducts()
            })
                .catch((error) => {
                 console.log("ayay",error)
            });
            this.setState({refresh:false})
        }else{
          this.props.navigation.navigate('ProductDetails', { 'product':prod,'location': this.distance(prod.product_lat,prod.product_lng,this.state.currentLocation.coords.latitude,this.state.currentLocation.coords.longitude),'page':'saved'})
        }
       
        }
    back = async ()=>{
    this.props.navigation.navigate('Home');
    }
    submit =()=>{
      //this.props.navigation.navigate('Home');
    }

   
  render(){
    const resizeMode = 'cover';
    return (
  
      <View style={styles.container}>
       
      <ImageBackground
         style={{
           backgroundColor: '#ccc',
           flex: 1,
           resizeMode,
           position: 'absolute',
           width: '120%', 
           height: '100%',
           justifyContent: 'center',
           
         }}
         source={require('../../../../assets/images/slice81-min.png')}
         />
       
       <View style={{flex: 1, flexDirection: 'row',justifyContent:'space-between', margin:15,marginTop:Dimensions.get('screen').height/50}}>
       <Snackbar
          visible={this.state.visibleSnack}
          onDismiss={() => this.setState({ visibleSnack: false })}
          // action={{
          //   label: 'Undo',
          //   onPress: () => {
          //     // Do something
          //   },
          // }}
          style={{backgroundColor:'#5e5e5e'}}
          duration={2000}
        >
        Elementet blev fjernet!
        </Snackbar>
          <BackButton/>
         <View>
          <Text  style={{ fontSize: Fsize3 ,alignSelf:'center',color:'#2db9b9',fontFamily:'KGPrimaryWhimsy'}}> Gemte børneting</Text>
          </View>
          <View>
            {this.state.deleteItem==false?(
                <TouchableOpacity onPress={()=>this.setState({deleteItem:true,color:'#ad0034'})} loading={true}>
                <Image resizeMode="contain" source={require('../../../../assets/images/slice50-min.png')} style={{width: 20,height: 20,}}></Image>
                
             </TouchableOpacity>
            ): <TouchableOpacity onPress={()=>this.setState({deleteItem:false,color:'white'})} loading={true}>
              <Text style={{fontSize: 13,color:'#2db9b9'}}>Færdig</Text>
            
         </TouchableOpacity>}
          
         </View>
      </View>
      {this.state.saveData==null&&this.state.noData===false?(
        <View style={styles.container3}>
        <Container style={{backgroundColor:'transparent'}} >
        <Content>
        <LottieView source={require('../../../../assets/images/loading2.json')} autoPlay loop style={{width:200,height:200,alignSelf:'center'}} /> 
          </Content>
         </Container>
       
        </View>
      ):
     null
  }
   {this.state.noData==true&&this.state.saveData===null?(
      <View style={styles.container3}>
      <Container style={{backgroundColor:'transparent'}} >
      <Content>
      <Text style={{alignSelf:'center',fontSize:20,fontFamily:'KGPrimaryWhimsy',color:'grey',marginTop:40}}>Der er endnu ingen gemte produkter</Text>
        </Content>
       </Container>
     
      </View>
   ):null}

    {this.state.saveData!==null&&this.state.noData===false?(
      <View style={styles.container3}>
      <Container style={{backgroundColor:'transparent'}} >
      <Content>
      {this.state.saveData.map((prop, key) => {
         return (
           
              <List style={{backgroundColor:this.state.color,borderColor:'black',borderWidth:.5,borderRadius: 15,marginBottom:7,margin:5}} key={prop.id}>
                  <ListItem button onPress={()=>this.goToProduct(prop)}>
                   <Body style={{flexDirection:'row',alignContent:'center',alignSelf:'center',alignItems:'center',marginRight:20}}>
                   <ProgressiveImage source={{ uri:prop.products[0].images[0]}} style={{marginRight:20,height:50,width:50,borderRadius: 30/ 2}} />
                    {/* <Thumbnail small square source={{ uri: prop.products[0].images.length>0?prop.products[0].images[0]:null}} style={{marginRight:30}}  /> */}
                 <Text adjustsFontSizeToFit={true} style={{fontFamily:'KGPrimaryWhimsy',fontSize:25,color:'#636363'}}>{prop.products[0].name} fra <Text style={{fontFamily:'KGPrimaryWhimsy',fontSize:25,color:'#d47d7d'}}>FAMILIEN </Text><Text style={{fontFamily:'KGPrimaryWhimsy',fontSize:25,color:'#54969c',marginRight:10}}>{prop.users.name}</Text></Text>
                    </Body> 
                    <Right>
                      <Block>
                        <Icon 
                        name="chevron-right"
                        family="feather"
                        size={20}
                        color={'#3d3d3d'}
                    
                        />
                    </Block>
                    </Right>
                     
                  </ListItem>
                  {/* <Icon ios='ios-arrow-forward' android='md-arrow-dropright' iconRight/> */}
             </List>           
         );
         
      })}
        </Content>
       </Container>
     
      </View>       
    ):null}

    <Dialog
        visible={this.state.showDialogMessage}
        onTouchOutside={() => { this.setState({ showDialogMessage: false }); }}
        onHardwareBackPress={() => {this.setState({ showDialogMessage: false });}}
        dialogStyle={{backgroundColor:'white',width:wp('90%'),alignItems:'center',alignContent:'center'}}>
        <DialogContent>
            <View styles={{alignItems:'center', }}>
                <Text  style={{fontSize:28, color:'#eca6a1', padding: 12, textAlign:'center', fontFamily:'KGPrimaryWhimsy',}}>{this.state.dialog_message}</Text>
            </View>
        </DialogContent>
    </Dialog>
      
  </View>
   
    );
  }
  }
   

const styles = StyleSheet.create({
  container: {
    flex: 1,
    //alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: 'transparent'
  },
  container2: {
    flex: 1,
    //alignItems: 'center',
    //justifyContent: 'flex-start',
    
    backgroundColor: 'transparent'
    //bottom: Dimensions.get('screen').height /5
  },
  textstyle:{
    fontFamily:'KGPrimaryWhimsy',
    fontSize:40
  },
  container3: {
    position:'absolute',
    backgroundColor: 'transparent',
    flex: 1,
    height:hp('75%'),
    width:wp('100%'),
    bottom:0,
    //justifyContent: "flex-end",
    //alignItems: "center"
  },
  dropdown: {
    height: Dimensions.get('screen').width /7,
    padding: 5,
    position:'relative',
    margin:5,
    borderWidth: .2,
    borderRadius: 12,
    textShadowColor: 'black',
    borderColor: 'black',
    marginBottom: 10,
    backgroundColor: 'white',
    opacity: 0.9,
    fontFamily:'KGPrimaryWhimsy',
    fontSize:20,
    alignContent:'center'
  },
  wrpper: {
    opacity: 0.9,
    fontSize:20,
    fontFamily:'KGPrimaryWhimsy',
    flexDirection: 'row', justifyContent: 'space-around',   
  },
  button: {
    backgroundColor: '#54969c',
    borderColor: 'white',
    borderWidth: .5,
    borderRadius: 12,
    color: 'white',
    overflow: 'hidden',
    padding: 12,
    textAlign:'center',
    width: 300,
    height: 48,
    opacity: 0.9,
    fontSize:20,
    fontFamily:'KGPrimaryWhimsy'
  },
});

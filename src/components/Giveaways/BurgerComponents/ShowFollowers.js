import React, { Component } from 'react';
import { ScrollView, StyleSheet,View,ImageBackground,Image,TouchableOpacity,Platform,Dimensions,Text,AsyncStorage} from 'react-native';
import Ionicons from 'react-native-vector-icons/Ionicons'
import { Picker } from 'react-native-picker-dropdown';
import BackButton from '../../../BackButton';
import HamburgerIcon from '../../../icons/HamburgerIcon';
import HeartBagBurger from '../../../icons/HeartBagBurger';
import LottieView from 'lottie-react-native';
import ProgressiveImage from '../../ProgressiveImage';
import { Container, Content, List, ListItem, Thumbnail,Icon,Button,InputGroup,Input,Body,Right,Left } from 'native-base';
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';
import { PH,SS } from 'react-native-dotenv'
const Fsize3 = Platform.OS === 'ios' ?25 : 20;
export default class ShowFollowers extends Component {
  static navigationOptions = () => {
    return {
        headerLeft: <HamburgerIcon/>,
        headerTitle: (
            <Image
            resizeMode="contain"
            source={require('../../../../assets/images/slice2.png')}
            style={{ width: 10, height:10 , flex:4,justifyContent: 'space-around', alignItems: 'center', padding: 25,}}
          />
          ),
        headerRight: <HeartBagBurger/>
    };
};

 
 state = {
      username: '',
      password: '',
      ButtonStateHolder : true,
      loading: false,
      fontLoaded:false,
      preferences:'',
      color:'white',
      deleteItem:false,
      followData:null,
      noData:false

    };
    // this.baseState = this.state 
 

  async componentDidMount(){
  //   this.didBlurSubscription = this.props.navigation.addListener(
  //   'didBlur',
  //   payload => {
    
  //   }
    
  // );
  this.getYouFollow()
    }
    componentWillUnmount() {
      // Remove the event listener
      // this.didBlurSubscription.remove();
    }

    async getSavedProducts() {
        fetch(PH+'/api/v1/users/reservation', {
          method: 'POST',
          headers: {
            'Accept':'application/json',
            'Content-Type': 'multipart/form-data',
            'Authorization': 'Bearer '+await AsyncStorage.getItem('token')
          },
          }).then((response) => response.json())
              .then((responseJson) => {
                  if(responseJson.code==404){
                    this.setState({noData:true})
                  }else if(responseJson.meta.products.length>0){
                    this.setState({followData:responseJson.meta.products,refresh:false})
                  }else{ 
                    this.setState({refresh:false})
                  }
                  
                
          })
              .catch((error) => {
               console.log("ayay",error)
          });
      }; 

      goToFollow = async (id)=>{
        if(this.state.deleteItem==true){
          this.setState({deleteItem:false})
          fetch(PH+'/api/v1/follow/delete?id='+id, {
            method: 'POST',
            headers: {
              'Accept':'application/json',
              'Content-Type': 'multipart/form-data',
              'Authorization': 'Bearer '+await AsyncStorage.getItem('token')
            },
            }).then((response) => response.json())
                .then((responseJson) => {
                  this.getYouFollow()
            })
                .catch((error) => {
                   console.log("ayay",error)
            });
            this.setState({refresh:false})
        }else{
          // this.props.navigation.navigate('UserProfile',{'user':Array.isArray(item.users)===true?item.users[0]:item.users})
        }
       
        }
      
      async getYouFollow() {
          fetch(PH+'/api/v1/users/you_follow', {
            method: 'POST',
            headers: {
              'Accept':'application/json',
              'Content-Type': 'multipart/form-data',
              'Authorization': 'Bearer '+await AsyncStorage.getItem('token')
            },
            }).then((response) => response.json())
                .then((responseJson) => {
                    if(responseJson.code==404){
                      this.setState({noData:true})
                    }else if(responseJson.meta.follow.length>0){
                      this.setState({followData:responseJson.meta.follow,refresh:false})
                    }else{ 
                      this.setState({refresh:false,noData:true})
                    }
                    
                  
            })
                .catch((error) => {
                  console.log("ayay",error)
            });
        };  
    back = async ()=>{
    this.props.navigation.navigate('Home');
    }
    submit =()=>{
      //this.props.navigation.navigate('Home');
    }

  render(){
    const resizeMode = 'cover';
    return (
  
      <View style={styles.container}>
       
      <ImageBackground
         style={{
           backgroundColor: '#ccc',
           flex: 1,
           resizeMode,
           position: 'absolute',
           width: '120%', 
           height: '100%',
           justifyContent: 'center',
           
         }}
         source={require('../../../../assets/images/slice81-min.png')}
         />
       
       <View style={{flex: 1, flexDirection: 'row',justifyContent:'space-between', margin:15,marginTop:Dimensions.get('screen').height/50}}>
         <BackButton/>
         <View>
          <Text  style={{ fontSize: Fsize3 ,alignSelf:'center',color:'#2db9b9',fontFamily:'KGPrimaryWhimsy'}}> Dem du følger </Text>
          </View>
          <View>
            {this.state.deleteItem==false?(
                <TouchableOpacity onPress={()=>this.setState({deleteItem:true,color:'#ad0034'})} loading={true}>
                <Image resizeMode="contain" source={require('../../../../assets/images/slice50-min.png')} style={{width: 20,height: 20,}}></Image>
                
             </TouchableOpacity>
            ): <TouchableOpacity onPress={()=>this.setState({deleteItem:false,color:'white'})} loading={true}>
              <Text style={{fontSize: 13,color:'#2db9b9'}}>Færdig</Text>
            
         </TouchableOpacity>}
          
         </View>
      </View>
      {this.state.followData==null&&this.state.noData===false?(
        <View style={styles.container3}>
        <Container style={{backgroundColor:'transparent'}} >
        <Content>
        <LottieView source={require('../../../../assets/images/loading2.json')} autoPlay loop style={{width:200,height:200,alignSelf:'center'}} /> 
          </Content>
         </Container>
       
        </View>
      ):null}
  {this.state.followData==null&&this.state.noData===true?(
  <View style={styles.container3}>
  <Container style={{backgroundColor:'transparent'}} >
  <Content scrollEnabled={false}>
  <Text style={{alignSelf:'center',fontSize:20,fontFamily:'KGPrimaryWhimsy',color:'grey',marginTop:40}}>Du følger ikke nogen endnu</Text>
    </Content>
    </Container>

  </View>
  ):null}
  {this.state.followData!==null&&this.state.noData===false?(
    <View style={styles.container3}>
    <Container style={{backgroundColor:'transparent'}} >
    <Content>
  {this.state.followData.map((prop, key) => {
     return (
       
          <List  style={{backgroundColor:this.state.color,borderColor:'grey',borderWidth:.5,borderRadius: 15,marginBottom:7,margin:5,shadowColor: '#000',
          shadowOffset: { width: 0, height: 2 },
          shadowOpacity: 0.5,
          shadowRadius: 1.5,
          elevation: 1,paddingBottom:10}} key={prop.id} >
              <ListItem button onPress={()=>this.goToFollow(prop.id)}>
              
             
              <ProgressiveImage source={{ uri: prop.usersid[0].image_url}} style={{marginRight:10,height:50,width:50,borderRadius: 30/ 2}} />
          
               
                 {prop.note!=''?(
                    <Body >
                  <Text style={{fontFamily:'KGPrimaryWhimsy',fontSize: 25,color:'#4d4d4d'}}>Du følger <Text style={{fontFamily:'KGPrimaryWhimsy',fontSize:25,color:'#d47d7d'}}>FAMILIEN </Text><Text  style={{fontFamily:'KGPrimaryWhimsy',fontSize:25,color:'#54969c'}}>{prop.usersid[0].name}</Text></Text>
                  {/* <Text style={{color:'grey'}}>{prop.note}</Text> */}
                  </Body> 
                 ):<Body><Text style={{fontFamily:'KGPrimaryWhimsy',fontSize: 25,color:'#4d4d4d'}}>{prop.usersid[0].name}</Text></Body> }
                 
                  
                
                <Right style={{alignSelf:'center'}}>
                  <Icon name="arrow-forward" />
                </Right>
                 
              </ListItem>
              {/* <Icon ios='ios-arrow-forward' android='md-arrow-dropright' iconRight/> */}
         </List>           
     );
     
  })}
    </Content>
   </Container>
 
  </View>
  ):null}

            
  </View>
   
    );
  }
  }
   

const styles = StyleSheet.create({
  container: {
    flex: 1,
    //alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: 'transparent'
  },
  container2: {
    flex: 1,
    //alignItems: 'center',
    //justifyContent: 'flex-start',
    
    backgroundColor: 'transparent'
    //bottom: Dimensions.get('screen').height /5
  },
  textstyle:{
    fontFamily:'KGPrimaryWhimsy',
    fontSize:40
  },
  container3: {
    position:'absolute',
    backgroundColor: 'transparent',
    flex: 1,
    height:hp('75%'),
    width:wp('100%'),
    bottom:0,
    //justifyContent: "flex-end",
    //alignItems: "center"
  },
  dropdown: {
    height: Dimensions.get('screen').width /7,
    padding: 5,
    position:'relative',
    margin:5,
    borderWidth: .2,
    borderRadius: 12,
    textShadowColor: 'black',
    borderColor: 'black',
    marginBottom: 10,
    backgroundColor: 'white',
    opacity: 0.9,
    fontFamily:'KGPrimaryWhimsy',
    fontSize:20,
    alignContent:'center'
  },
  wrpper: {
    opacity: 0.9,
    fontSize:20,
    fontFamily:'KGPrimaryWhimsy',
    flexDirection: 'row', justifyContent: 'space-around',   
  },
  button: {
    backgroundColor: '#54969c',
    borderColor: 'white',
    borderWidth: .5,
    borderRadius: 12,
    color: 'white',
    overflow: 'hidden',
    padding: 12,
    textAlign:'center',
    width: 300,
    height: 48,
    opacity: 0.9,
    fontSize:20,
    fontFamily:'KGPrimaryWhimsy'
  },
});

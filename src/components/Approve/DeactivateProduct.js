import React, { Component } from 'react';
import { ScrollView, StyleSheet,View,ImageBackground,Image,TouchableOpacity,Platform,Dimensions,Text,AsyncStorage,Easing } from 'react-native';
import Ionicons from 'react-native-vector-icons/Ionicons'
import { Picker } from 'react-native-picker-dropdown';
import BackButton from '../../BackButton';
import HamburgerIcon from '../../icons/HamburgerIcon';
import HeartBagBurger from '../../icons/HeartBagBurger';
import { Container, Content, List, ListItem, Thumbnail,Icon,Button,InputGroup,Input,Body,Right,Badge } from 'native-base';
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';
import Dialog, {SlideAnimation,DialogContent } from 'react-native-popup-dialog';
import { Rating, AirbnbRating } from 'react-native-ratings';
import { PH,SS } from 'react-native-dotenv'
import { Block } from 'galio-framework';
import { Circle } from 'react-native-animated-spinkit'
export default class DeactivateProduct extends Component {
  static navigationOptions = () => {
    return {
         headerLeft:<BackButton/>,
        headerTitle: (
        
            <Image
            resizeMode="contain"
            source={require('../../../assets/images/slice2.png')}
            style={{ width: 10, height:10 , flex:4,justifyContent: 'space-around', alignItems: 'center', padding: 25,}}
          />
          ),
        headerRight: <HeartBagBurger/>
    };
};

 
  state = {
      username: '',
      password: '',
      ButtonStateHolder : true,
      loading: false,
      fontLoaded:false,
      preferences:'',
      userImage:null,
      userName:null,
      userRate:null,
      popupVisible:false,
      refresh:false

    };
  async componentDidMount() {

    console.log(await AsyncStorage.getItem('image_url'))
    this.setState({
      userImage : await AsyncStorage.getItem('image_url'),userName : await AsyncStorage.getItem('name')
                  })
    }

    deleteProduct=async(id)=>{
      this.props.navigation.pop();
      let data = new FormData();
      data.append('id', id);
      fetch(PH+'/api/v1/products/delete', {
        method: 'POST',
        headers: {
          'Accept':'application/json',
          'Content-Type': 'multipart/form-data',
          'Authorization': 'Bearer '+await AsyncStorage.getItem('token')
        },
        body:data
        }).then((response) => response.json())
            .then((responseJson) => {
              console.log(responseJson)
              // this.setState({successDelete:true,deleteModal:false})
              this.props.navigation.pop();
                // console.log(responseJson.meta.status)
                // if(responseJson.code==404){
                //   this.setState({refresh:false})
                // }else{
                //   console.log(responseJson)
                //   // this.setState({refresh:false})
                // }
                
              
        })
            .catch((error) => {
              // this.setState({
              //   ButtonStateHolder : false,
              //   loading: false
              // })
              // console.log("ayay",error)
            // alert(error); 
        });
        this.setState({refresh:false})
    }


    reactivate = async(id) => {
      this.setState({refresh:true})
      let data = new FormData();
      data.append('id', id);
      fetch(PH+'/api/v1/products/restore', {
        method: 'POST',
        headers: {
          'Accept':'application/json',
          'Content-Type': 'multipart/form-data',
          'Authorization': 'Bearer '+await AsyncStorage.getItem('token')
        },
        body:data
        }).then((response) => response.json())
            .then((responseJson) => {
              console.log(responseJson)
              this.setState({refresh:false})
              
        })
            .catch((error) => {
             
              console.log("ayay",error)
           
        });
        this.props.navigation.pop();
    }

    back = async ()=>{
    this.props.navigation.navigate('Home');
    }
    next =()=>{
      this.props.navigation.navigate('DinKonto');
    }

  render(){
    const resizeMode = 'cover';
    const { navigation } = this.props;
    const item = navigation.getParam('item');
    console.log("itewm",item)
    const images = {
      starFilled: require('../../../assets/images/select_star.png'),
      starUnfilled: require('../../../assets/images/unselect_star.png')
    }
    return (
  
      <View style={styles.container}>
       
      <ImageBackground
         style={{
           backgroundColor: '#ccc',
           flex: 1,
           resizeMode,
           position: 'absolute',
           width: '100%', 
           height: '100%',
           justifyContent: 'center',
           
           
         }}
         source={require('../../../assets/images/slice81-min.png')}
         />
       
       <View style={{flex: 1, flexDirection: 'row',justifyContent:'space-between',top:10}}>
           <View>
            {/* <TouchableOpacity onPress={this.back.bind(this)} loading={true}>
           <Ionicons name={Platform.OS === "ios" ? "ios-arrow-back" : "md-arrow-back"}  size={30}></Ionicons>
         </TouchableOpacity> */}
         </View>
         <View>
          <Text  style={{ fontSize: 25 ,alignSelf:'center',color:'#2db9b9',fontFamily:'KGPrimaryWhimsy'}}>FAMILIEN <Text  style={{ fontSize: 25,color:'#ffb3b3',fontFamily:'KGPrimaryWhimsy',textTransform: 'uppercase'}}> {this.state.userName}</Text></Text>
          </View>
          <View>
          {/* <TouchableOpacity onPress={this.back.bind(this)} loading={true}>
            <Image source={require('../../../assets/images/slice50-min.png')} style={{width: 20,height: 20,}}></Image>
         </TouchableOpacity> */}
         </View>
         
      </View>     
      <View style={styles.container3}>
      <Dialog
          visible={this.state.popupVisible}
          onHardwareBackPress={() => { this.setState({ popupVisible: false }); } }
          onTouchOutside={() => {
            this.setState({ popupVisible: false });
          }}
        >
          
          <DialogContent style={{width:wp('90%')}}>
          {/* <Image  resizeMode="contain" style={{width:wp('90%') ,height:hp('20%'),alignSelf:'center'}} source={require('../../../assets/images/slice70-min.png')}/> */}
          <Text adjustsFontSizeToFit={true} style={{color:'black',fontSize:20,alignSelf:'center',justifyContent:'center',top:3}}>funktion stadig under udvikling..</Text>
          </DialogContent>
        </Dialog>
      <Container style={{backgroundColor:'transparent'}} >
        <View style={{flex:1,flexDirection:'column',alignContent:'center',alignItems:'center',bottom:10}}>
      <Body >
      <Thumbnail  source={{uri:this.state.userImage}} />        
        </Body>
        <Body  style={{top:5}} >
        <Rating
              ratingCount={6}
              imageSize={20}
              startingValue={4}
              onFinishRating={this.ratingCompleted}
            />
        </Body>
        <Body style={{width:wp('75%'),alignItems:'center',top:15}}>
        <Text  style={{ fontSize: 15 ,alignSelf:'center',color:'black'}}>Vi er en røgfrifamilie. Vi er et dyrefrithjem.
Vi bruger parfumefri vaske og skyllemidde</Text>    
        </Body>
        <Body style={{width:wp('75%'),alignItems:'center',top:15,marginBottom:10}}>
        <TouchableOpacity onPress={this.next.bind(this)}>
          <Text style={styles.button}>Gå til min konto</Text>
        </TouchableOpacity>
        </Body>
        <Body style={{width:wp('75%'),alignItems:'center',top:15}}>
        <TouchableOpacity onPress={()=>console.log("aay")}>
          <Text style={styles.button}>Prøv Familieplus gratis</Text>
        </TouchableOpacity>
        </Body>
        </View>
        <View style={{flex: 1, flexDirection: 'row', top: hp('5%'),zIndex:2000,justifyContent:'space-around',alignContent:'center',alignSelf:'center'}}>
              <View style={{marginLeft:-wp('5%')}}>
                <TouchableOpacity onPress={() => this.props.navigation.navigate('Shop')}>
                  <Image  resizeMode="contain" style={{width:wp('45%') ,height:hp('25%'),alignSelf:'center'}} source={require('../../../assets/images/slice72-min.png')}/>
                  <Image  resizeMode="contain" style={{width:wp('18%') ,height:hp('12%'),alignSelf:'center',position:'absolute',alignSelf:'center',top:hp('8%')}} source={require('../../../assets/images/slice65-min.png')}/>
                  <Text style={{fontFamily:'KGPrimaryWhimsy',fontSize: 15,position:'absolute',alignSelf:'center',top:hp('20%')}}>Opret børneting</Text>          
                </TouchableOpacity>
              </View>
              <View style={{top:hp('16.5%'),marginLeft:-wp('5%')}}>
                
                  <Image  resizeMode="contain" style={{width:wp('8%') ,height:hp('8%'),alignSelf:'center'}} source={require('../../../assets/images/slice34-min.png')}/> 
                
              </View>
              <TouchableOpacity onPress={() =>this.reactivate(item.id)}>
             
                <View>
                  <Image  resizeMode="contain" style={{width:wp('40%') ,height:hp('25%'),alignSelf:'center'}} source={{uri:item.approval_product[0].image_url}}/>           
                </View>
                <View style={{height:hp('25%'),width:wp('40%'), backgroundColor: 'red',opacity:.4,position:'absolute'}}>
                </View>
                <Image resizeMode="contain" style={{width:wp('15%') ,height:hp('15%'),alignSelf:'center',position:'absolute',top:15}} source={require('../../../assets/images/slice16.png')}/>   
               
                {this.state.refresh==true?(
                  
                  <Text  style={{fontFamily:'KGPrimaryWhimsy',fontSize:25,alignSelf:'center',color:'white',bottom:50}}>Aktivering..</Text>
               
              ): <Text  style={{fontFamily:'KGPrimaryWhimsy',fontSize:25,alignSelf:'center',color:'white',bottom:50}}>Aktiver igen</Text>}    
                
                {this.state.refresh==true?(
                  
                  <Circle  style={{marginTop:10,position:'absolute',top:15,zIndex:999,alignSelf:'center'}} size={40} color="#2db9b9"/>
               
              ):null}    
              </TouchableOpacity>
              <View  style={{position:'absolute',top:hp('22%'),marginLeft:wp('50%'),flexDirection:'row'}}>
              <View style={{marginLeft:wp('5%'),zIndex:3}}>
                <TouchableOpacity onPress={() =>this.deleteReservation(item.id)}>
                  <Image  resizeMode="contain" style={{width:wp('15%') ,height:hp('10%'),alignSelf:'center'}} source={require('../../../assets/images/slice31-min.png')}/>           
                </TouchableOpacity>
                
              </View>
              </View>
            </View>
         
        <View  style={{position:'absolute',top:hp('55%'),marginLeft:wp('3%')}}>              
                  <Image  resizeMode="contain" style={{width:wp('95%') ,height:hp('25%'),alignSelf:'center'}} source={require('../../../assets/images/slice70-min.png')}/> 
              </View>
         
       </Container>
     
      </View>
            
  </View>
   
    );
  }
  }
   

const styles = StyleSheet.create({
  container: {
    flex: 1,
    //alignItems: 'center',
    flexDirection:'column',
    justifyContent: 'center',
    backgroundColor: 'transparent'
  },
  container2: {
    flex: 1,
    //alignItems: 'center',
    //justifyContent: 'flex-start',
    
    backgroundColor: 'transparent'
    //bottom: Dimensions.get('screen').height /5
  },
  textstyle:{
    fontFamily:'KGPrimaryWhimsy',
    fontSize:40
  },
  container3: {
    position:'relative',
    backgroundColor: 'transparent',
    height:hp('75%'),
    width:wp('100%'),
    //alignItems: "center"
  },
  dropdown: {
    height: Dimensions.get('screen').width /7,
    padding: 5,
    position:'relative',
    margin:5,
    borderWidth: .2,
    borderRadius: 12,
    textShadowColor: 'black',
    borderColor: 'black',
    marginBottom: 10,
    backgroundColor: 'white',
    opacity: 0.9,
    fontFamily:'KGPrimaryWhimsy',
    fontSize:20,
    alignContent:'center'
  },
  wrpper: {
    opacity: 0.9,
    fontSize:20,
    fontFamily:'KGPrimaryWhimsy',
    flexDirection: 'row', justifyContent: 'space-around',   
  },
  button: {
    backgroundColor: 'white',
    borderColor: 'black',
    borderWidth: .5,
    borderRadius: 12,
    color: '#54969c',
    overflow: 'hidden',
    padding: 12,
    textAlign:'center',
    width: 300,
    height: 44,
    opacity: 0.9,
    fontFamily:'KGPrimaryWhimsy',
    fontSize:20
  },
});

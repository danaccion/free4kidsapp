//CHAT APPROVE

import React, { Component } from 'react';
  import {StyleSheet,View,ImageBackground,Image,TouchableOpacity,TextInput,Platform,Dimensions,AsyncStorage,Keyboard,StatusBar,TouchableWithoutFeedback,KeyboardAvoidingView,ScrollView} from 'react-native';
  import BackButton from '../../BackButton';
  import HamburgerIcon from '../../icons/HamburgerIcon';
  import UUIDGenerator from 'react-native-uuid-generator';
  import HeartBagBurger from '../../icons/HeartBagBurger';
  import Dialog, {SlideAnimation,DialogContent } from 'react-native-popup-dialog';
  import { Container, Content, List, ListItem, Thumbnail,Icon,InputGroup,Input,Body,Right,Badge,Text,Left,Textarea,Form,Item } from 'native-base';
  import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';
  import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view'
  import SwipeableRating from 'react-native-swipeable-rating';
  import Modal from 'react-native-modal';
  import ModalBox  from 'react-native-modalbox'
  import io from 'socket.io-client'
  import moment from 'moment'
  import ProgressiveImage from '../ProgressiveImage';
  import KeyboardSpacer from 'react-native-keyboard-spacer';
  import { PH,SS } from 'react-native-dotenv'
  import { GiftedChat,MessageText, Bubble,InputToolbar,Send } from 'react-native-gifted-chat'
  import {COMMUNITY_CHAT,PRIVATE_MESSAGE,USER_DISCONNECTED,VERIFY_USER,MESSAGE_RECEIVED,MESSAGE_SENT,TYPING} from '../chatEvents/events'
  const Fsize2 = Platform.OS === 'ios' ?25 : 18;
  const Fsize = Platform.OS === 'ios' ?22 : 16;
  const chatText = Platform.OS === 'ios' ?15 : 13;
  const { width, height } = Dimensions.get('window');
  const TAB_BAR_HEIGHT = 49;
  import { connect } from 'react-redux';
  import {heartBagNotifCount,messages_notif} from '../actions/index'
  import {
    Button,Block
  } from 'galio-framework';
  import { Alert } from 'react-native';
  const  socketURL= 'https://socket.free4kids.design4web.dk';
  class Chat extends Component {
    static navigationOptions = () => {
      return {
          headerLeft: <HamburgerIcon/>,
          headerTitle: (
              <Image
              resizeMode="contain"
              source={require('../../../assets/images/slice2.png')}
              style={{ width: 10, height:10 , flex:4,justifyContent: 'space-around', alignItems: 'center', padding: 25,}}
            />
            ),
          headerRight: <HeartBagBurger/>
      };
  };

  
      state = {
        socket:null,
        user:null,
        username: 'MAONI',
        password: '',
        ButtonStateHolder : true,
        loading: false,
        fontLoaded:false,
        preferences:'',
        panelMargin:0,
        chatMessages:[],
        chats:[],
        messages: [],
        userid:'',
        userImage:'',
        activeChat:null,
        isModalVisible:false,
        reservationModal:false,
        rating:3,
        swipeablePanelActive: false,
        reportModal:false,
        deleteModal:false,
        contact_approval:false,
        popupVisible:false,
        showDialogMessage:false,
        dialog_message:''
      };


    async componentDidMount() {
      await  this.connect()
      await this.getMessages()
      this.getContactRequest()
      const { navigation } = this.props;
      const item = navigation.getParam('item');
      const user_id=await AsyncStorage.getItem('user_id')
      const username=await AsyncStorage.getItem('name')
      this.socket.on("send_message", msg => {
        if(parseInt(msg.receiver_id)===parseInt(user_id)&&parseInt(msg.sender_id)===item.approval_user==null?parseInt(item.sender_id):parseInt(item.approval_user.id)){ 
          this.setState({ chatMessages: [...this.state.chatMessages, msg] });
        }
      });
      
      this.setState({userid:await AsyncStorage.getItem('user_id'),userImage:await AsyncStorage.getItem('image_url')})
      
    }


      handleRating = (rating) => {
        this.setState({rating});
      }
    
      toggleModal = () => {
        this.refs.slideUp.close()
        this.refs.rate.open()
      };

      toggleModalReservation = () => {
        this.refs.options.close()
        this.refs.reservation.open()
        
      };

      toggleModalContact = () => {
        this.refs.options.close()
        this.refs.contact.open()
        
      };

      toggleShowInformation = () => {
        this.refs.slideUp.close()
        this.refs.seeInformation.open()
        
      };
      toggleModalReport = () => {
        this.refs.options.close()
        this.refs.report.open()
      };

      toggleModalDelete = () => {
        // this.refs.options.close()
        this.refs.delete.open()
        console.log('delete convo');
      };
  
      initSocket =() =>{
        const socket= io(socketURL)
        socket.on('connect',()=>{
          console.log("connected")
        })
        this.setState({socket})
      
      }

    onDelete = async () =>{
      console.log("deleting convo...");
      this._isMounted = true;
      Keyboard.dismiss()
      this.setState({
        ButtonStateHolder : true,
        loading: true
      })

      const { navigation } = this.props;
      const item = navigation.getParam('item');
      const user_id=await AsyncStorage.getItem('user_id')
      
      console.log("sender_id", user_id);
      console.log("receiver_id", item.approval_user==null?item.sender_id:item.approval_user.id)
      const { sender_id,receiver_id } = this.state;
      this.props.navigation.pop();
      let data = new FormData();
      data.append('sender_id', user_id);
      data.append('receiver_id',  item.approval_user==null?item.sender_id:item.approval_user.id);
      fetch(PH+'/api/v1/messages/deleteAll', {
        method: 'POST',
        headers: {
          'Accept':'application/json',
          'Content-Type': 'multipart/form-data',
          'Authorization': 'Bearer '+await AsyncStorage.getItem('token')
        },
        body:data
        }).then((response) => response.json())
            .then((responseJson) => {
              if(responseJson.code!=200){
                console.log(responseJson)
                // console.log(responseJson.title)
                // this.setState({
                  
                //   loading: false,popupVisible:true
                // })
                // Keyboard.dismiss();
              }else if(responseJson.code == 200){
                console.log("Success Deleted");
                console.log(responseJson);
              }
        })
            .catch((error) => {
              console.log("ayay",error)
              this.setState({showDialogMessage:true, dialog_message:error});
            //alert(error); 
        });
  }

    getMessages = async  () => {
      const { navigation } = this.props;
    const item = navigation.getParam('item');
    const user_id=await AsyncStorage.getItem('user_id')
    var messageList=[]
    let data = new FormData();
    console.log('contact approval' + item.contact_approval)
    this.state.contact_approval = item.contact_approval;
    data.append('sender_id', user_id);
    data.append('receiver_id', item.approval_user==null?item.sender_id:item.approval_user.id);

    console.log("get message "+user_id+" "+ item.approval_user==null?item.sender_id:item.approval_user.id)
    fetch(PH+'/api/v1/users/getmessages', {
      method: 'POST',
      headers: {
        'Accept':'application/json',
        'Content-Type': 'multipart/form-data',
        'Authorization': 'Bearer '+await AsyncStorage.getItem('token')
      },
      body:data
      }).then((response) => response.json())
          .then((responseJson) => {
            this.setState({ chatMessages:responseJson.meta.messages  });
      })
          .catch((error) => {
            console.log("ayay",error)
      });

  };

    sendTyping = (chatId,isTyping)=>{
        const {socket} = this.state
        socket.emit(TYPING, {chatId,isTyping})
    }


  async openDetails(){
    await Keyboard.dismiss()
      this.refs.slideUp.open()
      
  }

    setActiveChat = (activeChat) =>{
        this.setState({activeChat})
    }

    // disconnect = () => {
    //   const {socket} = this.state
    //   socket.emit(USER_DISCONNECTED)
    //   this.setState({user:null})
    // }
    
  connect = async () => {
    var chatData=[]
    const { navigation } = this.props;
    const item = navigation.getParam('item');
    const user_id=await AsyncStorage.getItem('user_id')
    const username=await AsyncStorage.getItem('name')
      this.socket = io('http://socket.free4kids.design4web.dk', {
        autoConnect: true,
      });
      this.socket.on('connect', () => {
        this.socket.emit('come_online', {'name':username,'user_id' : user_id,'receiver_id':item.approval_user==null?item.sender_id:item.approval_user.id,'receiver_name':item.approval_user==null?item.name:item.approval_user.name});
      
      });
      this.socket.on('disconnect', (reason) => {
        console.log(`Disconnected: ${reason}`);
      })
    
      this.socket.open();
    };
    
  
    disconnect = () => {
      this.socket.disconnect();
    }
    componentWillUnmount(){
      this.disconnect()
    }
  
    back = async ()=>{
    this.props.navigation.navigate('ReservationChat');
    }


    
  async addMessage(message){
    let data = new FormData();
    this.socket.emit("sendmessage",message)
    // this.setState({ chatMessages:chatData});
    data.append('sender_id', message.sender_id);
    data.append('createdAt', message.createdAt.toString());
    data.append('_id', message.id);
    data.append('receiver_id', message.receiver_id);
    data.append('text', message.text);
    data.append('name', message.name);
    data.append('time', message.time);
    data.append('type', 'owner');
    fetch(PH+'/api/v1/messages/create', {
      method: 'POST',
      headers: {
        'Accept':'application/json',
        'Content-Type': 'multipart/form-data',
        'Authorization': 'Bearer '+await AsyncStorage.getItem('token')
      },
      body:data
      }).then((response) => response.json())
          .then((responseJson) => {
            console.log("added",responseJson)
      })
          .catch((error) => {
            console.log("ayay",error)
      });
  }  

  async submitMesssage(messages){
  
    const { navigation } = this.props;
    const item = navigation.getParam('item');
      const message ={id:messages[0]._id,sender_id:this.state.userid,_id:messages[0]._id,
      text:messages[0].text,createdAt:messages[0].createdAt,time:this.getDateNow(),image_url:this.state.userImage,receiver_id:item.approval_user==null?item.sender_id:item.approval_user.id,name:item.approval_user==null?item.name:item.approval_user.name}
      this.setState({ chatMessages: [...this.state.chatMessages, messages[0]] });
      this.addMessage(message)
      this.socket.emit("chat message",messages[0])
      
    
    }

  async submitRating(){
    const { navigation } = this.props;
    const item = navigation.getParam('item');
    let data = new FormData();
    data.append('sender_id', message.sender_id);
    data.append('_id', message.id);
    data.append('receiver_id', message.receiver_id);
    data.append('text', message.messages);
    data.append('name', message.name);
    data.append('time', message.time);
    fetch(PH+'/api/v1/messages/create', {
      method: 'POST',
      headers: {
        'Accept':'application/json',
        'Content-Type': 'multipart/form-data',
        'Authorization': 'Bearer '+await AsyncStorage.getItem('token')
      },
      body:data
      }).then((response) => response.json())
          .then((responseJson) => {
        
      })
          .catch((error) => {
            console.log("ayay",error)
          
      });
      
      }


  async getRequestContact(){
        const { navigation } = this.props;
        const item = navigation.getParam('item');
        let data = new FormData();
        const user_id=await AsyncStorage.getItem('user_id')
        console.log('getRequestContact');
        console.log(data.append('product_id', item.id));
        console.log(data.append('user_id', user_id));
        console.log(data.append('request_id',item.approval_user.id));
    
        fetch(PH+'/api/v1/products/getProductContact', {
          method: 'POST',
          headers: {
            'Accept':'application/json',
            'Content-Type': 'multipart/form-data',
            'Authorization': 'Bearer '+await AsyncStorage.getItem('token')
          },
          body:data
          }).then((response) => response.json())
              .then((responseJson) => {
                console.log(responseJson)
                //Dapat mag return ka dri ug value para info sa contact
            
          })
              .catch((error) => {
                console.log("ayays",error)
                return 0;
              
          });
          
  }

  getContactRequest = async () =>{
    const { navigation } = this.props;

    const user_id=await AsyncStorage.getItem('user_id')
    const item = navigation.getParam('item');
    let data = new FormData();

    console.log(item);
    console.log(item.approval_user.id);
    console.log(user_id);
    console.log(item.id);
    

    
    console.log('getContactRequest');
    console.log("-----------------------");
    data.append('product_id', item.id);
    data.append('user_id',item.approval_user.id);
    // data.append('description', 'owner tillad kontak');
    data.append('request_id',  user_id);
    fetch(PH+'/api/v1/request_contact', {
      method: 'POST',
      headers: {
        'Accept':'application/json',
        'Content-Type': 'multipart/form-data',
        'Authorization': 'Bearer '+await AsyncStorage.getItem('token')
      },
      body:data
      }).then((response) => response.json())
          .then((responseJson) => {
            console.log("Checking Request if already Approve....")
            console.log(responseJson)
             if(responseJson.code===201){
               console.log("Match!")
              //  Alert.alert("Already Shared")
                
              console.log("success");
              this.setState({
                showDialogMessage:true,
                dialog_message:'Already Shared',
                popupVisible:true,
                contact_approval:true
              })
              console.log("Link")
            }
            else if(responseJson.code == 200)
            {  
            
              console.log("success");
              this.setState({
                popupVisible:true,
                contact_approval:true
              })
              console.log("Done Approving")
            }
      }).catch((error) => {});
  }


  submitApproval = async  () => {

    
    const { navigation } = this.props;
  const item = navigation.getParam('item');

  const user_id=await AsyncStorage.getItem('user_id')
  var messageList=[]
  let data = new FormData();
  this.state.contact_approval = item.contact_approval;
  console.log(item)
  console.log("Start------------")
  console.log(item.approval_user.id);//89
  console.log(user_id);//51
  console.log(item.id);//129
  
  data.append('product_id', item.id);
  data.append('user_id',item.approval_user.id);
  // data.append('description', 'owner tillad kontak');
  data.append('request_id',  user_id);
  token = await AsyncStorage.getItem('token');
  fetch(PH+'/api/v1/products/setApproveRequestContact', {
    method: 'POST',
    headers: {
      'Accept':'application/json',
      'Content-Type': 'multipart/form-data',
      'Authorization': 'Bearer '+token
    },
    body:data
    }).then((response) => response.json())
        .then((responseJson) => {
          console.log("return code : "+responseJson.code+"\n title :"+responseJson.title);


          if(responseJson.code===200){
            
            console.log("success");
            //Alert.alert("Success Shared contact.");
            
            this.setState({
              popupVisible:true,
              contact_approval:true
            })
            
          }else if(responseJson.code == 401){
            this.setState({
              popupVisible:true,
              contact_approval:true,
              showDialogMessage:true,
                dialog_message:'Already Approved.'
            })
            //Alert.alert("Already Approved.");
          }
          else{
            data.append('owner_approved', '1');
            // Alert.alert("Failed to share contact info.");
            // this.state.contact_approval = 1
            // this.refs.contact.close();
            // console.log("failed to perform share contact");
            // this.setState({
            //   popupVisible:false
            // })
            fetch(PH+'/api/v1/request_contact/create', {
              method: 'POST',
              headers: {
                'Accept':'application/json',
                'Content-Type': 'multipart/form-data',
                'Authorization': 'Bearer '+token
              },
              body:data
              }).then((response) => response.json())
                  .then((responseJson) => {
                    console.log("return code : "+responseJson.code+"\n title :"+responseJson.title);
            
            
                    if(responseJson.code===200){

                      console.log("success");
                      //Alert.alert("Success Shared contact.");
                      this.setState({
                          showDialogMessage:true,
                        dialog_message:'Success Shared contact.',
                        popupVisible:true,
                        contact_approval:true
                      })

                      
                    }else{
                      //Alert.alert("Failed to share contact info.");
                      
                      this.refs.contact.close();
                      console.log("failed to perform share contact");
                      this.setState({
                          showDialogMessage:true,
                        dialog_message:'Failed to share contact info.',
                        popupVisible:false,
                        contact_approval:false
                      })
                    }
                  
              })
                  .catch((error) => {
                    console.log("ayay",error)
              });
          }
        
    })
        .catch((error) => {
          console.log("ayay",error)
    });

  };


  initialData = async  () => {

    
    const { navigation } = this.props;
  const item = navigation.getParam('item');

  const user_id=await AsyncStorage.getItem('user_id')
  var messageList=[]
  let data = new FormData();
  this.state.contact_approval = item.contact_approval;
  console.log(item)
  console.log("Start------------")
  console.log(item.approval_user.id);
  console.log(user_id);
  console.log(item.id);
  
  data.append('product_id', item.id);
  data.append('user_id',item.approval_user.id);
  // data.append('description', 'owner tillad kontak');
  data.append('request_id',  user_id);
  token = await AsyncStorage.getItem('token');
  fetch(PH+'/api/v1/products/setApproveRequestContact', {
    method: 'POST',
    headers: {
      'Accept':'application/json',
      'Content-Type': 'multipart/form-data',
      'Authorization': 'Bearer '+token
    },
    body:data
    }).then((response) => response.json())
        .then((responseJson) => {
          console.log("return code : "+responseJson.code+"\n title :"+responseJson.title);


          if(responseJson.code===200){
            this.setState({
              contact_approval:true,
              showDialogMessage:true,
                dialog_message:'Success Shared contact.'
            })
            console.log("success");
            //Alert.alert("Success Shared contact.");
          }else if(responseJson.code == 401){
            this.setState({
              contact_approval:true
            })
          }
        
    })
        .catch((error) => {
          console.log("ayay",error)
    });

  };

    getDateNow(){
        
      var date = moment()
      .format(' hh:mm a');
      return date;
          
    }
    randomid(len, arr) { 
      var ans = ""; 
      for (var i = len; i > 0; i--) { 
          ans +=  
            arr[Math.floor(Math.random() * arr.length)]; 
      } 
      return ans; 
    } 
    

      buyshipping = async () => {
        this.props.navigation.navigate('BuyShipping');
      }
      basket_overview = async () => {
        this.props.navigation.navigate('BasketOverview');
      }
      messages =()=>{
        this.props.navigation.navigate('ActivityChat');
      }
      isCloseToBottom = ({ layoutMeasurement, contentOffset, contentSize }) => {
        return  contentOffset.y == 0;
      };

      getCount(data){
        // console.log("uy:",""+data,this.props.message_notif)
        if(this.props.message_notif!==null){
          var index = this.props.message_notif.findIndex(function(o){
            return o.id === 1;
          })
          if (index !== -1) {
            this.props.message_notif.splice(index, 1);

          }
          // console.log("ayay:",this.props.message_notif);
          this.props.update_message_notif(this.props.message_notif);
        }
        
      }


    render(){
      const { navigation } = this.props;
      const item = navigation.getParam('item');
      const resizeMode = 'cover';
      const renderMessageText = (props) => {
        const {
          currentMessage,
        } = props;
        const { user: currUser } = currentMessage;
        if (+currUser._id === +this.state.userid) {
                  return <View  style={{backgroundColor:'white',borderColor:'#bfbfbf',borderWidth:.5,marginBottom:4,borderRadius: 15,padding:10}}>

                  <MessageText {...props} style={{flex: 1}} textStyle={{
                      left: {
                        color: 'black',
                        fontSize:chatText,
                        marginBottom:10
                      },
                      right: {
                        color: 'black',
                        fontSize:chatText,
                        marginBottom:10
                      },
                    }}/>
                  <View style={{bottom: -5,right:-2,zIndex:100,position:'absolute'}}>
                  <ProgressiveImage  resizeMode='contain'  source={{ uri: this.state.userImage}} style={{width:Fsize2,height:Fsize2,borderRadius:Fsize2/2}}/>
                  </View>
          </View>
        }
        return (  <View  style={{backgroundColor:'white',borderColor:'#bfbfbf',borderWidth:.5,borderRadius: 15,marginBottom:7,padding:10,left:-40}}>

                      <MessageText {...props} style={{flex: 1}} textStyle={{
                          left: {
                            color: 'black',
                            fontSize:chatText

                          },
                          right: {
                            color: 'black',
                            fontSize:chatText
                          },
                        }}/>
                      <View style={{bottom: -5,left:-2,zIndex:100,position:'absolute'}}>
                      <ProgressiveImage  resizeMode='contain'  source={{ uri: item.approval_user.image_url}} style={{width:Fsize2,height:Fsize2,borderRadius:Fsize2/2}}/>
                      </View>

                    
          </View>)
              
        
      };
      const renderBubble = (props) => { 
    
    
        return <Bubble {...props} 
          wrapperStyle={{
              left: {
                backgroundColor: 'transparent',
              },
              right: {
                backgroundColor: 'transparent'
              }
            }} 
    
          timeTextStyle={{
            left: {
              color: 'grey',
              left:-40
            },
            right: {
              color: 'grey'
            },
          }}
        />
      }
      const  renderInputToolbar = (props) => { 

      return (
      <View style={{alignContent:'center',alignItems:'center',left:5,bottom:-20}}>
      <InputToolbar {...props} containerStyle={{justifyContent:'center',
      alignItems: 'center',backgroundColor:'white',width:wp('98%'),bottom:1,borderColor: '#bfbfbf',
      borderWidth: .5,
      borderRadius: 12,shadowColor: '#000',
      shadowOffset: { width: 0, height: 2 },
      shadowOpacity: 0.5,
      shadowRadius: 1.5,
      elevation: 1}}/>
      </View>
      )
    }
    const  sendButton = (props) => { 
    return <Send
    {...props}
    containerStyle={{
      height: 40,
      width: 40,
      justifyContent: 'center',
      alignItems: 'center',
    }}
  >
      <Icon style={{padding:10,  color:"grey",bottom:10}} name="md-send" />
  </Send>
  }
  
      return (
        
        <View style={{flex:1,backgroundColor:'transparent'}}>
          <StatusBar barStyle = "dark-content" hidden = {false} backgroundColor = "#00BCD4" translucent = {true}/>
        <ImageBackground
          style={{
            backgroundColor: '#ccc',
            flex: 1,
            resizeMode,
            position: 'absolute',
            width: '120%', 
            height: '100%',
            justifyContent: 'center',
            
          }}
          source={require('../../../assets/images/slice81-min.png')}
          />
            <View style={{flexDirection: 'row',justifyContent:'space-between',marginTop:5,zIndex:10}}>
            <BackButton/>
            {/* <Image source={require('../../../assets/images/slice73-min.png')} style={{resizeMode,width:40,height:hp('65%'),}}></Image> */}
            <TouchableOpacity onPress={()=>this.refs.options.open()}>
              <Image source={require('../../../assets/images/img-inf.png')} style={{width: 20,height: 20,right:10}}></Image>
              
          </TouchableOpacity>
            </View>
          
          <GiftedChat
                messages={this.state.chatMessages}
                onSend={messages => this.submitMesssage(messages)}
                user={{
                  _id:+this.state.userid,
                  // avatar: this.state.userAvatar,
                }}
                // bottomOffset={-20}
                placeholder="Chat nærmere"
                // minInputToolbarHeight={20}
                // maxInputToolbarHeight={30}
                // onPressActionButton={()=>this.openDetails()}
                // renderInputToolbar={renderInputToolbar} 
                renderMessageText={renderMessageText}
                renderBubble={renderBubble}
                // renderSend={sendButton}
                // loadEarlier={true}
                // renderUsernameOnMessage={true}
                inverted={false}
                alwaysShowSend={true}
                showUserAvatar={false}
                avatar={false}
              />  
          <ModalBox coverScreen={true}  position={"bottom"} style={{height:hp('30%'),zIndex:999,borderTopLeftRadius: 20, 
          borderTopRightRadius: 20,
          overflow: "hidden"}} transparent={false} ref={"options"} >
          <View style={{flex:1,flexDirection:'column',alignContent:'center',alignItems:'center',justifyContent:'flex-end'}}>
          <Body style={{width:wp('85%'),alignItems:'center',marginTop:20,marginBottom:30}}>
            <Button round onPress={()=>console.log("to profile")}  style={{width:wp('80%') }} color="#c97190">Se profil</Button>
            <Button round onPress={()=>this.toggleModalContact()}   style={{width:wp('80%') }} color="#c97190">Kontaktoplysninge</Button>
            <Button round onPress={()=>this.toggleModalReservation()}  style={{width:wp('80%') }} color="#c97190">Fortryd reservation</Button>
            <Button round onPress={()=>this.toggleModalReport()}  style={{width:wp('80%') }} color="#c97190">Rapport</Button>
            <Button round onPress={()=>this.toggleModalDelete()} style={{width:wp('80%') }} color="#c97190">Slet samtale</Button>
          </Body>
          </View>       
        </ModalBox> 
        
        <ModalBox position={"center"}style={{height:hp('50%'),width:wp('90%'),zIndex:999,borderTopLeftRadius: 30, 
          borderTopRightRadius: 30,borderBottomLeftRadius: 30, 
          borderBottomRightRadius: 20,
          overflow: "hidden"}} transparent={false} ref={"seeInformation"}>
          <Block center style={{borderRadius:20,width:wp('80%')}} >
        {item.info!==undefined?(
        <View style={{backgroundColor:'white',alignContent:'center',alignItems:'center',height:hp('30%'),borderRadius: hp('5%')/1.2,width:wp('80%')}}>
        
            <Text style={{alignSelf:'center',top:20,fontSize:Fsize2+5,fontFamily:'KGPrimaryWhimsy',color:'#d47d7d'}}>Kontaktoplysninger</Text>
            <Text style={{alignSelf:'center',top:20,fontSize:Fsize2,fontFamily:'KGPrimaryWhimsy',color:'grey'}}>Telefon: {item.info.telephone}</Text>
            <Text style={{alignSelf:'center',top:20,fontSize:Fsize2,fontFamily:'KGPrimaryWhimsy',color:'grey'}}>Adresse: {item.info.address}  </Text>
          
        

        </View>
          ):<View style={{backgroundColor:'white',alignContent:'center',alignItems:'center',height:hp('30%'),borderRadius: hp('5%')/1.2,width:wp('80%')}}>
        
          <Text style={{alignSelf:'center',top:20,fontSize:Fsize2+5,fontFamily:'KGPrimaryWhimsy',color:'#d47d7d'}}>Kontaktoplysninger</Text>
          <View style={{justifyContent:'center',alignContent:'center',marginTop:50}}> 
          <Text adjustsFontSizeToFit style={{alignSelf:'center',top:25,fontSize:Fsize2,fontFamily:'KGPrimaryWhimsy',color:'grey',textAlignVertical: "center",textAlign: "center",}}>Familien har endnu ikke offentliggjort deres adresse og telefon. </Text>
          </View>
          <TouchableOpacity onPress={()=>this.toggleModal()} style={{marginTop:70}}>
            <Text style={styles.button6}>Anmod</Text>
          </TouchableOpacity>
        
      

          </View>}
      </Block>
        </ModalBox>

      <ModalBox coverScreen={true}  position={"bottom"} style={{height:hp('90%'),zIndex:999,borderTopLeftRadius: 20, 
          borderTopRightRadius: 20,
          overflow: "hidden"}} transparent={false} ref={"slideUp"} >
          <Block center >
        <Image resizeMode='contain' style={{width:wp('60%'),height:hp('60%'),alignSelf:'center'}} source={require('../../../assets/girls_info/slice4.png')} />        
          <Button round style={{width:wp('80%') }} color="#54969c">Køb porto</Button>
          <Button round onPress={()=>this.getRequestContact()}  style={{width:wp('80%') }} color="#54969c">Afstand på kort</Button>
       
          {/* <Button round onPress={()=>this.toggleShowInformation()}  style={{width:wp('80%') }} color="#54969c">Afstand på kort</Button> */}
          <Button round onPress={()=>this.toggleModal()}  style={{width:wp('80%') }} color="#54969c">Afslut vores aftale</Button>
          </Block>
              </ModalBox>   
      <ModalBox position={"center"} style={{height:hp('50%'),width:hp('10%'),backgroundColor:'transparent'}} transparent={true} ref={"reservation"} >
        <TouchableOpacity style={{position:'relative',alignSelf:'flex-end',height:100,width:100,zIndex:99,left:100,top:30}} onPress={()=>this.refs.reservation.close()}>
          
        </TouchableOpacity>
        <TouchableOpacity style={{position:'relative',alignSelf:'center',height:100,width:100,zIndex:99,top:90}} onPress={()=>console.log("Undo Reserve")}>
            
        </TouchableOpacity>
        <Image
          style={{
            zIndex:10,
            resizeMode:'contain',
            position: 'absolute',
            alignSelf:'center',
            width:wp('70%'),
            height:wp('70%'),
            justifyContent: 'center',
            
          }}
          source={require('../../../assets/icons/slice12.png')}
          />
          
        </ModalBox>

        <ModalBox position={"center"} style={{height:hp('50%'),width:hp('10%'),backgroundColor:'transparent'}} transparent={true} ref={"contact"} >
        <TouchableOpacity style={{position:'relative',alignSelf:'flex-end',height:100,width:100,zIndex:99,left:100,top:30}} onPress={()=>this.refs.contact.close()}>
          
        </TouchableOpacity>
        <TouchableOpacity style={{position:'relative',alignSelf:'center',height:100,width:100,zIndex:99,top:90}} onPress={()=>console.log("Undo Reserve")}>
            
        </TouchableOpacity>
        <Image
          style={{
            zIndex:10,
            resizeMode:'contain',
            position: 'absolute',
            alignSelf:'center',
            width:wp('70%'),
            height:wp('70%'),
            justifyContent: 'center',
            
          }}
          source={require('../../../assets/icons/slice12.png')}
          />
          
        </ModalBox>


        <ModalBox  position={"center"}style={{height:hp('40%'),width:wp('70%'),zIndex:999,borderTopLeftRadius: 30, 
          borderTopRightRadius: 30,borderBottomLeftRadius: 30, 
          borderBottomRightRadius: 20,
          overflow: "hidden",backgroundColor:'transparent'}} transparent={true} ref={"report"}>
          
        <TouchableOpacity style={{position:'relative',alignSelf:'flex-end',height:100,width:100,zIndex:99,left:100,top:30}} onPress={()=>this.toggleModalDelete()}>
        </TouchableOpacity>
        <TouchableOpacity style={{position:'relative',alignSelf:'center',height:60,width:80,zIndex:99,top:90}} onPress={()=>console.log("Reported")}>
            
        </TouchableOpacity>
        <Image
          style={{
            zIndex:10,
            resizeMode:'contain',
            position: 'absolute',
            alignSelf:'center',
            width:wp('70%'),
            height:wp('70%'),
            justifyContent: 'center',
            
          }}
          source={require('../../../assets/icons/slice14.png')}
          />
          
        </ModalBox>


        
        <ModalBox  position={"center"}style={{height:hp('40%'),width:wp('70%'),zIndex:999,borderTopLeftRadius: 30, 
          borderTopRightRadius: 30,borderBottomLeftRadius: 30, 
          borderBottomRightRadius: 20,
          overflow: "hidden",backgroundColor:'transparent'}} transparent={true} ref={"delete"}>
  {/*         
        <TouchableOpacity style={{position:'relative',alignSelf:'flex-end',height:100,width:100,zIndex:99,left:100,top:30}} onPress={()=>this.toggleModalDelete}>
        </TouchableOpacity> */}
        <TouchableOpacity style={{position:'relative',alignSelf:'center',height:60,width:80,zIndex:99,top:90}} onPress={this.onDelete}>
        <Text style={styles.button5}>Indsend</Text>    
        </TouchableOpacity>
        <Image
          style={{
            zIndex:10,
            resizeMode:'contain',
            position: 'absolute',
            alignSelf:'center',
            width:wp('70%'),
            height:wp('70%'),
            justifyContent: 'center',
            
          }}
          source={require('../../../assets/icons/errorLogin.png')}
          />
          
          
        </ModalBox>
            
        <ModalBox  position={"center"}  style={{height:hp('50%'),width:wp('90%'),zIndex:999,borderTopLeftRadius: 30, 
          borderTopRightRadius: 30,borderBottomLeftRadius: 30, 
          borderBottomRightRadius: 20,
          overflow: "hidden"}} transparent={false} ref={"contact"}>
        
          <TouchableWithoutFeedback onPress={()=>Keyboard.dismiss()}>
        
            <View style={{backgroundColor:'white',alignContent:'center',alignItems:'center',height:hp('50%'),borderRadius: hp('10%')/1.2}}>
            <Text style={{alignSelf:'center',top:20,fontSize:Fsize2,fontFamily:'KGPrimaryWhimsy',color:'#d47d7d'}}>Offentlig kontakt</Text>
              <View style={{top:40,zIndex:909}}>
              
            
          </View>
          <View style={{top:80}}>
            
      
    
    {/* <TouchableOpacity onPress={() => this.refs.options.open()} disabled={this.state.contact_approval?true:false}>
        <Text style={{backgroundColor: this.state.contact_approval?'grey':'#54969c',
                                                borderColor: 'white',
                                                borderWidth: .5,
                                                borderRadius: 12,
                                                color: 'white',
                                                overflow: 'hidden',
                                                padding: 12,
                                                textAlign:'center',
                                                width: 300,
                                                height: 44,
                                                opacity: 0.9,
                                                fontFamily:'KGPrimaryWhimsy',
                                                fontSize:20}}>{this.state.contact_approval==0?'Delt':'Del kontakt'}</Text>         
      </TouchableOpacity> */}
        </View>
        
            </View>
            </TouchableWithoutFeedback>
            
          </ModalBox>




                <ModalBox  position={"center"}  style={{height:hp('50%'),width:wp('90%'),zIndex:999,borderTopLeftRadius: 30, 
          borderTopRightRadius: 30,borderBottomLeftRadius: 30, 
          borderBottomRightRadius: 20,
          overflow: "hidden"}} transparent={false} ref={"contact"}>
        
          <TouchableWithoutFeedback onPress={()=>Keyboard.dismiss()}>
        
            <View style={{backgroundColor:'white',alignContent:'center',alignItems:'center',height:hp('50%'),borderRadius: hp('10%')/1.2}}>
            <Text style={{alignSelf:'center',top:20,fontSize:Fsize2,fontFamily:'KGPrimaryWhimsy',color:'#d47d7d'}}>Offentlig kontakt</Text>
              <View style={{top:40,zIndex:909}}>
              
            
          </View>
          <View style={{top:80}}>
            
      
    
    <TouchableOpacity onPress={() =>this.submitApproval()} disabled={this.state.contact_approval?true:false}>
        <Text style={{backgroundColor: this.state.contact_approval?'grey':'#54969c',
                                                borderColor: 'white',
                                                borderWidth: .5,
                                                borderRadius: 12,
                                                color: 'white',
                                                overflow: 'hidden',
                                                padding: 12,
                                                textAlign:'center',
                                                width: 300,
                                                height: 44,
                                                opacity: 0.9,
                                                fontFamily:'KGPrimaryWhimsy',
                                                fontSize:20}}>{this.state.contact_approval?'Delt':'Tillad kontakt'}</Text>         
      </TouchableOpacity>

      
        </View>
        
            </View>
            </TouchableWithoutFeedback>
            
          </ModalBox>
          
          <Dialog
            visible={this.state.showDialogMessage}
            onTouchOutside={() => { this.setState({ showDialogMessage: false }); }}
            onHardwareBackPress={() => {this.setState({ showDialogMessage: false });}}
            dialogStyle={{backgroundColor:'white',width:wp('90%'),alignItems:'center',alignContent:'center'}}>
            <DialogContent>
                <View styles={{alignItems:'center', }}>
                    <Text  style={{fontSize:28, color:'#eca6a1', padding: 12, textAlign:'center', fontFamily:'KGPrimaryWhimsy',}}>{this.state.dialog_message}</Text>
                </View>
            </DialogContent>
        </Dialog>
          
    </View>
    
      );
      
    }
    }

    const mapStateToProps = (state) =>{
      return{
        reservationList:state.count.reservationList,
        reserveCount:state.count.reservationBadgeCount,
        message_notif:state.count.messages_notif
      }
    }

  
  const mapDispatchToProps = (dispatch) =>{
    return{
      heartBagNotifCount:(data)=>dispatch(heartBagNotifCount(data)),
      update_message_notif:(data)=>dispatch(messages_notif(data))
    }
  }
    
  export default connect(mapStateToProps,mapDispatchToProps)(Chat)
    

  const styles = StyleSheet.create({
    container: {
      flex: 1,
      //alignItems: 'center',
      justifyContent: 'center',
      backgroundColor: 'transparent'
    },
    button5: {
      marginTop:5,
      backgroundColor: '#54969c',
      borderColor: 'white',
      borderWidth: .5,
      borderRadius: 12,
      color: 'white',
      overflow: 'hidden',
      padding: 12,
      textAlign:'center',
      width: wp('80%'),
      height: 48,
      opacity: 0.9,
      fontFamily:'KGPrimaryWhimsy',
      fontSize:20,
      alignSelf:'center',
      alignItems:'center'
    },
    container2: {
      flex: 1,
      //alignItems: 'center',
      //justifyContent: 'flex-start',
      
      backgroundColor: 'transparent'
      //bottom: Dimensions.get('screen').height /5
    },
    textstyle:{
      fontFamily:'KGPrimaryWhimsy',
      fontSize:40
    },
    container3: {
      position:'relative',
      backgroundColor: 'transparent',
      height:hp('65%'),
      width:wp('100%'),
      bottom:-10,
      flex:1
      // bottom:hp('15%'),
      //justifyContent: "flex-end",
      //alignItems: "center"
    },
    searchIcon: {
      padding: 10,
  },
  searchSection: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: 'transparent',
  },
    dropdown: {
      height: Dimensions.get('screen').width /7,
      padding: 5,
      position:'relative',
      margin:5,
      borderWidth: .2,
      borderRadius: 12,
      textShadowColor: 'black',
      borderColor: 'black',
      marginBottom: 10,
      backgroundColor: 'white',
      opacity: 0.9,
      fontFamily:'KGPrimaryWhimsy',
      fontSize:20,
      alignContent:'center'
    },
    wrpper: {
      opacity: 0.9,
      fontSize:20,
      fontFamily:'KGPrimaryWhimsy',
      flexDirection: 'row', justifyContent: 'space-around',   
    },
    button: {
      backgroundColor: '#54969c',
      borderColor: 'white',
      borderWidth: .5,
      borderRadius: 12,
      color: 'white',
      overflow: 'hidden',
      padding: 12,
      textAlign:'center',
      width: 300,
      height: 48,
      opacity: 0.9,
      fontSize:20,
      fontFamily:'KGPrimaryWhimsy'
    },
    button2: {
      backgroundColor: 'white',
      borderColor: 'grey',
      borderWidth: .5,
      borderRadius: 12,
      color: '#54969c',
      overflow: 'hidden',
      padding: 12,
      textAlign:'center',
      width: wp('80%'),
      height: 48,
      opacity: 0.9,
      fontSize:20,
      alignSelf:'center',
      fontFamily:'KGPrimaryWhimsy'
    },
    button3: {
      backgroundColor: 'white',
      borderColor: 'grey',
      borderWidth: .5,
      borderRadius: 12,
      color: '#54969c',
      overflow: 'hidden',
      padding: 10,
      textAlign:'center',
      width: wp('80%'),
      height: hp('5%'),
      opacity: 0.9,
      fontSize:Fsize,
      fontFamily:'KGPrimaryWhimsy'
    },
    bodyViewStyle: {
      flex: 1,
      justifyContent: 'center', 
      alignItems: 'center',
    },
    headerLayoutStyle: {
      width, 
      height: 20, 
      backgroundColor: '#54969c', 
      justifyContent: 'center', 
      alignItems: 'center',
    },
    slidingPanelLayoutStyle: {
    
      alignContent:'center',
      height:hp('100%'), 
      backgroundColor: 'transparent', 
      alignItems: 'center',
    },
    commonTextStyle: {
      color: 'white', 
      fontSize: 18,
    },
    button6: {
      marginTop:10,
      backgroundColor: '#54969c',
      borderColor: 'white',
      borderWidth: .5,
      borderRadius: 12,
      color: 'white',
      overflow: 'hidden',
      padding: 12,
      textAlign:'center',
      width: wp('70%'),
      height: 48,
      opacity: 0.9,
      fontFamily:'KGPrimaryWhimsy',
      fontSize:20,
      alignSelf:'center',
      alignItems:'center'
    },
  });

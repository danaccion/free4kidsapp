import React, { Component } from 'react';
import { ScrollView, StyleSheet,View,ImageBackground,Image,TouchableOpacity,Platform,Dimensions,Text,LayoutAnimation,AsyncStorage,RefreshControl} from 'react-native';
import Ionicons from 'react-native-vector-icons/Ionicons'
import { Picker } from 'react-native-picker-dropdown';
import BackButton from '../../BackButton';
import HamburgerIcon from '../../icons/HamburgerIcon';
import HeartBagBurger from '../../icons/HeartBagBurger';
import { Container, Content, List, ListItem, Thumbnail,Icon,Button,InputGroup,Input,Body,Right } from 'native-base';
import {Badge} from 'react-native-elements'
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';
import CountDown from 'react-native-countdown-component';
import { Snackbar } from 'react-native-paper';
import LottieView from 'lottie-react-native';
import moment from "moment";
import ProgressiveImage from '../ProgressiveImage';
import { PH,SS } from 'react-native-dotenv'
import Dialog, {DialogFooter,DialogButton,SlideAnimation,DialogContent } from 'react-native-popup-dialog';
import Toast from 'react-native-easy-toast'
const Fsize2 = Platform.OS === 'ios' ?20 : 15;
const Fsize3 = Platform.OS === 'ios' ?25 : 20;
const Fsize = Platform.OS === 'ios' ?30 : 23;
import { connect } from 'react-redux';
import {loadApproval,minusapprovalCount,deleteApproval,messages_notif} from '../actions/index'
import {
  Block
} from 'galio-framework';
class Index extends Component {
  static navigationOptions = () => {
    return {
        headerLeft: <HamburgerIcon/>,
        headerTitle: (
            <Image
            resizeMode="contain"
            source={require('../../../assets/images/slice2.png')}
            style={{ width: 10, height:10 , flex:4,justifyContent: 'space-around', alignItems: 'center', padding: 25,}}
          />
          ),
        headerRight: <HeartBagBurger/>
    };
};


    state = {
      username: '',
      password: '',
      ButtonStateHolder : true,
      loading: false,
      fontLoaded:false,
      preferences:'',
      deleteItem:false,
      approvalData:null,
      noData:false, 
      visibleSnack:false,
      refreshing:false,
      color:'white',
      deleted_id:null,
      deleteModal:false

    };
    baseState = this.state 


    componentDidMount(){
      this.getApprovals()
      this.didBlurSubscription = this.props.navigation.addListener(
        'didFocus',
        payload => {
          // this.setState(this.baseState)
          
          this.props.minusapproval(this.props.approvalCount)
        }
      )
    }

    getSeconds(time){
      var a = moment().utc().format('YYYY-MM-DD HH:mm:ss');
      var b = moment(time);
      var dateDiff = b.diff(a, 'seconds');
      return dateDiff
     
    }
    back = async (e)=>{
    this.props.navigation.navigate('DeactivateProduct');
    }
    deactivate = async ()=>{
        console.log("deactivate button");
      }
    submit =()=>{
      //this.props.navigation.navigate('Home');
    }
    TotalOrder = async (item)=>{
      if(this.state.deleteItem==true){
        this.setState({deleteItem:false,color:'white',deleteModal:true,deleted_id:item.id})
        
      }else{
        this.props.navigation.navigate('DeactivateProduct',{'item':item});
      }
     
      }

    messages =(item)=>{
      this.props.navigation.navigate('ApproveChat',{ item });
    }


    deleteReservation=async(id)=>{
      this.setState({refresh:false,successDelete:true,deleteModal:false})
      let data = new FormData();
      data.append('id', id);
      this.props.deleteApproval(id)
      fetch(PH+'/api/v1/approval/delete', {
        method: 'POST',
        headers: {
          'Accept':'application/json',
          'Content-Type': 'multipart/form-data',
          'Authorization': 'Bearer '+await AsyncStorage.getItem('token')
        },
        body:data
        }).then((response) => response.json())
            .then((responseJson) => {
              console.log("Delete Successful")
            
        })
            .catch((error) => {  
              console.log("ayay",error)   
        });
       
    }

        
    async onReject(id) {
        let data = new FormData();
        data.append('id', id);
        data.append('approved', 0);
        fetch(PH+'/api/v1/reservation/approvalstatus', {
          method: 'POST',
          headers: {
            'Accept':'application/json',
            'Content-Type': 'multipart/form-data',
            'Authorization': 'Bearer '+await AsyncStorage.getItem('token')
          },
          body:data
          }).then((response) => response.json())
              .then((responseJson) => {
                  this.setState({visibleSnack:true})
                 this.getApprovals()
          })
              .catch((error) => {
               
               console.log("ayay",error)
             
          });
      };   
    
  _onRefresh() {
    this.setState({refreshing: true});
    this.getApprovals()
  }
    
    async onApprove(id) {

      let user_id= await AsyncStorage.getItem('user_id') 
        let data = new FormData();
        data.append('id', id);
        data.append('approved', 1);
        fetch(PH+'/api/v1/reservation/approvalstatus', {
          method: 'POST',
          headers: {
            'Accept':'application/json',
            'Content-Type': 'multipart/form-data',
            'Authorization': 'Bearer '+await AsyncStorage.getItem('token')
          },
          body:data
          }).then((response) => response.json())
              .then((responseJson) => {
                  this.setState({visibleSnack:true})
                 this.getApprovals()
          })
              .catch((error) => {
               
                console.log("ayay",error)
              
          });
      };
      
      getCount(data){
      
        if(this.props.message_notif!==null){
          var result = this.props.message_notif.find(x => x.id === ""+data);
          console.log("huy",result);
          if(result!==undefined){
          return result.count
          }
          else{
            return 0
          }
          
        }
        
      }

    async getApprovals() {
      let approvalList= await AsyncStorage.getItem('approvalList')  
      if(approvalList!==null){
        this.props.loadApproval(JSON.parse(approvalList))
      }
      fetch(PH+'/api/v1/users/approval', {
          method: 'POST',
          headers: {
            'Accept':'application/json',
            'Content-Type': 'multipart/form-data',
            'Authorization': 'Bearer '+await AsyncStorage.getItem('token')
          },
          // body:data
          }).then((response) => response.json())
              .then(async (responseJson) => {
                console.log(responseJson)
                  if(responseJson.meta.approval.length>0){
                    this.props.loadApproval(responseJson.meta.approval)
                    this.setState({refresh:false,refreshing:false})
                    await AsyncStorage.setItem('approvalList', JSON.stringify(responseJson.meta.approval) )
                    .then( ()=>{
                    console.log("succesfully Saved Approval Lists")
                    } )
                    .catch( ()=>{
                    console.log('‘There was an error saving the product’')
                    } )
                    this.setState({refreshing:false})
                  }
                  else {
                   
                    this.props.loadApproval(null)
                    this.setState({noData:true,refreshing:false})
                  }
          })
              .catch((error) => {
                console.log("ayay",error)
             
          });
      };    
  render(){
    const resizeMode = 'cover';
    return (
  
      <View style={styles.container}>
        
      <ImageBackground
         style={{
           backgroundColor: '#ccc',
           flex: 1,
           resizeMode,
           position: 'absolute',
           width: '120%', 
           height: '100%',
           justifyContent: 'center',
           
         }}
         source={require('../../../assets/images/slice81-min.png')}
         />
       
       <View style={{flex: 1, flexDirection: 'row',justifyContent:'space-between', margin:15,marginTop:Dimensions.get('screen').height/50}}>
           <View>
          
         </View>
         <View>
          <Text  style={{ fontSize:Fsize3 ,alignSelf:'center',color:'#2db9b9',fontFamily:'KGPrimaryWhimsy'}}>Dine godkendelser</Text>
          </View>
          <View>
            {this.state.deleteItem==false?(
                <TouchableOpacity onPress={()=>this.setState({deleteItem:true,color:'#edaba6'})} loading={true}>
                <Image resizeMode='contain' source={require('../../../assets/images/slice50-min.png')} style={{width: 20,height: 20,}}></Image>
                
             </TouchableOpacity>
            ): <TouchableOpacity onPress={()=>this.setState({deleteItem:false,color:'white'})} loading={true}>
           <Text style={{fontSize: 13,color:'#2db9b9'}}>Færdig</Text>
            
         </TouchableOpacity>}
         <Dialog
          visible={this.state.deleteModal}
          onHardwareBackPress={() => { this.setState({ deleteModal: false }); } }
          onTouchOutside={() => {
            this.setState({ deleteModal: false });
          }}
          footer={
            <DialogFooter>
              <DialogButton
              
              textStyle={{color:'#404040'}}
                text="Ja"
                onPress={() => this.deleteReservation(this.state.deleted_id)}
              />
              <DialogButton
                textStyle={{color:'#404040'}}
                text="Ingen"
                onPress={() =>this.setState({ deleteModal: false })}
              />
            </DialogFooter>
          }
        >
          
          <DialogContent style={{width:wp('90%')}}>
          <Text adjustsFontSizeToFit={true} style={{color:'#404040',fontSize:15,alignSelf:'center',marginTop:30,textAlign:'center',textAlignVertical: "center",}}>Er du sikker på, at du vil
slette dette produkt?</Text>
          </DialogContent>
        </Dialog>
        
        
         </View>
      </View>
    {this.props.approvalList!==null&&this.props.approvalList.length>0?(
      <View style={styles.container3}>
        <Snackbar
          visible={this.state.visibleSnack}
          onDismiss={() => this.setState({ visibleSnack: false })}
          // action={{
          //   label: 'Undo',
          //   onPress: () => {
          //     // Do something
          //   },
          // }}
          style={{backgroundColor:'#5e5e5e'}}
          duration={2000}
        >
        Godkendt vellykket!
        </Snackbar>
         <Snackbar
          visible={this.state.successDelete}
          onDismiss={() => this.setState({ successDelete: false })}
          // action={{
          //   label: 'Undo',
          //   onPress: () => {
          //     // Do something
          //   },
          // }}
          style={{backgroundColor:'#5e5e5e'}}
          duration={3000}
        >
        Slet vellykket!
        </Snackbar>
 
      <Container style={{backgroundColor:'transparent'}} >
      <Content refreshControl={<RefreshControl refreshing={this.state.refreshing} onRefresh={this._onRefresh.bind(this)} />}>
      {this.props.approvalList.map((prop, key) => {
         return (
           
              <List showsHorizontalScrollIndicator={false} style={{backgroundColor:this.state.color,borderColor:'grey',borderWidth:.5,borderRadius: 15,marginBottom:3,margin:3,shadowColor: '#000',
              shadowOffset: { width: 0, height: 2 },
              shadowOpacity: 0.5,
              shadowRadius: 1.5,
              elevation: 1}} key={prop.id}>
                                                                                                                                                                                                                                                                                    
                  <ListItem button onPress={()=>{
                        this.TotalOrder(prop)
                      }}>
                   <Body style={{flexDirection:'row',alignContent:'center',alignSelf:'center',alignItems:'center'}}>
                    <ProgressiveImage  resizeMode='contain'  source={{ uri: prop.approval_product[0].images[0]}} style={{marginRight:30,height:60,width:60}}  />
                      
                    <View style={{flexDirection:'column'}}>
                      <Text style={{fontFamily:'KGPrimaryWhimsy',fontSize: 20,color:'grey'}}>{prop.approval_product[0].name} </Text>
                      {this.getSeconds(prop.expiry)>0&&prop.approved==null?(
                      <View style={{flexDirection:'row',alignItems:'center'}}>
                        
                      <Text style={{fontSize: 10,color:'grey'}}>Slutter om: </Text>
                      <CountDown
                          
                          until={this.getSeconds(prop.expiry)}
                          digitStyle={{backgroundColor: '#FFF', borderColor: 'grey'}}
                          digitTxtStyle={{color: 'grey'}}
                          timeLabelStyle={{color: 'red'}}
                          separatorStyle={{color: 'grey'}}
                          timeToShow={['D','H', 'M', 'S']}
                          timeLabels={{m: null, s: null}} 
                          showSeparator={true}
                          // onFinish={() => console.log('finished')}
                          // onPress={() => console.log('hello')}
                          size={10}
                          
                        />
                        
                       </View>):
                          null}
                           {this.getSeconds(prop.expiry)<0&&prop.approved==null?(
                          <View style={{flexDirection:'row',alignItems:'center'}}>
                            <Text style={{fontSize: 10,color:'grey'}}>Midleritidlig deaktiveret</Text>
                         </View>):
                          null}
                           {prop.approved==1?(
                          <Block>
                            <Text style={{fontSize: 10,color:'#2db9b9'}}>Godkent/aftal naemere</Text>
                            <Text style={{fontSize: 10,color:'#2db9b9'}}>se samlet bestilling</Text>
                         </Block>):
                          null}
                        </View>
                        
                    </Body>
                    {prop.approved===null&&this.getSeconds(prop.expiry)>0 ?
                    <View >
                    <Right>
                     <Thumbnail square small source={require('../../../assets/images/slice60-min.png')} style={{marginRight:5,padding:2,resizeMode:'contain'}}/>
                     <Block row style={{marginTop:20}}>
                    <TouchableOpacity onPress={()=>this.onReject(prop.id)} style={{alignItems:'center',height:30,width:50,alignItems:'center',padding:2,marginTop:3, margin:-5,backgroundColor:'transparent'}}>
                    <Image resizeMode='contain' style={{height:20,width:20}} source={require('../../../assets/icons/nogo.png')}></Image>
                    </TouchableOpacity>
                    <TouchableOpacity onPress={()=>this.onApprove(prop.id)} style={{alignItems:'center',height:30,width:50,alignItems:'center',padding:2,marginTop:3, margin:-3,backgroundColor:'transparent'}}>
                    <Image resizeMode='contain' style={{height:20,width:20}} source={require('../../../assets/icons/ok.png')}></Image>
                    </TouchableOpacity>
                    </Block>
                    </Right>
                  
                    </View>
                     
                    :   null
                    }
                     {prop.approved===1 ?
                    <View>
                    <TouchableOpacity onPress={()=>this.messages(prop)} loading={true}>
                        {this.props.message_notif!==null&&this.getCount(prop.owner_id)!==0?
                              <Badge
                              status="warning"
                              value={this.getCount(prop.owner_id)}
                              containerStyle={{ position: 'absolute', top: -5,right:1,zIndex:100 }}
                          />
                          :null} 
                   <Thumbnail resizeMode='contain' square small source={require('../../../assets/images/slice52-min.png')} style={{marginRight:5}}/>
                   <ProgressiveImage source={{ uri: prop.approval_user.image_url}} style={{marginRight:5,height:30,width:30,borderRadius: 30/ 2}}/>
                   </TouchableOpacity>
                   </View>
                     :null}
                     {prop.approved === 0 ?
                     <Right>
                     <Icon name="ios-arrow-forward" />
                     </Right>
                     :null}
                       {prop.approved === null&&this.getSeconds(prop.expiry)<0 ?
                     <Right>
                     <Icon name="ios-arrow-forward" />
                     </Right>
                     :null}
                  </ListItem>
                  {/* <Icon ios='ios-arrow-forward' android='md-arrow-dropright' iconRight/> */}
             </List>           
         );
         
      })}
        </Content>
      
       </Container>
     
      </View>
      ):null}
      {this.props.approvalList==null&&this.state.noData===false?(
        <View style={styles.container3}>
        <Container style={{backgroundColor:'transparent'}} >
        <Content>
        <LottieView source={require('../../../assets/images/loading2.json')} autoPlay loop style={{width:200,height:200,alignSelf:'center'}} /> 
          </Content>
         </Container>
       
        </View>
      ):null}
       {(this.props.approvalList==null||this.props.approvalList.length<=0)?(
      <View style={styles.container3}>
        
      <Container style={{backgroundColor:'transparent'}} >
      <Content scrollEnabled={false}>
      <Text style={{alignSelf:'center',fontSize:20,fontFamily:'KGPrimaryWhimsy',color:'grey',marginTop:40}}>Ingen ventende godkendelser endnu</Text>
        </Content>
        </Container>

      </View>
      ):null}
            
  </View>
   
    );
  }
  }
   
  const mapStateToProps = (state) =>{
    return{
      approvalList:state.count.approvalList,
      approvalCount:state.count.approvalBadgeCount,
      message_notif: state.count.messages_notif
    }
  }

  const mapDispatchToProps = (dispatch) =>{
    return{
      loadApproval:(data)=>dispatch(loadApproval(data)),
      minusapproval:(count)=>dispatch(minusapprovalCount(count)),
      deleteApproval:(data)=>dispatch(deleteApproval(data)),
      update_message_notif:(data)=>dispatch(messages_notif(data))
    }
  }
  
export default connect(mapStateToProps,mapDispatchToProps)(Index)
   
const styles = StyleSheet.create({
  container: {
    flex: 1,
    //alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: 'transparent'
  },
  container2: {
    flex: 1,
    //alignItems: 'center',
    //justifyContent: 'flex-start',
    
    backgroundColor: 'transparent'
    //bottom: Dimensions.get('screen').height /5
  },
  textstyle:{
    fontFamily:'KGPrimaryWhimsy',
    fontSize:40
  },
  container3: {
    position:'absolute',
    backgroundColor: 'transparent',
    flex: 1,
    height:hp('79%'),
    width:wp('100%'),
    bottom:0,
    top:45
    //justifyContent: "flex-end",
    //alignItems: "center"
  },
  dropdown: {
    height: Dimensions.get('screen').width /7,
    padding: 5,
    position:'relative',
    margin:5,
    borderWidth: .2,
    borderRadius: 12,
    textShadowColor: 'black',
    borderColor: 'black',
    marginBottom: 10,
    backgroundColor: 'white',
    opacity: 0.9,
    fontFamily:'KGPrimaryWhimsy',
    fontSize:20,
    alignContent:'center'
  },
  wrpper: {
    opacity: 0.9,
    fontSize:20,
    fontFamily:'KGPrimaryWhimsy',
    flexDirection: 'row', justifyContent: 'space-around',   
  },
  button: {
    backgroundColor: '#54969c',
    borderColor: 'white',
    borderWidth: .5,
    borderRadius: 12,
    color: 'white',
    overflow: 'hidden',
    padding: 12,
    textAlign:'center',
    width: 300,
    height: 48,
    opacity: 0.9,
    fontSize:20,
    fontFamily:'KGPrimaryWhimsy'
  },
  approve: {
    backgroundColor: '#54969c',
    borderColor: 'white',
    borderWidth: .5,
    borderRadius: 1,
    color: 'white',
    overflow: 'hidden',
    padding: 12,
    textAlign:'center',
    width: 40,
    height: 20,
    opacity: 0.9,
    fontSize:14,
    fontFamily:'KGPrimaryWhimsy'
  },
});

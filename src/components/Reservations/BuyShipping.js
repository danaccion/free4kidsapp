import React, { Component } from 'react';
import { ScrollView, StyleSheet,Text,View,ImageBackground,Image,TouchableOpacity,Platform,Dimensions,TouchableHighlight,ListView,FlatList } from 'react-native';
import Ionicons from 'react-native-vector-icons/Ionicons'
// import { Picker } from 'react-native-picker-dropdown';
import BackButton from '../../BackButton';
import { Container, Header, Content, Form, Item, Picker,Icon,Input,Card,CardItem,Body ,Thumbnail,Left,Button,Right,List,ListItem } from 'native-base';
import HamburgerIcon from '../../icons/HamburgerIcon';
import HeartBagBurger from '../../icons/HeartBagBurger';
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';
import SmoothPicker from "react-native-smooth-picker";
import Tooltip from 'react-native-walkthrough-tooltip';


const Bubble = props => {
  const { children, selected, horizontal } = props;
  return (
    <View
      style={[
        horizontal ? styles.itemStyleHorizontal : styles.itemStyleVertical,
        selected &&
          (horizontal
            ? styles.itemSelectedStyleHorizontal
            : styles.itemSelectedStyleVertical)
      ]}
    >
      <Text
        style={{
          textAlign: "center",
          fontSize: selected ? 20 : 17,
          color: selected ? "white" : "#b2d1e0",
          fontWeight: selected ? "bold" : "normal"
        }}
      >
        {children}
      </Text>
    </View>
  );
};


export default class BuyShipping extends Component {
  static navigationOptions = () => {
    return {
        headerLeft: <HamburgerIcon/>,
        headerTitle: (
            <Image
            resizeMode="contain"
            source={require('../../../assets/images/slice2.png')}
            style={{ width: 10, height:10 , flex:4,justifyContent: 'space-around', alignItems: 'center', padding: 25,}}
          />
          ),
        headerRight: <HeartBagBurger/>
    };
};
// constructor(props) {
//   super(props);
//   this.state = {
//     selected2: undefined,
//     selected:undefined
//   };
// }
// onValueChange2(value: string) {
//   this.setState({
//     selected2: value
//   });
// }
    back = async ()=>{
    this.props.navigation.navigate('Home');
    }
    submit =()=>{
      //this.props.navigation.navigate('Home');
    }
   
     state = {
        selected2: undefined,
        selected:2,
        toolTipVisible:true,
      };
   
    onValueChange2(value) {
      this.setState({
        selected2: value
      });
    }
    handleChange(index) {
      this.setState(
        {
          selected: index
        },
        () => {
          console.log(index);
          // this.refList.refs.smoothPicker.scrollToIndex({
          //   animated: false,
          //   index: index,
          //   viewOffset: -30
          // });
        }
      );
    }
    initialArr = [
      {
        id:1,
        user_id:1,
        details: "Tidligere kendt som Mobilporto",
      },
      {
        id:2,
        user_id:1,
        details: "Levering 5 dage eller naeste hverdag",
      },
      {
        id:3,
        user_id:1,
        details: "Levering til brevkasse",
      },
      
    ];
  render(){
    const resizeMode = 'cover';
    const { selected } = this.state;
    const kiloList=['250 g','500 g','1 kg','2 kg','5 kg'];
    
    return (
      <View style={styles.container}>
       
      <ImageBackground
         style={{
           backgroundColor: '#ccc',
           flex: 1,
           resizeMode,
           position: 'absolute',
           width: '100%', 
           height: '100%',
           justifyContent: 'center',
           
         }}
         source={require('../../../assets/images/slice81-min.png')}
         />
       
       <View style={{flex: 1, flexDirection: 'row',justifyContent:'space-between', margin:15,marginTop:Dimensions.get('screen').height/50}}>
           
           <BackButton/>
        
         <View>
          <Text  style={{ fontSize: 25 ,alignSelf:'center',color:'#2db9b9',fontFamily:'KGPrimaryWhimsy'}}> Køb fragt</Text>
          </View>
          <View></View>
      </View>
      <View style={styles.container2}>
      <Container>

      <Content>
          <Card style={{backgroundColor:'#0099cc', marginBottom:hp('5%'),height:hp('20%')}}>
            <CardItem scrollEnabled={false} style={{backgroundColor:'transparent',borderWidth:1,borderColor:'white',height:50,width:wp('55%'),marginLeft:20,marginTop:hp('1%')}}>
            <Content scrollEnabled={false} bounces={false}>
          <Form style={{marginTop:-15}}>
            <Item picker scrollEnabled={false} >
            <Thumbnail small square source={{ uri: 'https://cdn.countryflags.com/thumbs/denmark/flag-400.png' }} />
              <Picker
                mode="dropdown"
                iosIcon={<Icon name="arrow-down" />}
                style={{ width: undefined,color:'white' }}
                placeholder="Select your SIM"
                placeholderStyle={{ color: "#bfc6ea" }}
                placeholderIconColor="#007aff"
                selectedValue={this.state.selected2}
                onValueChange={this.onValueChange2.bind(this)}
                
              >
                <Picker.Item style={{color:'white'}} label="TIL DANMARK" value="key0" />
                <Picker.Item style={{color:'white'}}  label="ATM Card" value="key1" />
                <Picker.Item style={{color:'white'}}  label="Debit Card" value="key2" />
                <Picker.Item style={{color:'white'}}  label="Credit Card" value="key3" />
              </Picker>
            </Item>
          </Form>
        </Content>
      
            </CardItem>
            <CardItem style={{backgroundColor:'white',borderWidth:1,borderColor:'white',height:hp('5%'),width:wp('30%'),marginTop:hp('2%'),alignSelf:'center',alignContent:'center'}}>
              <Text style={{color:'#0099cc',fontSize:hp('2%')}}>Maks. vægt</Text>
            </CardItem>
            <CardItem  style={{backgroundColor:'#0099cc'}} header>
            
          <View style={styles.wrapperHorizontal}>
            <SmoothPicker
              onScrollToIndexFailed={() => {}}
              initialScrollToIndex={selected}
              // ref={ref => (this.refList = ref)}
              keyExtractor={(_, index) => index.toString()}
              horizontal={true}
              showsHorizontalScrollIndicator={false}
              bounces={true}
              offsetSelection={-1}
              scrollAnimation
              data={kiloList}
              onSelected={({ item, index }) => this.handleChange(index)}
              renderItem={({ item, index }) => (
                <Bubble selected={index === selected}>{item}</Bubble>
              )}
            />
          </View>
            
            
            </CardItem>
            
          </Card>
          
        </Content>
        <Container style={{marginTop:-hp('22%')}}>
        <Content padder>
        <View padder>
          <Text style={{fontSize:hp('2.8%'),marginLeft:wp('2%')}} padder>Vælg Produkt</Text>
          </View>
          <Card>
          <CardItem style={{borderTopColor:'#0099cc',borderTopWidth:2}}>
              <Left>
                <Button transparent>
                  <Text style={{fontSize:hp('3%'),fontWeight:'500'}}>Brev</Text>
                </Button>
              </Left>
              <Right style={{marginRight:4}}>
              <Text style={{marginRight:-wp('20%'),fontSize:hp('3%'),color:'#0099cc',fontWeight:'100'}}>fra <Text style={{color:'#0099cc',fontSize:hp('3%'),fontWeight:'bold'}}>60</Text></Text>
              </Right>
              <Right>
              <Text style={{color:'#0099cc',fontWeight:'100',paddingBottom:hp('1%')}}>DKK</Text>
              </Right>
            </CardItem>
            <CardItem style={{backgroundColor:'white',borderWidth:.2,borderColor:'#5c5e5d',height:hp('7%'),width:wp('35%'),marginLeft:wp('3%')}}>
              <Text style={{color:'#0099cc',fontSize:hp('2%'),fontWeight:'600'}}>IKKE SPORBAR</Text>
            </CardItem>

            <CardItem>
              
                <View style={{flexDirection:'column',marginLeft:-wp('5%'),marginBottom:hp('10%')}}>
               {this.initialArr.map((prop, key) => {
                    return (
                      <View key={prop.id}>
                        <List >
                            <ListItem style={{borderColor:'transparent',marginBottom:-hp('5%')}}>
                                <Text style={{color:'#0099cc',fontSize:hp('5%')}}>{`\u2022`}</Text><Text style={{color:'#464747',fontSize:hp('2.5%'),fontWeight:'100'}}>{prop.details}</Text>
                            </ListItem>
                      </List> 
                     
                    </View>
                );   
               })} 
               </View>
            
            </CardItem>
          </Card>
          <Card>
          <CardItem style={{borderTopColor:'#0099cc',borderTopWidth:2}}>
              <Left>
                <Button transparent>
                  <Text style={{fontSize:hp('3%'),fontWeight:'500'}}>Pakke</Text>
                </Button>
              </Left>
              <Right style={{marginRight:5}}>
              <Text style={{marginRight:-wp('20%'),fontSize:hp('3%'),color:'#0099cc',fontWeight:'100'}}>fra <Text style={{color:'#0099cc',fontSize:hp('3%'),fontWeight:'bold'}}>60</Text></Text>
              </Right>
              <Right>
              <Text style={{color:'#0099cc',fontWeight:'100',paddingBottom:hp('.5%')}}>DKK</Text>
              </Right>
            </CardItem>
            <CardItem style={{backgroundColor:'white',borderWidth:.2,borderColor:'#5c5e5d',height:hp('7%'),width:wp('30%'),marginLeft:wp('3%')}}>
              <Text style={{color:'#0099cc',fontSize:hp('2%'),fontWeight:'600'}}>SPORBAR</Text>
            </CardItem>
            <CardItem>
              <Left>
                <Button transparent textStyle={{color: '#87838B'}}>
                  <Icon name="logo-github" />
                  <Text>1,926 stars</Text>
                </Button>
              </Left>
            </CardItem>
          </Card>
        </Content>
      </Container>
      </Container>
        </View>
      
  </View>
   
    );
  }
  }
   

const styles = StyleSheet.create({
  container: {
    flex: 1,
    //alignItems: 'center',
    justifyContent: 'flex-end',
    backgroundColor: 'transparent'
  },
  container2: {
    position:'absolute',
    backgroundColor: 'transparent',
    flex: 1,
    height:hp('75%'),
    width:wp('100%'),
    bottom:0,
  },
  textstyle:{
    fontFamily:'KGPrimaryWhimsy',
    fontSize:40
  },
  container3: {
    flex: 1,
    position:'absolute',
    alignItems: "center",
    alignContent:'center',
    justifyContent: 'flex-end',
  },
  dropdown: {
    height: Dimensions.get('screen').width /7,
    padding: 5,
    position:'relative',
    margin:5,
    borderWidth: .2,
    borderRadius: 12,
    textShadowColor: 'black',
    borderColor: 'black',
    marginBottom: 10,
    backgroundColor: 'white',
    opacity: 0.9,
    fontFamily:'KGPrimaryWhimsy',
    fontSize:20,
    alignContent:'center'
  },
  wrpper: {
    opacity: 0.9,
    fontSize:20,
    fontFamily:'KGPrimaryWhimsy',
    flexDirection: 'row', justifyContent: 'space-around',   
  },
  button: {
    alignSelf:'center',
    marginLeft:10,
    marginRight:20,
    margin:10,
    backgroundColor: 'white',
    borderColor: 'black',
    borderWidth: 3,
    borderTopColor:'red',
    borderRadius: 12,
    color: 'white',
    overflow: 'hidden',
    textAlign:'center',
    width: 350,
    height: 48,
    // opacity: 0.9,
    fontSize:20,
    fontFamily:'KGPrimaryWhimsy'
  },
  button2: {
    backgroundColor: '#54969c',
    borderColor: 'white',
    borderWidth: .5,
    borderRadius: 12,
    color: 'white',
    overflow: 'hidden',
    padding: 12,
    textAlign:'center',
    width: 350,
    height: 48,
    opacity: 0.9,
    fontSize:20,
    fontFamily:'KGPrimaryWhimsy'
  },
  wrapperHorizontal: {
    width: wp('80%'),
    height: 70,
    justifyContent: 'space-between',
    alignItems: "center",
    alignSelf:'center',
    marginRight: 10,
    marginTop:-hp('1%'),
    // color: "black",
  },
  wrapperVertical: {
    width: 100,
    height: 300,
    justifyContent: 'space-between',
    alignItems: "center",
    margin: "auto",
    color: "black"
  },
  itemStyleVertical: {
    marginTop: 10,
    marginBottom: 10,
    width: 50,
    height: 50,
    marginRight:wp('5%'),
    // paddingTop: 13,
    // borderWidth: 1,
    // borderColor: "grey",
    // borderRadius: 25
  },
  itemSelectedStyleVertical: {
    // paddingTop: 11,
    color:'red',
    // borderWidth: 2,
    // borderColor: "#DAA520"
  },
  itemStyleHorizontal: {
    marginLeft: 50,
    marginRight: 50,
    width: 50,
    height: 50,
    // paddingTop: 13,
    // borderWidth: 1,
    // borderColor: "grey",
    // borderRadius: 25
  },
  itemSelectedStyleHorizontal: {
    paddingTop: 11,
    borderWidth: 2,
    borderColor: "#DAA520"
  }
});

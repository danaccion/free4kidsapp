import React, { Component } from 'react';
import { ScrollView, StyleSheet,View,ImageBackground,Image,TouchableOpacity,Platform,Dimensions,Text,AsyncStorage,RefreshControl, Alert} from 'react-native';
import Ionicons from 'react-native-vector-icons/Ionicons'
import { Picker } from 'react-native-picker-dropdown';
import BackButton from '../../BackButton';
import HamburgerIcon from '../../icons/HamburgerIcon';
import HeartBagBurger from '../../icons/HeartBagBurger';
import LottieView from 'lottie-react-native';
import { Container, Content, List, ListItem, Thumbnail,Icon,Button,InputGroup,Input,Body,Right } from 'native-base';
import {Badge} from 'react-native-elements';
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';
import { Asset } from 'expo-asset';
import CountDown from 'react-native-countdown-component';
import moment from "moment";
import { PH } from 'react-native-dotenv';
import ProgressiveImage from '../ProgressiveImage';
import ProgressiveImageWithCase from '../ProgressiveImageWC';
import { Snackbar } from 'react-native-paper';
import Dialog, {DialogFooter,DialogButton,SlideAnimation,DialogContent } from 'react-native-popup-dialog';
import { connect } from 'react-redux';
import {loadReservation,minusreserveCount,deleteReservation,messages_notif} from '../actions/index';
const Fsize3 = Platform.OS === 'ios' ?25 : 20;
async function loadResourcesAsync() {
  await Promise.all([
    Asset.loadAsync([
      require('../../../assets/images/slice81-min.png')
    ]),
  ]);
}

class Index extends Component {
  static navigationOptions = () => {
    return {
        headerLeft: <HamburgerIcon/>,
        headerTitle: (
            <Image
            resizeMode="contain"
            source={require('../../../assets/images/slice2.png')}
            style={{ width: 10, height:10 , flex:4,justifyContent: 'center', alignItems: 'center', padding: 25,bottom:10}}
          />
          ),
        headerRight: <HeartBagBurger/>
    };
};
 async componentDidMount(){

  this.didBlurSubscription = this.props.navigation.addListener(
  'didFocus',
  payload => {
    this.props.minusreserve(this.props.reserveCount)
  }
  
);
this.didBlurSubscription = this.props.navigation.addListener(
  'didBlur',
  payload => {
    this.setState(this.baseState)
    // this.getReservations()
  }
)
// this.didBlurSubscription = this.props.navigation.addListener(
//   'willFocus',
//   payload => {
//     // this.setState(this.baseState)
//     this.getReservations()
//   }
  
// );
 
this.getReservations()

// Remove the listener when you are done
      loadResourcesAsync();
      
  }

   

 
  componentWillUnmount() {
    // Remove the event listener
    this.didBlurSubscription.remove();
  }

 
  state = {
      username: '',
      password: '',
      ButtonStateHolder : true,
      loading: false,
      fontLoaded:false,
      preferences:'',
      deleteItem:false,
      noData:false,
      color:'white',
      successDelete:false,
      deleteModal:false,
      deleted_id:null,
      refreshing:false

    };
   baseState = this.state 

    back = async ()=>{
    this.props.navigation.navigate('Home');
    }

    async _onRefresh() {
      this.setState({refreshing: true});
      this.getReservations();
    }

    deleteReservation=async(id)=>{
      let data = new FormData();
      data.append('id', id);
      this.props.deleteReservation(id)
      fetch(PH+'/api/v1/reservation/delete', {
        method: 'POST',
        headers: {
          'Accept':'application/json',
          'Content-Type': 'multipart/form-data',
          'Authorization': 'Bearer '+await AsyncStorage.getItem('token')
        },
        body:data
        }).then((response) => response.json())
            .then((responseJson) => {})
            .catch((error) => {
              // this.setState({
              //   ButtonStateHolder : false,
              //   loading: false
              // })
            // alert(error); 
            });
        this.setState({refresh:false,successDelete:true,deleteModal:false})
    }

    TotalOrder = async (item)=>{
      if(this.state.deleteItem==true&&item.approved==null){
        this.setState({deleteModal:true,deleted_id:item.id})
      }if(this.state.deleteItem==false&&item.approved==1){
        this.props.navigation.navigate('TotalOrder',{'user_id':item.id});
      }
     
      }
      seeTotal = async (id)=>{
          this.props.navigation.navigate('TotalOrder',{'user_id':id});
      }
    messages =async (item)=>{

      let owner_id = item.owner_id.toString()
      let product_id = item.product_id.toString()
      AsyncStorage.setItem('owner_id', owner_id);
      AsyncStorage.setItem('product_id',product_id);
      this.props.navigation.navigate('ReservationChat',{ item });
    }
    
  getSeconds(time){
    var a = moment().utc().format('YYYY-MM-DD HH:mm:ss');
    var b = moment(time);
    var dateDiff = b.diff(a, 'seconds');
    return +dateDiff
  }
  
  async getReservations() {
        let user_id= await AsyncStorage.getItem('user_id') 
        let reservationlist=await AsyncStorage.getItem('reservationList');
        if(reservationlist!==null){
          this.props.loadReservation(JSON.parse(reservationlist))
        }
        //else {
            let data = new FormData();
            data.append('user_id', user_id);
            fetch(PH+'/api/v1/users/reservation', {
            method: 'POST',
            headers: {
              'Accept':'application/json',
              'Content-Type': 'multipart/form-data',
              'Authorization': 'Bearer '+await AsyncStorage.getItem('token')
            },
            body:data
            }).then((response) => response.json())
                .then(async (responseJson) => {
                    if(responseJson.meta.reservation.length>0){
                      this.props.loadReservation(responseJson.meta.reservation)
                      this.setState({refresh:false,refreshing:false})
                      await AsyncStorage.setItem('reservationList', JSON.stringify(responseJson.meta.reservation) )
                      .then( ()=>{
                      console.log("succesfully Saved")
                      } )
                      .catch( ()=>{
                      console.log('‘There was an error saving the product’')
                      } )
                    }
                    else {
                      console.log("ayay")
                      this.props.loadReservation(null)
                      this.setState({noData:true,refreshing:false})
                    }
            })
            .catch((error) => {
                if(reservationlist.length>0){
                  this.props.loadReservation(JSON.parse(reservationlist))
                }else{
                  this.props.loadReservation(null)
                }
                this.setState({noData:true,refreshing:false})
               console.log(error)
            });
        //}
    };    

    getCount(data){
      
      if(this.props.message_notif!==null){
        var result = this.props.message_notif.find(x => x.id === ""+data);
        if(result!==undefined){
        return result.count
        }
        else{
          return 0
        }
        
      }
      
    }
    
  render(){
    console.disableYellowBox = true;
    return (
  
      <View style={styles.container}>
       
      <ImageBackground
         style={{
            backgroundColor: '#ccc',
          // flex: 1,
          resizeMode:'stretch',
          position: 'absolute',
          width: wp('126%'),
          height: hp('100%'),
           
         }}
         source={require('../../../assets/images/slice81-min.png')}
         />
       
       <View style={{flex: 1, flexDirection: 'row',justifyContent:'space-between', margin:15,marginTop:Dimensions.get('screen').height/50}}>
           <View>
         </View>
         <View>
          <Text  style={{ fontSize: Fsize3 ,alignSelf:'center',color:'#2db9b9',fontFamily:'KGPrimaryWhimsy'}}>Dine reservationer</Text>
          </View>
          <View>
            {this.state.deleteItem==false?(
                <TouchableOpacity onPress={()=>this.setState({deleteItem:true,color:'white'})} >
                <Image resizeMode='contain' source={require('../../../assets/images/slice50-min.png')} style={{width: 20,height: 20,}}></Image>
                
             </TouchableOpacity>
            ): <TouchableOpacity onPress={()=>this.setState({deleteItem:false,color:'white'})}>
           <Text style={{fontSize: 13,color:'#2db9b9'}}>Færdig</Text>
            
         </TouchableOpacity>}
          
         </View>
      </View>
      {this.props.reservationList===null&&this.state.noData===false?(
        <View style={styles.container3}>
        <Container style={{backgroundColor:'transparent'}} >
        <Content>
        <LottieView source={require('../../../assets/images/loading2.json')} autoPlay loop style={{width:200,height:200,alignSelf:'center'}} /> 
          </Content>
         </Container>
       
        </View>
      ):null
      }
       {this.state.noData===true&&this.props.reservationList===null?(
        <View style={styles.container3}>
        <Container style={{backgroundColor:'transparent'}} >
        <Content scrollEnabled={false}>
        <Text style={{alignSelf:'center',fontSize:20,fontFamily:'KGPrimaryWhimsy',color:'grey',marginTop:40}}>Ingen reservation endnu..</Text>
          </Content>
         </Container>
       
        </View>
      ):null
  }     
   {this.props.reservationList!==null&&this.state.noData===false?(
  <View style={styles.container3}>
        <Dialog
          visible={this.state.deleteModal}
          onTouchOutside={() => {
            this.setState({ deleteModal: false });
          }}
          footer={
            <DialogFooter>
              <DialogButton
              
              textStyle={{color:'#404040'}}
                text="Ja"
                onPress={() => this.deleteReservation(this.state.deleted_id)}
              />
              <DialogButton
                textStyle={{color:'#404040'}}
                text="Ingen"
                onPress={() =>this.setState({ deleteModal: false })}
              />
            </DialogFooter>
          }
        >
          
          <DialogContent style={{width:wp('90%')}}>
          <Text adjustsFontSizeToFit={true} style={{color:'#404040',fontSize:15,alignSelf:'center',marginTop:30,textAlign:'center',textAlignVertical: "center",}}>Er du sikker på, at du vil
slette dette produkt?</Text>
          </DialogContent>
        </Dialog>
      <Container style={{backgroundColor:'transparent'}} >
      <Content  refreshControl={<RefreshControl refreshing={this.state.refreshing} onRefresh={this._onRefresh.bind(this)} />}>
      {this.props.reservationList.map((prop, key) => {
         return (
               
              <List  showsVerticalScrollIndicator={false} style={{flex:1,backgroundColor:prop.approved!==1?this.state.color:'white',borderColor:'grey',borderWidth:.5,borderRadius: 5,marginBottom:1,margin:5,shadowColor: '#000',
              shadowOffset: { width: 0, height: 2 },
              shadowOpacity: 0.5,
              shadowRadius: 1.5,}} key={prop.id}>
                  <ListItem  onPress={()=>this.TotalOrder(prop)}>
                   <Body style={{flexDirection:'row',alignContent:'center',alignSelf:'center',alignItems:'center'}}>
      
                    <ProgressiveImageWithCase resizeMode='contain'  source={{ uri:prop.products.length>0?prop.products[0].images[0]:null}}style={{marginRight:30,height:60,width:60,left:1}} />

                      <View style={{flexDirection:'column'}}>
                      {prop.approved==1&&prop.products[0].closed==="Yes"?(
                      <Text style={{fontFamily:'KGPrimaryWhimsy',fontSize: Platform.OS === 'ios' ?18 : 17,color:prop.approved==1?'#429985':'grey',marginBottom:-1}}>Tillykke - Aftale afsluttet</Text>
                      ):<Text style={{fontFamily:'KGPrimaryWhimsy',fontSize: Platform.OS === 'ios' ?18 : 17,color:prop.approved==1?'#429985':'grey',marginBottom:-1}}>{prop.products.length>0?prop.products[0].name:null} </Text>
                      }
                      {this.getSeconds(prop.expiry)>0&&prop.approved!==1?(
                    <View style={{flexDirection:'row',alignItems:'center'}}>
                        
                    <Text style={{fontSize: 10,color:'grey'}}>Slutter om: </Text>
                    <CountDown
                        
                        until={this.getSeconds(prop.expiry)}
                        digitStyle={{backgroundColor: '#FFF', borderColor: 'grey'}}
                        digitTxtStyle={{color: 'grey'}}
                        timeLabelStyle={{color: 'red'}}
                        separatorStyle={{color: 'grey'}}
                        timeToShow={['D','H', 'M', 'S']}
                        timeLabels={{m: null, s: null}} 
                        showSeparator={true}
                        //onFinish={() => console.log('finished')}
                        //onPress={() => console.log('hello')}
                        size={10}
                        
                      />
                      
                     </View> 
                   
                         ): null}

                         {this.getSeconds(prop.expiry)<0&&prop.approved!==1?(
                           <View style={{flexDirection:'row',alignItems:'baseline',justifyContent:'flex-start'}}>
                        
                           <Text style={{color:'grey',fontSize:Platform.OS=='ios'?12:9}}>Op på hesten igen :-)</Text>
                           </View>
                         ):null}
                         {prop.approved==1?(
                            <View style={{flexDirection:'row',alignItems:'baseline',justifyContent:'flex-start'}}>
                              <TouchableOpacity onPress={()=>this.seeTotal()}>
                            <Text style={{color:prop.approved==1?'#429985':'grey',fontSize:Platform.OS=='ios'?12:9}}>Se samlet bestilling</Text>
                            </TouchableOpacity>
                            </View>
                         ):null}
                       {/* <View style={{flexDirection:'row',alignItems:'baseline',justifyContent:'flex-end'}}>
                         <TouchableOpacity onPress={()=>this.seeTotal()}>
                       <Text style={{color:'grey',fontSize:Platform.OS=='ios'?12:9}}>Aftale lukket</Text>
                       </TouchableOpacity>
                       </View> */}
                        </View>
                      
                    </Body> 
  
                    <Right>
                    {prop.approved==1?
                        <View>
                          <TouchableOpacity onPress={()=> this.messages(prop)} loading={true}>
                          {this.props.message_notif!==null&&this.getCount(prop.owner_id)!==0?
                              <Badge
                              status="warning"
                              value={this.getCount(prop.owner_id)}
                              containerStyle={{ position: 'absolute', top: -5,right:1,zIndex:100 }}
                          />
                          :null} 
                            
                       
                         <Thumbnail resizeMode='contain' square  small source={require('../../../assets/images/slice52-min.png')} style={{marginRight:5}}/>
                         <ProgressiveImage source={{ uri: prop.users.image_url}} style={{marginRight:5,height:30,width:30,borderRadius: 30/ 2}}/>
                         </TouchableOpacity>
                         </View>
                            :
                            <View>
                            
                            <Thumbnail resizeMode='contain' square small source={require('../../../assets/images/slice66-min.png')} style={{position:'absolute',  height:20, top: -5,right:0,zIndex:100}}/>
                           <Thumbnail resizeMode='contain' square small source={require('../../../assets/images/slice52-min.png')} style={{marginRight:5}}/>
                           
                           </View>
                      }
                     
                   
                     
                    {/* <ProgressiveImage source={prop.users.image_url===null? require('../../../assets/icons/user_image.png'):{ uri: prop.users.image_url}}  style={{marginRight:5,height:30,width:30,borderRadius: 30/ 2}}/> */}
                   
                    </Right>
                     
                  </ListItem>
                  {/* <Icon ios='ios-arrow-forward' android='md-arrow-dropright' iconRight/> */}
             </List>           
         );
         
      })}
        </Content>
       </Container>
     
      </View> ):null}
      <Snackbar
          visible={this.state.successDelete}
          onDismiss={() => this.setState({ successDelete: false })}
          // action={{
          //   label: 'Undo',
          //   onPress: () => {
          //     // Do something
          //   },
          // }}
          style={{backgroundColor:'#5e5e5e'}}
          duration={3000}
        >
        Slet succes!
        </Snackbar>
       
  </View>
   
    );
  }
  }

  const mapStateToProps = (state) =>{
    return{
      reservationList:state.count.reservationList,
      reserveCount:state.count.reservationBadgeCount,
      message_notif: state.count.messages_notif
    }
  }

  const mapDispatchToProps = (dispatch) =>{
    return{
      loadReservation:(data)=>dispatch(loadReservation(data)),
      minusreserve:(count)=>dispatch(minusreserveCount(count)),
      deleteReservation:(data)=>dispatch(deleteReservation(data)),
      update_message_notif:(data)=>dispatch(messages_notif(data))
    }
  }
  
export default connect(mapStateToProps,mapDispatchToProps)(Index)
   

const styles = StyleSheet.create({
  container: {
    flex: 1,
    //alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: 'transparent'
  },
  container2: {
    flex: 1,
    //alignItems: 'center',
    //justifyContent: 'flex-start',
    
    backgroundColor: 'transparent'
    //bottom: Dimensions.get('screen').height /5
  },
  textstyle:{
    fontFamily:'KGPrimaryWhimsy',
    fontSize:40
  },
  container3: {
    position:'absolute',
    backgroundColor: 'transparent',
    flex: 1,
    // height:hp('79%'),
    width:wp('100%'),
    bottom:0,
    top:45
    
    //justifyContent: "flex-end",
    //alignItems: "center"
  },
  dropdown: {
    height: Dimensions.get('screen').width /7,
    padding: 5,
    position:'relative',
    margin:5,
    borderWidth: .2,
    borderRadius: 12,
    textShadowColor: 'black',
    borderColor: 'black',
    marginBottom: 10,
    backgroundColor: 'white',
    opacity: 0.9,
    fontFamily:'KGPrimaryWhimsy',
    fontSize:20,
    alignContent:'center'
  },
  wrpper: {
    opacity: 0.9,
    fontSize:20,
    fontFamily:'KGPrimaryWhimsy',
    flexDirection: 'row', justifyContent: 'space-around',   
  },
  button: {
    backgroundColor: '#54969c',
    borderColor: 'white',
    borderWidth: .5,
    borderRadius: 12,
    color: 'white',
    overflow: 'hidden',
    padding: 12,
    textAlign:'center',
    width: 300,
    height: 48,
    opacity: 0.9,
    fontSize:20,
    fontFamily:'KGPrimaryWhimsy'
  },
});

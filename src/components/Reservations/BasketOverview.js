import React, { Component } from 'react';
import { ScrollView, StyleSheet,View,ImageBackground,Image,TouchableOpacity,Platform,Dimensions,Text,Modal,AsyncStorage} from 'react-native';
import Ionicons from 'react-native-vector-icons/Ionicons'
import { Picker } from 'react-native-picker-dropdown';
import BackButton from '../../BackButton';
import HamburgerIcon from '../../icons/HamburgerIcon';
import HeartBagBurger from '../../icons/HeartBagBurger';
import { Container, Content, List, ListItem, Thumbnail,Icon,Button,InputGroup,Input,Body,Right,Badge,Left } from 'native-base';
import Dialog, {DialogFooter,DialogButton,SlideAnimation,DialogContent } from 'react-native-popup-dialog';
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';
import { PH,SS } from 'react-native-dotenv'
import {connect} from 'react-redux'
import {deleteBasketItems,loadBasketItems,heartBagNotifCount} from '../actions/index'
const Fsize3 = Platform.OS === 'ios' ?25 : 20;
 class BasketOverview extends Component {
  static navigationOptions = () => {
    return {
        headerLeft: <HamburgerIcon/>,
        headerTitle: (
            <Image
            resizeMode="contain"
            source={require('../../../assets/images/slice2.png')}
            style={{ width: 10, height:10 , flex:4,justifyContent: 'space-around', alignItems: 'center', padding: 25}}
          />
          ),
        headerRight: <HeartBagBurger/>
    };
};

 
    state = {
      username: '',
      password: '',
      ButtonStateHolder : true,
      loading: false,
      fontLoaded:false,
      preferences:'',
      popupVisible:true,
      total_points:1,

    };
  
  async  componentDidMount(){
    if(this.props.heartBagNotif!==0){
      this.props.heartBagNotifCount(-this.props.heartBagNotif)
    }
    
    this.getNotifs()
    }
    async getNotifs() {
      let user_id= await AsyncStorage.getItem('user_id')
      let activitylist=await AsyncStorage.getItem('activityList');
      if(activitylist!==null){
        this.props.loadBasketItems(JSON.parse(activitylist))
      }
        let data = new FormData();
        data.append('user_id', user_id);
        fetch(PH+'/api/v1/users/activity', {
          method: 'POST',
          headers: {
            'Accept':'application/json',
            'Content-Type': 'multipart/form-data',
            'Authorization': 'Bearer '+await AsyncStorage.getItem('token')
          },
          body:data
          }).then((response) => response.json())
              .then(async (responseJson) => {
                console.log("activity",responseJson.meta)
                if(responseJson.meta.activity_list!==undefined){
                  await AsyncStorage.setItem('activityList', JSON.stringify(responseJson.meta.activity_list) )
                  this.props.loadBasketItems(responseJson.meta.activity_list)
                  this.setState({total_points:responseJson.meta.total_points})
                }else{
                  console.log(responseJson.meta)
                }
          })
              .catch((error) => {
               console.log(error)
          });
      };    
    back = async ()=>{
    this.props.navigation.navigate('Home');
    }
    submit =()=>{
      //this.props.navigation.navigate('Home');
    }
    messages =()=>{
      this.props.navigation.navigate('ActivityChat');
     }

  render(){
    const resizeMode = 'cover';
    return (
  
      <View style={styles.container}>
       
      <ImageBackground
         style={{
           backgroundColor: '#ccc',
           flex: 1,
           resizeMode,
           position: 'absolute',
           width: '120%', 
           height: '100%',
           justifyContent: 'center',
           
         }}
         source={require('../../../assets/images/slice81-min.png')}
         />
       
       <View style={{flex: 1, flexDirection: 'row',justifyContent:'space-between', margin:15,marginTop:Dimensions.get('screen').height/50}}>
     
           <View>
            <BackButton/>
         </View>
         <View>
          <Text adjustsFontSizeToFit numberOfLines={1}  style={{ fontSize: Fsize3 ,alignSelf:'center',color:'#2db9b9',fontFamily:'KGPrimaryWhimsy'}}> Sejt du har genbrugt!</Text>
          <Text adjustsFontSizeToFit numberOfLines={1}  style={{color:'#524e4f', fontSize: Platform.OS === 'ios' ?20 : 15,alignSelf:'center',fontFamily:'KGPrimaryWhimsy'}}>Du kan nu vælge {this.state.total_points} stk. børneting</Text>
          </View>
          <View>
          <TouchableOpacity onPress={this.back.bind(this)} loading={true}>
            <Image resizeMode='contain' source={require('../../../assets/images/slice50-min.png')} style={{width: 20,height: 20,}}></Image>
            
         </TouchableOpacity>
         </View>
      </View>
      <View style={styles.container3}>
      <Container style={{backgroundColor:'transparent',marginTop:hp('5%')}} >
      <Content>
     
      {this.props.allBasketItems && this.props.allBasketItems.map((prop, key) => {
         return (
           
              <List style={{backgroundColor:'white',borderColor:'black',borderWidth:.5,borderRadius: 15,marginBottom:3,margin:3,shadowColor: '#000',
              shadowOffset: { width: 0, height: 2 },
              shadowOpacity: 0.5,
              shadowRadius: 1.5,
              elevation: 1}} key={prop.id}>
                  <ListItem>
                      <Left>
                      <Thumbnail resizeMode='contain' small square source={require('../../../assets/images/slice62-min.png')}  />
                      </Left>
                   <Body style={{flexDirection:'column',marginLeft:-wp('50%')}}>
                    
                      <Text style={{color:'#cc586f',fontFamily:'KGPrimaryWhimsy',fontSize:hp('2.8%')}}>{prop.message}</Text>
                      <Text style={{color:'#524e4f'}} note>{prop.description}</Text>
                    </Body>
                    
                    {prop.type === "closed_deal" ?
                    <View >
                    <Right>
                     <Thumbnail square small source={require('../../../assets/images/slice71-min.png')} style={{marginRight:5,padding:2,resizeMode:'contain'}}/>
                    </Right>
                    </View>
                     
                    :   null
                    }
                    {prop.type === "reservation" ?
                    <View>
                       
                     <Right> 
                    {/* <TouchableOpacity onPress={this.messages.bind(this)} loading={true}> */}
                   <Thumbnail resizeMode='contain' square small source={require('../../../assets/images/slice60-min.png')} style={{marginRight:5}}/>
                   {/* <Thumbnail  small source={require('../../../assets/images/slice62-min.png')} style={{marginRight:5}}/> */}
                   {/* </TouchableOpacity> */}
                   </Right> 
                   </View>
                     :null}
                     {prop.type === 'add_product' ?
                       <View >
                     <Right>
                     <Icon style={{opacity:100,marginRight:10,padding:1}} name="arrow-forward" />
                     </Right></View>
                     :null}
                  </ListItem>
                  {/* <Icon ios='ios-arrow-forward' android='md-arrow-dropright' iconRight/> */}
             </List>           
         );
         
      })}
        </Content>
       </Container>
     
      </View>
            
  </View>
   
    );
  }
  }
   
  const mapStateToProps = (state) =>{
    return{
      allBasketItems: state.count.basketItems,
      heartBagNotif: state.count.heartBagNotifCount
    }
  }
  
  const mapDispatchToProps = (dispatch) =>{
    return{
      deleteBasketItems:(data)=>dispatch(deleteBasketItems(data)),
      loadBasketItems:(data)=>dispatch(loadBasketItems(data)),
      heartBagNotifCount:(count)=>dispatch(heartBagNotifCount(count))
    }
  }
  
  export default connect(mapStateToProps,mapDispatchToProps)(BasketOverview);
const styles = StyleSheet.create({
  container: {
    flex: 1,
    //alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: 'transparent'
  },
  container2: {
    flex: 1,
    //alignItems: 'center',
    //justifyContent: 'flex-start',
    
    backgroundColor: 'transparent'
    //bottom: Dimensions.get('screen').height /5
  },
  textstyle:{
    fontFamily:'KGPrimaryWhimsy',
    fontSize:40
  },
  container3: {
    position:'absolute',
    backgroundColor: 'transparent',
    flex: 1,
    height:hp('75%'),
    width:wp('100%'),
    bottom:0,
    //justifyContent: "flex-end",
    //alignItems: "center"
  },
  dropdown: {
    height: Dimensions.get('screen').width /7,
    padding: 5,
    position:'relative',
    margin:5,
    borderWidth: .2,
    borderRadius: 12,
    textShadowColor: 'black',
    borderColor: 'black',
    marginBottom: 10,
    backgroundColor: 'white',
    opacity: 0.9,
    fontFamily:'KGPrimaryWhimsy',
    fontSize:20,
    alignContent:'center'
  },
  wrpper: {
    opacity: 0.9,
    fontSize:20,
    fontFamily:'KGPrimaryWhimsy',
    flexDirection: 'row', justifyContent: 'space-around',   
  },
  button: {
    backgroundColor: '#54969c',
    borderColor: 'white',
    borderWidth: .5,
    borderRadius: 12,
    color: 'white',
    overflow: 'hidden',
    padding: 12,
    textAlign:'center',
    width: 300,
    height: 48,
    opacity: 0.9,
    fontSize:20,
    fontFamily:'KGPrimaryWhimsy'
  },
});

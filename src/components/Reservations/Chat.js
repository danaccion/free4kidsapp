//CHAT RESERVATION

import React, { Component } from 'react';
import {StyleSheet,View,ImageBackground,Image,TouchableOpacity,TextInput,Platform,Dimensions,AsyncStorage,Keyboard,StatusBar,TouchableWithoutFeedback,ScrollView,SafeAreaView} from 'react-native';
import BackButton from '../../BackButton';
import HamburgerIcon from '../../icons/HamburgerIcon';
import UUIDGenerator from 'react-native-uuid-generator';
import HeartBagBurger from '../../icons/HeartBagBurger';
import { Container, Content, List, ListItem, Thumbnail,Icon,InputGroup,Input,Body,Right,Badge,Text,Left,Textarea,Form,Item } from 'native-base';
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view'
import SwipeableRating from 'react-native-swipeable-rating';
import Modal from 'react-native-modal';
import ModalBox  from 'react-native-modalbox'
import io from 'socket.io-client'
import moment from 'moment'
import ProgressiveImage from '../ProgressiveImage';
import { GiftedChat,MessageText, Bubble,InputToolbar,Send } from 'react-native-gifted-chat'
import { PH,SS } from 'react-native-dotenv'
import { Snackbar } from 'react-native-paper';
import {COMMUNITY_CHAT,PRIVATE_MESSAGE,USER_DISCONNECTED,VERIFY_USER,MESSAGE_RECEIVED,MESSAGE_SENT,TYPING} from '../chatEvents/events'
const Fsize2 = Platform.OS === 'ios' ?25 : 18;
const Fsize = Platform.OS === 'ios' ?22 : 16;
const chatText = Platform.OS === 'ios' ?18 : 15;
const { width, height } = Dimensions.get('window');
const TAB_BAR_HEIGHT = 49;
import { connect } from 'react-redux';
// import Dialog, {SlideAnimation,DialogContent } from 'react-native-popup-dialog';
import {heartBagNotifCount,messages_notif} from '../actions/index'
import {
  Button,Block
} from 'galio-framework';

import Dialog, { DialogTitle,
  DialogContent,
  DialogFooter,
  DialogButton,
  SlideAnimation,
  ScaleAnimation, } from 'react-native-popup-dialog';
import { Alert } from 'react-native';

const  socketURL= 'https://socket.free4kids.design4web.dk';
 class Chat extends Component {
  static navigationOptions = () => {
    return {
        headerLeft: <HamburgerIcon/>,
        headerTitle: (
            <Image
            resizeMode="contain"
            source={require('../../../assets/images/slice2.png')}
            style={{ width: 10, height:10 , flex:4,justifyContent: 'space-around', alignItems: 'center', padding: 25}}
          />
          ),
        headerRight: <HeartBagBurger/>
    };
};

 
    state = {
      socket:null,
      user:null,
      username: 'MAONI',
      password: '',
      ButtonStateHolder : true,
      loading: false,
      fontLoaded:false,
      preferences:'',
      panelMargin:0,
      chatMessages:[],
      chats:[],
      messages: [],
      userid:'',
      userImage:'',
      activeChat:null,
      isModalVisible:false,
      reservationModal:false,
      rating:0,
      swipeablePanelActive: false,
      reportModal:false,
      seeInformation:false,comment:'',
      successClose:false,
        owner_approved:false,
        contact_approved:false,
        notif:false,
        kontaktoplysninger1:false,
        kontaktoplysninger2:false,
        meta_address:'',
        meta_contact:'',
        is_contact:false,
        has_address:false,
        has_phone:false,
        showDialogMessage:false,
        dialog_message:''
    };


  async componentDidMount() {
   
    this.giftedChatRef.scrollToBottom();
    await this.getMessages()
    console.log(PH)
    
    const { navigation } = this.props;
    
    const item = navigation.getParam('item');
    
    //console.log("chatt",item)
    const user_id=await AsyncStorage.getItem('user_id')
    const username=await AsyncStorage.getItem('name')
    this.getCount(item.owner_id);
    AsyncStorage.setItem('owner_id', item.owner_id);
    this.socket.on("send_message", msg => {
      //console.log("gikan sa srever",msg)
      if(parseInt(msg.receiver_id)===parseInt(user_id)&&parseInt(msg.sender_id)===parseInt(item.owner_id)==null?parseInt(item.sender_id):parseInt(item.owner_id)){
        this.setState({ chatMessages: [...this.state.chatMessages, msg] });
      }
      
    });
     this.setState({userid:await AsyncStorage.getItem('user_id'),userImage:await AsyncStorage.getItem('image_url')})
    
    console.log('INIT');
     await this.getContactRequest()
   
     
    }

    getCount(data){
      // console.log("uy:",""+data,this.props.message_notif)
      if(this.props.message_notif!==null){
        var index = this.props.message_notif.findIndex(function(o){
          return o.id === 1;
        })
        if (index !== -1) {
          this.props.message_notif.splice(index, 1);

        }
        // console.log("ayay:",this.props.message_notif);
        this.props.update_message_notif(this.props.message_notif);
      }
      
    }
    handleRating = (rating) => {
      this.setState({rating});
    }
    
    getProductContact = async () =>{
      const { navigation } = this.props;
      const item = navigation.getParam('item');
      let data = new FormData();
      data.append('user_id', item.users.id);
      data.append('product_id',item.id);
      data.append('request_id', item.products[0].user_id);
      fetch(PH+'/api/v1/products/getProductContact', {
        method: 'POST',
        headers: {
          'Accept':'application/json',
          'Content-Type': 'multipart/form-data',
          'Authorization': 'Bearer '+await AsyncStorage.getItem('token')
        },
        body:data
        }).then((response) => response.json())
        console.log("Automated Values:------>{")
            .then((responseJson) => {
                console.log("product contact: ",responseJson);
                console.log("--------->}")
                this.setState({meta_contact:responseJson.meta.contact.number, meta_address:responseJson.meta.contact.street_address1});
        }).catch((error) => {});
    }

    getContactRequest = async () =>{
      const { navigation } = this.props;
      const user_id=await AsyncStorage.getItem('user_id')
      const item = navigation.getParam('item');
      let data = new FormData();
      data.append('user_id', user_id);
      // data.append('product_id',item.product_id);
      data.append('product_id', item.id);
      data.append('request_id', item.products[0].user_id);
      console.log('getContactRequest');
      console.log("-----------------------");
      console.log('product_id '+item.id)
      console.log('user_id '+user_id)
      console.log('request_id '+item.products[0].user_id)
      fetch(PH+'/api/v1/request_contact', {
        method: 'POST',
        headers: {
          'Accept':'application/json',
          'Content-Type': 'multipart/form-data',
          'Authorization': 'Bearer '+await AsyncStorage.getItem('token')
        },
        body:data
        }).then((response) => response.json())
            .then((responseJson) => {
              console.log("Checking Request if already Approve....")
              console.log(responseJson)
               if(responseJson.code===201){
                 this.setState({owner_approved:true});
                 console.log("Match!")
                 console.log("--")
                 this.getContact();
                 this.setState({kontaktoplysninger1:true});
                 console.log(responseJson.meta.request_contact[0].number)
                 this.state.meta_contact = responseJson.meta.request_contactcontact[0].number
                 this.setState({meta_contact:responseJson.meta.request_contact[0].number, meta_address:responseJson.meta.request_contact[0].street_address1});
              }else{
              console.log("No Match!")
              this.setState({owner_approved:false,kontaktoplysninger2:false, seeInformation:false, kontaktoplysninger1:false});
              //   this.state.contact_approval = 1
                
              }
        }).catch((error) => {});
    }

    
    
  toggleModal = () => {
    this.refs.slideUp.close()
    this.refs.rate.open()
  };

  togglekob = () => {
    this.refs.slideUp.close()
    this.refs.kob.open()
  };

  toggleModalReservation = () => {
    this.refs.options.close()
    this.refs.reservation.open()
     
  };
  toggleShowInformation = () => {
    this.refs.slideUp.close()
    this.refs.seeInformation.open()
    
  };
  toggleModalReport = () => {
    this.refs.options.close()
    this.refs.report.open()
  };
 
  toWebView(mode){
    AsyncStorage.setItem('modepayment', mode);
    this.props.navigation.navigate('StripeWebView');
  }


  async submitRating(){
    const { navigation } = this.props;
    const item = navigation.getParam('item');
    let data = new FormData();
    //console.log(this.state,item)
    this.props.heartBagNotifCount(1)
    data.append('author_id', this.state.userid);
    data.append('comment', this.state.comment);
    data.append('rate', this.state.rating);
    data.append('user_id', item.owner_id==null?item.sender_id:item.owner_id);
  
    fetch(PH+'/api/v1/userrating/create', {
      method: 'POST',
      headers: {
        'Accept':'application/json',
        'Content-Type': 'multipart/form-data',
        'Authorization': 'Bearer '+await AsyncStorage.getItem('token')
      },
      body:data
      }).then((response) => response.json())
        .then((responseJson) => {})
        .catch((error) => {});

      let data2 = new FormData();
      //console.log(this.state,item)
      data2.append('product_id', item.product_id);
      data2.append('buyer_id', this.state.userid);
      data2.append('seller_id', item.owner_id==null?item.seller_id:item.owner_id);

      fetch(PH+'/api/v1/closeddeal/create', {
        method: 'POST',
        headers: {
          'Accept':'application/json',
          'Content-Type': 'multipart/form-data',
          'Authorization': 'Bearer '+await AsyncStorage.getItem('token')
        },
        body:data2
        }).then((response) => response.json())
            .then((responseJson) => {
              //console.log("close")
        })
        .catch((error) => {});
        this.props.navigation.pop();
      }
  async openDetails(){
    this.refs.slideUp.open()
    await Keyboard.dismiss()
  }

  toProfile(){
    const { navigation } = this.props;
    const item = navigation.getParam('item');
    this.refs.options.close()
    this.props.navigation.navigate('UserProfile',{'user':Array.isArray(item.users)===true?item.users[0]:item.users})
    
  }
  getMessages = async  () => {
    
    // console.log("geetting message",PH)
    const { navigation } = this.props;
      const item = navigation.getParam('item');
      const user_id=await AsyncStorage.getItem('user_id')
      var messageList=[]
      let data = new FormData();
      data.append('sender_id', user_id);
      data.append('receiver_id', item.users.id);
      fetch(PH+'/api/v1/users/getmessages', {
        method: 'POST',
        headers: {
          'Accept':'application/json',
          'Content-Type': 'multipart/form-data',
          'Authorization': 'Bearer '+await AsyncStorage.getItem('token')
        },
        body:data
        }).then((response) => response.json())
            .then((responseJson) => {
              this.setState({ chatMessages:responseJson.meta.messages  });
        })
            .catch((error) => {
              //console.log("ayay",error)
        });
   this.connect()
};

  sendTyping = (chatId,isTyping)=>{
      const {socket} = this.state
      socket.emit(TYPING, {chatId,isTyping})
  }

  setActiveChat = (activeChat) =>{
      this.setState({activeChat})
  }
  
 connect = async () => {
  const { navigation } = this.props;
  const item = navigation.getParam('item');                          
  const user_id=await AsyncStorage.getItem('user_id')
  const username=await AsyncStorage.getItem('name')
    this.socket = io('http://socket.free4kids.design4web.dk', {
      autoConnect: true,
    });
    this.socket.on('connect', () => {
      console.log("connected")
      this.socket.emit('come_online', {'name':username,'user_id' : user_id,'receiver_id':item.owner_id==null?item.sender_id:item.owner_id,'receiver_name':item.users.name});
     
    });
  
    this.socket.on('disconnect', (reason) => {
      console.log(`Disconnected: ${reason}`);
      // statusInput.value = `Disconnected: ${reason}`;
    })
  
    this.socket.open();
  };
  
 
  disconnect = () => {
    this.socket.disconnect();
  }
  
  componentWillUnmount(){
    console.log("unmounted")
    this.disconnect()
  }
 
  back = async ()=>{
    this.props.navigation.navigate('ReservationChat');
  }

 
   
async addMessage(message){
  this.socket.emit("sendmessage",message)
  let data = new FormData();
  data.append('sender_id', message.sender_id);
  data.append('createdAt', message.createdAt.toString());
  data.append('_id', message.id);
  data.append('receiver_id', message.receiver_id);
  data.append('text', message.text);
  data.append('name', message.name);
  data.append('time', message.time);
  data.append('type', 'buyer');
  fetch(PH+'/api/v1/messages/create', {
    method: 'POST',
    headers: {
      'Accept':'application/json',
      'Content-Type': 'multipart/form-data',
      'Authorization': 'Bearer '+await AsyncStorage.getItem('token')
    },
    body:data
    }).then((response) => response.json())
        .then((responseJson) => {}).catch((error) => {});
 }
 async submitMesssage(messages){
  this.giftedChatRef.scrollToBottom();
  const { navigation } = this.props;
  const item = navigation.getParam('item');
  //console.log(messages)
    const message ={id:messages[0]._id,sender_id:this.state.userid,_id:messages[0]._id,
    text:messages[0].text,createdAt:messages[0].createdAt,time:this.getDateNow(),image_url:this.state.userImage,receiver_id:item.owner_id==null?item.sender_id:item.owner_id,name:item.users.name==null?item.name:item.users.name}
    this.setState({ chatMessages: [...this.state.chatMessages, messages[0]] });
    this.addMessage(message)
    // this.socket.emit("chat message",messages[0])
  }
  
  async showDialog() {
    this.refs.slideUp.close()  
    this.getRequestContact();
    // if(this.state.contact_approved=="Yes") {//this.state.contact_approved 
    //       this.setState({kontaktoplysninger1:true});
    // }
    // else {
    //     this.setState({kontaktoplysninger2:true});
    // }
  }
  
  
  async askForApproval() {
    console.log("inside method")
    this.setState({kontaktoplysninger2:true});
    const { navigation } = this.props;
    const item = navigation.getParam('item');
    const user_id=await AsyncStorage.getItem('user_id')
    let data = new FormData();
console.log("---StarT---")
console.log(item);
console.log("---EnD---")
console.log("user_id "+user_id);
console.log("request_id "+item.products[0].user_id);
console.log("product id "+item.id);



data.append('user_id', user_id);
data.append('product_id', item.id);
data.append('description', 'ask for approval');
data.append('request_id', item.products[0].user_id);

    fetch(PH+'/api/v1/request_contact/create', {
      method: 'POST',
      headers: {
        'Accept':'application/json',
        'Content-Type': 'multipart/form-data',
        'Authorization': 'Bearer '+await AsyncStorage.getItem('token')
      },
      body:data
      }).then((response) => response.json())
          .then((responseJson) => {
            console.log("return code : "+responseJson.code+"\n title :"+responseJson.title);
    
    
            if(responseJson.code===200){
              this.state.contact_approval = 0 
              console.log("success");
              //Alert.alert("Request contact sent.");
              this.setState({ seeInformation:false, kontaktoplysninger1:false, kontaktoplysninger2:false, showDialogMessage:true, dialog_message:'Request contact sent.' });
              
            }
            else if(responseJson.code===201){
              this.state.contact_approval = 1
              
              this.setState({kontaktoplysninger2:false, kontaktoplysninger1:true});
              
            }else if(responseJson.code===202)
            {
              this.state.contact_approval = 1
              
              //Alert.alert("Already Requested");
              this.setState({owner_approved:true,kontaktoplysninger2:false, seeInformation:false, kontaktoplysninger1:false, showDialogMessage:true, dialog_message:'Already Requested'});
              
              
            }
            else{
              //Alert.alert("Failed to share contact info.");
              this.state.contact_approval = 1
              this.refs.contact.close();
              console.log("failed to perform share contact");
              this.setState({ seeInformation:false, kontaktoplysninger1:false, kontaktoplysninger2:false, showDialogMessage:true, dialog_message:'Failed to share contact info.' });
            }
          
      })
          .catch((error) => {
             console.log("ayay",error)
      });
  }

  async getContact(){
    const { navigation } = this.props;
    const item = navigation.getParam('item');
    let data = new FormData();
    const user_id=await AsyncStorage.getItem('user_id')
    console.log(item)
    
    console.log("-----------------------");
    console.log('getRequestContact');
    console.log(item.id);   
    console.log(user_id);
    console.log(item.owner_id);
    data.append('product_id', item.id);
    data.append('user_id', user_id);
    data.append('request_id',item.products[0].user_id);

    fetch(PH+'/api/v1/products/getProductContact', {
      method: 'POST',
      headers: {
        'Accept':'application/json',
        'Content-Type': 'multipart/form-data',
        'Authorization': 'Bearer '+await AsyncStorage.getItem('token')
      },
      body:data
      }).then((response) => response.json())
          .then((responseJson) => {
            console.log("Product Details ->")
            console.log(responseJson)
            if(responseJson.code == 200){
            console.log(responseJson.meta.contact[0].number)
            this.setState({meta_contact:responseJson.meta.contact[0].number, meta_address:responseJson.meta.contact[0].street_address1});
            this.setState({ seeInformation:false, owner_approved:true ,kontaktoplysninger2:false });
            this.refs.slideUp.close()  
          }else{
            this.setState({ owner_approved:false });
          }
        
      })
          .catch((error) => {
            console.log("ayays",error)
            return 0;
          
      });

    console.log("-----------------------");

      
}

 async getRequestContact(){
      const { navigation } = this.props;
      const item = navigation.getParam('item');
      let data = new FormData();
      const user_id=await AsyncStorage.getItem('user_id')
      console.log(item)
      console.log("-----------------------");
      console.log('getRequestContact');
      console.log(item.id);   
      console.log(user_id);
      console.log(item.owner_id);
      data.append('product_id', item.id);
      data.append('user_id', user_id);
      data.append('request_id',item.products[0].user_id);
  
      fetch(PH+'/api/v1/products/getProductContact', {
        method: 'POST',
        headers: {
          'Accept':'application/json',
          'Content-Type': 'multipart/form-data',
          'Authorization': 'Bearer '+await AsyncStorage.getItem('token')
        },
        body:data
        }).then((response) => response.json())
            .then((responseJson) => {
              console.log(responseJson)
              if(responseJson.meta.count > 0)
              {
                //already requested
                //checking algo for approved request
                console.log("COUNTER");
                console.log(responseJson.meta.count);
                this.getApprovedRequested();
              }else  if(responseJson.meta.count == 0){
                console.log("ask for approval method");
                this.setState({kontaktoplysninger2:true});
              }
              //Dapat mag return ka dri ug value para info sa contact
          
        })
            .catch((error) => {
              console.log("ayays",error)
              return 0;
            
        });

      console.log("-----------------------");

        
}


async getApprovedRequested(){
  const { navigation } = this.props;
  const item = navigation.getParam('item');
  let data = new FormData();
  const user_id=await AsyncStorage.getItem('user_id')
  
  
  console.log(item); 
  console.log("-----------------------");
  console.log('getApprovedRequested');
  console.log(item.id);
  console.log(user_id);
  console.log(item.products[0].user_id);

  
  data.append('product_id', item.id);
  data.append('user_id', user_id);
  data.append('request_id', item.products[0].user_id);
  data.append('owner_approved', '1');
  fetch(PH+'/api/v1/users/getApprovedRequested', {
    method: 'POST',
    headers: {
      'Accept':'application/json',
      'Content-Type': 'multipart/form-data',
      'Authorization': 'Bearer '+await AsyncStorage.getItem('token')
    },
    body:data
    }).then((response) => response.json())
        .then((responseJson) => {
          console.log("----------------------->>>>"+responseJson.code)
          if(responseJson.code == 200)
          {
            // this.setState({owner_approved:true});
            console.log("APPROVE------------------------------>");
            this.setState({ seeInformation:false, kontaktoplysninger1:true, kontaktoplysninger2:false });
            this.state.contact_approved = 1;
            // Alert.alert("Already Requested");
          }
          else if(responseJson.code == 201)
          {
            if(responseJson.meta.count > 0){
              //Alert.alert("Waiting For Approval");
              this.setState({ showDialogMessage:true, dialog_message:'Waiting For Approval' });
            }
            else{
              this.setState({kontaktoplysninger2:true});
            }
          }
          else{
            this.setState({kontaktoplysninger2:true});
          }
          //Dapat mag return ka dri ug value para info sa contact
      
    })
        .catch((error) => {
          console.log("ayays",error)
          return 0;
        
    });

  console.log("-----------------------");

    
}

  getDateNow(){
       
    var date = moment()
    .format(' hh:mm a');
    return date;
        
  }

  randomid(len, arr) { 
    var ans = ""; 
    for (var i = len; i > 0; i--) { 
        ans +=  
          arr[Math.floor(Math.random() * arr.length)]; 
    } 
    return ans; 
  } 

    buyshipping = async () => {
      this.props.navigation.navigate('BuyShipping');
    }
    basket_overview = async () => {
      this.props.navigation.navigate('BasketOverview');
    }
    messages =()=>{
      this.props.navigation.navigate('ActivityChat');
     }
    isCloseToBottom = ({ layoutMeasurement, contentOffset, contentSize }) => {
      return  contentOffset.y == 0;
    };

  render(){
    const { navigation } = this.props;
    //console.log(this.state)
    const item = navigation.getParam('item');
    const resizeMode = 'cover';
    const renderMessageText = (props) => {
      const {
        currentMessage,
      } = props;
      const { user: currUser } = currentMessage;
      if (+currUser._id === +this.state.userid) {
                return <View  style={{backgroundColor:'white',borderColor:'#bfbfbf',borderWidth:.5,marginBottom:7,borderRadius: 15,padding:10}}>

                <MessageText {...props} style={{flex: 1}} textStyle={{
                    left: {
                      color: 'black',
                      fontSize:chatText
                    },
                    right: {
                      color: 'black',
                      fontSize:chatText
                    },
                  }}/>
                <View style={{bottom: -5,right:-2,zIndex:100,position:'absolute'}}>
                <ProgressiveImage  resizeMode='contain'  source={{ uri: this.state.userImage}} style={{width:Fsize2,height:Fsize2,borderRadius:Fsize2/2}}/>
                </View>
        </View>
      }
      return (  <View  style={{backgroundColor:'white',borderColor:'#bfbfbf',borderWidth:.5,borderRadius: 15,marginBottom:7,padding:10,left:-40}}>

                    <MessageText {...props} style={{flex: 1}} textStyle={{
                        left: {
                          color: 'black',
                          fontSize:chatText

                        },
                        right: {
                          color: 'black',
                          fontSize:chatText
                        },
                      }}/>
                    <View style={{bottom: -5,left:-2,zIndex:100,position:'absolute'}}>
                    <ProgressiveImage  resizeMode='contain'  source={{ uri: item.users.image_url}} style={{width:Fsize2,height:Fsize2,borderRadius:Fsize2/2}}/>
                    </View>
        </View>)
            
      
    };
    const renderBubble = (props) => { 
   
  
      return <Bubble {...props} 
        wrapperStyle={{
            left: {
              backgroundColor: 'transparent',
            },
            right: {
              backgroundColor: 'transparent'
            }
          }} 
  
        timeTextStyle={{
          left: {
            color: 'grey',
            // fontSize:15
          },
          right: {
            color: 'grey',
            // fontSize:15
          },
        }}
      />
    }
    const  renderInputToolbar = (props) => { 

     return (
     <View style={{alignContent:'center',alignItems:'center',left:5,bottom:-20}}>
     <InputToolbar {...props} containerStyle={{position:'relative',justifyContent:'center',
     alignItems: 'center',backgroundColor:'white',width:wp('98%'),bottom:3,borderColor: '#bfbfbf',
     borderWidth: .5,
     borderRadius: 12,shadowColor: '#000',
     shadowOffset: { width: 0, height: 2 },
     shadowOpacity: 0.5,
     shadowRadius: 1.5,
     elevation: 1}}/>
     </View>
     )
   }
   const  sendButton = (props) => { 
   return <Send
   {...props}
   containerStyle={{
     height: 40,
     width: 40,
     justifyContent: 'center',
     alignItems: 'center',
   }}
 >
    <Icon style={{padding:10,  color:"grey",bottom:10}} name="md-send" />
 </Send>
 }
 
    return (
      
      <View style={{flex:1,backgroundColor:'transparent'}}>
         <StatusBar barStyle = "dark-content" hidden = {false} backgroundColor = "#00BCD4" translucent = {true}/>
      <ImageBackground
         style={{
           backgroundColor: '#ccc',
           flex: 1,
           resizeMode,
           position: 'absolute',
           width: '120%', 
           height: '100%',
           justifyContent: 'center',
         }}
         source={require('../../../assets/images/slice81-min.png')}
         />
          <View style={{flexDirection: 'row',justifyContent:'space-between',marginTop:5,zIndex:10}}>
          <BackButton/>
          {/* <Image source={require('../../../assets/images/slice73-min.png')} style={{resizeMode,width:40,height:hp('65%'),}}></Image> */}
          <TouchableOpacity style={{width:40,height:20}} onPress={()=>this.refs.options.open()}>
            <Image source={require('../../../assets/images/img-inf.png')} style={{width: 20,height: 20,right:0,top:10}}></Image>
         </TouchableOpacity>
       
          </View>
         
         <GiftedChat
              ref={ref => this.giftedChatRef = ref}
              messages={this.state.chatMessages}
              onSend={messages => this.submitMesssage(messages)}
              user={{_id:+this.state.userid,}}
              // bottomOffset={-20}
              placeholder="Chat nærmere"
              // minInputToolbarHeight={20}
              // maxInputToolbarHeight={30}
              onPressActionButton={()=>this.openDetails()}
              // renderInputToolbar={renderInputToolbar} 
              renderMessageText={renderMessageText}
              renderBubble={renderBubble}
              // renderSend={sendButton}
              // loadEarlier={true}
              // renderUsernameOnMessage={true}
              inverted={false}
              alwaysShowSend={false}
              showUserAvatar={false}
              avatar={false}
            />  
        <Snackbar
          visible={this.state.successClose}
          onDismiss={() => this.setState({ successClose: false })}
          // action={{
          //   label: 'Undo',
          //   onPress: () => {
          //     // Do something
          //   },
          // }}
          style={{backgroundColor:'#5e5e5e',marginTop:40}}
          duration={3000}
        >
       Aftale lukket!
        </Snackbar>
             <ModalBox coverScreen={true}  position={"bottom"} style={{height:hp('30%'),zIndex:999,borderTopLeftRadius: 20, 
        borderTopRightRadius: 20,
        overflow: "hidden"}} transparent={false} ref={"options"} >
        <View style={{flex:1,flexDirection:'column',alignContent:'center',alignItems:'center',justifyContent:'flex-end'}}>
        <Body style={{width:wp('85%'),alignItems:'center',marginTop:20
        ,marginBottom:30}}>
        {/* <TouchableOpacity onPress={()=>console.log("to profile")}
          // this.props.navigation.navigate('UserProfile')} 
          style={{marginBottom:10,alignContent:'center'}}>
          <Text style={styles.button3}>Se profil</Text>
        </TouchableOpacity> */}
        <Button round onPress={()=>this.toProfile()}  style={{width:wp('80%') }} color="#c97190">Se profil</Button>
        <Button round onPress={()=>this.toggleModalReservation()}  style={{width:wp('80%') }} color="#c97190">Fortryd reservation</Button>
        <Button round onPress={()=>this.toggleModalReport()}  style={{width:wp('80%') }} color="#c97190"><Text style={{fontFamily:'KGPrimaryWhimsy'}}>Rapport</Text></Button>
        </Body>
        </View>       
            </ModalBox> 
      <ModalBox position={"center"} style={{height:hp('50%'),width:hp('10%'),backgroundColor:'transparent'}} transparent={true} ref={"reservation"} >
       <TouchableOpacity style={{position:'relative',alignSelf:'flex-end',height:100,width:100,zIndex:99,left:100,top:30}} onPress={()=>this.refs.reservation.close()}></TouchableOpacity>
       <TouchableOpacity style={{position:'relative',alignSelf:'center',height:100,width:100,zIndex:99,top:90}} onPress={()=>console.log("Undo Reserve")}>
          
       </TouchableOpacity>
       <Image
         style={{
           zIndex:10,
           resizeMode:'contain',
           position: 'absolute',
           alignSelf:'center',
          width:wp('70%'),
          height:wp('70%'),
           justifyContent: 'center',
           
         }}
         source={require('../../../assets/icons/slice12.png')}
         />
         
       </ModalBox>

       {/* <ModalBox position={"center"}style={{height:hp('50%'),width:wp('90%'),zIndex:999,borderTopLeftRadius: 30, 
        borderTopRightRadius: 30,borderBottomLeftRadius: 30, 
        borderBottomRightRadius: 20,
        overflow: "hidden"}} transparent={false} ref={"seeInformation"}>
        <Block center style={{borderRadius:20,width:wp('80%')}} >
       {item.info!==undefined?(
       <View style={{backgroundColor:'white',alignContent:'center',alignItems:'center',height:hp('30%'),borderRadius: hp('5%')/1.2,width:wp('80%')}}>
           <Text style={{alignSelf:'center',top:20,fontSize:Fsize2+5,fontFamily:'KGPrimaryWhimsy',color:'#d47d7d'}}>Kontaktoplysninger</Text>
           <Text style={{alignSelf:'center',top:20,fontSize:Fsize2,fontFamily:'KGPrimaryWhimsy',color:'grey'}}>Telefon: {item.info.telephone}</Text>
           <Text style={{alignSelf:'center',top:20,fontSize:Fsize2,fontFamily:'KGPrimaryWhimsy',color:'grey'}}>Adresse: {item.info.address}  </Text>
      </View>
         ):<View style={{backgroundColor:'white',alignContent:'center',alignItems:'center',height:hp('30%'),borderRadius: hp('5%')/1.2,width:wp('80%')}}>
       
         <Text style={{alignSelf:'center',top:20,fontSize:Fsize2+5,fontFamily:'KGPrimaryWhimsy',color:'#d47d7d'}}>Kontaktoplysninger</Text>
         <View style={{justifyContent:'center',alignContent:'center',marginTop:50}}> 
         <Text adjustsFontSizeToFit style={{alignSelf:'center',top:25,fontSize:Fsize2,fontFamily:'KGPrimaryWhimsy',color:'grey',textAlignVertical: "center",textAlign: "center",}}>Familien har endnu ikke offentliggjort deres adresse og telefon. </Text>
         </View>
         <TouchableOpacity onPress={()=>this.toggleModal()} style={{marginTop:70}}>
          <Text style={styles.button6}>Anmod</Text>
        </TouchableOpacity>
        </View>}
     </Block>
       </ModalBox> */}

       <ModalBox  position={"center"}style={{height:hp('40%'),width:wp('70%'),zIndex:999,borderTopLeftRadius: 30, 
        borderTopRightRadius: 30,borderBottomLeftRadius: 30, 
        borderBottomRightRadius: 20,
        overflow: "hidden",backgroundColor:'transparent'}} transparent={true} ref={"report"}>
       <TouchableOpacity style={{position:'relative',alignSelf:'flex-end',height:100,width:100,zIndex:99,left:100,top:30}} onPress={()=>this.toggleModalReport()}>
         <Text>AYAY</Text>
       </TouchableOpacity>
       <TouchableOpacity style={{position:'relative',alignSelf:'center',height:60,width:80,zIndex:99,top:90}} onPress={()=>console.log("Reported")}></TouchableOpacity>
       <Image
         style={{
           zIndex:10,
           resizeMode:'contain',
           position: 'absolute',
           alignSelf:'center',
          width:wp('70%'),
          height:wp('70%'),
           justifyContent: 'center',
         }}
         source={require('../../../assets/icons/slice14.png')} />
         
       </ModalBox>
          
       <ModalBox position={"center"}  style={{height:hp('50%'),width:wp('90%'),zIndex:999,borderTopLeftRadius: 30, 
        borderTopRightRadius: 30,borderBottomLeftRadius: 30, 
        borderBottomRightRadius: 20,
        overflow: "hidden"}} transparent={false} ref={"rate"}>
       
        <TouchableWithoutFeedback onPress={()=>Keyboard.dismiss()}>
       
          <View style={{backgroundColor:'white',alignContent:'center',alignItems:'center',height:hp('50%'),borderRadius: hp('10%')/1.2}}>
        <Block center style={{top:20}}>
        
                   <Text style={{alignSelf:'center',fontSize:Fsize2,fontFamily:'KGPrimaryWhimsy',color:'#d47d7d'}}>Bedøm familien</Text>
                  
              </Block>
        <Block center style={{position:'absolute',top:40,zIndex:1000}}>
          
        <SwipeableRating
                        rating={this.state.rating}
                        size={28}
                        swipeable={false}
                        maxRating={6}
                        gap={2}
                        color='#e8b14a'
                        emptyColor='#e8b14a'
                        onPress={this.handleRating}
                        allowHalves={true}
                        style={{marginBottom:10}}
            />
          <TextInput multiline  style={{maxHeight: 80}} rounded style={{backgroundColor:'white',height:100,width:wp('80%'),borderColor: '#bfbfbf',
          borderWidth: .5,
          borderRadius: 12,padding:15,paddingBottom:5,paddingTop:2,shadowColor: '#000',
          shadowOffset: { width: 0, height: 2 },
          shadowOpacity: 0.5,
          shadowRadius: 1.5,
          elevation: 1,fontFamily:'KGPrimaryWhimsy',fontSize:Fsize,marginBottom:20}} 
          onChangeText={text => this.setState({comment:text})}
          autoCorrect={true}
        //   onEndEditing={(e) => 
        //   {
        //       )
        //   }
        // }
   
        ref={input => { this.textInput = input }}
    />
   
   <TouchableOpacity onPress={() =>this.submitRating()} >
      <Text style={styles.button5}>Afslut aftalen</Text>           
    </TouchableOpacity>
      </Block>
      
          </View>
          </TouchableWithoutFeedback>
          
        </ModalBox>



        <ModalBox position={"center"}  style={{height:hp('50%'),width:wp('90%'),zIndex:999,borderTopLeftRadius: 30, 
        borderTopRightRadius: 30,borderBottomLeftRadius: 30, 
        borderBottomRightRadius: 20,
        overflow: "hidden"}} transparent={false} ref={"kob"}>
       
        <TouchableWithoutFeedback onPress={()=>Keyboard.dismiss()}>
       
          <View style={{backgroundColor:'white',alignContent:'center',alignItems:'center',height:hp('50%'),borderRadius: hp('10%')/1.2}}>
        <Block center style={{top:20}}>
        
                   <Text style={{alignSelf:'center',fontSize:Fsize2,fontFamily:'KGPrimaryWhimsy',color:'#d47d7d'}}>Betalingsmetode</Text>
                  
              </Block>
        <Block center style={{position:'absolute',top:40,zIndex:1000}}>
          
        {/* <SwipeableRating
                        rating={this.state.rating}
                        size={28}
                        swipeable={false}
                        maxRating={6}
                        gap={2}
                        color='#e8b14a'
                        emptyColor='#e8b14a'
                        onPress={this.handleRating}
                        allowHalves={true}
                        style={{marginBottom:10}}
            /> */}
          {/* <TextInput multiline  style={{maxHeight: 80}} rounded style={{backgroundColor:'white',height:100,width:wp('80%'),borderColor: '#bfbfbf',
          borderWidth: .5,
          borderRadius: 12,padding:15,paddingBottom:5,paddingTop:2,shadowColor: '#000',
          shadowOffset: { width: 0, height: 2 },
          shadowOpacity: 0.5,
          shadowRadius: 1.5,
          elevation: 1,fontFamily:'KGPrimaryWhimsy',fontSize:Fsize,marginBottom:20}} 
          onChangeText={text => this.setState({comment:text})}
          autoCorrect={true}
        //   onEndEditing={(e) => 
        //   {
        //       )
        //   }
        // }
   
        ref={input => { this.textInput = input }}
    />
    */}
   <TouchableOpacity onPress={() =>this.toWebView('samlop')} >
      <Text style={styles.button5}>Saml op</Text>           
    </TouchableOpacity>

    <TouchableOpacity onPress={() =>this.toWebView('Kontant')} >
      <Text style={styles.button5}>Kontant ved levering</Text>           
    </TouchableOpacity>
      </Block>
      
          </View>
          </TouchableWithoutFeedback>
          
        </ModalBox>


        
    <Dialog
        visible={this.state.kontaktoplysninger1}
        onTouchOutside={() => { this.setState({ kontaktoplysninger1: false }); }}
        onHardwareBackPress={() => {this.setState({ kontaktoplysninger2: false, kontaktoplysninger1: false });}}
        onPress={()=>{ this.setState({ kontaktoplysninger1: false }); }}
        dialogStyle={{backgroundColor:'white',width:wp('90%'),alignItems:'center',alignContent:'center', zIndex:999}}>
        <DialogContent>
            <View styles={{alignItems:'center'}}>
                <Text  style={{fontSize:30, color:'#eca6a1', padding: 12, textAlign:'center', fontFamily:'KGPrimaryWhimsy',}}>Kontaktoplysninger</Text>
            </View>
            <View styles={{flex:1, flexDirection:'column', justifyContent:'space-between'}}>
                <View styles={{alignItems:'center'}}>
                    <Text  style={{fontSize:24, color:'grey', padding: 12, textAlign:'center', fontFamily:'KGPrimaryWhimsy', paddingBottom:5, }}>Telefon: </Text>
                    <Text  style={{fontSize:22, color:'grey', padding: 12, textAlign:'center', fontFamily:'KGPrimaryWhimsy', paddingTop:0}}>{this.state.meta_contact}</Text>
                </View>
                <View styles={{alignItems:'center'}}>
                    <Text  style={{fontSize:24, color:'grey', padding: 12, textAlign:'center', fontFamily:'KGPrimaryWhimsy', paddingBottom:5, }}>Adresse: </Text>
                    <Text  style={{fontSize:22, color:'grey', padding: 12, textAlign:'center', fontFamily:'KGPrimaryWhimsy', paddingTop:0}}>{this.state.meta_address}</Text>
                </View>
            </View>
        </DialogContent>
    </Dialog>

    <Dialog
        visible={this.state.kontaktoplysninger2}
        onTouchOutside={() => { this.setState({ kontaktoplysninger2: false }); }}
        onPress={()=>{ this.setState({ kontaktoplysninger1: false }); }}
        onHardwareBackPress={() => {this.setState({ kontaktoplysninger2: false, kontaktoplysninger1: false });}}
        dialogStyle={{backgroundColor:'white',width:wp('90%'),alignItems:'center',alignContent:'center', zIndex:999}}>
        <DialogContent>
            <View styles={{flexDirection:'column',alignItems:'center'}}>
                <Text  style={{fontSize:30, color:'#eca6a1', padding: 12, textAlign:'center', fontFamily:'KGPrimaryWhimsy',}}>Kontaktoplysninger</Text>
                <View styles={{flex:1, flexDirection:'column', justifyContent:'space-between'}}>
                    <Text style={{fontSize:22, color:'grey', padding: 12, textAlign:'center', fontFamily:'KGPrimaryWhimsy',}}>Familien har endnu ikke offentliggjort deres adresse og telefon.</Text>
                    <TouchableOpacity onPress={()=>this.askForApproval()} style={{alignSelf:'flex-end'}}>
                        <Text style={[styles.button,{width:wp('80%')}]}>Anmod</Text>
                    </TouchableOpacity>
                </View>
            </View>
        </DialogContent>
    </Dialog>
    
    <Dialog visible={this.state.notif} onTouchOutside={() => {this.setState({ notif: false });}} onHardwareBackPress={() => {this.setState({ notif: false });}} dialogStyle={{backgroundColor:'transparent'}} >
        <DialogContent>
            <View styles={{alignItems:'center'}}>
                <TouchableOpacity onPress={()=>this.setState({ notif: false })} style={{position:'absolute',right:5,top:50,height:60,width:60,zIndex:10}}>
                {/* <Text style={{color:'black',right:0}}>x</Text> */}
                </TouchableOpacity> 
                <Text  style={{position:'absolute',right:60,bottom:100,zIndex:10,fontSize:16,color:'grey'}}>Request Contact Send</Text>
                <Image  resizeMode="contain" style={{width:wp('60%') ,height:hp('40%'),alignSelf:'center'}} source={require('../../../assets/icons/errorLogin.png')}/> 
            </View>
        </DialogContent>
    </Dialog>
        
    <ModalBox coverScreen={true}  position={"bottom"} style={{height:hp('90%'),zIndex:9,borderTopLeftRadius: 20,borderTopRightRadius: 20, overflow: "hidden"}} transparent={false} ref={"slideUp"} >
        <Block center >
            <Image resizeMode='contain' style={{width:wp('60%'),height:hp('60%'),alignSelf:'center'}} source={require('../../../assets/girls_info/slice4.png')} />        
            <Button round onPress={()=>this.togglekob()}  style={{width:wp('80%'),borderColor:'grey',borderWidth:.5 }}  color="white"><Text style={{color:'#3b3b3b',fontFamily:'KGPrimaryWhimsy',fontSize:20}}>Køb porto {"\n"}Spar penge med FamiliePlus</Text></Button>
            {/* <Button round onPress={() =>this.submitRating()}  style={{width:wp('80%'),borderColor:'grey',borderWidth:.5}} color="white"><Text style={{color:'#3b3b3b',fontFamily:'KGPrimaryWhimsy',fontSize:20}}>Afslut aftale</Text></Button> */}
            <Button round onPress={()=> this.state.owner_approved ? this.kontaktoplysninger1 : this.showDialog() }  style={{width:wp('80%'),borderColor:'grey',borderWidth:.5}} color="white"><Text style={{color:'#3b3b3b',fontFamily:'KGPrimaryWhimsy',fontSize:20}}>Afstand på kort</Text></Button>
            <Button round onPress={()=>this.toggleModal()}  style={{width:wp('80%') }} color="#54969c"><Text style={{color:'white',fontFamily:'KGPrimaryWhimsy',fontSize:20}}>Afslut vores aftale</Text></Button>
            
        </Block>
    </ModalBox>
    
    <Dialog
        visible={this.state.showDialogMessage}
        onTouchOutside={() => { this.setState({ showDialogMessage: false }); }}
        onHardwareBackPress={() => {this.setState({ showDialogMessage: false });}}
        dialogStyle={{backgroundColor:'white',width:wp('90%'),alignItems:'center',alignContent:'center'}}>
        <DialogContent>
            <View styles={{alignItems:'center', }}>
                <Text  style={{fontSize:28, color:'#eca6a1', padding: 12, textAlign:'center', fontFamily:'KGPrimaryWhimsy',}}>{this.state.dialog_message}</Text>
            </View>
        </DialogContent>
    </Dialog>

  </View>
  
    );
    
  }
  }


  const mapStateToProps = (state) =>{
    return{
      reservationList:state.count.reservationList,
      reserveCount:state.count.reservationBadgeCount,
      message_notif:state.count.messages_notif
    }
  }

 
const mapDispatchToProps = (dispatch) =>{
  return{
    heartBagNotifCount:(data)=>dispatch(heartBagNotifCount(data)),
    update_message_notif:(data)=>dispatch(messages_notif(data))
  }
}
  
export default connect(mapStateToProps,mapDispatchToProps)(Chat)
   

const styles = StyleSheet.create({
  container: {
    flex: 1,
    //alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: 'transparent'
  },
  button5: {
    marginTop:5,
    backgroundColor: '#54969c',
    borderColor: 'white',
    borderWidth: .5,
    borderRadius: 12,
    color: 'white',
    overflow: 'hidden',
    padding: 12,
    textAlign:'center',
    width: wp('80%'),
    height: 48,
    opacity: 0.9,
    fontFamily:'KGPrimaryWhimsy',
    fontSize:20,
    alignSelf:'center',
    alignItems:'center'
  },
  button6: {
    marginTop:10,
    backgroundColor: '#54969c',
    borderColor: 'white',
    borderWidth: .5,
    borderRadius: 12,
    color: 'white',
    overflow: 'hidden',
    padding: 12,
    textAlign:'center',
    width: wp('70%'),
    height: 48,
    opacity: 0.9,
    fontFamily:'KGPrimaryWhimsy',
    fontSize:20,
    alignSelf:'center',
    alignItems:'center'
  },
  container2: {
    flex: 1,
    //alignItems: 'center',
    //justifyContent: 'flex-start',
    
    backgroundColor: 'transparent'
    //bottom: Dimensions.get('screen').height /5
  },
  textstyle:{
    fontFamily:'KGPrimaryWhimsy',
    fontSize:40
  },
  container3: {
    position:'relative',
    backgroundColor: 'transparent',
    height:hp('65%'),
    width:wp('100%'),
    bottom:-10,
    flex:1
    // bottom:hp('15%'),
    //justifyContent: "flex-end",
    //alignItems: "center"
  },
  searchIcon: {
    padding: 10,
},
searchSection: {
  flex: 1,
  flexDirection: 'row',
  justifyContent: 'center',
  alignItems: 'center',
  backgroundColor: 'transparent',
},
  dropdown: {
    height: Dimensions.get('screen').width /7,
    padding: 5,
    position:'relative',
    margin:5,
    borderWidth: .2,
    borderRadius: 12,
    textShadowColor: 'black',
    borderColor: 'black',
    marginBottom: 10,
    backgroundColor: 'white',
    opacity: 0.9,
    fontFamily:'KGPrimaryWhimsy',
    fontSize:20,
    alignContent:'center'
  },
  wrpper: {
    opacity: 0.9,
    fontSize:20,
    fontFamily:'KGPrimaryWhimsy',
    flexDirection: 'row', justifyContent: 'space-around',   
  },
  pop_button: {
    backgroundColor: '#54969c',
    borderColor: 'white',
    borderWidth: .5,
    borderRadius: 12,
    color: 'white',
    overflow: 'hidden',
    padding: 12,
    textAlign:'center',
    width: 300,
    height: 48,
    opacity: 0.9,
    fontSize:20,
    fontFamily:'KGPrimaryWhimsy',
    alignSelf:'flex-end',
  },
  button: {
    backgroundColor: '#54969c',
    borderColor: 'white',
    borderWidth: .5,
    borderRadius: 12,
    color: 'white',
    overflow: 'hidden',
    padding: 12,
    textAlign:'center',
    width: 300,
    height: 48,
    opacity: 0.9,
    fontSize:20,
    fontFamily:'KGPrimaryWhimsy'
  },
  button2: {
    position:'relative',
    backgroundColor: 'white',
    borderColor: 'grey',
    borderWidth: .5,
    borderRadius: 12,
    color: '#54969c',
    overflow: 'hidden',
    padding: 12,
    textAlign:'center',
    width: wp('80%'),
    height: 48,
    opacity: 0.9,
    fontSize:20,
    alignSelf:'center',
    fontFamily:'KGPrimaryWhimsy'
  },
  button3: {
    backgroundColor: 'white',
    borderColor: 'grey',
    borderWidth: .5,
    borderRadius: 12,
    color: '#54969c',
    overflow: 'hidden',
    padding: 10,
    textAlign:'center',
    width: wp('80%'),
    height: hp('5%'),
    opacity: 0.9,
    fontSize:Fsize,
    fontFamily:'KGPrimaryWhimsy'
  },
  bodyViewStyle: {
    flex: 1,
    justifyContent: 'center', 
    alignItems: 'center',
  },
  headerLayoutStyle: {
    width, 
    height: 20, 
    backgroundColor: '#54969c', 
    justifyContent: 'center', 
    alignItems: 'center',
  },
  slidingPanelLayoutStyle: {
   
    alignContent:'center',
    height:hp('100%'), 
    backgroundColor: 'transparent', 
    alignItems: 'center',
  },
  commonTextStyle: {
    color: 'white', 
    fontSize: 18,
  },
});
import React from "react";
import {
  ScrollView,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
  Image,
  KeyboardAvoidingView,
  TouchableWithoutFeedback,
  Platform,
  TextInput,
  Keyboard
} from "react-native";
import SwipeableRating from 'react-native-swipeable-rating';
import { Container, Content, List, ListItem, Thumbnail,Icon,Button,InputGroup,Input,Body,Right,Badge } from 'native-base';
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';
import Modal from 'react-native-modal';
const Fsize2 = Platform.OS === 'ios' ?25 : 18;
const Fsize = Platform.OS === 'ios' ?22 : 16;
export default class MusicScreen extends React.Component {

  state = {
      activeSlide: 0,
      isModalVisible:false,
      rating:0
    };

  toggleModal = () => {
    this.setState({
        isModalVisible: !this.state.isModalVisible,
        
    });
  };

  componentDidMount() {
    // this.props.navigation.setParams({
    //   menuIcon: this.props.user.profileURL
    // });
  }
  basketOverview =()=>{
    this.props.overview();
  }
  buyshipping(){
    this.props.buyshipping();
     
   }
  next =()=>{
    this.props.navigation.navigate('DinKonto');
  }

  render() {
    return (
       
          <View style={{flex:1,backgroundColor:'transparent'}}>
             <Modal isVisible={this.state.isModalVisible} style={{height:hp('50%')}} onBackdropPress={()=>this.setState({isModalVisible:false})} animationOut="fadeOutLeft" onSwipeStart={()=>console.log("ayay")}>
        <KeyboardAvoidingView>
        <TouchableWithoutFeedback onPress={()=>Keyboard.dismiss()}>
          <View style={{backgroundColor:'white',alignContent:'center',alignItems:'center',height:hp('50%'),borderRadius: hp('10%')/1.2}}>
          <Text style={{alignSelf:'center',top:20,fontSize:Fsize2,fontFamily:'KGPrimaryWhimsy',color:'#d47d7d'}}>Bedøm familien</Text>
            <View style={{top:40}}>
             
            <SwipeableRating
            rating={this.state.rating}
            size={28}
            maxRating={6}
            gap={2}
            color='#e8b14a'
            emptyColor='#e8b14a'
            onPress={this.handleRating}
            allowHalves={true}
            style={{position:'absolute',alignSelf:'center'}}
        />
        </View>
        <View style={{top:80}}>
          
    
    <TextInput multiline  style={{maxHeight: 80}} rounded style={{backgroundColor:'white',height:100,width:wp('80%'),borderColor: '#bfbfbf',
    borderWidth: .5,
    borderRadius: 12,padding:15,paddingBottom:5,paddingTop:10,shadowColor: '#000',
    shadowOffset: { width: 0, height: 2 },
    shadowOpacity: 0.5,
    shadowRadius: 1.5,
    elevation: 1,fontFamily:'KGPrimaryWhimsy',fontSize:Fsize,marginBottom:20}} 
    // onChangeText={text => this.setState({messages:text})}
    autoCorrect={true}
  //   onEndEditing={(e) => 
  //   {
  //       )
  //   }
  // }
   
    ref={input => { this.textInput = input }}
    />
   
   <TouchableOpacity onPress={() =>console.log("ayay")} >
      <Text style={styles.button5}>Afslut aftalen</Text>           
    </TouchableOpacity>
      </View>
      
          </View>
          </TouchableWithoutFeedback>
          </KeyboardAvoidingView>
        </Modal>
          <Container style={{backgroundColor:'white',height:wp('100%'),width:wp('100%'),marginBottom:-40}} >
        <View style={{flex:1,flexDirection:'column',alignContent:'center',alignItems:'center'}}>
        <Body >
      <Image resizeMode='contain' style={{width:wp('30%'),height:hp('30%'),top:hp('15%')}} source={require('../../../assets/images/slice12.png')} />        
        </Body>
        <Body style={{width:wp('85%'),alignItems:'center',bottom:50}}>
        <TouchableOpacity onPress={()=>console.log("kob")} style={{marginBottom:20}}>
          <Text style={styles.button}>Køb porto</Text>
        </TouchableOpacity>
        <TouchableOpacity onPress={()=>console.log("ayay")} style={{marginBottom:20}}>
          <Text style={styles.button}>Afstand på kort</Text>
        </TouchableOpacity>
        <TouchableOpacity onPress={()=>this.toggleModal()}>
          <Text style={styles.buttonGreen}>Afslut vores aftale</Text>
        </TouchableOpacity>
        </Body>
        </View>       
       </Container>
       
       <Dialog
            visible={this.state.kontaktoplysninger1}
            onTouchOutside={() => { this.setState({ reportPopupShow: false }); }}
            dialogStyle={{backgroundColor:'white',width:wp('75%'),alignItems:'center',alignContent:'center'}}>
            <DialogContent>
                <View styles={{alignItems:'center'}}>
                    <Text  style={{fontSize:22, color:'grey', padding: 12, textAlign:'center', fontFamily:'KGPrimaryWhimsy',}}>Kontaktoplysninger</Text>
                </View>
                <View styles={{alignItems:'center'}}>
                    <Text  style={{fontSize:22, color:'grey', padding: 12, textAlign:'center', fontFamily:'KGPrimaryWhimsy',}}>Telefon:</Text>
                    <Text  style={{fontSize:22, color:'grey', padding: 12, textAlign:'center', fontFamily:'KGPrimaryWhimsy',}}></Text>
                </View>
                <View styles={{alignItems:'center'}}>
                    <Text  style={{fontSize:22, color:'grey', padding: 12, textAlign:'center', fontFamily:'KGPrimaryWhimsy',}}>Adresse:</Text>
                    <Text  style={{fontSize:22, color:'grey', padding: 12, textAlign:'center', fontFamily:'KGPrimaryWhimsy',}}></Text>
                </View>
            </DialogContent>
        </Dialog>
        
        <Dialog
            visible={this.state.kontaktoplysninger2}
            onTouchOutside={() => { this.setState({ reportPopupShow: false }); }}
            dialogStyle={{backgroundColor:'white',width:wp('75%'),alignItems:'center',alignContent:'center'}}>
            <DialogContent>
                <View styles={{alignItems:'center'}}>
                    <Text  style={{fontSize:22, color:'grey', padding: 12, textAlign:'center', fontFamily:'KGPrimaryWhimsy',}}>Kontaktoplysninger</Text>
                    <Text  style={{fontSize:22, color:'grey', padding: 12, textAlign:'center', fontFamily:'KGPrimaryWhimsy',}}>Familien har endnu ikke offentliggjort deres adresse og telefon.</Text>
                    <TouchableOpacity onPress={()=>console.log("kob")} style={{marginBottom:20}}>
                        <Text style={styles.button}>Anmod</Text>
                    </TouchableOpacity>
                </View>
            </DialogContent>
        </Dialog>
     
          </View> 
      
    );
  }
}

const styles = StyleSheet.create({
  container: {
    backgroundColor: "white",
    flex: 1,
    width:wp('100%'),
    height:hp('100%')
  },
  button5: {
    marginTop:5,
    backgroundColor: '#54969c',
    borderColor: 'white',
    borderWidth: .5,
    borderRadius: 12,
    color: 'white',
    overflow: 'hidden',
    padding: 12,
    textAlign:'center',
    width: 300,
    height: 44,
    opacity: 0.9,
    fontFamily:'KGPrimaryWhimsy',
    fontSize:20,
    alignSelf:'center',
    alignItems:'center'
  },
  title: {
    fontSize: 25
  },
  userPhoto: {
    width: 40,
    height: 40,
    borderRadius: 20,
    marginLeft: 5
  },
  button: {
    backgroundColor: 'white',
    borderColor: 'black',
    borderWidth: .5,
    borderRadius: 12,
    color: '#54969c',
    overflow: 'hidden',
    padding: 12,
    textAlign:'center',
    width: wp('90%'),
    height: hp('7%'),
    opacity: 0.9,
    fontFamily:'KGPrimaryWhimsy',
    fontSize:20
  },
  buttonGreen: {
    backgroundColor: '#54969c',
    borderColor: 'black',
    borderWidth: .5,
    borderRadius: 12,
    color: 'white',
    overflow: 'hidden',
    padding: 12,
    textAlign:'center',
    width: wp('90%'),
    height: hp('7%'),
    opacity: 0.9,
    fontFamily:'KGPrimaryWhimsy',
    fontSize:20
  },
});

 
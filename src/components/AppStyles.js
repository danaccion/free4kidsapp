import { StyleSheet, Dimensions,Platform } from 'react-native';

// screen sizing
const { width, height } = Dimensions.get('window');
// orientation must fixed
const SCREEN_WIDTH = width;
const Fsize = Platform.OS === 'ios' ?17 : 13;
const Fsize2 = Platform.OS === 'ios' ?15 : 11;
const recipeNumColums = 2;
// item size
const RECIPE_ITEM_HEIGHT = 160;
const RECIPE_ITEM_MARGIN = 30;

// 2 photos per width
export const RecipeCard = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    marginLeft: 6,
    marginRight:30,
    marginTop: 20,
    width: (SCREEN_WIDTH - (recipeNumColums + 1) * RECIPE_ITEM_MARGIN) / recipeNumColums,
    height: RECIPE_ITEM_HEIGHT + 70,
    backgroundColor:'transparent',
    // marginBottom:10,

  },
   
  photo: {
    width: (SCREEN_WIDTH - (recipeNumColums + 1) * RECIPE_ITEM_MARGIN) / recipeNumColums-20,
    height: RECIPE_ITEM_HEIGHT,
    marginTop:10,
    // borderRadius: 15,
    padding:10,
    borderBottomLeftRadius: 0,
    borderBottomRightRadius: 0,
    zIndex:11
  },
  photo4: {
    width: (SCREEN_WIDTH - (recipeNumColums + 1) * RECIPE_ITEM_MARGIN) / recipeNumColums,
    height: RECIPE_ITEM_HEIGHT,
    // borderRadius: 15,
    // padding:10,
    fontSize: Platform.OS === 'ios' ?11 : 9,
    position:'absolute',
    zIndex:0,
    borderBottomLeftRadius: 0,
    borderBottomRightRadius: 0,
    zIndex:11
  },
  loader: {
    width: (SCREEN_WIDTH - (recipeNumColums + 1) * RECIPE_ITEM_MARGIN) / recipeNumColums,
    height: RECIPE_ITEM_HEIGHT-50,
    marginBottom:50
  },
  title: {
    flex: 1,
    fontFamily: 'KGPrimaryWhimsy',
    fontSize: Fsize,
    fontWeight: 'bold',
    textAlign: 'center',
    color: '#444444',
    // marginTop: 3,
  },
  category: {
    marginTop: 1,
    marginBottom:10,
    fontWeight:'600',
    color:'#660404'
  },
  type: {
    // marginTop: 2,
    flex: 1,
    fontFamily: 'KGPrimaryWhimsy',
    fontSize: Platform.OS === 'ios' ?11 : 9,
    marginBottom:4,
    textAlign: 'center',
    fontWeight:'400',
    color:'#444444'
  }
});

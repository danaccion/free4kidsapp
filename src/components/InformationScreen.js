import React, { Component } from "react";
import {
    View,
    Text,
    StyleSheet,
    ScrollView,
    Image,TouchableOpacity,
    Platform,
    PanResponder,
    ImageBackground
} from "react-native";
import { Container, Content, Header, Item, Icon, Input, Button } from 'native-base'
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons'
import { PanGestureHandler, State } from 'react-native-gesture-handler';
import Animated from 'react-native-reanimated';

 
const {
  set,
  cond,
  eq,
  spring,
  startClock,
  stopClock,
  clockRunning,
  defined,
  Value,
  Clock,
  event
} = Animated;

function runSpring(clock, value, velocity, dest) {
  const state = {
    finished: new Value(0),
    velocity: new Value(0),
    position: new Value(0),
    time: new Value(0) 
  };
  const config = {
    damping: 7,
    mass: 1,
    stiffness: 121.6,
    overshootClamping: false,
    restSpeedThreshold: 0.001,
    restDisplacementThreshold: 0.001,
    toValue: new Value(0)
  };

  return [
    cond(clockRunning(clock), 0, [
      set(state.finished, 0),
      set(state.velocity, velocity),
      set(state.position, value),
      set(config.toValue, dest),
      startClock(clock)
    ]),
    spring(clock, state, config),
    cond(state.finished, stopClock(clock)),
    state.position
  ];
}
class Information extends Component {
  constructor() {
    super();
    this.translateX = new Value(0);
    const dragX = new Value(0);
    const state = new Value(-1);
    const dragVX = new Value(0);

    this.state = {
      pages:1,
      move:true
    };

    this.onGestureEvent = event([
      {
        nativeEvent: {
          translationX: dragX,
          velocityX: dragVX,
          state: state
        }
      }
    ]);

    
    const clock = new Clock();
    const transX = new Value();
    this.translateX = cond(
      eq(state, State.ACTIVE),
      [
        //state active,
        stopClock(clock),
        set(transX, dragX),
        transX
      ],
      [
        set(
          transX,
          cond(defined(transX), runSpring(clock, transX, dragVX, 0), 0)
        )
      ]
    );
    this.translateX = cond(
      eq(state, State.ACTIVE),
      [
        //state active,
        stopClock(clock),
        set(transX, dragX),
        transX
      ],
      [
        set(
          transX,
          cond(defined(transX), runSpring(clock, transX, dragVX, 0), 0)
        )
      ]
    );
    this._panResponder=PanResponder.create({
      onStartShouldSetPanResponder:(evt,gestureState)=>this.state.move,
      onPanResponderRelease:(evt, gestureState)=>{
        // console.log(evt.touchHistory.touchBank[1].currentPageX);
        if((gestureState.dx<0&&this.state.pages<4)){
         this.setState({pages:this.state.pages+1})
         if(this.state.pages==3){
          this.SubmitDetails();
        }else{
          this.setState({move:true})
        }
        }else if((gestureState.dx>0&&this.state.pages>1)){
          if(this.state.pages!==1){
           this.setState({pages:this.state.pages-1})
          }
           if(this.state.pages==1){
            console.log("stop")
          }else{
            this.setState({move:true})
          }  
        }   

        if(gestureState.dx<0&&this.state.pages<4){
          this.setState({pages:this.state.pages+1})
          if(this.state.pages==3){
           this.SubmitDetails();
         }else{
           this.setState({move:true})
         }
         }else if(gestureState.dx>0&&this.state.pages>1){
           if(this.state.pages!==1){
            this.setState({pages:this.state.pages-1})
            }
            if(this.state.pages==1){
             console.log("stop")
           }else{
             this.setState({move:true})
           }  
         }   
        
      }
    })
  }
  componentDidMount() {
    this.ayay();
  }
  ayay(){
    console.log("ComponenDidMount on InformationScreen : Done Executing Render ")
  }
  SubmitDetails(){
   this.props.submit();
    
  }


  
    render() {
      const resizeMode = 'cover';
      // if(this.state.pages==4){
      //   this.props.submit();
      //   console.log(this.props.submit)
      // }
            return (  
              <View style={styles.container}
             
              >
                <ImageBackground
                  style={{
                    backgroundColor: '#ccc',
                    flex: 1,
                    resizeMode,
                    position: 'absolute',
                    width: '100%',
                    height: '100%',
                    justifyContent: 'center',
                  }}
                  source={require('../../assets/images/slice81-min.png')}
                >
                  
                </ImageBackground>  
               
              <View  {...this._panResponder.panHandlers}>
              <PanGestureHandler
                  onGestureEvent={this.onGestureEvent}
                  onHandlerStateChange={this.onGestureEvent}
                >
                  <Animated.View
                    style={[
                      styles.container2,
                      {
                        transform: [{ translateX: this.translateX }]
                      }
                    ]}
                  >

                    {this.state.pages==1?(
                    <View style={styles.dropdown}>
                        <Image resizeMode="contain"
                        style={{transform: [{ rotate: '140deg' }],width:100,position:'absolute',zIndex:100,bottom:250,left:-10}}
                        source={require('../../assets/images/slice15.png')}
                        />           
            
                      <Image resizeMode="contain"
                        style={{ width: 360,
                          height: 230,}}
                        source={require('../../assets/images/slice28-min.png')}
                        />      
                    </View>
                    ):null}
                    {this.state.pages==2?(
                     <View style={styles.dropdown}>
                    
                     <Image resizeMode="contain"
                       style={{ width: 360,
                         height: 230,}}
                       source={require('../../assets/images/slice27-min.png')}
                       />      
                   </View>
                    ):null}
                     {this.state.pages==3?(
                     <View style={styles.dropdown}>
                    <Image resizeMode="contain"
                        style={{transform: [{ rotate: '-45deg' }],width:120,position:'absolute',zIndex:100,bottom:50,right:-13}}
                        source={require('../../assets/images/slice7.png')}
                        />        
                     <Image resizeMode="contain"
                       style={{ width: 360,
                         height: 230,}}
                       source={require('../../assets/images/slice29-min.png')}
                       />      
                   </View>
                    ):null}
                      </Animated.View>
                </PanGestureHandler>
                </View>
                
              </View>          
            )
        }
    }

export default Information;

const styles = StyleSheet.create({
    container: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center'
    },
    button: {
      backgroundColor: '#54969c',
      borderColor: 'white',
      borderWidth: .5,
      borderRadius: 12,
      color: 'white',
      overflow: 'hidden',
      padding: 12,
      textAlign:'center',
      width: 300,
      height: 44,
      opacity: 0.9,
    },
    container2: {
        flex: 5,
        justifyContent: "flex-end",
        alignItems: "center",
        alignContent:'center',
      },
      dropdown: {
        width: 410,
        height: 300,
        padding: 10,
        alignItems: "center", 
      },
      container3: {
        justifyContent: "center",
        alignItems: "center",
      },
      box: {
        flex:1,
        height: 200,
        width: 400,
        backgroundColor: 'red'
      }

    
});


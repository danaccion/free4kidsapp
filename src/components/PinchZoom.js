import React, { Component } from 'react';
import { Platform, StyleSheet, View,Dimensions } from 'react-native';
 
import { PinchGestureHandler, State, TapGestureHandler } from 'react-native-gesture-handler';
const { width, height } = Dimensions.get('window');
export default class PinchZoom extends Component {
  onGesturePinch = ({ nativeEvent }) => {
    this.props.onPinchProgress(nativeEvent.scale)
  }
  doubleTapRef = React.createRef();
  onPinchHandlerStateChange = (event) => {
    const pinch_end = event.nativeEvent.state === State.END
    const pinch_begin = event.nativeEvent.oldState === State.BEGAN
    const pinch_active = event.nativeEvent.state === State.ACTIVE
    if (pinch_end) {
      this.props.onPinchEnd()
    }
    else if (pinch_begin && pinch_active) {
      this.props.onPinchStart()
    }
  }
  _onDoubleTap = event => {
    if (event.nativeEvent.state === State.ACTIVE) {
      this.props.doubleTap()
    }
  };
  render() {
    return (
      <PinchGestureHandler
        onGestureEvent={this.onGesturePinch}
        onHandlerStateChange={this.onPinchHandlerStateChange}>
          <TapGestureHandler
            ref={this.doubleTapRef}
            onHandlerStateChange={this._onDoubleTap}
            numberOfTaps={2}>
        <View>
          {this.props.children}
        </View>
        </TapGestureHandler>
      </PinchGestureHandler>
    )
  }
}

const styles = StyleSheet.create({
  preview: {
    height: Dimensions.get('window').height,
    width: "100%",
  },
})
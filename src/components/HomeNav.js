import React ,{Component} from 'react';
import { View, StyleSheet, Image ,Text,TouchableOpacity,Dimensions,Platform,Keyboard} from 'react-native';
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';
import {
    Block,Icon,NavBar
  } from 'galio-framework';
  import Modal from 'react-native-modalbox';
  import { Searchbar } from 'react-native-paper';
  const Fsize2 = Platform.OS === 'ios' ?20 : 16;

  export default class HomeNav extends Component {
      state={
        searchValue:null,
      }
    searchData(){
        this.refs.searchModal.open()
        // this._input.focus();
      }
      updateSearch = search => {
        console.log(search)
        this.setState({ searchValue:search })
        this.props.onSearch(search);
      };  
      openBotNav(){
        this.props.openBotNav();
     }
     submitSearch(value){
        this.props.onSearch(value);
     }
  render() {
    const {searchValue} = this.state
    const pickerValues = [
        {
          title: 'Lav en specifik søgen',
          value: '1'
        },
        {
          title: 'Ændre din placering',
          value: '2'
        },
        {
          title: 'Vis dem jeg følger',
          value: '3'
        },
        {
          title: 'Vis gemte børneting',
          value: '4'
        },
        {
          title: 'Donér børneting',
          value: '5'
        }
      ]
      return (
       
            <Block row middle style={{marginTop:5,backgroundColor:'transparent'}}>
                 <Modal backButtonClose={true} position={"bottom"} style={{height:hp('30%'),zIndex:4000,borderTopLeftRadius: 20, 
        borderTopRightRadius: 20,
        overflow: "hidden"}} transparent={true} ref={"modal4"} >
                <Block style={{
                  flex:1,
                  backgroundColor: 'transparent',
                  bottom: 10,
                  zIndex:200,
                  width:  wp('98%'),
                  alignItems: 'center',
                  position: 'absolute',
                  height:hp('28%'),
                  zIndex:5000
                 }}>
                  { pickerValues.map((value, index) => {
                    return <TouchableOpacity key={index} onPress={() => this.setPickerValue(value.value)} style={{ paddingTop: hp('0.54%'), paddingBottom: hp('0.54%'),borderColor: 'grey',borderWidth: .2,
                    borderRadius: 12,marginBottom:hp('0.27%'),width:  Dimensions.get('screen').width/1.1, marginTop:hp('0.27%'),shadowColor: '#000',
                    shadowOffset: { width: 0, height: 2 },
                    shadowOpacity: 0.5,
                    shadowRadius: 1,
                    elevation: 1,}}>
                        <Text style={{
                            margin:1,
                            textAlign:'center',
                            fontSize: Platform.OS === 'ios' ?20 : 18,
                            color:'#e87289',
                            alignSelf:'center',
                            fontFamily:'KGPrimaryWhimsy',
                            }}>{ value.title }</Text>
                      </TouchableOpacity>
                  })}
                </Block>
               
              </Modal>   
            <Modal backButtonClose={true} entry={"top"} backdropOpacity={0} onClosed={()=>Keyboard.dismiss()}  position={"top"} style={{position:'absolute',backgroundColor:'transparent',top:12,borderRadius:10,height:hp('13%'),zIndex:3000,
              width:wp('95%')}} transparent={true} ref={"searchModal"} >
              <Block middle style={{backgroundColor:'white'}}>
               <Searchbar
                  onIconPress={()=>this.submitSearch(this.state.searchValue)}
                  placeholder="Søg"
                  ref={(c) => this._input = c}
                  searchAccessibilityLabel={'Søg'}
                  onChangeText={query => this.updateSearch(query)}
                  value={searchValue}
                  // onFocus={()=>this.setState({topSpace:-10})}
                  onSubmitEditing={()=>this.submitSearch(this.state.searchValue)}
                  onTouchCancel={()=>this.setState({searchValue:null})}
                  inputStyle={{fontFamily:'KGPrimaryWhimsy',width:wp('90%'),color:'grey',zIndex:500}}>
                
                  </Searchbar>
                  
               
                </Block>
                {this.state.searchValue===''||this.state.searchValue===null?(
                <TouchableOpacity style={{position:'absolute',marginRight:15,width:20,zIndex:999,top:14,right:1}} onPress={() => this.refs.searchModal.close()}>
                    <Icon 
                    name="x"
                    family="feather"
                    size={20}
                    color={'#3d3d3d'}
                
                    />
                </TouchableOpacity>
                ):null}
              </Modal>
            <Block middle row space="evenly" style={{  
                backgroundColor: '#c97190',
                borderColor:'white',
                borderWidth: .5,
                borderRadius: 10,
                overflow: 'hidden',
                top:hp('2%'),
                shadowColor:'black',
                width:  wp('95%'),
                height: Dimensions.get('screen').height /17,
                opacity: 0.9,
                }}>
                <TouchableOpacity onPress={()=>this.searchData()} style={{marginLeft:10}} >
                      <Image resizeMode="contain"  style={{ 
                                zIndex: 100,width:Dimensions.get('screen').height /17+10,height:Dimensions.get('screen').height /17,right:8}} source={require('../../assets/images/slice15_crop.png')}/>  
                </TouchableOpacity>
                <Text adjustsFontSizeToFit numberOfLines={1}   style={{ fontSize:Fsize2 ,zIndex: 100,color:'white',fontFamily:'KGPrimaryWhimsy',alignSelf:'center',marginRight:15}}> Fortæl mig hvad du søger
                </Text>
                <TouchableOpacity style={{marginLeft:10}} onPress={() => this.openBotNav()}>
                    <Icon 
                    name="menu"
                    family="feather"
                    size={25}
                    color={'#3d3d3d'}
                
                    />
                </TouchableOpacity>
                
                </Block>
          </Block> 
      );
  }
}

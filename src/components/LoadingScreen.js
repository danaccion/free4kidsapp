
import React from 'react'
import {
  View,
  Button,
  TextInput,
  StyleSheet,
  TouchableHighlight,
  Image,
  Text,
  TouchableOpacity,
  Picker,
  Platform,
  ImageBackground,
  Dimensions,
  AppState
} from 'react-native'
 import ProgressBarAnimated from 'react-native-progress-bar-animated';
import { Asset } from 'expo-asset';
const checkB = [
  {
      label: '',
  },
];
const { width, height } = Dimensions.get('window');
async function loadResourcesAsync() {
  await Promise.all([
    Asset.loadAsync([
      require('../../assets/images/slice80-min.png'),
    ]),
  ]);
}
 
export default class LoadingScreen extends React.Component {
  static navigationOptions = {
    //To hide the NavigationBar from current Screen
    header: null
  };
  
  increase = (key, value) => {
    this.setState({
      [key]: this.state[key] + value,
    });
  }
  state = {
    appState: AppState.currentState,
    progress: 100,
    progressWithOnComplete: 0,
    progressCustomized: 0,
  };

  componentDidMount() {
    loadResourcesAsync()
    AppState.addEventListener('change', this._handleAppStateChange);
  }

  componentWillUnmount() {
    AppState.removeEventListener('change', this._handleAppStateChange);
  }

  _handleAppStateChange = (nextAppState) => {
    if (
      this.state.appState.match(/inactive|background/) &&
      nextAppState === 'active'
    ) {
      console.log('App has come to the foreground!');
    }
    this.setState({appState: nextAppState});
    console.log(this.state.appState);
  };

  
  render() {
    const resizeMode = 'cover';
    const barWidth = Dimensions.get('screen').width / 2;
    const barHeight = Dimensions.get('screen').height / 90;
   
    return (

        <View style={styles.container}>
            
           <ImageBackground
        style={{
          backgroundColor: '#ccc',
          flex: 1,
          resizeMode,
          position: 'absolute',
          width: '100%',
          height: '100%',
          justifyContent: 'center',
        }}
        source={require('../../assets/images/slice80-min.png')}
      >
         
      </ImageBackground>
      <View style={{position:'absolute',flex: 1,
                            top:10}}>
      <Image resizeMode="contain"
                        style={{width:300,height:200, }}
                        source={require('../../assets/images/slice2.png')}
                        />    
       <View style={{alignSelf:'center'}}>
        <ProgressBarAnimated
            
            width={barWidth}
            height={barHeight}
            value={this.state.progress}
            backgroundColorOnComplete="red"
            backgroundColor="#e87289"
          />   
          </View>
          
      </View>
      <View allowFontScaling={false} style={{position:'absolute',bottom:10,alignSelf:'center'}}> 
      <Text allowFontScaling={false} style={{alignSelf:'center',color:'#525252',fontSize:17 }}>Version 19 Development Mode</Text> 
      </View>
     
        </View>
    )
  }
    
}

const styles = StyleSheet.create({ 
    buttonContainer: {
        marginTop: 15,
      },
      separator: {
        marginVertical: 30,
        borderWidth: 0.5,
        borderColor: '#DCDCDC',
      },
      label: { 
        color: '#999',
        fontSize: 14,
        fontWeight: '500',
        marginBottom: 10,
      },
  container: {
    flex: 1,
    justifyContent: 'flex-start',
    alignItems: 'center'
  },
  container2: {
    flex: 5,
    justifyContent: "flex-end",
    alignItems: "center"
  },
})

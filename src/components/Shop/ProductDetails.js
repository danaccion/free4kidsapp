import React, { Component } from 'react';
import { SafeAreaView, StyleSheet,View,ImageBackground,Image,ScrollView,Platform,Dimensions,KeyboardAvoidingView,ListView,StatusBar,AsyncStorage,RefreshContro,TouchableOpacity } from 'react-native';
import Ionicons from 'react-native-vector-icons/Ionicons'
import BackButton from '../../BackButton';
import { Container, Header, Content, Form, Item, Picker,Icon,Input,Card,CardItem,Left,Button,Body,Spinner,Thumbnail } from 'native-base';
import HamburgerIcon from '../../icons/HamburgerIcon';
import HeartBagBurger from '../../icons/HeartBagBurger';
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';
import caroustyles, { colors } from '../../styles/carousel/index.style';
import { SliderBox } from "react-native-image-slider-box";
import Carousel, { Pagination } from 'react-native-snap-carousel';
import Dialog, {SlideAnimation,DialogContent } from 'react-native-popup-dialog';
import { Rating, AirbnbRating } from 'react-native-ratings';
import WrappedText from "react-native-wrapped-text";
import ProgressiveImage from '../ProgressiveImage';
import SwipeableRating from 'react-native-swipeable-rating';
import Modal from 'react-native-modal';
import { Snackbar } from 'react-native-paper';
import styles2 from '../Giveaways/styles';
import { PH,SS } from 'react-native-dotenv'
const { width } = Dimensions.get('screen');
import {
  Block,Text
} from 'galio-framework';
import {createStore} from 'redux'
import SingleProduct from './SingleProduct';
// const IS_ANDROID = Platform.OS === 'android';
const SLIDER_1_FIRST_ITEM = 1;
const Fsize2 = Platform.OS === 'ios' ?14 : 13;
const Fsize3 = Platform.OS === 'ios' ?10 : 8;
const Fsize = Platform.OS === 'ios' ?20 : 20;
const { width: viewportWidth } = Dimensions.get('window');


export default class ProductDetails extends Component {
  static navigationOptions = () => {
    return {
        headerLeft: <BackButton/>,
        headerTitle: (
            <Image
            resizeMode="contain"
            source={require('../../../assets/images/slice2.png')}
            style={{ width: 10, height:10 , flex:4,justifyContent: 'space-around', alignItems: 'center', padding: 25,}}
          />
          ),//<Provider store={store}></Provider>
        headerRight: <HeartBagBurger/>
    };
};

 
  render(){
     
    return (
      // <Provider store={storeA} context={ContextA}>
        <SingleProduct />
      // </Provider>
  
   
    );
}
  }
  // export default connect(null,null)(ProductDetails);  // <<--- here :)
  
   

const styles = StyleSheet.create({
  container: {
    flex: 1,
    //alignItems: 'center',
    justifyContent: 'flex-end',
    backgroundColor: 'transparent'
  },
  container2: {
    position:'absolute',
    backgroundColor: 'transparent',
    flex: 1,
    height:hp('100%'),
    width:wp('100%'),
    top:0,
    bottom:0,
    alignContent:'center'
  },
  textstyle:{
    fontFamily:'KGPrimaryWhimsy',
    fontSize:40
  },
  container3: {
    flex: 1,
    position:'absolute',
    alignItems: "center",
    alignContent:'center',
    justifyContent: 'flex-end',
  },
  dropdown: {
    height: Dimensions.get('screen').width /7,
    padding: 5,
    position:'relative',
    margin:5,
    borderWidth: .2,
    borderRadius: 12,
    textShadowColor: 'black',
    borderColor: 'black',
    marginBottom: 10,
    backgroundColor: 'white',
    opacity: 0.9,
    fontFamily:'KGPrimaryWhimsy',
    fontSize:20,
    alignContent:'center'
  },
  wrpper: {
    opacity: 0.9,
    fontSize:20,
    fontFamily:'KGPrimaryWhimsy',
    flexDirection: 'row', justifyContent: 'space-around',   
  },
  button: {
    alignSelf:'center',
    marginLeft:10,
    marginRight:20,
    margin:10,
    backgroundColor: 'white',
    borderColor: 'black',
    borderWidth: 3,
    borderTopColor:'red',
    borderRadius: 12,
    color: 'white',
    overflow: 'hidden',
    textAlign:'center',
    width: 350,
    height: 48,
    // opacity: 0.9,
    fontSize:20,
    fontFamily:'KGPrimaryWhimsy'
  },
  button2: {
    backgroundColor: '#54969c',
    borderColor: 'white',
    borderWidth: .5,
    borderRadius: 12,
    color: 'white',
    overflow: 'hidden',
    padding: 12,
    textAlign:'center',
    width: 350,
    height: 48,
    opacity: 0.9,
    fontSize:20,
    fontFamily:'KGPrimaryWhimsy'
  },
  wrapperHorizontal: {
    width: wp('90%'),
    height: hp('20%'),
    justifyContent: 'space-between',
    alignItems: "center",
    alignSelf:'center',
    marginTop:-hp('1%'),
    // color: "black",
  },
  wrapperVertical: {
    width: 100,
    height: 300,
    justifyContent: 'space-between',
    alignItems: "center",
    margin: "auto",
    color: "black"
  },
  itemStyleVertical: {
    marginTop: 10,
    marginBottom: 10,
    width: 50,
    height: 50,
    marginRight:wp('5%'),
    // paddingTop: 13,
    // borderWidth: 1,
    // borderColor: "grey",
    // borderRadius: 25
  },
  itemSelectedStyleVertical: {
    // paddingTop: 11,
    color:'red',
    // borderWidth: 2,
    // borderColor: "#DAA520"
  },
  itemStyleHorizontal: {
    marginLeft: 50,
    marginRight: 50,
    width: 50,
    height: 50,
    // paddingTop: 13,
    // borderWidth: 1,
    // borderColor: "grey",
    // borderRadius: 25
  },
  itemSelectedStyleHorizontal: {
    paddingTop: 11,
    borderWidth: 2,
    borderColor: "#DAA520"
  },
  underline: {textDecorationLine: 'underline'},
  button5: {
    backgroundColor: '#54969c',
    borderColor: 'white',
    borderWidth: .5,
    borderRadius: 12,
    color: 'white',
    overflow: 'hidden',
    padding: 12,
    textAlign:'center',
    width: 300,
    height: 44,
    opacity: 0.9,
    fontFamily:'KGPrimaryWhimsy',
    fontSize:20
  },
  button3: {
    backgroundColor: '#54969c',
    borderColor: 'white',
    color: 'white',
    overflow: 'hidden',
    padding: 12,
    textAlign:'center',
    width: wp('93%'),
    height: 44,
    opacity: 0.9,
    fontFamily:'KGPrimaryWhimsy',
    fontSize:15,
    alignSelf:'center'
  },
});


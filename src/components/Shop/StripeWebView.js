import React, { Component } from 'react';
import {  Alert, Platform, View, ViewPropTypes,AsyncStorage } from 'react-native';
import {WebView} from 'react-native-webview';
import { PropTypes } from 'prop-types';
import { PH,SS } from 'react-native-dotenv'

export default class StripeCheckout extends Component {

  state = {
    token: '',
  
  };

  componentDidMount = async ()=>{
    const { navigation } = this.props;
    const item = navigation.getParam('product');
    const location = AsyncStorage.getItem('location');
    const page = navigation.getParam('page');
    let user_id= await AsyncStorage.getItem('user_id')
  }
  
  
  render() {
    const jsCode = `(function() {
                    var originalPostMessage = window.postMessage;
                    var patchedPostMessage = function(message, targetOrigin, transfer) {
                      originalPostMessage(message, targetOrigin, transfer);
                    };
                    patchedPostMessage.toString = function() {
                      return String(Object.hasOwnProperty).replace('hasOwnProperty', 'postMessage');
                    };
                    window.postMessage = patchedPostMessage;
                  })();`;
        return  (<WebView
        javaScriptEnabled={true}
        scrollEnabled={false}
        bounces={false}
        injectedJavaScript={jsCode}
        source={{ html: `<script src="https://checkout.stripe.com/checkout.js"></script>
            <script>
            var handler = StripeCheckout.configure({
              key: '${this.props.publicKey}',
              image: '${this.props.imageUrl}',
              locale: 'auto',
              token: function(token) {
                window.ReactNativeWebView.postMessage(JSON.stringify(token.id));
              },
            });
            window.onload = function() {
              handler.open({
                image: '${this.props.imageUrl}',
                description: '${this.props.description}',
                amount: ${this.props.amount},
                currency: '${this.props.currency}',
                allowRememberMe: ${this.props.allowRememberMe},
                email: '${this.props.repopulatedEmail}',
                closed: function() {
                  alert("closed: "+token)
                  window.postMessage("WINDOW_CLOSED", "*");
                }
              });
            };
            </script>`, baseUrl: ''}}
        onMessage={event => event.nativeEvent.data === 'WINDOW_CLOSED' ? this.onClose() :this. onPaymentSuccess(event.nativeEvent.data)}
        style={[{ flex: 1 }]}
        scalesPageToFit={Platform.OS === 'android'}
      />
        ) 
  }
  onPaymentSuccess = (token) => {
    this.getProductContact(token);
  }
  
  onClose = () => {
   Alert.alert('unsucessful')
  }


  async   getProductContact(token){
    let data = new FormData();

    const location = AsyncStorage.getItem('location');
    let owner_id = await AsyncStorage.getItem('owner_id');
    let user_id= await AsyncStorage.getItem('user_id')
    let product_id = await AsyncStorage.getItem('product_id');
    let mode = await AsyncStorage.getItem('modepayment');


    // console.log( location);
    console.log('*-------Call Backend ModePayment--------*')
    console.log(user_id);
    console.log(parseInt(product_id));
    console.log(parseInt(owner_id));
    console.log(mode);
    console.log(token)
    console.log("*---------------------------------------*")
    //
    data.append('product_id',parseInt(product_id));
    data.append('owner_id', parseInt(owner_id));
    data.append('user_id', user_id);
    data.append('description',mode);
    data.append('token',token);
    fetch(PH+'/api/v1/delivery/modeofdelivery', {
      method: 'POST',
      headers: {
        'Accept':'application/json',
        'Content-Type': 'multipart/form-data',
        'Authorization': 'Bearer '+await AsyncStorage.getItem('token')
      },
      body:data
    }).then((response) => response.json())
    .then((responseJson) => {
      console.log("Product Details ->")
      console.log(responseJson)
      if(responseJson.code == 200){
        console.log("Success Mode of Payment")
      // console.log(responseJson.meta.contact[0].number)
      // this.setState({meta_contact:responseJson.meta.contact[0].number, meta_address:responseJson.meta.contact[0].street_address1});
      // this.setState({ seeInformation:false, owner_approved:true ,kontaktoplysninger2:false });
      // this.refs.slideUp.close()  
    }else{
      console.log("ayays",error)
    }
  
})
    .catch((error) => {
      console.log("ayays",error)
      return 0;
    
});

    // console.log("Insert Query ");
  }

}

// image: '${this.props.imageUrl}',
// name: '${this.props.storeName}',
// description: '${this.props.description}',
// amount: ${this.props.amount},
// currency: USD',
// allowRememberMe: ${this.props.allowRememberMe},

StripeCheckout.propTypes = {
  amount: PropTypes.number,
  imageUrl: PropTypes.string,
  storeName: PropTypes.string,
  description: PropTypes.string,
  allowRememberMe: PropTypes.bool,
  onClose: PropTypes.func,
  currency: PropTypes.string,
  prepopulatedEmail: PropTypes.string,
};

StripeCheckout.defaultProps = {
  imageUrl:"https://pbs.twimg.com/profile_images/778378996580888577/MFKh-pNn_400x400.jpg",
  name: 'stripe',
  description: 'stripe',
  amount: 100,
  currency: 'USD',
  allowRememberMe: false,
  prepopulatedEmail: 'danaccion@gmail.com',
  publicKey: 'pk_test_51JdCT6JFaVM4ExmyF2iLFyVL7EPm6NiWGITupmY7lhWm8hGfoHmnrXH8ZP8xb7ohhZ0pSFWOpKcHhL0xLpr4c1Wj00FpoS9UGe'
};

// export default StripeCheckout;
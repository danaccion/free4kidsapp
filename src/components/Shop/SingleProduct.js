import React, { Component } from 'react';
import { SafeAreaView, StyleSheet,View,ImageBackground,Image,ScrollView,Platform,Dimensions,KeyboardAvoidingView,ListView,StatusBar,AsyncStorage,RefreshContro,TouchableOpacity,Share } from 'react-native';

import Ionicons from 'react-native-vector-icons/Ionicons'
import { Container, Header, Content, Form, Item, Picker,Icon,Input,Card,CardItem,Left,Button,Body,Spinner,Thumbnail } from 'native-base';
import HamburgerIcon from '../../icons/HamburgerIcon';
import HeartBagBurger from '../../icons/HeartBagBurger';
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';
import caroustyles, { colors } from '../../styles/carousel/index.style';
import { SliderBox } from "react-native-image-slider-box";
import Carousel, { Pagination } from 'react-native-snap-carousel';
import Dialog, {SlideAnimation,DialogContent } from 'react-native-popup-dialog';
import { Rating, AirbnbRating } from 'react-native-ratings';
import WrappedText from "react-native-wrapped-text";
import ProgressiveImage from '../ProgressiveImage';
import SwipeableRating from 'react-native-swipeable-rating';
import Modal from 'react-native-modal';
import { Snackbar } from 'react-native-paper';
import styles2 from '../Giveaways/styles';
import { PH,SS } from 'react-native-dotenv'
const { width,height } = Dimensions.get('screen');
import {connect} from 'react-redux'
import {reserveCount} from '../actions/index'
import {
  Block,Text
} from 'galio-framework';
import store from '../../store';
import { withNavigation } from 'react-navigation';
import { Alert } from 'react-native';
// const IS_ANDROID = Platform.OS === 'android';
const SLIDER_1_FIRST_ITEM = 1;
const Fsize2 = Platform.OS === 'ios' ?14 : 13;
const Fsize3 = Platform.OS === 'ios' ?10 : 8;
const Fsize = Platform.OS === 'ios' ?20 : 20;
const { width: viewportWidth } = Dimensions.get('window');


class SingleProduct extends Component {


  
state = {
    mine:false,
    reportPopupShow:false,
    selected2: undefined, productName: '',productRemark: '',
    selectedType:null,
    picture:'http://ville.montreal.qc.ca/culture/sites/ville.montreal.qc.ca.culture/themes/vdm_bootstrap_culture/images/image_generique_camera.png',
    toCamera:0,
    images:null,
    activeSlide: 0,
    slider1ActiveSlide: 0,
    refreshing: true,
    fullscreen:false,
    userImage :"",
    userName:"",
    popupVisible:false,
    popupVisibleRapport:false,
    product:null,
    imagefullurl:'',
    locationDetails:null,
    heartColor:'#757575',
    rating:4,popupVisibleError:false,
    errorModal:false,
    canReserve:'Reserver børneting',
    isReserved:false,
    isSaved:false,
    visibleSnack:false,
    productName:'',
    productDescription:'',
    deliveryType:null,
    showDialogMessage:false,
    dialog_message:''
  };
  baseSate=this.state


 componentDidMount = async ()=>{
  const { navigation } = this.props;
  const item = navigation.getParam('product');
  const location = AsyncStorage.getItem('location');
  const page = navigation.getParam('page');
  let user_id= await AsyncStorage.getItem('user_id')
  
  if(page==='saved'){
    this.getProduct(item.products[0].id)
  }else{
    this.getProduct(item.id)
  }
  
  this.didBlurSubscription = this.props.navigation.addListener(
    'didBlur',
    payload => {
      this.getProduct(item.id)
    }
    
  );
  if(item.save=="Yes"){
    this.setState({
      heartColor:'#d47d7d',userImage : await AsyncStorage.getItem('image_url'),userName : await AsyncStorage.getItem('name'),product:item,imagefullurl:item.images[0],locationDetails:location
      })
  }else{
    this.setState({
        userImage : await AsyncStorage.getItem('image_url'),userName : await AsyncStorage.getItem('name'),product:item,locationDetails:location
      })
  }
}

postRapport = async (repText) =>{
    this.setState({
      popupVisible:true,
      reportPopupShow:false
    });

    let Token=await AsyncStorage.getItem('token');
    const { navigation } = this.props;
    const item = navigation.getParam('user');

    //for(x in this.state.curr_item) {
        console.log('product: '+this.state.curr_item.name);
    //}

    let userID=await AsyncStorage.getItem('user_id');
    let data = new FormData();
    //data.append('description','test');
    //data.append('reporter_id',userID);
    data.append('user_id',item.id);
    data.append('description',repText);
    data.append('reporter_id',userID);
    data.append('product_id',this.state.curr_item.id);
    //data.append('report_text', repText);
    await  fetch(PH+'/api/v1/reports/create', {
        method: 'POST',
        headers: {
            'Accept':'application/json',
            'Content-Type': 'multipart/form-data',
            'Authorization': 'Bearer '+Token
        },
        body:data,
    }) 
    .then((response) => response.json())
    .then((responseJson) => {       
        console.log(responseJson);
        if(responseJson.code==200) {
//              console.log('Item Report send');
          let dat = this;
          this.setState({
            popupVisibleRapport:true
          });
          //setTimeout(function(){}, 3000);
          setTimeout(() => { dat.setState({popupVisibleRapport: false}) }, 5000);
        }
        else{
          console.log('description',repText,'reporter_id',userID,'product_id', item.id,'token',Token);
          console.log("Code: "+responseJson.code);
        }
    })
    .catch((error) => { console.log(error) });
}

back = () => {
  this.setState({
    toCamera: 0
  })
};    

//for report
onCreate = async () => {
  const { navigation } = this.props;
  const item = navigation.getParam('product');
  
  let Token=await AsyncStorage.getItem('token');
  let userID=await AsyncStorage.getItem('user_id');
  console.log('user_id',userID);
  console.log('product_id', item.id);
  console.log('item user_id', item.user_id);
  if(this.state.currentLocation!=null){
     address = await Location.reverseGeocodeAsync({'latitude':this.state.currentLocation.coords.latitude,'longitude':this.state.currentLocation.coords.longitude})
     if(address[0].street!=null){
      userPlace=address[0].street+" "+address[0].city+", "+address[0].region+", "+address[0].country
      }else{
        userPlace=address[0].city+", "+address[0].region+", "+address[0].country
      }
    }else{
      this._getLocationAsync()
    }
  let type="image/png"
  if(this.state.picture!=null){
    type = "image/" + (this.state.picture[0].split(".").pop() || "jpg");
  }
  let data = new FormData();
      data.append('user_id',item.user_id);
      data.append('description','test');
      data.append('reporter_id',userID);
      data.append('product_id', item.id);
      console.log(data);
 await  fetch(PH+'/api/v1/reports/create', {
    method: 'POST',
    headers: {
      'Accept':'application/json',
       'Content-Type': 'multipart/form-data',
      'Authorization': 'Bearer '+Token
    },
    body:data,
    }).then((response) => response.json())
        .then((responseJson) => {
         
          if(responseJson.code==200){
            console.log('Report send');
            this.setState({
              popupVisibleRapport:true
            })
          }
          else{
            //Alert(responseJson.title)
            this.setState({ showDialogMessage:true, dialog_message:responseJson.title })
          }
    })
        .catch((error) => {
        console.log(error)
    });
   
}; 

onShare = async () => {
  // const base64 = await FileSystem.readAsStringAsync(this.state.imagefullurl, { encoding: 'base64' });
  try {
    
    const result = await Share.share({
      message:
        'Free4kids | To see more item download our App '+this.state.imagefullurl
        // eg.'http://img.gemejo.com/product/8c/099/cf53b3a6008136ef0882197d5f5.jpg',
    });
    if (result.action === Share.sharedAction) {
      if (result.activityType) {
        // shared with activity type of result.activityType
      } else {
        // shared
      }
    } else if (result.action === Share.dismissedAction) {
      // dismissed
    }
  } catch (error) { 
    alert(error.message);
  }
};

async getProduct(id){
  let token=await AsyncStorage.getItem('token');
  let data = new FormData();
  data.append('product_id', id);
  let userID=await AsyncStorage.getItem('user_id');
  console.log('user_id',userID);
fetch(PH+'/api/v1/users/products', {
  method: 'POST',
  headers: {
    'Accept':'application/json',
    'Content-Type': 'multipart/form-data',
    'Authorization': 'Bearer '+token
  },
  body:data
  }).then((response) => response.json())
      .then((responseJson) => {
        console.log(responseJson)
        if(responseJson.code==404){
          // alert("item not found anymore")
          console.log("item not found anymore");
          this.props.navigation.navigate('Donate');
        }
        if(responseJson.meta.product[0].user_id==userID){
        console.log("own");
        this.setState({
          heartColor:'#d47d7d',product:responseJson.meta.product[0],imagefullurl:responseJson.meta.product[0].images[0],images:responseJson.meta.product[0].images,productDescription:responseJson.meta.product[0].description,
          mine:true
        })}else{
          console.log("------???????"+responseJson.meta.product[0].images[0])
          this.setState({
            heartColor:'#d47d7d',product:responseJson.meta.product[0],imagefullurl:responseJson.meta.product[0].images[0],images:responseJson.meta.product[0].images,productDescription:responseJson.meta.product[0].description,
            isReserved:responseJson.meta.product[0].reserved=="Yes"?true:false,isSaved:responseJson.meta.product[0].save=="Yes"?true:false,productName:responseJson.meta.product[0].name,deliveryType:responseJson.meta.product[0].delivery_type
            })
           }
        
  })
      .catch((error) => {
       console.log("ayay daut ni",error)
  });
}


async saveProduct() {
  const { navigation } = this.props;
  const item = navigation.getParam('product');
  let user_id= await AsyncStorage.getItem('user_id')
if(item.save!=='Yes'){
this.setState({isSaved:true,visibleSnack:true})
  let data = new FormData();
  data.append('user_id', user_id);
  data.append('product_id', item.id);
  data.append('status', 1);
  fetch(PH+'/api/v1/saved_products/create', {
    method: 'POST',
    headers: {
      'Accept':'application/json',
      'Content-Type': 'multipart/form-data',
      'Authorization': 'Bearer '+await AsyncStorage.getItem('token')
    },
    body:data
    }).then((response) => response.json())
        .then((responseJson) => {
            console.log("Save Success")
    })
        .catch((error) => {
         console.log("ayay",error)
    });
  }
};    
toggleModalReport = () => {
  this.setState({errorModal: !this.state.errorModal});
};
async reserveProduct() {
  
  const { navigation } = this.props;
  const item = navigation.getParam('product');
  let user_id= await AsyncStorage.getItem('user_id')
  const page = navigation.getParam('page');
  console.log(item.reserved)
if(item.reserved!=='YES'){
  
  let data = new FormData();
  data.append('user_id', user_id);
  if(page==='saved'){
    data.append('product_id', item.products[0].id);
  }else{
    data.append('product_id', item.id);
  }
 
  // data.append('status', 1);
  fetch(PH+'/api/v1/reservation/create', {
    method: 'POST',
    headers: {
      'Accept':'application/json',
      'Content-Type': 'multipart/form-data',
      'Authorization': 'Bearer '+await AsyncStorage.getItem('token')
    },
    body:data
    }).then((response) => response.json())
        .then((responseJson) => {
          console.log(responseJson)
            if(responseJson.code===500||responseJson.code===404){
              this.toggleModalReport()
            }
            
            if(responseJson.code===200){
              this.setState({popupVisible:true,isReserved:true})
              this.props.reserve(1);
              
            }else if(responseJson.code===402||responseJson.code===402){
              this.setState({popupVisibleError:true})
            }
            // if(responseJson.code==404){
            //   this.setState({allData:[],refresh:false})
            // }else if(responseJson.meta.product.length==0){
            //   this.props.navigation.navigate('Shop');
            // }else{ 
            //   this.setState({allData:responseJson.meta.product,refresh:false})
            // }
            
          
    })
        .catch((error) => {
           console.log("ayay",error)  
    });
  }else{
    this.setState({popupVisible:true})
  }
};    

SelectType = (value) => {
  this.setState({
    selectedType: value
  })
};    
handleRating = (rating) => {
 
} 
  

layoutExample (number, title, type) {
  const isTinder = type === 'tinder';
  const { slider1ActiveSlide } = this.state;
  const { navigation } = this.props;
  const item = navigation.getParam('product');
 
  const location = navigation.getParam('location');
  return (
    <View>
    <View>
          <Card style={{ elevation: 0 ,paddingBottom:-10}}>
                  <TouchableOpacity onPress={()=>this.setState({fullscreen:true})}>
                  <Image resizeMode='contain' style={{ height: 250, flex: 1 }} source={{uri:item.image_url}} />
                  </TouchableOpacity>
              </Card>
       
    </View>
    </View> 
  );
}
  
renderImage = ({ item }) => (
  <TouchableOpacity  onPress={()=>{this.setState({fullscreen:true})}}>
    <View style={{flex: 1,
    justifyContent: 'center',
    width: wp('85%'),
    zIndex:10,
    height:this.state.fullscreen==true?400:200}}>
      <ProgressiveImage   
          source={{  uri: item }}
          thumbnailSource={require('../../../assets/images/slice3.png')}
          style={{width:viewportWidth,height:this.state.fullscreen==true?400:200}}
          resizeMode="contain"/> 
    </View>
  </TouchableOpacity>
);
  render(){
    const { activeSlide } = this.state;  
    const resizeMode = 'cover';
    const { navigation } = this.props;
    const item = navigation.getParam('product');
    const location = navigation.getParam('location');
    const example4 = this.layoutExample(4, '"Tinder-like" layout | Loop', 'tinder');
    return (
      <View>
        
    <Modal isVisible={this.state.errorModal} style={{height:hp('50%')}} onBackdropPress={()=>this.setState({errorModal:false})} animationOut="fadeOutLeft" onSwipeStart={()=>console.log("ayay")}>
       <TouchableOpacity style={{position:'relative',alignSelf:'flex-end',height:60,width:60,zIndex:99,right:25,top:-65}} onPress={()=>this.toggleModalReport()}>
       </TouchableOpacity>
       <Image
         style={{
           zIndex:10,
           resizeMode:'contain',
           position: 'absolute',
           alignSelf:'center',
          width:wp('70%'),
          height:wp('70%'),
           justifyContent: 'center',
           
         }}
         source={require('../../../assets/icons/slice19.png')}
         />
         
       </Modal>
       
       <Dialog
            visible={this.state.reportPopupShow}
            onTouchOutside={() => { this.setState({ reportPopupShow: false }); }}
            onHardwareBackPress={() => {this.setState({ reportPopupShow: false });}}
            dialogStyle={{backgroundColor:'white',width:wp('90%'),alignItems:'center',alignContent:'center'}}>
            <DialogContent>
                <View styles={{alignItems:'center', }}>
                    <Text  style={{fontSize:28, color:'#eca6a1', padding: 12, textAlign:'center', fontFamily:'KGPrimaryWhimsy',}}>Why do you want to report this product?</Text>
                    <TouchableOpacity onPress={() =>this.postRapport('The product is fake.')} >
                        <Text  style={[styles.button, {alignSelf:'center', textAlign:'center',fontSize:17,color:'white',padding: 12, fontFamily:'KGPrimaryWhimsy', marginTop:20, backgroundColor:'#54969c'}]}>The product is fake.</Text>
                    </TouchableOpacity>
                    <TouchableOpacity onPress={() =>this.postRapport('The product should not be passed on.')} >
                        <Text  style={[styles.button, {alignSelf:'center',textAlign:'center',fontSize:17,color:'white',padding: 12, fontFamily:'KGPrimaryWhimsy', marginTop:20, backgroundColor:'#54969c'}]}>The product should not be passed on.</Text>
                    </TouchableOpacity>
                    <TouchableOpacity onPress={() =>this.postRapport('The product is in the wrong category.')} >
                        <Text  style={[styles.button, {alignSelf:'center',textAlign:'center',fontSize:17,color:'white',padding: 12, fontFamily:'KGPrimaryWhimsy', marginTop:20, backgroundColor:'#54969c'}]}>The product is in the wrong category.</Text>
                    </TouchableOpacity>
                </View>
            </DialogContent>
        </Dialog>
       
       <Dialog
    visible={this.state.popupVisible}
    onTouchOutside={() => {
      this.setState({ popupVisible: false });
    }}
    dialogStyle={{backgroundColor:'transparent'}}
  >
    <DialogContent>
     <View>
     <TouchableOpacity onPress={()=>this.setState({ popupVisible: false })} style={{position:'absolute',right:5,top:50,height:60,width:60,zIndex:10}}>
        {/* <Text style={{color:'black',right:0}}>x</Text> */}
      </TouchableOpacity> 
     
     <Image  resizeMode="contain" style={{width:wp('60%') ,height:hp('40%'),alignSelf:'center'}} source={require('../../../assets/icons/slice5.png')}/> 
     </View>
   
     
    </DialogContent>
  </Dialog>
         <Dialog
    visible={this.state.popupVisibleError}
    onTouchOutside={() => {
      this.setState({ popupVisibleError: false });
    }}
    dialogStyle={{backgroundColor:'white',width:wp('70%'),height:wp('40%'),alignItems:'center',alignContent:'center'}}
  >
    <DialogContent>
      
      <Text  adjustsFontSizeToFit numberOfLines={2} style={{color:'grey',fontFamily:'KGPrimaryWhimsy', fontSize:Fsize,alignSelf:'center',marginTop:50}} > Produkt er allerede reserveret / kan ikke reservere dit eget produkt.</Text>
     
   
     
    </DialogContent>
  </Dialog>

  {/* This part is for rapport family dialog */}
  <Dialog
    visible={this.state.popupVisible}
    onTouchOutside={() => {
      this.setState({ popupVisible: false });
    }}
    dialogStyle={{backgroundColor:'transparent'}}
  >
    <DialogContent>
     <View>
     <TouchableOpacity onPress={()=>this.setState({ popupVisible: false })} style={{position:'absolute',right:5,top:50,height:60,width:60,zIndex:10}}>
        {/* <Text style={{color:'black',right:0}}>x</Text> */}
      </TouchableOpacity> 
     
     <Image  resizeMode="contain" style={{width:wp('60%') ,height:hp('40%'),alignSelf:'center'}} source={require('../../../assets/icons/slice5.png')}/> 
     </View>
   
     
    </DialogContent>
  </Dialog>
         <Dialog
    visible={this.state.popupVisibleRapport}
    onTouchOutside={() => {
      this.setState({ popupVisibleRapport: false });
    }}
    dialogStyle={{backgroundColor:'white',width:wp('70%'),height:wp('40%'),alignItems:'center',alignContent:'center'}}
  >
    <DialogContent>
      
      <Text  adjustsFontSizeToFit numberOfLines={2} style={{color:'grey',fontFamily:'KGPrimaryWhimsy', fontSize:Fsize,alignSelf:'center',marginTop:50}} > 
Rapporter allerede</Text>
    </DialogContent>
  </Dialog>
      <ImageBackground
         style={{
           backgroundColor: '#ccc',
           flex: 1,
           resizeMode,
           position: 'absolute',
           width: '100%', 
           height: '100%',
           justifyContent: 'center',
           
         }}
         source={require('../../../assets/images/slice81-min.png')}
         />
      <View style={styles.container2}>
  
       {this.state.images!==null?(
          <Container style={{marginTop:-5,marginBottom:-5}}
          //  refreshControl={
          //     <RefreshControl
          //       //refresh control used for the Pull to Refresh
          //       refreshing={this.state.refreshing}
          //       onRefresh={this.onRefresh.bind(this)}
          //     />
          //   }
            >
               <Content scrollEnabled={false}>
                <Card  transparent>
                
            
                  <View style={{marginTop:hp('2%')}} >
                  <Carousel
                ref={c => {
                  this.slider1Ref = c;
                }}
                data={this.state.images}
                renderItem={this.renderImage}
                sliderWidth={viewportWidth}
                itemWidth={viewportWidth}
                inactiveSlideScale={1}
                inactiveSlideOpacity={1}
                firstItem={0}
                loop={false}
                autoplay={false}
                autoplayDelay={500}
                autoplayInterval={3000}
                onSnapToItem={index => this.setState({ activeSlide: index })}
              />
              <Pagination
                dotsLength={parseInt(this.state.images.length)}
                activeDotIndex={activeSlide}
                containerStyle={{ flex: 1,
                  position: 'absolute',
                  alignSelf: 'center',
                  paddingVertical: 8,
                  marginTop: 195,zIndex:999}}
                  
                dotColor="rgba(128, 128, 128, 0.92)"
                dotStyle={{ width: 8,
                  height: 8,
                  borderRadius: 4,
                  marginHorizontal: 0}}
                inactiveDotColor="grey"
                inactiveDotOpacity={0.4}
                inactiveDotScale={0.6}
                carouselRef={this.slider1Ref}
                tappableDots={!!this.slider1Ref}
              />
                  </View>
                  
                 <View style={{position:'absolute',alignSelf:'center',top:hp('21%'),flexDirection: 'row'}}>
                 <Image  resizeMode="contain" style={{width:wp('10%') ,height:hp('10%'),left:100,zIndex:5, bottom:30}} source={require('../../../assets/images/slice34-min.png')}/> 
                 <Image  resizeMode="contain" style={{width:wp('95%'),left:30,alignSelf:'center',height:hp('13.56%'),zIndex:1,bottom:70}} source={require('../../../assets/images/slice70-min.png')}/> 
                 <Image  resizeMode="contain" style={{width:wp('25%') ,height:hp('30%'),right:80,zIndex:5,bottom:150}} source={require('../../../assets/images/slice14.png')}/> 
                 </View>
                 
                 </Card>
                 <Text style={{color:'#666666', fontSize:Fsize3,alignSelf:'center',marginBottom:15}}><Text style={styles.underline} onPress={()=>this.onShare()} >Share</Text></Text>
                 </Content>
                 
                 <Dialog
                      visible={this.state.fullscreen}
                      onTouchOutside={() => {
                        this.setState({ fullscreen: false });
                      }}
                  >
              <DialogContent style={{width:wp('90%'),height:hp('50%'),alignItems:'center'}}>
              <TouchableOpacity style={{width:50,height:48,right:-5,top:7,position:'absolute',zIndex:100}} onPress={() => {this.setState({fullscreen:false})}}>
              <Image  resizeMode="contain" style={{width:30,height:30}} source={require('../../../assets/icons/close.png')}/>
              </TouchableOpacity>
              <View style={{marginTop:height*.05}}>
              <Carousel
                ref={c => {
                  this.slider1Ref = c;
                }}
                data={this.state.images}
                renderItem={this.renderImage}
                sliderWidth={viewportWidth}
                itemWidth={viewportWidth}
                inactiveSlideScale={1}
                inactiveSlideOpacity={1}
                firstItem={0}
                loop={false}
                autoplay={false}
                autoplayDelay={500}
                autoplayInterval={3000}
                onSnapToItem={index => this.setState({ activeSlide: index })}
              />
              <Pagination
                dotsLength={parseInt(this.state.images.length)}
                activeDotIndex={activeSlide}
                containerStyle={{ flex: 1,
                  position: 'absolute',
                  alignSelf: 'center',
                  paddingVertical: 8,
                  marginTop: 399,zIndex:999}}
                  
                dotColor="rgba(128, 128, 128, 0.92)"
                dotStyle={{ width: 8,
                  height: 8,
                  borderRadius: 4,
                  marginHorizontal: 0}}
                inactiveDotColor="grey"
                inactiveDotOpacity={0.4}
                inactiveDotScale={0.6}
                carouselRef={this.slider1Ref}
                tappableDots={!!this.slider1Ref}
              />
             </View>
              </DialogContent>
            </Dialog>
           
              <Container   style={{top:-hp('25%'),marginBottom:-hp('8%'),zIndex:2}}>
             
                      <Content>
                    
                      <Card>
                        
                      <Snackbar
                        visible={this.state.visibleSnack}
                        onDismiss={() => this.setState({ visibleSnack: false })}
                        // action={{
                        //   label: 'Undo',
                        //   onPress: () => {
                        //     // Do something
                        //   },
                        // }}
                        style={{backgroundColor:'#5e5e5e'}}
                        duration={2000}
                      >
                      Elementet er gemt!
                      </Snackbar>
                      
                        <View style={{left:wp('85%'),top:hp('1%'),zIndex:3}}>
                          <TouchableOpacity onPress={()=>this.saveProduct()}>
                          <Icon name="heart" style={{ color: this.state.isSaved===false?'grey':'#d47d7d'}} />
                          </TouchableOpacity>
                        </View>
                        

                      <CardItem style={{alignItems:'center',alignContent:'center',alignSelf:'center'}}>    
                                <Body style={{alignItems:'center'}}> 
                                  <Text  style={{color:'#538075', fontSize:25,fontFamily:'KGPrimaryWhimsy'}}> {this.state.productName}</Text>
                                  <Text   style={{ textAlignVertical: "center",textAlign: "center",fontSize: Fsize3 ,alignSelf:'center',color:'grey',marginTop:5,width:wp('80%')}}> {this.state.productDescription}</Text>
                                  {this.state.product.sex!==null&&this.state.product.sex!=="undefined"?(  
                                  <Block row center width={width-50}>
                                    <Text muted italic style={{color:'grey', fontSize:13,fontFamily:'KGPrimaryWhimsy',margin:10}}>
                                      Køn:    
                                    </Text>
                                    <Text  style={{color:'#538075', fontSize:18,fontFamily:'KGPrimaryWhimsy'}}>
                                      {this.state.product.sex}    
                                    </Text>
                                    </Block>
                                    ):null} 
                                    {this.state.product.size!==null&&this.state.product.size!=="undefined"?(  
                                    <Block row center width={width-50}>
                                    <Text muted italic style={{color:'grey', fontSize:13,fontFamily:'KGPrimaryWhimsy',margin:10}}>
                                    Størrelse:    
                                    </Text>
                                    <Text  style={{color:'#538075', fontSize:13,fontFamily:'KGPrimaryWhimsy'}}>
                                      {this.state.product.size}    
                                    </Text>
                                    </Block>
                                     ):null} 
                                     {this.state.product.stand!==null&&this.state.product.stand!=="undefined"?( 
                                    <Block row center width={width-50}>
                                    <Text muted italic style={{color:'grey', fontSize:13,fontFamily:'KGPrimaryWhimsy',margin:10}}>
                                      Stand:    
                                    </Text>

                                    <Text  style={{color:'#538075', fontSize:18,fontFamily:'KGPrimaryWhimsy'}}>
                                      {this.state.product.stand}    
                                    </Text>
                                    </Block>
                                     ):null} 
                                        {this.state.product.brand!==null&&this.state.product.brand!=="undefined"?( 
                                    <Block row center width={width-50}>
                                    <Text muted italic style={{color:'grey', fontSize:13,fontFamily:'KGPrimaryWhimsy',margin:10}}>
                                      Mærke:    
                                    </Text>
                                    <Text  style={{color:'#538075', fontSize:18,fontFamily:'KGPrimaryWhimsy'}}>
                                      {this.state.product.brand}    
                                    </Text>
                                    </Block>
                                    ):null} 
                                  </Body>          
                          </CardItem>
                          <View style={{alignItems:'center'}}>
                          <Button onPress={()=>this.onShare()} title="Share" />

                          <TouchableOpacity disabled={this.state.mine?true:(this.state.isReserved==false?false:true)} onPress={() => this.state.isReserved===false?this.reserveProduct():null}>
                              <Text style={{backgroundColor: this.state.mine==true?'grey':(this.state.isReserved==false?'#54969c':'grey'),
                                              borderColor: 'white',
                                              borderWidth: .5,
                                              borderRadius: 12,
                                              color: 'white',
                                              overflow: 'hidden',
                                              padding: 12,
                                              textAlign:'center',
                                              width: 300,
                                              height: 44,
                                              opacity: 0.9,
                                              fontFamily:'KGPrimaryWhimsy',
                                              fontSize:20}}>{this.state.mine==true?'Owner':(this.state.isReserved==false?'Reserver børneting':'Allerede Reserveret')}</Text>
                          </TouchableOpacity>
                          
                          </View>
                          
                          {this.state.deliveryType==1?(
                            <View>
                          <Image resizeMode='contain' style={{width:wp('15%'),height:hp('10%'),alignSelf:'center'}} source={require('../../../assets/images/slice33-min.png')} /> 
                          <Text style={{color:'#666666', fontSize:Fsize2,alignSelf:'center',fontWeight:'600'}}>Afhentning</Text>   
                          <Text style={{color:'#666666', fontSize:Fsize3,alignSelf:'center',marginBottom:30}}>{location} - <Text style={styles.underline} onPress={() =>this.props.navigation.navigate('Clocations',{item:this.state.product})}>vis på kort</Text></Text> 
                          </View>
                          ):null}
                          
                          {this.state.deliveryType==2?(
                            <View>
                               {/* <Image style={{zIndex:100,position:'absolute',bottom:100,width:wp('15%'),height:hp('8%'),alignSelf:'center'}} source={require('../../../assets/images/slice66-min.png')} />  */}
                               <Image resizeMode='contain' style={{width:wp('12%'),height:hp('10%'),alignSelf:'center'}} source={require('../../../assets/images/slice32-min.png')} /> 
                                <Text style={{color:'#666666', fontSize:Fsize2,alignSelf:'center',fontWeight:'600'}}>Forsendelse</Text>    
                                <Text style={{color:'#666666', fontSize:Fsize3,alignSelf:'center', marginBottom:10}}>Familien sender til dig</Text> 
                                  </View>
                         
                          ):null}

                            {this.state.deliveryType==3?(
                          <View>
                          <Image resizeMode='contain' style={{width:wp('15%'),height:hp('10%'),alignSelf:'center'}} source={require('../../../assets/images/slice33-min.png')} /> 
                          <Text style={{color:'#666666', fontSize:Fsize2,alignSelf:'center',fontWeight:'600'}}>Afhentning</Text>   
                          <Text style={{color:'#666666', fontSize:Fsize3,alignSelf:'center',marginBottom:15}}>{location} - <Text style={styles.underline} onPress={() =>this.props.navigation.navigate('Clocations',{item:item})}>vis på kort</Text></Text> 
                          <Image resizeMode='contain' style={{width:wp('15%'),height:hp('10%'),alignSelf:'center'}} source={require('../../../assets/images/slice32-min.png')} />
                          <Text style={{color:'#666666', fontSize:Fsize2,alignSelf:'center',fontWeight:'600'}}>Forsendelse</Text>    
                                <Text style={{color:'#666666', fontSize:Fsize3,alignSelf:'center', marginBottom:10}}>Familien sender til dig</Text> 
                          </View>
                          ):null}
                          {this.state.deliveryType!==3&&this.state.deliveryType!==1&&this.state.deliveryType!==2?(
                            <Block center>
                           <Image style={{zIndex:100,bottom:0,width:wp('15%'),height:hp('8%'),alignSelf:'center'}} source={require('../../../assets/images/slice66-min.png')} /> 
                           </Block>
                          ):null}
   
                         
                      </Card>
  
                    
                      <Card style={{paddingBottom:20}}>
                      <CardItem style={{alignItems:'center',alignContent:'center',alignSelf:'center'}}>    
                                <Body style={{alignItems:'center'}}> 
                                <ProgressiveImage source={{ uri: Array.isArray(item.users)===true?item.users[0].image_url:item.users.image_url}}  style={{alignSelf:'center',height:40,width:40,borderRadius: 40/ 2}}/>
                                
                          <Text style={{color:'#ffb3b3',fontFamily:'KGPrimaryWhimsy', fontSize:Platform.OS === 'ios' ?20 : 18}}>FAMILIEN <Text style={{color:'#55aaaa',textTransform: 'uppercase'}}>{Array.isArray(item.users)===true?item.users[0].name:item.users.name}</Text></Text>
                          <SwipeableRating
                              rating={Array.isArray(item.users)===true?item.users[0].AverageRating:item.users.AverageRating}
                              size={28}
                              maxRating={6}
                              gap={2}
                              color='#e8b14a'
                              emptyColor='#e8b14a'
                              // onPress={this.handleRating}
                              swipeable={false}
                              allowHalves={true}
                          />
                                  </Body>          
                          </CardItem>
                          
                          <TouchableOpacity style={{paddingTop:10}} onPress={() =>
                           
                            this.props.navigation.navigate('UserProfile',{'user':Array.isArray(item.users)===true?item.users[0]:item.users})}
                            >
                            <Text style={styles.button3}>Se familiens profil</Text>
                            
                          </TouchableOpacity>
                          
                        <TouchableOpacity style={{paddingTop:10}} onPress={() => this.setState({reportPopupShow:true})}>
                            <Text style={styles.button3}>Rapport</Text>
                        </TouchableOpacity>

                      </Card>
  
                      </Content>
                  </Container>
                
            </Container> 
       ):
               <View style={{ flex: 1, paddingTop: 20 }}>
         
               <Content>
                 <Spinner/>
               </Content>
             
       </View>}
      
        </View>
       
        <Dialog
            visible={this.state.showDialogMessage}
            onTouchOutside={() => { this.setState({ showDialogMessage: false }); }}
            onHardwareBackPress={() => {this.setState({ showDialogMessage: false });}}
            dialogStyle={{backgroundColor:'white',width:wp('90%'),alignItems:'center',alignContent:'center'}}>
            <DialogContent>
                <View styles={{alignItems:'center', }}>
                    <Text  style={{fontSize:28, color:'#eca6a1', padding: 12, textAlign:'center', fontFamily:'KGPrimaryWhimsy',}}>{this.state.dialog_message}</Text>
                </View>
            </DialogContent>
        </Dialog>
       
        </View>
    );
}
  }
  const mapStateToProps = (state) =>{
    return{
      count:state.count.approvalBadgeCount
    }
  }

  const mapDispatchToProps = (dispatch) =>{
    return{
      reserve:(count)=>dispatch(reserveCount(count))
    }
  }
  
export default connect(mapStateToProps,mapDispatchToProps)(withNavigation(SingleProduct));

const styles = StyleSheet.create({
  container: {
    flex: 1,
    //alignItems: 'center',
    justifyContent: 'flex-end',
    backgroundColor: 'transparent'
  },
  container2: {
    position:'absolute',
    backgroundColor: 'transparent',
    flex: 1,
    height:hp('100%'),
    width:wp('100%'),
    top:0,
    bottom:0,
    alignContent:'center'
  },
  textstyle:{
    fontFamily:'KGPrimaryWhimsy',
    fontSize:40
  },
  container3: {
    flex: 1,
    position:'absolute',
    alignItems: "center",
    alignContent:'center',
    justifyContent: 'flex-end',
  },
  dropdown: {
    height: Dimensions.get('screen').width /7,
    padding: 5,
    position:'relative',
    margin:5,
    borderWidth: .2,
    borderRadius: 12,
    textShadowColor: 'black',
    borderColor: 'black',
    marginBottom: 10,
    backgroundColor: 'white',
    opacity: 0.9,
    fontFamily:'KGPrimaryWhimsy',
    fontSize:20,
    alignContent:'center'
  },
  wrpper: {
    opacity: 0.9,
    fontSize:20,
    fontFamily:'KGPrimaryWhimsy',
    flexDirection: 'row', justifyContent: 'space-around',   
  },
  button: {
    alignSelf:'center',
    marginLeft:10,
    marginRight:20,
    margin:10,
    backgroundColor: 'white',
    borderColor: 'black',
    borderWidth: 3,
    borderTopColor:'red',
    borderRadius: 12,
    color: 'white',
    overflow: 'hidden',
    textAlign:'center',
    width: 350,
    height: 48,
    // opacity: 0.9,
    fontSize:20,
    fontFamily:'KGPrimaryWhimsy'
  },
  button2: {
    backgroundColor: '#54969c',
    borderColor: 'white',
    borderWidth: .5,
    borderRadius: 12,
    color: 'white',
    overflow: 'hidden',
    padding: 12,
    textAlign:'center',
    width: 350,
    height: 48,
    opacity: 0.9,
    fontSize:20,
    fontFamily:'KGPrimaryWhimsy'
  },
  wrapperHorizontal: {
    width: wp('90%'),
    height: hp('20%'),
    justifyContent: 'space-between',
    alignItems: "center",
    alignSelf:'center',
    marginTop:-hp('1%'),
    // color: "black",
  },
  wrapperVertical: {
    width: 100,
    height: 300,
    justifyContent: 'space-between',
    alignItems: "center",
    margin: "auto",
    color: "black"
  },
  itemStyleVertical: {
    marginTop: 10,
    marginBottom: 10,
    width: 50,
    height: 50,
    marginRight:wp('5%'),
    // paddingTop: 13,
    // borderWidth: 1,
    // borderColor: "grey",
    // borderRadius: 25
  },
  itemSelectedStyleVertical: {
    // paddingTop: 11,
    color:'red',
    // borderWidth: 2,
    // borderColor: "#DAA520"
  },
  itemStyleHorizontal: {
    marginLeft: 50,
    marginRight: 50,
    width: 50,
    height: 50,
    // paddingTop: 13,
    // borderWidth: 1,
    // borderColor: "grey",
    // borderRadius: 25
  },
  itemSelectedStyleHorizontal: {
    paddingTop: 11,
    borderWidth: 2,
    borderColor: "#DAA520"
  },
  underline: {textDecorationLine: 'underline'},
  button5: {
    backgroundColor: '#54969c',
    borderColor: 'white',
    borderWidth: .5,
    borderRadius: 12,
    color: 'white',
    overflow: 'hidden',
    padding: 12,
    textAlign:'center',
    width: 300,
    height: 44,
    opacity: 0.9,
    fontFamily:'KGPrimaryWhimsy',
    fontSize:20
  },
  button3: {
    backgroundColor: '#54969c',
    borderColor: 'white',
    color: 'white',
    overflow: 'hidden',
    padding: 12,
    textAlign:'center',
    width: wp('93%'),
    height: 44,
    opacity: 0.9,
    fontFamily:'KGPrimaryWhimsy',
    fontSize:15,
    alignSelf:'center'
  },
});


import React, { Component } from 'react';
import { SafeAreaView, StyleSheet,Text,View,ImageBackground,Image,ScrollView,Platform,Dimensions,KeyboardAvoidingView,ListView,StatusBar,AsyncStorage,RefreshContro,TouchableOpacity } from 'react-native';
import Ionicons from 'react-native-vector-icons/Ionicons'
import BackButton from '../../BackButton';
import { Container, Header, Content, Form, Item, Picker,Icon,Input,Card,CardItem,Left,Button,Body,Spinner,Thumbnail } from 'native-base';
import HamburgerIcon from '../../icons/HamburgerIcon';
import HeartBagBurger from '../../icons/HeartBagBurger';
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';
import Carousel, { Pagination } from 'react-native-snap-carousel';
import { sliderWidth, itemWidth } from '../../styles/carousel/SliderEntry.style';
import SliderEntry from '../../styles/carousel/SliderEntry';
import caroustyles, { colors } from '../../styles/carousel/index.style';
import { ENTRIES1, ENTRIES2 } from '../../styles/carousel/entries';
import Dialog, {SlideAnimation,DialogContent } from 'react-native-popup-dialog';
import { Rating, AirbnbRating } from 'react-native-ratings';
import SwipeableRating from 'react-native-swipeable-rating';
import { PH,SS } from 'react-native-dotenv'
// const IS_ANDROID = Platform.OS === 'android';
const SLIDER_1_FIRST_ITEM = 1;

 
export default class ProductProfile extends Component {
  static navigationOptions = () => {
    return {
        headerLeft: <BackButton/>,
        headerTitle: (
            <Image
            resizeMode="contain"
            source={require('../../../assets/images/slice2.png')}
            style={{ width: 10, height:10 , flex:4,justifyContent: 'space-around', alignItems: 'center', padding: 25,}}
          />
          ),
        headerRight: <HeartBagBurger/>
    };
};

  state = {
    selected2: undefined, productName: '',productRemark: '',
    selectedType:null,
    picture:'http://ville.montreal.qc.ca/culture/sites/ville.montreal.qc.ca.culture/themes/vdm_bootstrap_culture/images/image_generique_camera.png',
    toCamera:0,
    products:[],
    slider1ActiveSlide: 0,
    refreshing: true,
    userImage :"",
    userName:"",
    popupVisible:false,
    rating:4
  };

async componentDidMount(){
   
 let user_id= await AsyncStorage.getItem('user_id')
 this.setState({
  userImage : await AsyncStorage.getItem('image_url'),userName : await AsyncStorage.getItem('name')
})
let data = new FormData();
  data.append('user_id', user_id);
  fetch(PH+'/api/v1/users/products', {
    method: 'POST',
    headers: {
      'Accept':'application/json',
      'Content-Type': 'multipart/form-data',
      'Authorization': 'Bearer '+await AsyncStorage.getItem('token')
    },
    body:data
    }).then((response) => response.json())
        .then((responseJson) => {
            if(responseJson.meta.product.length==0){
              this.props.navigation.navigate('Shop');
            }else{
              
              this.setState({products:responseJson.meta.product,refreshing:false})
            }
            
          
    })
        .catch((error) => {
          console.log("ayay",error)
    });
}
onValueChange2(value) {
  this.setState({
    selected2: value
  });
}

onUpdate = (val) => {
  this.setState({
    picture: val
  })
};  
camera = () => {
  this.setState({
    toCamera: 1
  })
};   
handleRating = (rating) => {
  this.setState({rating});
} 
back = () => {
  this.setState({
    toCamera: 0
  })
};    

SelectType = (value) => {
  this.setState({
    selectedType: value
  })
};    
 
onCreate = async () => {
  const data = new FormData();
      data.append('name', this.state.productName); // you can append anyone.
      data.append('category_id', this.state.selectedType);
      data.append('user_id',await AsyncStorage.getItem('user_id'));
      data.append('image', {
          uri: this.state.picture,
          type: 'image/jpeg', // or photo.type
          name: 'ProfilePic'
      });

  fetch(PH+'/api/v1/products/create', {
    method: 'POST',
    headers: {
      'Accept':'application/json',
      'Content-Type': 'multipart/form-data',
      'Authorization': 'Bearer '+await AsyncStorage.getItem('token')
    },
    body:data,
    }).then((response) => response.json())
        .then((responseJson) => {
          if(responseJson.code!==200){
            console.log(responseJson.title)
          }else{
            this.props.navigation.navigate('Home');
          }
    })
        .catch((error) => {
          console.log("ayay",error)
        
    });
};    
    _renderItem ({item, index}) {
        return <SliderEntry data={item} even={(index + 1) % 2 === 0} />;
    }

    _renderItemWithParallax ({item, index}, parallaxProps) {
        return (
            <SliderEntry
              data={item}
              even={(index + 1) % 2 === 0}
              parallax={true}
              parallaxProps={parallaxProps}
            />
        );
    }

    _renderLightItem ({item, index}) {
        return <SliderEntry data={item} even={false} />;
    }

    _renderDarkItem ({item, index}) {
        return <SliderEntry data={item} even={true} />;
    }


layoutExample (number, title, type) {
  const isTinder = type === 'tinder';
  const { slider1ActiveSlide } = this.state;
  return (
    <View>
      <View style={[caroustyles.exampleContainer, isTinder ? caroustyles.exampleContainerLight : caroustyles.exampleContainerLight]}>
          <Carousel
            ref={c => this._slider1Ref = c}
            data={this.state.products}
            renderItem={isTinder ? this._renderLightItem : this._renderItem}
            sliderWidth={sliderWidth}
            itemWidth={itemWidth}
            containerCustomStyle={caroustyles.slider}
            contentContainerCustomStyle={caroustyles.sliderContentContainer}
            layout={type}
            loop={false}
            onSnapToItem={(index) => this.setState({ slider1ActiveSlide: index }) }
          />
         
      </View>
      <View style={{position:'absolute',top:hp('25%'),width:18,alignSelf:'center',paddingRight:40}}>
          <Pagination
                  dotsLength={this.state.products.length}
                  activeDotIndex={slider1ActiveSlide}
                  containerStyle={styles.paginationContainer}
                  dotColor={'#538075'}
                  dotStyle={styles.paginationDot}
                  inactiveDotColor={'#f0ebeb'}
                  inactiveDotOpacity={1}
                  inactiveDotScale={1}
                  carouselRef={this._slider1Ref}
                  tappableDots={!!this._slider1Ref}
                />
      </View>
       
                    {/* <Container style={{alignItems:'center',alignContent:'center',alignSelf:'center'}}>    
                              <Body style={{alignItems:'center'}}> 
                              <Image  resizeMode="contain" style={{width:wp('90%') ,height:hp('20%'),alignSelf:'center',marginTop:-wp('40%')}} source={require('../../../assets/images/slice70-min.png')}/>
                                </Body>          
                        </Container> */}
                    
      </View>
  );
}
  
  render(){
    if (this.state.refreshing==true) {
      return (
        //loading view while data is loading
        <View style={{ flex: 1, paddingTop: 20 }}>
         
                <Content>
                  <Spinner/>
                </Content>
              
        </View>
      );
    }else if (this.state.products.length==0) {
      return (
        //loading view while data is loading
        <View style={{ flex: 1, paddingTop: 20 }}>
         
                <Content>
                  <Spinner/>
                </Content>
              
        </View>
      );
    }else{

    const resizeMode = 'cover';
    const example4 = this.layoutExample(4, '"Tinder-like" layout | Loop', 'tinder');
    return (
      <View style={styles.container}>
       <Dialog
    visible={this.state.popupVisible}
    onHardwareBackPress={() => { this.setState({ popupVisible: false }); } }
    onTouchOutside={() => {
      this.setState({ popupVisible: false });
    }}
  >
    <DialogContent>
    <Icon name="heart" style={{ color: '#ED4A6A' }} />
    </DialogContent>
  </Dialog>
      <ImageBackground
         style={{
           backgroundColor: '#ccc',
           flex: 1,
           resizeMode,
           position: 'absolute',
           width: '100%', 
           height: '100%',
           justifyContent: 'center',
           
         }}
         source={require('../../../assets/images/slice81-min.png')}
         />
      <View style={styles.container2}>
       
        <Container style={{marginTop:-5}}
        //  refreshControl={
        //     <RefreshControl
        //       //refresh control used for the Pull to Refresh
        //       refreshing={this.state.refreshing}
        //       onRefresh={this.onRefresh.bind(this)}
        //     />
        //   }
          >
             <Content scrollEnabled={false}>
              <Card >
              
            <SafeAreaView style={caroustyles.safeArea}>
                <View style={caroustyles.container}>
                    <ScrollView
                      style={caroustyles.scrollview}
                      scrollEventThrottle={200}
                      directionalLockEnabled={true}
                    >
                        { example4 }
                    </ScrollView>
                </View>
               </SafeAreaView>
               <View style={{position:'absolute',alignSelf:'center',top:hp('28%'),right:wp('5.09%')}}>
               <Image  resizeMode="contain" style={{position:'absolute',width:wp('10%') ,height:hp('10%'),left:40,zIndex:5, bottom:50}} source={require('../../../assets/images/slice34-min.png')}/> 
               <Image  resizeMode="contain" style={{width:wp('93%'),height:hp('13.56%'),left:wp('2%'),zIndex:1}} source={require('../../../assets/images/slice70-min.png')}/> 
               <Image  resizeMode="contain" style={{position:'absolute',width:wp('25%') ,height:hp('30%'),right:20,zIndex:5,bottom:15}} source={require('../../../assets/images/slice14.png')}/> 
               </View>
               </Card>
               </Content>
               <Text style={{color:'#538075', fontSize:Platform.OS === 'ios' ?18 : 16,fontFamily:'KGPrimaryWhimsy'}}> Del denne brneting</Text>
            
            {this.state.products.length>0?(
            <Container style={{top:-70}}>
                    <Content>
                    <Card style={{paddingBottom:50}}>
                      <View style={{left:wp('85%'),top:hp('1%'),zIndex:3}}>
                      <Icon name="heart" style={{ color: '#ED4A6A' }} />
                      </View>
                    
                    <CardItem style={{alignItems:'center',alignContent:'center',alignSelf:'center'}}>    
                              <Body style={{alignItems:'center'}}> 
                                <Text style={{color:'#538075', fontSize:Platform.OS === 'ios' ?18 : 16,fontFamily:'KGPrimaryWhimsy'}}> {this.state.products[this.state.slider1ActiveSlide].name}</Text>
                                <Text style={{color:'black', fontSize:Platform.OS === 'ios' ?16 : 14}}> {this.state.products[this.state.slider1ActiveSlide].description}</Text>
                                </Body>          
                        </CardItem>
                        <View style={{alignItems:'center'}}>
                        <TouchableOpacity onPress={() => {
                            this.setState({ popupVisible: true });
                          }}>
                            <Text style={styles.button5}>Reserver børneting's</Text>
                        </TouchableOpacity>
                        
                        </View>
                        

                        <Image style={{width:wp('15%'),height:hp('10%'),alignSelf:'center'}} source={require('../../../assets/images/slice33-min.png')} />
                        <Text style={{color:'#666666', fontSize: Platform.OS === 'ios' ?18 : 16,alignSelf:'center',fontWeight:'600'}}>Afhentning</Text>   
                        <Text style={{color:'#666666', fontSize: Platform.OS === 'ios' ?16 : 14,alignSelf:'center',marginBottom:30}}>5 km - <Text style={styles.underline} onPress={() =>this.props.navigation.navigate('Clocations')}>vis på kort</Text></Text> 

                        <Image style={{width:wp('12%'),height:hp('10%'),alignSelf:'center'}} source={require('../../../assets/images/slice32-min.png')} />  
                        <Image style={{zIndex:100,position:'absolute',bottom:100,width:wp('15%'),height:hp('8%'),alignSelf:'center'}} source={require('../../../assets/images/slice66-min.png')} /> 
                        <Text style={{color:'#666666', fontSize: Platform.OS === 'ios' ?16 : 14,alignSelf:'center',fontWeight:'600'}}>Forsendelse</Text>    
                        <Text style={{color:'#666666', fontSize: Platform.OS === 'ios' ?14: 12,alignSelf:'center'}}>Familien sender til dig</Text> 
                    </Card>

                  
                    <Card style={{paddingBottom:50}}>
                    <CardItem style={{alignItems:'center',alignContent:'center',alignSelf:'center'}}>    
                              <Body style={{alignItems:'center'}}> 
                               {/* <Thumbnail  source={{uri:this.state.userImage}} />         */}
                        <Text style={{color:'#ffb3b3',fontFamily:'KGPrimaryWhimsy', fontSize:Platform.OS === 'ios' ?18 : 16}}>FAMILIEN <Text style={{color:'#55aaaa',textTransform: 'uppercase'}}>{this.state.userName}</Text></Text>
                        <SwipeableRating
                            rating={this.state.rating}
                            size={28}
                            maxRating={6}
                            gap={2}
                            swipeable={false}
                            color='#e8b14a'
                            emptyColor='#e8b14a'
                            onPress={this.handleRating}
                            allowHalves={true}
                            xOffset={30}
                        />
                                </Body>          
                        </CardItem>
                        
                        <TouchableOpacity onPress={() =>
                          this.props.navigation.navigate('UserProfile')}>
                          <Text style={styles.button3}>Se familiens profil</Text>
                        </TouchableOpacity>
                       
               

                    </Card>

                    </Content>
                </Container>
                ):<Container>
                
                <Content>
                  <Spinner/>
                </Content>
              </Container>}
          </Container> 
        </View>
        
       
        </View>
      
  
  
   
    );
  }
}
  }
   

const styles = StyleSheet.create({
  container: {
    flex: 1,
    //alignItems: 'center',
    justifyContent: 'flex-end',
    backgroundColor: 'transparent'
  },
  container2: {
    position:'absolute',
    backgroundColor: 'transparent',
    flex: 1,
    height:hp('100%'),
    width:wp('100%'),
    top:0,
    alignContent:'center'
  },
  textstyle:{
    fontFamily:'KGPrimaryWhimsy',
    fontSize:40
  },
  container3: {
    flex: 1,
    position:'absolute',
    alignItems: "center",
    alignContent:'center',
    justifyContent: 'flex-end',
  },
  dropdown: {
    height: Dimensions.get('screen').width /7,
    padding: 5,
    position:'relative',
    margin:5,
    borderWidth: .2,
    borderRadius: 12,
    textShadowColor: 'black',
    borderColor: 'black',
    marginBottom: 10,
    backgroundColor: 'white',
    opacity: 0.9,
    fontFamily:'KGPrimaryWhimsy',
    fontSize:20,
    alignContent:'center'
  },
  wrpper: {
    opacity: 0.9,
    fontSize:20,
    fontFamily:'KGPrimaryWhimsy',
    flexDirection: 'row', justifyContent: 'space-around',   
  },
  button: {
    alignSelf:'center',
    marginLeft:10,
    marginRight:20,
    margin:10,
    backgroundColor: 'white',
    borderColor: 'black',
    borderWidth: 3,
    borderTopColor:'red',
    borderRadius: 12,
    color: 'white',
    overflow: 'hidden',
    textAlign:'center',
    width: 350,
    height: 48,
    // opacity: 0.9,
    fontSize:20,
    fontFamily:'KGPrimaryWhimsy'
  },
  button2: {
    backgroundColor: '#54969c',
    borderColor: 'white',
    borderWidth: .5,
    borderRadius: 12,
    color: 'white',
    overflow: 'hidden',
    padding: 12,
    textAlign:'center',
    width: 350,
    height: 48,
    opacity: 0.9,
    fontSize:20,
    fontFamily:'KGPrimaryWhimsy'
  },
  wrapperHorizontal: {
    width: wp('90%'),
    height: hp('20%'),
    justifyContent: 'space-between',
    alignItems: "center",
    alignSelf:'center',
    marginTop:-hp('1%'),
    // color: "black",
  },
  wrapperVertical: {
    width: 100,
    height: 300,
    justifyContent: 'space-between',
    alignItems: "center",
    margin: "auto",
    color: "black"
  },
  itemStyleVertical: {
    marginTop: 10,
    marginBottom: 10,
    width: 50,
    height: 50,
    marginRight:wp('5%'),
    // paddingTop: 13,
    // borderWidth: 1,
    // borderColor: "grey",
    // borderRadius: 25
  },
  itemSelectedStyleVertical: {
    // paddingTop: 11,
    color:'red',
    // borderWidth: 2,
    // borderColor: "#DAA520"
  },
  itemStyleHorizontal: {
    marginLeft: 50,
    marginRight: 50,
    width: 50,
    height: 50,
    // paddingTop: 13,
    // borderWidth: 1,
    // borderColor: "grey",
    // borderRadius: 25
  },
  itemSelectedStyleHorizontal: {
    paddingTop: 11,
    borderWidth: 2,
    borderColor: "#DAA520"
  },
  underline: {textDecorationLine: 'underline'},
  button5: {
    backgroundColor: '#54969c',
    borderColor: 'white',
    borderWidth: .5,
    borderRadius: 12,
    color: 'white',
    overflow: 'hidden',
    padding: 12,
    textAlign:'center',
    width: 300,
    height: 44,
    opacity: 0.9,
    fontFamily:'KGPrimaryWhimsy',
    fontSize:Platform.OS === 'ios' ?18 : 16
  },
  button3: {
    backgroundColor: '#54969c',
    borderColor: 'white',
    color: 'white',
    overflow: 'hidden',
    padding: 12,
    textAlign:'center',
    width: wp('93%'),
    height: 44,
    opacity: 0.9,
    fontFamily:'KGPrimaryWhimsy',
    fontSize:15,
    alignSelf:'center'
  },
});


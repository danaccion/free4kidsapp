import React, { PureComponent } from 'react';
import { Picker,ScrollView, StyleSheet,View,ImageBackground,Image,TouchableOpacity,FlatList,Platform,Dimensions,Text,AsyncStorage,Keyboard,TouchableWithoutFeedback,KeyboardAvoidingView } from 'react-native';
import * as Permissions from 'expo-permissions'
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view'
import Constants from 'expo-constants'
import RNPickerSelect from "react-native-picker-select";
import * as Location from 'expo-location'; 
import BackButton from '../../BackButton';
import styles2 from '../Giveaways/styles';
import ProgressiveImage from '../ProgressiveImage';
import HeartBagBurger from '../../icons/HeartBagBurger';
import {Icon} from 'react-native-elements';
import { Container, Card, List, ListItem, Thumbnail,Button,InputGroup,Input,Body,Right,Badge,Spinner,CardItem} from 'native-base';
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';
import { SliderBox } from 'react-native-image-slider-box';
import Modal from 'react-native-modal';
import { Snackbar } from 'react-native-paper';

import SwipeableRating from 'react-native-swipeable-rating';
import { PH,SS } from 'react-native-dotenv'
import Dialog, { DialogTitle,
  DialogContent,
  DialogFooter,
  DialogButton,
  SlideAnimation,
  ScaleAnimation, } from 'react-native-popup-dialog';
import { TextInput } from 'react-native-gesture-handler';
import {
  Block
} from 'galio-framework';
  const Fsize2 = Platform.OS === 'ios' ?20 : 18;
  const Fsize3 = Platform.OS === 'ios' ?13 : 11;
  const Fsize = Platform.OS === 'ios' ?16 : 14;

  
export default class FamilyProfile extends PureComponent {
  static navigationOptions = () => {
    
    return {
         headerLeft:<BackButton/>,
        headerTitle: (
        
            <Image
            resizeMode="contain"
            source={require('../../../assets/images/slice2.png')}
            style={{ width: 10, height:10 , flex:4,justifyContent: 'space-around', alignItems: 'center', padding: 25,}}
          />
          ),
        headerRight: <HeartBagBurger/>
    };
};


  state = {
    reportPopupShow:false,
    curr_item:'',
    popupVisible:false,
      user: '',
      username: '',
      password: '',
      userInfo:null,
      ButtonStateHolder : true,
      loading: false,
      fontLoaded:false,
      preferences:'',
      userImage:null,
      userName:null,
      userRate:null,
      images1:[],
      images2:[],
      descriptions1:[],
      descriptions2:[],
      fetching:true,
      displayDesc1:'',
      displayDesc2:'',
      fullscreen:false,
      imagefullurl:'',
      rating:3,
      hasLocationPermission:null,
      location:undefined,
      currentLocation:null,
      isModalVisible: false,
      visibleSnack:false,
      comment:'',
      reviews:null,
      allData:null,
      refreshing:false,
      average_rate:0
    };

    updateUser = (user) => {
      this.setState({ user: user })
   }
   
  async componentDidMount() {
    const { navigation } = this.props;
    const item = navigation.getParam('user');
    this.locationPermission()
    this.getUserRating()
    this.setState({userInfo:item})
        this.getUserProducts(item.id)

    }
  

  
  followFamily=async()=>{
    const { navigation } = this.props;
    const item = navigation.getParam('user');
    let data = new FormData();
    data.append('user_id', item.id);
    data.append('status',1);
    fetch(PH+'/api/v1/follow/create', {
      method: 'POST',
      headers: {
        'Accept':'application/json',
        'Content-Type': 'multipart/form-data',
        'Authorization': 'Bearer '+await AsyncStorage.getItem('token')
      },
      body:data
      }).then((response) => response.json())
          .then((responseJson) => {
            this.setState({visibleSnack:true})
                 
      }).catch((error) => {
            console.log(error)
          
      });
  }

  getUserProducts = async (id) =>{
  
      let data = new FormData();
      data.append('user_id', id);
      fetch(PH+'/api/v1/users/products', {
        method: 'POST',
        headers: {
          'Accept':'application/json',
          'Content-Type': 'multipart/form-data',
          'Authorization': 'Bearer '+await AsyncStorage.getItem('token')
        },
        body:data
        }).then((response) => response.json())
            .then((responseJson) => {
               this.setState({allData:responseJson.meta.product})
                if(responseJson.meta.product.length==0){
                 console.log("no products")
                }else{
                  
                  var dataArray=[]
                   

                  responseJson.meta.product.forEach(element => {
                      dataArray.push({image:element.images[0],name:element.name,lat:element.product_lat,lng:product_lng})
                  });
                  var half_length = Math.ceil(responseJson.meta.product/ 2);    
                  var p2 = arrayni.splice(half_length);
                  var desc2 = desc.splice(half_length);
                  this.setState({images1:arrayni,images2:p2,fetching:false,descriptions1:desc,descriptions2:desc2,displayDesc1:desc[0].name,displayDesc2:desc2[0].name})
                }
                
              
        })
            .catch((error) => { 
            console.log("ayay")
           
        });
    }

      postRapport = async (repText) =>{
        this.setState({
          popupVisible:true,
          reportPopupShow:false
        });
        
        let Token=await AsyncStorage.getItem('token');
        const { navigation } = this.props;
        const item = navigation.getParam('user');
        
        //for(x in this.state.curr_item) {
            console.log('product: '+this.state.curr_item.name);
        //}
        
        let userID=await AsyncStorage.getItem('user_id');
        let data = new FormData();
        //data.append('description','test');
        //data.append('reporter_id',userID);
        data.append('user_id',item.id);
        data.append('description',repText);
        data.append('reporter_id',userID);
        data.append('product_id',this.state.curr_item.id);
        //data.append('report_text', repText);
        await  fetch(PH+'/api/v1/reports/create', {
            method: 'POST',
            headers: {
                'Accept':'application/json',
                'Content-Type': 'multipart/form-data',
                'Authorization': 'Bearer '+Token
            },
            body:data,
        }) 
        .then((response) => response.json())
        .then((responseJson) => {       
            console.log(responseJson);
            if(responseJson.code==200) {
//              console.log('Item Report send');
              let dat = this;
              this.setState({
                popupVisibleRapport:true
              });
              //setTimeout(function(){}, 3000);
              setTimeout(() => { dat.setState({popupVisibleRapport: false}) }, 5000);
            }
            else{
              console.log('description',repText,'reporter_id',userID,'product_id', item.id,'token',Token);
              console.log("Code: "+responseJson.code);
            }
        })
        .catch((error) => {
        console.log(error)
    });
 
    }

    addRating = async () =>{
      const { navigation } = this.props;
      const item = navigation.getParam('user');
      let userID=await AsyncStorage.getItem('user_id');
      let data = new FormData();
      data.append('user_id', item.id);
      data.append('comment', this.state.comment);
      data.append('author_id', userID);
      data.append('rate', this.state.rating);
      fetch(PH+'/api/v1/userrating/create', {
        method: 'POST',
        headers: {
          'Accept':'application/json',
          'Content-Type': 'multipart/form-data',
          'Authorization': 'Bearer '+await AsyncStorage.getItem('token')
        },
        body:data
        }).then((response) => response.json())
            .then((responseJson) => {
              this.getUserRating()
              this.setState({isModalVisible:false})
        })
            .catch((error) => {
              console.log(error); 
        });
    }
    

    getUserRating = async () =>{
      const { navigation } = this.props;
      const item = navigation.getParam('user');
      let userID=await AsyncStorage.getItem('user_id');

      fetch(PH+'/api/v1/users/getUserRating?user_id='+item.id, {
        method: 'POST',
        headers: {
          'Accept':'application/json',
          'Content-Type': 'multipart/form-data',
          'Authorization': 'Bearer '+await AsyncStorage.getItem('token')
        },
        // body:data
        }).then((response) => response.json())
            .then((responseJson) => {
              this.setState({reviews:responseJson.meta.users.userrating,average_rate:responseJson.meta.users.AverageRating})
        })
            .catch((error) => {
              console.log(error); 
        });
    }

    

    _getLocationAsync = async () => {
      let location = await Location.getCurrentPositionAsync({});
      this.setState({ currentLocation:location });
    };

    getPermissionAsync = async () => {
      if (Constants.platform.ios) {
        const { status } = await Permissions.askAsync(Permissions.LOCATION);
        // let loca = await Location.getCurrentPositionAsync();
        
        if (status !== 'granted') {
          //alert('Sorry, we need Location permissions to serve you better!');
          this.setState({ showDialogMessage:true, dialog_message:'Sorry, we need Location permissions to serve you better!' })
          const { status } = await Permissions.askAsync(Permissions.LOCATION);
        }else{
          this._getLocationAsync();
          this.setState({ hasLocationPermission: status === 'granted' })
          
        }
       
        
      }else{
        const { status } = await Permissions.askAsync(Permissions.LOCATION);
        this.setState({ hasLocationPermission: status === 'granted' })
        if (status !== 'granted') {
          //alert('Sorry, we need Location permissions to serve you better!');
          this.setState({ showDialogMessage:true, dialog_message:'Sorry, we need Location permissions to serve you better!' })
        }else{
        this._getLocationAsync();
        this.setState({ hasLocationPermission: status === 'granted' })
        
      }
     
      
      }
     
    }

    distance(lat1,lon1,lat2,lon2) {
      var R = 6371; // km (change this constant to get miles)
      var dLat = (lat2-lat1) * Math.PI / 180;
      var dLon = (lon2-lon1) * Math.PI / 180;
      var a = Math.sin(dLat/2) * Math.sin(dLat/2) +
        Math.cos(lat1 * Math.PI / 180 ) * Math.cos(lat2 * Math.PI / 180 ) *
        Math.sin(dLon/2) * Math.sin(dLon/2);
      var c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1-a));
      var d = R * c;
      if (d>1) return Math.round(d)+"km";
      else if (d<=1) return Math.round(d*1000)+"m";
      return d;
    }
    locationPermission =  () =>{
      this.getPermissionAsync();
    }

    handleRating = (rating) => {
      this.setState({rating});
    }
    back = async ()=>{
    this.props.navigation.navigate('Home');
    }
    submit =()=>{
      //this.props.navigation.navigate('Home');
    }
    messages =()=>{
      this.props.navigation.navigate('ApproveChat');
    }
    next =()=>{
      this.props.navigation.navigate('DinKonto');
    }
    ratingCompleted(rating) {
      console.log("Rating is: " + rating)
    }
    toggleModal = () => {
      this.setState({isModalVisible: !this.state.isModalVisible});
    };
  
  render(){
    const resizeMode = 'cover';
    
    return (
        
      <View style={styles.container}>

            
      <ImageBackground
         style={{
           backgroundColor: '#ccc',
           flex: 1,
           resizeMode,
           position: 'absolute',
           width: wp('140%'), 
           height: '100%',
           justifyContent: 'center',
           
         }}
         source={require('../../../assets/images/slice81-min.png')}
         />
          <View>
            
        <Dialog
            visible={this.state.reportPopupShow}
            onTouchOutside={() => { this.setState({ reportPopupShow: false }); }}
            onHardwareBackPress={() => {this.setState({ reportPopupShow: false });}}
            dialogStyle={{backgroundColor:'white',width:wp('90%'),alignItems:'center',alignContent:'center'}}>
            <DialogContent>
                <View styles={{alignItems:'center', }}>
                    <Text  style={{fontSize:28, color:'#eca6a1', padding: 12, textAlign:'center', fontFamily:'KGPrimaryWhimsy',}}>Why do you want to report this product?</Text>
                    <TouchableOpacity onPress={() =>this.postRapport('The product is fake.')} >
                        <Text  style={[styles.button, {alignSelf:'center', textAlign:'center',fontSize:17,color:'white',padding: 12, fontFamily:'KGPrimaryWhimsy', marginTop:20, backgroundColor:'#54969c'}]}>The product is fake.</Text>
                    </TouchableOpacity>
                    <TouchableOpacity onPress={() =>this.postRapport('The product should not be passed on.')} >
                        <Text  style={[styles.button, {alignSelf:'center',textAlign:'center',fontSize:17,color:'white',padding: 12, fontFamily:'KGPrimaryWhimsy', marginTop:20, backgroundColor:'#54969c'}]}>The product should not be passed on.</Text>
                    </TouchableOpacity>
                    <TouchableOpacity onPress={() =>this.postRapport('The product is in the wrong category.')} >
                        <Text  style={[styles.button, {alignSelf:'center',textAlign:'center',fontSize:17,color:'white',padding: 12, fontFamily:'KGPrimaryWhimsy', marginTop:20, backgroundColor:'#54969c'}]}>The product is in the wrong category.</Text>
                    </TouchableOpacity>
                </View>
            </DialogContent>
        </Dialog>
          
          <Dialog
            visible={this.state.popupVisible}
            onHardwareBackPress={() => { this.setState({ popupVisible: false }); } }
            onTouchOutside={() => {
              this.setState({ popupVisible: false });
            }}
            dialogStyle={{backgroundColor:'transparent'}}
          >
    <DialogContent>
   
     <View styles={{alignItems:'center'}}>
     <TouchableOpacity onPress={()=>this.setState({ popupVisible: false })} style={{position:'absolute',right:5,top:50,height:60,width:60,zIndex:10}}>
        {/* <Text style={{color:'black',right:0}}>x</Text> */}
      </TouchableOpacity> 
      <Text  style={{position:'absolute',right:60,bottom:100,zIndex:10,fontSize:16,color:'grey'}}>Rapport Send</Text>
     <Image  resizeMode="contain" style={{width:wp('60%') ,height:hp('40%'),alignSelf:'center'}} source={require('../../../assets/icons/errorLogin.png')}/> 
       
     </View>
   
     
    </DialogContent>
  </Dialog>
          <Text  style={{ fontSize: 25 ,alignSelf:'center',color:'#2db9b9',fontFamily:'KGPrimaryWhimsy'}}>FAMILIEN <Text  style={{ fontSize: 25,color:'#ffb3b3',fontFamily:'KGPrimaryWhimsy',textTransform: 'uppercase'}}> {this.state.userInfo!==null?this.state.userInfo.name:"name"}</Text></Text>
         <View style={{marginTop:10}}>
          {this.state.userInfo!==null?(     
          // <ProgressiveImage source={{ uri: this.state.userInfo.image_url}}  style={{alignSelf:'center',height:50,width:50,borderRadius: 50/ 2}}/>
          <Thumbnail   style={{alignSelf:'center'}} source={{uri:this.state.userInfo.image_url}} /> 
          )
          : <Thumbnail style={{alignSelf:'center'}}   source={require('../../../assets/icons/user_image.png')} />  }
           <TouchableOpacity onPress={() =>console.log("chat")}>
          <Thumbnail  resizeMode='contain' square small source={require('../../../assets/images/slice52-min.png')} style={{alignSelf:'center',marginLeft:80,bottom:65}} />  
          </TouchableOpacity>  
              <Body style={{alignItems:'center',top:60,position:'absolute'}}> 
              <SwipeableRating
                  rating={this.state.average_rate}
                  size={17}
                  maxRating={6}
                  gap={2}
                  swipeable={false}
                  color='#e8b14a'
                  emptyColor='#e8b14a'
                  onPress={()=>this.toggleModal()}
                  allowHalves={true}
                  xOffset={30}
                  style={{zIndex:999,alignSelf:'center'}}
              />
              <Text style={{color:'#666666', fontSize:hp('2%'),alignSelf:'center'}}><Text style={styles.underline} onPress={() =>this.toggleModal()}>Bedøm</Text></Text> 
              <Text  style={{ textAlignVertical: "center",textAlign: "center",fontSize: Fsize3 ,alignSelf:'center',color:'grey',marginTop:5,width:wp('80%')}}>Vi er en røgfrifamilie. Vi er et dyrefrithjem.
Vi bruger parfumefri vaske og skyllemidde</Text>    

        <TouchableOpacity onPress={() =>this.followFamily()} style={{marginTop:13}}>
          <Text style={styles.button}>Følg familien</Text>
        </TouchableOpacity>

        <TouchableOpacity onPress={() =>console.log("sort")} style={{marginTop:15}}>
        <Icon name='expand-more' color='#424242' size={25} />
        </TouchableOpacity>

        <View style={{alignSelf:'center',marginTop:10,height:hp('50%'),bottom:13}}>
        {this.state.allData!==null?(
        <View style={styles.container4}>
                <FlatList style={styles.list}
                  contentContainerStyle={styles.listContainer}
                  ref={(ref) => { this.flatListRef = ref; }}
                  data={this.state.allData}
                  horizontal={false}
                  numColumns={2}
                  keyExtractor= {(item) => {
                    return item.id;
                  }}
                  refreshing={this.state.refreshing}
                  onRefresh={() => {
                     this.getUserProducts(this.state.userInfo.id);
                  }}
                  extraData={extra=1,datalenth=this.state.allData.length}
                  renderItem={({item,index}) => {
                
                    console.log(extra++,datalenth,index+1,item.name)
                    
                    return (
                      <View style={{alignSelf:'center'}}>
                        <ImageBackground
                            style={styles.frame}
                            source={require('../../../assets/images/slice72-min.png')}
                            resizeMode='contain'
                          >
                            {/* ORIGINAL */}
                        {/* <TouchableOpacity style={{alignItems:'center',alignContent:'center',alignSelf:'center'}}  onPress={() => this.props.navigation.navigate('ProductDetails', { 'product':item,'location': this.distance(item.product_lat,item.product_lng,this.state.currentLocation.coords.latitude,this.state.currentLocation.coords.longitude)})}> */}
                        {/* Change To Report */}
                        <TouchableOpacity style={{alignItems:'center',alignContent:'center',alignSelf:'center'}}  onPress={() => this.setState({reportPopupShow:true, curr_item:item})/*this.postRapport(item)*/ }>
                        <Text style = {styles.text}>{this.state.user}</Text>
                        <ProgressiveImage
                            source={{uri: item.images[0]}} 
                            thumbnailSource={require('../../../assets/images/slice3.png')}
                            style={styles.userImage}
                            // resizeMode="cover"
                          />
                      
                          </TouchableOpacity>
                        <View style={styles.cardFooter}>
                          <View style={{alignItems:"center", justifyContent:"center"}}>
                            <Text adjustsFontSizeToFit numberOfLines={1}  style={styles.name}>{item.name}</Text>
                            <Text adjustsFontSizeToFit numberOfLines={1}  style={{fontSize:Platform.OS === 'ios' ?13 : 11}}>{this.state.currentLocation!=null?this.distance(item.product_lat,item.product_lng,this.state.currentLocation.coords.latitude,this.state.currentLocation.coords.longitude)+' fra dig':'0 km fra dig'}</Text>
                          </View>
                          
                          {/* {item.delivery_type==1?(
                              <Image style={{width:40,height:40,position:'absolute',right:10,bottom:-10,zIndex:100}}  source={require('../../../assets/images/slice33-min.png')} />
                            ):null}     
                            {item.delivery_type==2?(
                          <Image style={{width:40,height:40,position:'absolute',left:10,bottom:-10,zIndex:100}}  source={require('../../../assets/images/slice32-min.png')} /> 
                            ):null} 
                            {item.delivery_type==3||item.delivery_type==null||item.delivery_type==0?(
                              <View style={{zIndex:100}}>
                            <Image style={{width:40,height:40,position:'absolute',left:-2}}  source={require('../../../assets/images/slice33-min.png')} />
                          <Image style={{width:40,height:40,position:'absolute',right:70}}  source={require('../../../assets/images/slice32-min.png')} /> 
                          </View>
                            ):null}  */}
                           
                        </View>
                        {extra%2==0&&index+1!=datalenth?(
                            // <Block center style={{position:'absolute',width:wp('94%'),marginTop:Platform.OS=='ios'?148:143,zIndex:-60}} >
                            <Image resizeMode='contain' style={{position:'absolute',width:wp('94%'),marginTop:Platform.OS=='ios'?148:143,zIndex:-60}} source={require('../../../assets/images/slice70-min.png')}/>
                            // </Block>
                           )
                            :null}
                            {datalenth%2!=0&&index+1==datalenth?(
                      
                            <Block center style={{position:'absolute',width:wp('94%'),marginTop:Platform.OS=='ios'?148:143,zIndex:-60}} >
                            <Image resizeMode='contain' style={{width:wp('94%')}} source={require('../../../assets/images/slice70-min.png')}/>
                            </Block>
                            )
                              :null}
                              
                        </ImageBackground>
                        </View>
                      
                    )
                  }}/>
            
              </View>  
                ):null}
              </View>
            
                </Body>          
         
        
          </View> 
          <Modal isVisible={this.state.isModalVisible} style={{height:hp('90%')}} onBackdropPress={()=>this.setState({isModalVisible:false})} animationOut="fadeOutLeft" onSwipeStart={()=>console.log("ayay")}>
              {/* <TouchableWithoutFeedback onPress={()=>Keyboard.dismiss()}> */}
                <View style={{flex:1,backgroundColor:'white',height:hp('95%'),borderRadius: 10}}>
                <TouchableOpacity  style={{position:'absolute',right:10,top:10,zIndex:999}} onPress={()=>this.setState({isModalVisible:false})}  >
                <Icon name='close' size={20} />
                </TouchableOpacity>
                <KeyboardAwareScrollView> 
                
                   <View style={{flex:1,marginTop:30}}>
                   {this.state.reviews===null?null:this.state.reviews.map((data) => {
                          return (
                            <View key={data.id}>
                              
                              <Body >
                  {data.Author.image_url!==null?(     
                  <ProgressiveImage source={{ uri: data.Author.image_url}}  style={{alignSelf:'center',height:50,width:50,borderRadius: 50/ 2}}/>)
                  : <Thumbnail  source={require('../../../assets/icons/user_image.png')} />  }
                    </Body>
                    <Body  style={{top:5}} >
                    <SwipeableRating
                        rating={data.rate}
                        size={18}
                        swipeable={false}
                        maxRating={6}
                        gap={2}
                        color='#e8b14a'
                        emptyColor='#e8b14a'
                        // onPress={this.handleRating}
                        swipeable={false}
                        allowHalves={true}
                    />
                    </Body>
                    <Body style={{width:wp('75%'),alignItems:'center',top:15}}>
                    <Text adjustsFontSizeToFit={true}  style={{ textAlignVertical: "center",textAlign: "center",fontSize: Fsize3 ,alignSelf:'center',color:'grey'}}>{data.comment}</Text>    
                    </Body>
                    <Body style={{width:wp('75%'),alignItems:'center',marginTop:20,marginBottom:10}}>
                    <View
                            style={{
                              borderBottomColor: 'grey',
                              borderBottomWidth: 1,width:wp('80%')
                            }}
                          />
                    </Body>
                              </View>
                          )
                        })}
               
                   </View>  
                 
                   <Body  style={{top:20,marginBottom:30}} >
                   <Body >
                   <Text style={{alignSelf:'center',fontSize:Fsize2,fontFamily:'KGPrimaryWhimsy',color:'#d47d7d'}}>Bedøm familien</Text>
                   <Body>
                    <SwipeableRating
                        rating={this.state.rating}
                        size={28}
                        swipeable={false}
                        maxRating={6}
                        gap={2}
                        color='#e8b14a'
                        emptyColor='#e8b14a'
                        onPress={this.handleRating}
                        allowHalves={true}
                    />
                      </Body>
                    </Body>
                <Body>
                
                  <TextInput multiline  style={{maxHeight: 80}} rounded style={{backgroundColor:'white',height:100,width:wp('80%'),borderColor: '#bfbfbf',
                  borderWidth: .5,
                  borderRadius: 12,padding:15,paddingBottom:5,paddingTop:10,shadowColor: '#000',
                  shadowOffset: { width: 0, height: 2 },
                  shadowOpacity: 0.5,
                  shadowRadius: 1.5,
                  elevation: 1,fontFamily:'KGPrimaryWhimsy',fontSize:Fsize,marginBottom:20}} 
                  onChangeText={text => this.setState({comment:text})}
                  autoCorrect={true}
                  ref={input => { this.textInput = input }}
                  />
                  <TouchableOpacity onPress={() =>this.addRating()} >
                    <Text style={styles.button5}>Afslut aftalen</Text>           
                  </TouchableOpacity>
                  
                  </Body>
                  </Body>
                </KeyboardAwareScrollView>
                </View>
                {/* </TouchableWithoutFeedback> */}
                </Modal> 
               
          </View> 
          <Snackbar
          visible={this.state.visibleSnack}
          onDismiss={() => this.setState({ visibleSnack: false })}
          // action={{
          //   label: 'Undo',
          //   onPress: () => {
          //     // Do something
          //   },
          // }}
          style={{backgroundColor:'#5e5e5e'}}
          duration={2000}
        >
        Bruger Fulgte!
        </Snackbar>
  </View>
   
    );
  }
  }
   

const styles = StyleSheet.create({
  container: {
    flex: 1,
    //alignItems: 'center',
    // flexDirection:'column',
    // justifyContent: 'center',
    backgroundColor: 'transparent'
  },
  container4:{
    flex:1,
    backgroundColor:'transparent',
     marginBottom:20,
    zIndex:99,
    alignContent:'center',
    alignSelf:'center'
  },
  list: {
    paddingHorizontal: 5,
     
    backgroundColor:"transparent",
    zIndex:99
  },
  listContainer:{
   alignItems:'center',
    bottom:3,
   zIndex:99
  },
  /******** card **************/
  card:{
    shadowColor: '#00000021',
    shadowOffset: {
      width: 0,
      height: 6,
    },
    shadowOpacity: 0.37,
    shadowRadius: 7.49,
    elevation: 12,
    alignSelf:'center',
    marginVertical: 5,
    backgroundColor:"transparent",
    flexBasis: '46%',
    zIndex:99
    
  },
  frame:{
    backgroundColor:"transparent",
    flexBasis: '45%',
    marginHorizontal: 5,
    width:wp('40%'),
    zIndex:999
  },
  cardFooter: {
    paddingVertical: 17,
    paddingHorizontal: 16,
    borderTopLeftRadius: 1,
    borderTopRightRadius: 1,
    flexDirection: 'row',
    alignItems:"center", 
    justifyContent:"center",
    zIndex:99
  },
  cardContent: {
    paddingVertical: 12.5,
    paddingHorizontal: 16,
    zIndex:99
  },
  cardHeader:{
    flexDirection: 'row',
    justifyContent: 'space-between',
    paddingTop: 12.5,
    paddingBottom: 25,
    paddingHorizontal: 16,
    borderBottomLeftRadius: 1,
    borderBottomRightRadius: 1,
  },
  userImage:{
    height: Platform.OS=='ios'?125:120,
    width: Platform.OS=='ios'?125:120,
    // borderRadius:60,
    marginTop:Platform.OS=='ios'?10:12,
    alignSelf:'center',
    // borderColor:"#DCDCDC",
     // borderWidth:3,
     zIndex:99
  },
  name:{
    fontSize:Fsize,
    flex:1,
    alignSelf:'center',
    color: '#444444',
    fontWeight:'bold',
    fontFamily:'KGPrimaryWhimsy',
    zIndex:99,
    padding:2
  },
  position:{
    fontSize:14,
    flex:1,
    alignSelf:'center',
    color:"#696969",
    zIndex:99
  },
  followButton: {
    marginTop:10,
    height:35,
    width:100,
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius:30,
    backgroundColor: "#00BFFF",
  },
  followButtonText:{
    color: "#FFFFFF",
    fontSize:20,
  },
  icon:{
    height: 20,
    width: 20, 
  },
  container2: {
    flex: 1,
    //alignItems: 'center',
    //justifyContent: 'flex-start',
    
    backgroundColor: 'transparent'
    //bottom: Dimensions.get('screen').height /5
  },
  textstyle:{
    fontFamily:'KGPrimaryWhimsy',
    fontSize:40
  },
  button5: {
    marginTop:5,
    backgroundColor: '#54969c',
    borderColor: 'white',
    borderWidth: .5,
    borderRadius: 12,
    color: 'white',
    overflow: 'hidden',
    padding: 12,
    textAlign:'center',
    width: 300,
    height: 44,
    opacity: 0.9,
    fontFamily:'KGPrimaryWhimsy',
    fontSize:20,
    alignSelf:'center',
    alignItems:'center'
  },
  container3: {
    position:'relative',
    backgroundColor: 'transparent',
    height:hp('75%'),
    width:wp('100%'),
    //alignItems: "center"
  },
  dropdown: {
    height: Dimensions.get('screen').width /7,
    padding: 5,
    position:'relative',
    margin:5,
    borderWidth: .2,
    borderRadius: 12,
    textShadowColor: 'black',
    borderColor: 'black',
    marginBottom: 10,
    backgroundColor: 'white',
    opacity: 0.9,
    fontFamily:'KGPrimaryWhimsy',
    fontSize:20,
    alignContent:'center'
  },
  wrpper: {
    opacity: 0.9,
    fontSize:20,
    fontFamily:'KGPrimaryWhimsy',
    flexDirection: 'row', justifyContent: 'space-around',   
  },
  underline: {textDecorationLine: 'underline'},
  button: {
    backgroundColor: 'white',
    borderColor: 'black',
    borderWidth: .5,
    borderRadius: 12,
    color: '#54969c',
    overflow: 'hidden',
    padding: 12,
    textAlign:'center',
    width: 300,
    height: 44,
    opacity: 0.9,
    fontFamily:'KGPrimaryWhimsy',
    fontSize:20
  },
});

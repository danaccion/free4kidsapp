import React, { PureComponent } from 'react';
import { SafeAreaView, StyleSheet,Text,View,ImageBackground,Image,TouchableOpacity,Platform,Dimensions,KeyboardAvoidingView,ListView,FlatList,AsyncStorage,ScrollView, Alert } from 'react-native';
import Ionicons from 'react-native-vector-icons/Ionicons'
import BackButton from '../../BackButton';
import { Container, Header, Content, Form, Item, Picker,Icon,Card,CardItem,Label,Button,Toast} from 'native-base';
import HamburgerIcon from '../../icons/HamburgerIcon';
import HeartBagBurger from '../../icons/HeartBagBurger';
import CameraPage from '../CameraScreen'
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';
import Dialog, {DialogFooter,DialogButton,SlideAnimation,DialogContent } from 'react-native-popup-dialog';
import {horizontalTabs} from '../../icons/horizontalTabIcons';
import MaterialIcon from 'react-native-vector-icons/MaterialCommunityIcons';
import Loader from '../../icons/addLoading';
import ImageLoader from '../../icons/uploadImageLoad';
import { withTheme } from 'react-native-elements';
import * as Location from 'expo-location';
import SectionedMultiSelect from 'react-native-sectioned-multi-select';
import { SliderBox } from "react-native-image-slider-box";
import { PH,SS } from 'react-native-dotenv'
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view'
import DropDownPicker from 'react-native-dropdown-picker';
import { connect } from 'react-redux';
import {addProductData} from '../actions/index'
import {
  Block,Input
} from 'galio-framework';
const { width, height } = Dimensions.get('window');
const extraHeight = Platform.OS === 'ios' ? 10 : 20
const Fsize = Platform.OS === 'ios' ?16 : 14;
 const Fsize2 = Platform.OS === 'ios' ?25 : 18;
class HorizontalMenuList extends PureComponent{
  
  
    state = {
      type: '',
    
    };
  

  onSelectType =(e,id) =>{

    this.props.SelectType(e,id); 
     this.setState({type:e})
    // this.setState(this.state);
    
  }
  render(){
    return(
      <View style={{ height:60}}>
      {this.props.props.selectedType===this.props.item.description?(
      <View style={{
        flex:1,
        flexDirection:'column',
        alignItems:'center',
        width:95,
        margin:4,
        borderWidth: 1,
        borderColor:'#cc586f',
        padding:-1,
        borderRadius:8,
        borderStyle: 'dashed',
      }}>
        <TouchableOpacity onPress={()=>{
          this.onSelectType(this.props.item.description,this.props.item.id)
          
        }}>
      
          <View style={{margin:4}}>
          <Image  resizeMode="contain" style={{width:wp('9.5%') ,height:hp('3%'),alignSelf:'center'}} source={this.props.item.image}/> 
          <Text adjustsFontSizeToFit numberOfLines={1} style={{color:'#cc586f',fontFamily:'KGPrimaryWhimsy', fontSize:Fsize}}>
            {this.props.item.description}
          </Text>
          </View>
        </TouchableOpacity>
      </View>): <View style={{
        flex:1,
        flexDirection:'column',
        alignItems:'center',
        width:95,
        margin:4,
       
      }}>
        <TouchableOpacity onPress={()=>{
          this.onSelectType(this.props.item.description,this.props.item.id)
          
        }}>
        <View style={{margin:4}}>
        <Image  resizeMode="contain" style={{width:wp('9.5%') ,height:hp('3%'),alignSelf:'center'}} source={this.props.item.image}/> 
        <Text adjustsFontSizeToFit numberOfLines={1} style={{color:'grey',fontFamily:'KGPrimaryWhimsy', fontSize:Fsize}}>
          {this.props.item.description}
        </Text>
        </View>
       
        </TouchableOpacity>
      </View>}
      </View>
    )
  }
}

 class CreateProduct extends PureComponent {
  
  static navigationOptions = ( ) => {
     
      return {
        gesturesEnabled: false,
          headerLeft: <HamburgerIcon/>,
          headerTitle: (
            <Image
            resizeMode="contain"
            source={require('../../../assets/images/slice2.png')}
            style={{ width: 10, height:10 , flex:4,justifyContent: 'space-around', alignItems: 'center', padding: 25,}}
          />
          ),
          headerRight: <HeartBagBurger/>,
  
      };

   
};

  state = {
    selectedSize: undefined, productName: '',productRemark: '',selectedStand: undefined,selectedOption:null,selectedSex:null,productBrand:null,
    selectedType:'Beklædning',
    picture:[],
    toCamera:1,
    popupVisible:false,
    deliverytype:'',
    popupdelivery:false,
    loading:false,
    uploadingImage:false,
    selectedAge:[],
    selectedAgeSingle:null,
    isShownPicker: false,
    currentLocation:null,
    selectedTypeId:2,
    doneCreate:false,
    fullscreen:false,
    hasproduct:false,
    showDialogMessage:false,
    dialog_message:''
  };
  baseState = this.state 

selectSize(value) {
  this.setState({
    selectedSize: value
  });
}
selectSex(value) {
  this.setState({
    selectedSex: value
  });
}
selectStand(value) {
  this.setState({
    selectedStand: value
  });
}
selectAge(value) {
  this.setState({
    selectedAgeSingle: value
  });
}
selectedOption(value) {
  this.setState({
    selectedOption: value
  });
}
deliveryChange(value) {
  if(value!="2"){
    this.setState({
      deliverytype: value,
    });
  }else{
    this.setState({
      deliverytype: value,popupdelivery:true
    });
  }
 
 
}
onUpdate = (val) => {
  this.setState({
    picture: val,
  })
};  
camera = () => {
  this.setState({
    toCamera: 1
  })
};    
onBack = () => {
  this.setState({
    toCamera: 0
  })
};    
nextPage = () => {
  this.setState({
    toCamera: 0
  })
};   
SelectType = (value,id) => {
 
  this.setState({
    selectedType: value,selectedTypeId:id,selectedAgeSingle:null,selectedAge:[]
  })
};   
onSelectedItemsChange = (selectedAge) => {
  this.setState({ selectedAge });
};
onDone = async () =>{
  
  this.setState({selectedSize: undefined, productName: '',productRemark: '',selectedStand: undefined,selectedOption:null,selectedSex:null,productBrand:null,
    selectedType:'Beklædning',
    picture:[],
    toCamera:1,
    popupVisible:false,
    deliverytype:'',
    popupdelivery:false,
    loading:false,
    uploadingImage:false,
    selectedAge:null,
    selectedAgeSingle:null,
    isShownPicker: false,
    currentLocation:null,
    selectedTypeId:2,
    doneCreate:false,
    fullscreen:false,
    hasproduct:false,})
    this.props.navigation.navigate('Shop');
}
onFinish(){
  this.setState({doneCreate:false})
  setTimeout(() => {
    this.onDone()
  }, 500)
   
}
 async componentDidMount(){
  this.checkProduct()
  const { navigation } = this.props;
  const pictures = navigation.getParam('pictures');
  this.setState({picture:pictures})
 }

 
 checkProduct = async () =>{
  fetch(PH+'/api/v1/users/hasproduct', {
    method: 'GET',
    headers: {
      'Accept':'application/json',
      'Content-Type': 'multipart/form-data',
      'Authorization': 'Bearer '+await AsyncStorage.getItem('token')
    },
    }).then((response) => response.json())
        .then((responseJson) => {
          if(responseJson.meta.productexist === false){
            this.setState({hasproduct:false})
          }
    })
        .catch((error) => {
            console.log(error)
    });
}
 

 _getLocationAsync = async () => {
  let location = await Location.getCurrentPositionAsync({});
  this.setState({ currentLocation:location });
};

discardCreate = () => {
  this.setState(this.baseState)
  this.props.navigation.navigate('Shop');
  // this.doneImage()
};  

changeImage = async()=>{
  this.setState({
    picture:[],
    toCamera:1,
    fullscreen:false
})
this.props.navigation.navigate('Shop');
}

 

closeDelivery = async () => {
  this.setState({popupdelivery:false})
  
};   

 
onCreate = async () => {
  this.setState({loading:true})
  let Token=await AsyncStorage.getItem('token');
  let userID=await AsyncStorage.getItem('user_id');
  let address;
  let userPlace;
  const that=this
  const pictures=this.state.picture
  if(this.state.currentLocation!=null){
     address = await Location.reverseGeocodeAsync({'latitude':this.state.currentLocation.coords.latitude,'longitude':this.state.currentLocation.coords.longitude})
     if(address[0].street!=null){
      userPlace=address[0].street+" "+address[0].city+", "+address[0].region+", "+address[0].country
      }else{
        userPlace=address[0].city+", "+address[0].region+", "+address[0].country
      }
    }else{
      this._getLocationAsync()
    }
  let type="image/png"
  if(this.state.picture!=null){
    type = "image/" + (this.state.picture[0].split(".").pop() || "jpg");
  }
  let data = new FormData();
      data.append('user_id',userID);
      data.append('name', this.state.productName); // you can append anyone.
      data.append('description', this.state.productRemark);
      data.append('location', userPlace);
      data.append('delivery_type', this.state.deliverytype);

      if(this.state.productBrand!==null){
        data.append('brand', this.state.productBrand);
      }
      if(this.state.selectedStand!==null){
        data.append('stand', this.state.selectedStand);
      }
      if(this.state.selectedAge!==null&&this.state.selectedAgeSingle!==null){
      if(this.state.selectedAge.length>1){
        this.state.selectedAge.sort(function(a, b){return a - b});
        let f=this.state.selectedAge[0];  
          // storing the last item  
          let l=this.state.selectedAge[this.state.selectedAge.length-1];  
          let age= f+"-"+l+" år";
        data.append('age', age);
      }else{
        let f=this.state.selectedAge;  
          // storing the last item  
          // let l=this.state.selectedAge[this.state.selectedAge.length-1];  
          // let age= f+"-"+l+" år";
        data.append('age', f+" ar");
      }
    }if(this.state.selectedAge===null&&this.state.selectedAgeSingle!==null){
      data.append('age', this.state.selectedAgeSingle);
    }
      if(this.state.selectedSex!==null){
        data.append('sex', this.state.selectedSex);
      }
      if(this.state.selectedSize!==null){
        data.append('size', this.state.selectedSize);
      }
      data.append('category_id', this.state.selectedTypeId);
      data.append('pay',0);
      data.append('product_lat',this.state.currentLocation.coords.latitude);
      data.append('product_lng',this.state.currentLocation.coords.longitude);
      if(pictures!=null){
        data.append("image", {
          uri: this.state.picture[0],
          type: type,
          name: "DP"
        });
      }
 await  fetch(PH+'/api/v1/products/create', {
    method: 'POST',
    headers: {
      'Accept':'application/json',
       'Content-Type': 'multipart/form-data',
      'Authorization': 'Bearer '+Token
    },
    body:data,
    }).then((response) => response.json())
        .then((responseJson) => {
          if(responseJson.code!==500||responseJson.code!==401){
            this.setState({loading:false});
            if(pictures.length>1){
            that.setState({uploadingImage:true,loading:false})
            let count = 0;
            pictures.forEach(async function(i, idx, array){
              if(count!==0){
              let type = "image/" + (i.split(".").pop() || "jpg");
              let data = new FormData();
              data.append('product_id',responseJson.meta.product.id);
              data.append("image", {
              uri: i, 
              type: type,
              name: "Productphoto"
            });
            await  fetch(PH+'/api/v1/image/create', {
                 method: 'POST',
                 headers: {
                   'Accept':'application/json',
                    'Content-Type': 'multipart/form-data',
                   'Authorization': 'Bearer '+Token
                 },
                 body:data,
                 }).then((response) => response.json())
                     .then((responseJson) => {
                      count++;
                      if(count===pictures.length){
                        that.setState({uploadingImage:false,doneCreate:true})
                      }
                 })
                     .catch((error) => {                  
                       console.log(error)

                 });
              }else{
                count++
              }
              
             });
            }
            else{
              that.setState({doneCreate:true,loading:false})
            }
          }
          else{
            //Alert(responseJson.title)
            this.setState({ showDialogMessage:true, dialog_message:responseJson.title })
          }
    })
        .catch((error) => {
        console.log(error)
    });
   
};    

 
  
  render(){
    if(this.state.currentLocation===null){
      this._getLocationAsync()
    }
    const { navigation } = this.props;
    const pictures = navigation.getParam('pictures');
    const items = [{
            label: '0 år',
            value:0,
          },
          {
            label: '1 år',
            value: 1,
          },
          {
            label: '2 år',
            value: 2,
          },
          {
            label: '3 år',
            value: 3,
          },
          {
            label: '4 år',
            value: 4,
          },
          {
            label: '5 år',
            value: 5,
          },
          {
            label: '6 år',
            value: 6,
          },
          {
            label: '7 år',
            value: 7,
          },

          {
            label: '8 år',
            value: 8,
          },
          {
            label: '9 år',
            value:9,
          },
          {
            label: '10 år',
            value: 10,
          },
          {
            label: '11 år',
            value: 11,
          },
          {
            label: '12 år',
            value: 12,
          },
          {
            label: '13 år',
            value: 13,
          },
          {
            label: '14 år',
            value: 14,
      }
    ];
    const resizeMode = 'contain';
    console.log(this.state.age)
  
    return (
      <View style={styles.container}>
      <ImageBackground
         style={{
           backgroundColor: '#ccc',
           flex: 1,
           resizeMode,
          //  position: 'absolute',
           width: wp('100%'), 
           height: hp('100%'),
            justifyContent: 'center',
           
         }}
         source={require('../../../assets/images/slice81-min.png')}
         />
      <KeyboardAwareScrollView>
   
          <Card transparent style={{backgroundColor:'transparent',height:hp('40%'),alignSelf:'center',alignContent:'center',justifyContent:'center',borderColor:'transparent',borderBottomColor:'transparent',borderTopColor:'transparent'}}>
            <CardItem style={{backgroundColor:'transparent',alignContent:'center',zIndex:100,marginTop:30,borderBottomColor: 'transparent',borderTopColor:'transparent'}}>
            <Image  resizeMode="contain" style={{width:wp('30%') ,height:hp('10%'),alignSelf:'center',marginLeft:wp('10%'),paddingLeft:40,marginBottom:-hp('16%')}} source={require('../../../assets/images/slice34-min.png')}/>  
          
            {this.state.picture==''?(
              <Block center>
              <Text>Loading..</Text>
           </Block>
           ): 
           <View >
           <SliderBox
           images={this.state.picture}
           sliderBoxHeight={200}
           onCurrentImagePressed={() => this.setState({fullscreen:true})}
           dotColor="#d18089"
           inactiveDotColor="#90A4AE"
           paginationBoxVerticalPadding={20}
           autoplay
           circleLoop
           parentWidth={wp('40%')}
           resizeMethod={'resize'}
           resizeMode={'contain'}
           paginationBoxStyle={{
             position: "absolute",
             bottom: 0,
             padding: 0,
             alignItems: "center",
             alignSelf: "center",
             justifyContent: "center",
             paddingVertical: 10
           }}
           dotStyle={{
             width: 10,
             height: 10,
             borderRadius: 5,
             marginHorizontal: 0,
             padding: 0,
             margin: 0,
             backgroundColor: "rgba(128, 128, 128, 0.92)"
           }}
           ImageComponentStyle={{borderRadius: 2, width: wp('50%'), marginTop: 20, marginBottom:10,marginLeft:10}}
         /></View>}
            <Image  resizeMode="contain" style={{width:wp('35%') ,height:hp('20%'),alignSelf:'center',marginRight:wp('10%'),marginBottom:-50}} source={require('../../../assets/images/slice14.png')}/>  
            <Loader
                  loading={this.state.loading} />
            <ImageLoader
                  loading={this.state.uploadingImage} />
            </CardItem>
            <Image  resizeMode="contain" style={{width:wp('90%') ,height:hp('20%'),alignSelf:'center',marginTop:-95}} source={require('../../../assets/images/slice70-min.png')}/>
           
            <View style={{height:100,marginTop:-wp('10%'),marginLeft:40,paddingLeft:30,marginRight:50,paddingRight:30,borderColor:'transparent'}}>
              
              <FlatList style={{backgroundColor:'transparent'}} 
                        horizontal={true} 
                        data={horizontalTabs}
                        showsHorizontalScrollIndicator={false}
                        renderItem={({item,index})=>{
                          return(
                            <HorizontalMenuList SelectType={this.SelectType} props={this.state} item={item} index={index} parentFlatlist={this}/>
                          );
                        }}
                        keyExtractor={(item,index)=>item.type}
                        >
              </FlatList>
            </View>    
      </Card>
      {/* <KeyboardAvoidingView style={{backgroundColor:'transparent'}} behavior="padding" enabled> */}
    <View style={{alignItems:'center'}}>  
        <Dialog
          visible={this.state.popupVisible}
          onHardwareBackPress={() => { this.setState({ popupVisible: false }); } }
          onTouchOutside={() => {
            this.setState({ popupVisible: false });
          }}
        >
          
          <DialogContent style={{width:wp('90%')}}>
          <Image  resizeMode="contain" style={{width:wp('90%') ,height:hp('20%'),alignSelf:'center'}} source={require('../../../assets/images/slice70-min.png')}/>
          <Text adjustsFontSizeToFit={true} style={{color:'black',fontSize:20}}>Vi har brug for det rigtige foto her. denne er kun en prøve.</Text>
          </DialogContent>
        </Dialog>
        <Dialog
                    visible={this.state.fullscreen}
                    onHardwareBackPress={() => { this.setState({ fullscreen: false }); } }
                    onTouchOutside={()=> this.setState({ fullscreen: false })}
          >
            <DialogContent style={{width:wp('90%'),height:hp('60%'),alignItems:'center'}}>
            <TouchableOpacity style={{width:50,height:48,right:-5,top:2,position:'absolute',zIndex:100}} onPress={() => {this.setState({fullscreen:false})}}>
            <Image  resizeMode="contain" style={{width:25,height:25}} source={require('../../../assets/icons/close.png')}/>
            </TouchableOpacity>
            <TouchableOpacity style={{width:80,height:48,left:10,top:7,position:'absolute',zIndex:100}} onPress={()=>this.changeImage()}>
            <Text>Veksling</Text>
            </TouchableOpacity>
            <View style={{marginTop:30}}> 
            <SliderBox
           images={this.state.picture}
           sliderBoxHeight={hp('50%')}
          //  onCurrentImagePressed={index => console.warn(`image ${index} pressed`)}
           dotColor="#d18089"
           inactiveDotColor="#90A4AE"
           paginationBoxVerticalPadding={20}
           autoplay
           circleLoop
           parentWidth={wp('100%')}
           resizeMethod={'resize'}
           resizeMode={'contain'}
           paginationBoxStyle={{
             position: "absolute",
             bottom: 0,
             padding: 0,
             marginTop:3,
             alignItems: "center",
             alignSelf: "center",
             justifyContent: "center",
             paddingVertical: 10
           }}
           dotStyle={{
             width: 10,
             height: 10,
             borderRadius: 5,
             marginHorizontal: 0,
             padding: 0,
             margin: 0,
             marginTop:3,
             backgroundColor: "rgba(128, 128, 128, 0.92)"
           }}
           ImageComponentStyle={{borderRadius: 15, width: wp('80%'),alignSelf:'center', marginTop: 10, marginBottom:10}}
         />
        
         </View>
            </DialogContent>
          </Dialog>  
        <Dialog
          visible={this.state.popupdelivery}
          onHardwareBackPress={() => { this.setState({ popupdelivery: false }); } }
          onTouchOutside={() => {
            this.setState({ popupdelivery: false });
          }}
          dialogStyle={{backgroundColor:'transparent'}}
        >
          
          <DialogContent>
            <View>
          <TouchableOpacity onPress={()=>this.closeDelivery()} style={{position:'absolute',right:10,top:20,width:wp('100%'),height:hp('60%'),zIndex:10}}>
              {/* <Text style={{color:'black',right:0}}>x</Text> */}
            </TouchableOpacity> 
                <Image  resizeMode="contain" style={{width:wp('90%') ,height:hp('40%'),alignSelf:'center'}} source={require('../../../assets/icons/slice3.png')}/>
                </View>
        
          
          </DialogContent>
              </Dialog>
        <Dialog
    visible={this.state.doneCreate}
    onTouchOutside={() => {
     this.onFinish()
    
    }}
    dialogStyle={{backgroundColor:'transparent'}}
  >
    <DialogContent>
     <View>
     <TouchableOpacity onPress={()=>this.onFinish()} style={{position:'absolute',right:5,top:10,height:wp('60%'),width:wp('60%'),zIndex:10}}>
        {/* <Text style={{color:'black',right:0}}>x</Text> */}
      </TouchableOpacity> 
     
     <Image  resizeMode="contain" style={{width:wp('90%') ,height:hp('40%'),alignSelf:'center'}} source={require('../../../assets/icons/slice16.png')}/> 
     </View>
   
     
    </DialogContent>
  </Dialog>
            </View>
            <Block flex={1} safe center>
            <Block  flex={1} center style={{zIndex:999,marginTop:10}}>
              <Block center style={{width:width*.95,height: 50,marginBottom:20}}>
            <Input style={{height: 50,borderColor:'#bfbfbf'}} onChangeText={(productName) => this.setState({ productName })} placeholder='Hvad giver du videre?'/>
            </Block>
            </Block>
            {/* START OF CLOTHING OPTIONS */}
            {this.state.selectedType=="Beklædning"?(
              
               
                    <DropDownPicker
                    zIndex={9000}
                    arrowColor={'grey'}
                    items={[ {label: 'Tøj', value: 'Clothing'},{label: 'Sko', value:'Shoes'},{label: 'Tilbehør', value:'Accessories'}]}
                    labelStyle={{
                        color: 'grey'
                    }}
                    style={{
                      borderTopLeftRadius: 10, borderTopRightRadius: 10,
                      borderBottomLeftRadius: 10, borderBottomRightRadius: 10
                  }}
                    arrowStyle={{color: 'grey'}}
                    defaultNull
                    dropDownStyle={{zIndex:6000}}
                    placeholder="Beklædning"
                    containerStyle={{height: 50,width:width*0.95,borderRadius:8}}
                    onChangeItem={item =>  this.setState({ selectedOption: item.value})}
                />
              ):null}          
            {this.state.selectedType=="Beklædning"&&this.state.selectedOption=="Clothing"?(
                 
                  <DropDownPicker 
                  zIndex={8500}
                   style={{
                    borderTopLeftRadius: 10, borderTopRightRadius: 10,
                    borderBottomLeftRadius: 10, borderBottomRightRadius: 10
                }}
                  arrowColor={'grey'}
                  items={[ 
                  {label:"0 md / 40" ,value:"0 md / 40"},
                  {label: '0 md / 44', value:'0 md / 44'},
                  {label: '0 md / 50', value:'0 md / 50'},
                  {label: '2 mdr / 56', value: '2 mdr / 56'},
                  {label: '3 mdr / 62', value:'3 mdr / 62'},
                  {label: '6 mdr / 68', value:'6 mdr / 68'},
                  {label: '9 mdr / 74', value: '9 mdr / 74'},
                  {label: '12 mdr / 80', value:'12 mdr / 80'},
                  {label: '12-18 mdr / 86', value:'12-18 mdr / 86'},
                  {label: '18 mdr - 2 år / 92', value:'18 mdr - 2 år / 92'},
                  {label:"2-3 år / 98" ,value:"2-3 år / 98"},
                  {label: '3-4 år 104', value:'3-4 år 104'},
                  {label: '4-5 år / 110', value:'4-5 år / 110'},
                  {label: '5-6 år / 116', value: '5-6 år / 116'},
                  {label: '6-7 år / 122', value:'6-7 år / 122'},
                  {label: '7-8 år / 128', value:'7-8 år / 128'},
                  {label: '8-9 år / 134', value: '8-9 år / 134'},
                  {label: '9-10 år / 140', value:'9-10 år / 140'},
                  {label: '10-11 år / 146', value:'10-11 år / 146'},
                  {label: '11-12 år / 152', value:'11-12 år / 152'},
                  {label: '12-13 år / 158', value:'12-13 år / 158'},
                  {label: '13-14 år / 164', value: '13-14 år / 164'},
                  {label: '14 år+ / 170', value:'14 år+ / 170'},
                ]}
                  labelStyle={{
                      color: 'grey'
                  }}
                  arrowStyle={{color: 'grey'}}
                  defaultNull
                  placeholder="Størrelse"
                  containerStyle={{height: 50,width:width*0.95,borderRadius:8,marginTop:10}}
                  onChangeItem={item =>  this.setState({ selectedSize: item.value})}
              />
               
            ):null}  
            {this.state.selectedType=="Legetøj"||this.state.selectedType=="Udstyr"||this.state.selectedType=="Sport & fritid"||this.state.selectedType=="Rodekassen"?(
                 
                <DropDownPicker multiple multipleText={'%d valgte aldre'}
                zIndex={8000}
                 style={{
                  borderTopLeftRadius: 10, borderTopRightRadius: 10,
                  borderBottomLeftRadius: 10, borderBottomRightRadius: 10
              }}
                arrowColor={'grey'}
                items={items}
                labelStyle={{
                    color: 'grey'
                }}
                arrowStyle={{color: 'grey'}}
                placeholder="Valgt alder"
                containerStyle={{height: 50,width:width*0.95,borderRadius:8}}
                onChangeItem={defaultValue =>  this.setState({ selectedAge: defaultValue})}
            /> 
           
            ):null}   
             {this.state.selectedType=="Børneværelset"?(
               
                   <DropDownPicker 
                   zIndex={7500}
                    style={{
                      borderTopLeftRadius: 10, borderTopRightRadius: 10,
                      borderBottomLeftRadius: 10, borderBottomRightRadius: 10
                  }}
                   arrowColor={'grey'}
                   items={[ 
                   {label:"Babyværelset" ,value:"Babyværelset"},
                   {label: 'Børneværelse', value:'Børneværelse'},
                   {label: 'Teenagerværelse', value:'Teenagerværelse'},
                 ]}
                   labelStyle={{
                       color: 'grey'
                   }}
                   arrowStyle={{color: 'grey'}}
                   defaultNull
                   placeholder="Alder"
                   containerStyle={{height: 50,width:width*0.95,borderRadius:8}}
                   onChangeItem={item =>  this.setState({ selectedSize: item.value})}
               />
               
            ):null}    
             {this.state.selectedType=="Cykler"?(
                
                    <DropDownPicker 
                      zIndex={7000}
                     style={{
                      borderTopLeftRadius: 10, borderTopRightRadius: 10,
                      borderBottomLeftRadius: 10, borderBottomRightRadius: 10
                  }}
                    arrowColor={'grey'}
                    items={[ 
                    {label:"1-2 år" ,value:"1-2 år"},
                    {label: '10" (2-3 år)', value:'10" (2-3 år)'},
                    {label: '12" (3-4 år)', value:'12" (3-4 år)'},
                    {label: '14" (4-5 år)', value:'14" (4-5 år)'},
                    {label: '16" (5-6 år)', value:'16" (5-6 år)'},
                    {label: '18" (6-7 år)', value:'18" (6-7 år)'},
                    {label: '20" (7-8 år)', value:'20" (7-8 år)'},
                    {label: '24" (8-10 år)', value:'24" (8-10 år)'},
                    {label: '26" (10-13 år)', value:'26" (10-13 år)'},
                  ]}
                    labelStyle={{
                        color: 'grey'
                    }}
                    arrowStyle={{color: 'grey'}}
                    defaultNull
                    placeholder="Alder"
                    containerStyle={{height: 50,width:width*0.95,borderRadius:8}}
                    onChangeItem={item =>  this.setState({ selectedAgeSingle: item.value})}
                />
                 
            ):null}  
            
                    <DropDownPicker 
                    zIndex={6500}
                    
                    dropDownStyle={{alignSelf:'center'}}
                     style={{alignSelf:'center',
                      borderTopLeftRadius: 10, borderTopRightRadius: 10,
                      borderBottomLeftRadius: 10, borderBottomRightRadius: 10
                  }}
                    arrowColor={'grey'}
                    items={[ 
                    {label:"Pige" ,value:"Pige"},
                    {label: 'Dreng', value:'Dreng'},
                    {label: 'Unisex', value:'Unisex'},
                  ]}
                    labelStyle={{
                        color: 'grey'
                    }}
                    arrowStyle={{color: 'grey'}}
                    defaultNull
                    placeholder="Køn"
                    containerStyle={{height: 50,width:width*0.95,borderRadius:8,marginTop:10}}
                    onChangeItem={item =>  this.setState({ selectedSex: item.value})}
                />
           
            <Block center style={{width:width*.95}}>
             <Input style={{height: 50,borderColor:'#bfbfbf'}} onChangeText={(productBrand) => this.setState({ productBrand })} placeholder='Mærke'/>        
             </Block>    
             {this.state.selectedType=="Beklædning"&&this.state.selectedOption=="Shoes"?(
                      <DropDownPicker 
                      zIndex={6000}
                       style={{
                        borderTopLeftRadius: 10, borderTopRightRadius: 10,
                        borderBottomLeftRadius: 10, borderBottomRightRadius: 10
                    }}
                      dropDownStyle={{zIndex:10000,height:300}}
                      arrowColor={'grey'}
                      items={[ 
                        {label:'14' ,value:'14'},
                        {label:'15', value:'15'},
                        {label:'16' ,value:'16'},
                        {label:'17', value:'17'},
                        {label:'18', value:'18'},
                        {label:'19', value:'19'},
                        {label:'20', value:'20'},
                        {label:'21', value:'21'},
                        {label:'22', value:'22'},
                        {label:'23', value:'23'},
                        {label:'24', value:'24'},
                        {label:'25', value:'25'},
                        {label:'26', value:'26'},
                        {label:'27', value:'27'},
                        {label:'28', value:'28'},
                        {label:'30', value:'30'},
                        {label:'31', value:'31'},
                        {label:'32', value:'32'},
                        {label:'33', value:'33'},
                        {label:'34', value:'34'},
                        {label:'35', value:'35'},
                        {label:'36', value:'36'},
                        {label:'37', value:'37'},
                        {label:'38', value:'38'},
                        {label:'39', value:'39'},
                        {label:'40', value:'40'},
                        {label:'41', value:'41'},
                        {label:'42', value:'42'},
                        {label:'43', value:'43'},
                      ]}
                      labelStyle={{
                          color: 'grey'
                      }}
                      arrowStyle={{color: 'grey'}}
                      defaultNull
                      placeholder="Alder"
                      containerStyle={{height: 50,width:width*0.95,borderRadius:8,marginBottom:10}}
                      onChangeItem={item =>  this.setState({ selectedSize: item.value})}
                  /> 
            ):null}  
             {this.state.selectedType=="Beklædning"&&this.state.selectedOption=="Accessories"?(
                
                    <DropDownPicker multiple multipleText={'%d valgte aldre'}
                    zIndex={5500}
                     style={{
                      borderTopLeftRadius: 10, borderTopRightRadius: 10,
                      borderBottomLeftRadius: 10, borderBottomRightRadius: 10
                  }}
                    arrowColor={'grey'}
                    items={items}
                    labelStyle={{
                        color: 'grey'
                    }}
                    arrowStyle={{color: 'grey'}}
                    defaultNull
                    placeholder="Valgt alder"
                    containerStyle={{height: 50,width:width*0.95,borderRadius:8,marginBottom:10}}
                    onChangeItem={defaultValue =>  this.setState({ selectedAge: defaultValue})}
                />
               
            ):null}  
                                                      
         {this.state.selectedType=="Rodekassen"||this.state.selectedType=="Beklædning"||this.state.selectedType=="Legetøj"||this.state.selectedType=="Børneværelset"||this.state.selectedType=="Cykler"||this.state.selectedType=="Udstyr"||this.state.selectedType=="Sport & fritid"?(
               
              <DropDownPicker 
               zIndex={5000}
                style={{
                  borderTopLeftRadius: 10, borderTopRightRadius: 10,
                  borderBottomLeftRadius: 10, borderBottomRightRadius: 10
              }}
               arrowColor={'grey'}
               items={[ 
                {label:"Helt ny (ubrugt)" ,value:"Helt ny (ubrugt)"},
                {label: 'Næsten som ny', value:'Næsten som ny'},
                {label: 'Brugt, men fin', value:'Brugt, men fin'},
              ]}
               labelStyle={{
                   color: 'grey'
               }}
               arrowStyle={{color: 'grey'}}
               defaultNull
               placeholder="Stand"
               containerStyle={{height: 50,width:width*0.95,borderRadius:8}}
               onChangeItem={item =>  this.setState({ selectedStand: item.value})}
           />
          
              ):null}  
               <Block style={{zIndex:300}}>
               <DropDownPicker 
               zIndex={4500}
                style={{
                  borderTopLeftRadius: 10, borderTopRightRadius: 10,
                  borderBottomLeftRadius: 10, borderBottomRightRadius: 10,zIndex:999
              }}
               arrowColor={'grey'}
               items={[ 
                {label:"Sender gerne" ,value:"1"},
                {label: 'Kan afhentes', value:'2'},
                {label: 'Tilbyder begge dele', value:'3'},
              ]}
               labelStyle={{
                   color: 'grey'
               }}
               arrowStyle={{color: 'grey'}}
               defaultNull
               placeholder="Forsendelse / fragt"
               containerStyle={{height: 50,width:width*0.95,borderRadius:8,marginTop:10}}
               onChangeItem={item =>  this.setState({ deliverytype: item.value})}
           />
           <View style={{flexDirection: 'row',position:'absolute',right:100,zIndex:4000,top:10}}>
                <Image  resizeMode="contain" style={{width:wp('6%') ,height:hp('6%')}} source={require('../../../assets/images/slice71-min.png')}/>
               <Text style={{color:'#54969c',fontFamily:'KGPrimaryWhimsy',fontSize:20,position:'absolute',marginTop:13,marginLeft:30,marginRight:30}}>1 ud af 5</Text>
            </View>
            </Block> 
           <Block  flex={1} center style={{zIndex:100}}>           
           <Block center style={{width:width*.95,zIndex:10}}>
              <Input style={{height: 50,borderColor:'#bfbfbf'}} onChangeText={(productRemark) => this.setState({ productRemark })} placeholder='Kort beskrivelse'/>
          </Block>
          </Block> 
          <Block  flex={1} center style={{zIndex:100}}>       
          <Block center style={{width:width*.95}}>
        <View style={{alignSelf:'center'}}>
        <TouchableOpacity onPress={() => {
                            
                           this.onCreate()
                          }}>
          <Text style={styles.opret}>Opret</Text>
      </TouchableOpacity>
        </View>
        <View style={{alignSelf:'center',marginBottom:10}}>
        <TouchableOpacity onPress={() => {
                          //  this.props.navigation.navigate('ProductProfile',); 
                          this.discardCreate()
                          }}>
          <Text style={styles.myItems}>Annuller</Text>
      </TouchableOpacity>
      
        </View>
        </Block>       
         
        </Block>
        </Block>
        </KeyboardAwareScrollView>
        
        <Dialog
            visible={this.state.showDialogMessage}
            onTouchOutside={() => { this.setState({ showDialogMessage: false }); }}
            onHardwareBackPress={() => {this.setState({ showDialogMessage: false });}}
            dialogStyle={{backgroundColor:'white',width:wp('90%'),alignItems:'center',alignContent:'center'}}>
            <DialogContent>
                <View styles={{alignItems:'center', }}>
                    <Text  style={{fontSize:28, color:'#eca6a1', padding: 12, textAlign:'center', fontFamily:'KGPrimaryWhimsy',}}>{this.state.dialog_message}</Text>
                </View>
            </DialogContent>
        </Dialog>
        
  </View>
  
  
   
    );
  }
  }

  const mapStateToProps = (state) =>{
    return{
      addProductData:state.addProductData,
      approvalCount:state.count.approvalBadgeCount,
    }
  }

  const mapDispatchToProps = (dispatch) =>{
    return{
      addProductData:(data)=>dispatch(addProductData(data)),
    }
  }
  
export default connect(mapStateToProps,mapDispatchToProps)(CreateProduct)

const styles = StyleSheet.create({
  container: {
    flex: 1,
    //alignItems: 'center',
    justifyContent: 'flex-end',
    backgroundColor: 'transparent'
  },
  container2: {
   
    backgroundColor: 'transparent',
    flex: 1,
    height:hp('100%'),
    width:wp('100%'),
    
  },
  textstyle:{
    fontFamily:'KGPrimaryWhimsy',
    fontSize:40
  },
  container3: {
    flex: 1,
    position:'absolute',
    alignItems: "center",
    alignContent:'center',
    justifyContent: 'flex-end',
  },
  dropdown: {
    height: Dimensions.get('screen').width /7,
    padding: 5,
    position:'relative',
    margin:5,
    borderWidth: .2,
    borderRadius: 12,
    textShadowColor: 'black',
    borderColor: 'black',
    marginBottom: 10,
    backgroundColor: 'white',
    opacity: 0.9,
    fontFamily:'KGPrimaryWhimsy',
    fontSize:20,
    alignContent:'center'
  },
  wrpper: {
    opacity: 0.9,
    fontSize:20,
    fontFamily:'KGPrimaryWhimsy',
    flexDirection: 'row', justifyContent: 'space-around',   
  },
  button: {
    alignSelf:'center',
    marginLeft:10,
    marginRight:20,
    margin:10,
    backgroundColor: 'white',
    borderColor: 'black',
    borderWidth: 3,
    borderTopColor:'red',
    borderRadius: 12,
    color: 'white',
    overflow: 'hidden',
    textAlign:'center',
    width: 350,
    height: 48,
    // opacity: 0.9,
    fontSize:20,
    fontFamily:'KGPrimaryWhimsy'
  },
  button2: {
    backgroundColor: '#54969c',
    borderColor: 'white',
    borderWidth: .5,
    borderRadius: 12,
    color: 'white',
    overflow: 'hidden',
    padding: 12,
    textAlign:'center',
    width: 350,
    height: 48,
    opacity: 0.9,
    fontSize:20,
    fontFamily:'KGPrimaryWhimsy'
  },
  wrapperHorizontal: {
    width: wp('90%'),
    height: hp('20%'),
    justifyContent: 'space-between',
    alignItems: "center",
    alignSelf:'center',
    marginTop:-hp('1%'),
    // color: "black",
  },
  wrapperVertical: {
    width: 100,
    height: 300,
    justifyContent: 'space-between',
    alignItems: "center",
    margin: "auto",
    color: "black"
  },
  itemStyleVertical: {
    marginTop: 10,
    marginBottom: 10,
    width: 50,
    height: 50,
    marginRight:wp('5%'),
    // paddingTop: 13,
    // borderWidth: 1,
    // borderColor: "grey",
    // borderRadius: 25
  },
  itemSelectedStyleVertical: {
    // paddingTop: 11,
    color:'red',
    // borderWidth: 2,
    // borderColor: "#DAA520"
  },
  itemStyleHorizontal: {
    marginLeft: 50,
    marginRight: 50,
    width: 50,
    height: 50,
    // paddingTop: 13,
    // borderWidth: 1,
    // borderColor: "grey",
    // borderRadius: 25
  },
  itemSelectedStyleHorizontal: {
    paddingTop: 11,
    borderWidth: 2,
    borderColor: "#DAA520"
  },
  button5: {
    backgroundColor: 'white',
    borderColor: '#4f4e4d',
    borderWidth: .5,
    borderRadius: 12,
    color: '#4f4e4d',
    overflow: 'hidden',
    padding: 12,
    
    width: wp('95%'),
    height: 50,
    opacity: 0.9,
    fontSize:15,
    marginBottom:10,
    marginTop:10
  },
  backButton: {
    height: 44,
    width: 44,
    justifyContent: 'center',
    alignItems: 'center',
  },
  opret: {
    backgroundColor: '#54969c',
    borderColor: 'white',
    borderWidth: .5,
    borderRadius: 12,
    color: 'white',
    overflow: 'hidden',
    padding: 12,
    textAlign:'center',
    width: wp('95%'),
    height: 44,
    opacity: 0.9,
    fontFamily:'KGPrimaryWhimsy',
    fontSize:Platform.OS==='ios'?20:14
  },
  myItems: {
    backgroundColor: 'grey',
    borderColor: 'white',
    borderWidth: .5,
    borderRadius: 12,
    color: 'white',
    overflow: 'hidden',
    padding: 12,
    textAlign:'center',
    width: wp('95%'),
    height: 44,
    opacity: 0.9,
    fontFamily:'KGPrimaryWhimsy',
    fontSize:Platform.OS==='ios'?20:14,
    marginTop:20
  },
});

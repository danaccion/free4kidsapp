import React from 'react';
import { View, StyleSheet, Image ,Animated,ActivityIndicator} from 'react-native';
import LottieView from 'lottie-react-native';
import { RecipeCard } from '../components/AppStyles';
import CacheImage from '../components/CacheImage'
const styles = StyleSheet.create({
    imageOverlay: {
        position: 'absolute',
        left: 0,
        right: 0,
        bottom: 0,
        top: 0,
      },
      container: {
        backgroundColor: 'transparent',
      },
});
class ProgressiveImage extends React.Component {

    
  render() {
    const {
        thumbnailSource,
        source,
        style,
        ...props
      } = this.props;
      return (
        // <View style={styles.container}>
        
        // <LottieView resizeMode="contain" source={require('../../assets/images/imageLoader.json')} autoPlay loop style={RecipeCard.loader}/>
        // </View>
        <View style={styles.container}>
        <ActivityIndicator size="small" color="#d47d7d" style={style} />
        <CacheImage
          // {...props}
          resizeMode={this.props.resizeMode}
          uri={this.props.source}
          style={[styles.imageOverlay, style]}
        />
      </View>
      );
  }
}
export default ProgressiveImage;
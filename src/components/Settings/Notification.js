import React, { Component } from 'react';
import { ScrollView, StyleSheet,View,ImageBackground,Image,TouchableOpacity,Platform,Dimensions,Text} from 'react-native';
import Ionicons from 'react-native-vector-icons/Ionicons'
import { Picker } from 'react-native-picker-dropdown';
import BackButton from '../../BackButton';
import HamburgerIcon from '../../icons/HamburgerIcon';
import HeartBagBurger from '../../icons/HeartBagBurger';
import { Container, Content, List, ListItem, Thumbnail,Icon,Button,InputGroup,Input,Body,Right,Badge } from 'native-base';
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';
export default class Notification extends Component {
  static navigationOptions = () => {
    return {
      
        headerTitle: (
            <Image
            resizeMode="contain"
            source={require('../../../assets/images/slice2.png')}
            style={{ width: 10, height:10 , flex:4,justifyContent: 'center', alignItems: 'center', padding: 25,bottom:10}}
          />
          ),
        headerRight: <HeartBagBurger/>
    };
};

  
    state = {
      username: '',
      password: '',
      ButtonStateHolder : true,
      loading: false,
      fontLoaded:false,
      preferences:''

    };
 
    back = async ()=>{
    this.props.navigation.navigate('Home');
    }
    submit =()=>{
      //this.props.navigation.navigate('Home');
    }
    messages =()=>{
      this.props.navigation.navigate('Activity');
    }

    approve =()=>{
      this.props.navigation.navigate('Approve');
    }

    initialArr = [
         {
             id: 1,
             category: "phone_message",
             text: "Accepteret",
             color: "black",
             note: "Besked når en familie har accepteret handlen",
             //image: "https://images.unsplash.com/photo-1511283402428-355853756676?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=1350&q=80"
  //image: "https://images.unsplash.com/photo-1511283402428-355853756676?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=1350&q=80"
           },
           {
             id: 2,
             category: "timer",
             text: "Godkend inden 48 timer",
             color: "black",
             note: "Besked når du skal acceptere en handel",
            // image: "https://images.unsplash.com/photo-1543886151-3bc2b944c718?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=1350&q=80"
           },
           {
             id: 3,
             category: "chat",
             color: "pink",
             text: "Chat",
             note: "Besked når du får en ny chatbesked",
             //image: "https://images.unsplash.com/photo-1543886151-3bc2b944c718?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=1350&q=80"
           },
           {
             id: 4,
             category: "phone",
             color: "black",
             text: "Nyhedsbrev",
             note: "Spændende tips og inspiration pr. mail",
            // image: "https://images.unsplash.com/photo-1543886151-3bc2b944c718?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=1350&q=80"
           },
          
      
    ];
  render(){
    const resizeMode = 'cover';
    return (
  
      <View style={styles.container}>
       
      <ImageBackground
         style={{
           backgroundColor: '#ccc',
           flex: 1,
           resizeMode,
           position: 'absolute',
           width: '120%', 
           height: '100%',
           justifyContent: 'center',
           
         }}
         source={require('../../../assets/images/slice81-min.png')}
         />
       
       <View style={{flex: 1, flexDirection: 'row',justifyContent:'space-between', margin:15,marginTop:Dimensions.get('screen').height/50}}>
           <View>
            {/* <TouchableOpacity onPress={this.back.bind(this)} loading={true}>
           <Ionicons name={Platform.OS === "ios" ? "ios-arrow-back" : "md-arrow-back"}  size={30}></Ionicons>
         </TouchableOpacity> */}
         </View>
         <View>
          <Text  style={{ fontSize: 30 ,alignSelf:'center',color:'#2db9b9',fontFamily:'KGPrimaryWhimsy'}}>Notifikationer</Text>
          </View>
          <View>
          {/* <TouchableOpacity onPress={this.back.bind(this)} loading={true}>
            <Image source={require('../../../assets/images/slice50-min.png')} style={{width: 20,height: 20,}}></Image>
         </TouchableOpacity> */}
         </View>
      </View>
      <View style={styles.container3}>
      <Container style={{backgroundColor:'transparent'}} >
      <Content>
      {this.initialArr===null?null:this.initialArr.map((prop, key) => {
        console.log(key);
         return (
           
              <List style={{backgroundColor:'white',borderColor:'black',borderWidth:.5,borderRadius: 15,marginBottom:3,margin:8}} key={prop.id}>
                  <ListItem button onPress={this.back}>
                   <Body style={{flexDirection:'column'}}>
         <Text style={{fontSize:25,marginRight:15,fontFamily:'KGPrimaryWhimsy'}}>{prop.text}</Text>
                    <Text note style={{fontSize:12,marginRight:15}}>{prop.note}</Text>
                    </Body>
                    {prop.category === "phone" ?
                    <View >
                    <Right>
                     <Thumbnail square small source={require('../../../assets/images/slice55-min.png')} style={{marginRight:55,padding:2,resizeMode:'cover'}}/>
                    </Right>
                    </View>
                     
                    :   null
                    }
                    {prop.category === "phone_message" ?
                    <View style={{flexDirection:'row'}}>
                       <Thumbnail square small source={require('../../../assets/images/slice55-min.png')} style={{marginRight:-30,padding:2,resizeMode:'cover'}}/>
                    <TouchableOpacity onPress={this.messages.bind(this)} loading={true}>
                   <Thumbnail  small source={{ uri: prop.image}} style={{marginRight:5}}/>
                   </TouchableOpacity>
                   <TouchableOpacity onPress={this.messages.bind(this)} loading={true}>
                   <Thumbnail square small source={require('../../../assets/images/slice67-min.png')} style={{marginRight:5,padding:2,resizeMode:'cover'}}/>
                   </TouchableOpacity>
                   </View>
                     :null}
                      {prop.category === "timer" ?
                    <View style={{flexDirection:'row'}}>
                       <Thumbnail square small source={require('../../../assets/images/slice55-min.png')} style={{marginRight:-30,padding:2,resizeMode:'cover'}}/>
                    <TouchableOpacity onPress={this.approve.bind(this)} loading={true}>
                   <Thumbnail  small source={{ uri: prop.image}} style={{marginRight:5}}/>
                   </TouchableOpacity>
                   <TouchableOpacity onPress={this.approve.bind(this)} loading={true}>
                   <Thumbnail square small source={require('../../../assets/images/slice67-min.png')} style={{marginRight:5,padding:2,resizeMode:'cover'}}/>
                   </TouchableOpacity>
                   </View>
                     :null}
                        {prop.category === "chat" ?
                    <View style={{flexDirection:'row'}}>
                       <Thumbnail square small source={require('../../../assets/images/slice55-min.png')} style={{marginRight:-30,padding:2,resizeMode:'cover'}}/>
                    <TouchableOpacity onPress={this.approve.bind(this)} loading={true}>
                   <Thumbnail  small source={{ uri: prop.image}} style={{marginRight:5}}/>
                   </TouchableOpacity>
                   <TouchableOpacity onPress={this.approve.bind(this)} loading={true}>
                   <Thumbnail square small source={require('../../../assets/images/slice67-min.png')} style={{marginRight:5,padding:2,resizeMode:'cover'}}/>
                   </TouchableOpacity>
                   </View>
                     :null}
                     {prop.category === "normal" ?
                      <View >
                      <Right>
                      <Icon name="ios-arrow-forward" />
                      </Right>
                      </View>
                     :null}
                  </ListItem>
                  {/* <Icon ios='ios-arrow-forward' android='md-arrow-dropright' iconRight/> */}
             </List>           
         );
         
      })}
            
        </Content>
         
       </Container>
      
      </View>
            
  </View>
   
    );
  }
  }
   

const styles = StyleSheet.create({
  container: {
    flex: 1,
    //alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: 'transparent'
  },
  container2: {
    flex: 1,
    //alignItems: 'center',
    //justifyContent: 'flex-start',
    
    backgroundColor: 'transparent'
    //bottom: Dimensions.get('screen').height /5
  },
  textstyle:{
    fontFamily:'KGPrimaryWhimsy',
    fontSize:40
  },
  container3: {
    position:'absolute',
    backgroundColor: 'transparent',
    flex: 1,
    height:hp('75%'),
    width:wp('100%'),
    top:hp('10%'),
    //justifyContent: "flex-end",
    //alignItems: "center"
  },
  dropdown: {
    height: Dimensions.get('screen').width /7,
    padding: 5,
    position:'relative',
    margin:5,
    borderWidth: .2,
    borderRadius: 12,
    textShadowColor: 'black',
    borderColor: 'black',
    marginBottom: 10,
    backgroundColor: 'white',
    opacity: 0.9,
    fontFamily:'KGPrimaryWhimsy',
    fontSize:20,
    alignContent:'center'
  },
  wrpper: {
    opacity: 0.9,
    fontSize:20,
    fontFamily:'KGPrimaryWhimsy',
    flexDirection: 'row', justifyContent: 'space-around',   
  },
  button: {
    backgroundColor: '#54969c',
    borderColor: 'white',
    borderWidth: .5,
    borderRadius: 12,
    color: 'white',
    overflow: 'hidden',
    padding: 12,
    textAlign:'center',
    width: 300,
    height: 48,
    opacity: 0.9,
    fontSize:20,
    fontFamily:'KGPrimaryWhimsy'
  },
});

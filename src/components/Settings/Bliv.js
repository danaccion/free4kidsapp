import React, { Component } from 'react';
import { ScrollView, StyleSheet,View,ImageBackground,Image,TouchableOpacity,Platform,Dimensions,Text,AsyncStorage} from 'react-native';
import Ionicons from 'react-native-vector-icons/Ionicons'
import { Picker } from 'react-native-picker-dropdown';
import BackButton from '../../BackButton';
import HamburgerIcon from '../../icons/HamburgerIcon';
import HeartBagBurger from '../../icons/HeartBagBurger';
import { Container, Content, List, ListItem, Thumbnail,Icon,Button,InputGroup,Input,Body,Right,Badge } from 'native-base';
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';
import { PH,SS } from 'react-native-dotenv'
export default class Bliv extends Component {
  static navigationOptions = () => {
    return {
      
        headerTitle: (
            <Image
            resizeMode="contain"
            source={require('../../../assets/images/slice2.png')}
            style={{ width: 10, height:10 , flex:4,justifyContent: 'space-around', alignItems: 'center', padding: 25,}}
          />
          ),
        headerRight: <HeartBagBurger/>
    };
};

  
    state = {
      username: '',
      password: '',
      ButtonStateHolder : true,
      loading: false,
      fontLoaded:false,
      preferences:'',
      kr:0

    };
    back = async ()=>{
    this.props.navigation.navigate('Home');
    }
    submit =()=>{
      //this.props.navigation.navigate('Home');
    }
    messages =()=>{
      this.props.navigation.navigate('ApproveChat');
    }

    async componentDidMount() {
      // await this.getViaLatLong()
      const { navigation } = this.props;
      const item = navigation.getParam('item');
      const user_id=await AsyncStorage.getItem('user_id')
      const username=await AsyncStorage.getItem('name')
    }

    
//     async getViaLatLong(){
//       console.log("DISTANCE");
//       const { navigation } = this.props;
//       const item = navigation.getParam('item');
//       let data = new FormData();
//       const user_id=await AsyncStorage.getItem('user_id')
//       console.log('getDistanceViaLatLong');
//       console.log(data.append('user_id1', user_id));
//       console.log(data.append('user_id2', 11));
//       console.log(data.append('product_id1',1));
//       console.log(data.append('product_id2',1));
      
//       fetch(PH+'/api/v1/products/getDistanceViaLatLong', {
//         method: 'POST',
//         headers: {
//           'Accept':'application/json',
//           'Content-Type': 'multipart/form-data',
//           'Authorization': 'Bearer '+await AsyncStorage.getItem('token')
//         },
//         body:data
//         }).then((response) => response.json())
//             .then((responseJson) => {
//               if(responseJson.code!=200){
//                 console.log(responseJson)
//                 // console.log(responseJson.title)
//                 // this.setState({
                  
//                 //   loading: false,popupVisible:true
//                 // })
//                 // Keyboard.dismiss();
//               }else if(responseJson.code == 200){
//                 console.log("Success getViaLatLong");
//                     this.setState({ 
//                       kr:responseJson.meta.DistanceViaLatLong
//                     })
//               }
          
//         })
//             .catch((error) => {
//               console.log("ayays",error)
//               return 0;
            
//         });
        
// }


    initialArr = [
      {
        id: 1,
        category: "normal",
        text: "Ofte stillende spørgsmål",
        color: "black",
        note: "Måske du kan finde hurtigere svar her",
        image: "https://images.unsplash.com/photo-1511283402428-355853756676?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=1350&q=80"
      },
      {
        id: 2,
        category: "normal",
        text: "Rapporter en fejl",
        color: "black",
        note: "Fortæl os gerne hvis du oplever fejl",
        image: "https://images.unsplash.com/photo-1543886151-3bc2b944c718?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=1350&q=80"
      },
      {
        id: 3,
        category: "normal",
        color: "pink",
        text: "Vores vilkår / betingelser",
        note: "Her finder du alle vores vilkår som bruge",
        image: "https://images.unsplash.com/photo-1543886151-3bc2b944c718?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=1350&q=80"
      },
      {
        id: 4,
        category: "normal",
        color: "black",
        text: "Kontakt os",
        note: "Vi svarer indenfor 24 timer",
        image: "https://images.unsplash.com/photo-1543886151-3bc2b944c718?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=1350&q=80"
      },
      
    ];
  render(){
    const resizeMode = 'cover';
    return (
  
      <View style={styles.container}>
       
      <ImageBackground
         style={{
           backgroundColor: '#ccc',
           flex: 1,
           resizeMode,
           position: 'absolute',
           width: '120%', 
           height: '100%',
           justifyContent: 'center',
           
         }}
         source={require('../../../assets/images/slice81-min.png')}
         />
       
       <View style={{flex: 1, flexDirection: 'row',justifyContent:'space-between', margin:15,marginTop:Dimensions.get('screen').height/50}}>
           <View>
            {/* <TouchableOpacity onPress={this.back.bind(this)} loading={true}>
           <Ionicons name={Platform.OS === "ios" ? "ios-arrow-back" : "md-arrow-back"}  size={30}></Ionicons>
         </TouchableOpacity> */}
         </View>
         <View>
          <Text  style={{ fontSize: 30 ,alignSelf:'center',color:'#2db9b9',fontFamily:'KGPrimaryWhimsy'}}>Bliv en del af os</Text>
          </View>
          <View>
          {/* <TouchableOpacity onPress={this.back.bind(this)} loading={true}>
            <Image source={require('../../../assets/images/slice50-min.png')} style={{width: 20,height: 20,}}></Image>
         </TouchableOpacity> */}
         </View>
      </View>
      <View style={styles.container3}>
      <Container style={{backgroundColor:'transparent'}} >
      <Content>
      <View style={styles.container4}>
      <View style={styles.containerx}>
      <View style={{flexDirection:'row', alignContent:'center',alignSelf:'center'}}>
          <Image source={require('../../../assets/images/slice12.png')} style={{width: wp('25%'),height: hp('20%')}}></Image>
          <Image source={require('../../../assets/images/slice6.png')} style={{width: wp('27%'),height: hp('20%')}}></Image>
          </View>
          </View>
          <View>
          <Text  style={{ fontSize:30 ,alignSelf:'center',color:'#fc82b3',fontFamily:'KGPrimaryWhimsy'}}>Opgrader til Familieplus</Text>
          </View>
          <View>
          <Text  style={{ fontSize: 15,alignSelf:'center',color:'#2db9b9'}}>Ingen binding - opsig når du vil</Text>
          </View>
          <View style={{marginTop:hp('1%')}}>
          <Text  style={{ fontSize: 15,alignSelf:'center'}}>✓Udvidet søgning i hele</Text>
          <Text  style={{ fontSize: 15,alignSelf:'center'}}>✓1stk. valgfri børneting hver måned</Text>
          <Text  style={{ fontSize: 15,alignSelf:'center'}}>✓Billig forsendelse</Text>
          <Text  style={{ fontSize: 15,alignSelf:'center'}}>✓Reklamefri app</Text>
          </View>
          <View style={{marginTop:hp('1%')}}>
          <Text  style={{ fontSize: 30,alignSelf:'center',fontFamily:'KGPrimaryWhimsy',color:'#fc82b3'}}> {this.state.kr},-</Text>
          {/* <Text  style={{ fontSize: 15,alignSelf:'center',color:'#fc82b3'}}>til de første 500</Text>
          <Text  style={{ fontSize: 15,alignSelf:'center',color:'#fc82b3'}}>familiemedlemmer</Text> */}
          </View>
          <View style={{alignSelf:'center',top:10}}>
             <TouchableOpacity onPress={this.submit.bind(this)}>
            <Text style={styles.button}>Bliv GRATIS</Text>
            </TouchableOpacity>   
            </View>
            <Text  style={{ fontSize: 13,alignSelf:'center',top:10}}>Hjælp?</Text>  
      </View>
            
        </Content>
       </Container>
      </View>
            
  </View>
   
    );
  }
  }
   

const styles = StyleSheet.create({
  container: {
    flex: 1,
    //alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: 'transparent'
  },
  container2: {
    flex: 1,
    //alignItems: 'center',
    //justifyContent: 'flex-start',
    
    backgroundColor: 'transparent'
    //bottom: Dimensions.get('screen').height /5
  },
  textstyle:{
    fontFamily:'KGPrimaryWhimsy',
    fontSize:40
  },
  container3: {
    position:'absolute',
    backgroundColor: 'transparent',
    flex: 1,
    height:hp('90%'),
    width:wp('100%'),
    top:hp('10%'),
    //justifyContent: "flex-end",
    //alignItems: "center"
  },
  dropdown: {
    height: Dimensions.get('screen').width /7,
    padding: 5,
    position:'relative',
    margin:5,
    borderWidth: .2,
    borderRadius: 12,
    textShadowColor: 'black',
    borderColor: 'black',
    marginBottom: 10,
    backgroundColor: 'white',
    opacity: 0.9,
    fontFamily:'KGPrimaryWhimsy',
    fontSize:20,
    alignContent:'center'
  },
  wrpper: {
    opacity: 0.9,
    fontSize:20,
    fontFamily:'KGPrimaryWhimsy',
    flexDirection: 'row', justifyContent: 'space-around',   
  },
  button: {
    backgroundColor: '#54969c',
    borderColor: 'white',
    borderWidth: .5,
    borderRadius: 12,
    color: 'white',
    overflow: 'hidden',
    padding: 12,
    textAlign:'center',
    width: wp('40%'),
    height: 48,
    opacity: 0.9,
    fontSize:20,
    fontFamily:'KGPrimaryWhimsy'
  },
  container4: {
    backgroundColor: 'white',
    borderColor: 'black',
    borderWidth: .5,
    borderRadius: 100,
    width: wp('90%'),
    height: hp('65%'),
    alignContent:'center',
    alignSelf:'center',
    alignItems:'center'
  },
});

import React, { Component } from 'react';
import { ScrollView, StyleSheet,View,ImageBackground,Image,TouchableOpacity,Platform,Dimensions,Text} from 'react-native';
import Ionicons from 'react-native-vector-icons/Ionicons'
import { Picker } from 'react-native-picker-dropdown';
import BackButton from '../../BackButton';
import HamburgerIcon from '../../icons/HamburgerIcon';
import HeartBagBurger from '../../icons/HeartBagBurger';
import { Container, Content, List, ListItem, Thumbnail,Icon,Button,InputGroup,Input,Body,Right,Badge } from 'native-base';
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';
export default class Dine extends Component {
  static navigationOptions = () => {
    return {
        headerLeft:<BackButton/>,
        headerTitle: (
            <Image
            resizeMode="contain"
            source={require('../../../assets/images/slice2.png')}
            style={{ width: 10, height:10 , flex:4,justifyContent: 'center', alignItems: 'center', padding: 25,bottom:10}}
          />
          ),
        // headerRight: <HeartBagBurger/>
    };
};

 
    state = {
      username: '',
      password: '',
      ButtonStateHolder : true,
      loading: false,
      fontLoaded:false,
      preferences:''

    };

    back = async ()=>{
    this.props.navigation.navigate('Home');
    }
    submit =()=>{
      //this.props.navigation.navigate('Home');
    }
    messages =()=>{
      this.props.navigation.navigate('ApproveChat');
    }


    async getFamilien(){
      console.log("familien");
      const user_id=await AsyncStorage.getItem('user_id')
      console.log(data.append('user_id1', user_id));
      console.log(data.append('user_id2', 11));
      console.log(data.append('product_id1',1));
      console.log(data.append('product_id2',1));
      
      fetch(PH+'/api/v1/products/', {
        method: 'POST',
        headers: {
          'Accept':'application/json',
          'Content-Type': 'multipart/form-data',
          'Authorization': 'Bearer '+await AsyncStorage.getItem('token')
        },
        body:data
        }).then((response) => response.json())
            .then((responseJson) => {
              if(responseJson.code!=200){
                console.log(responseJson)
                // console.log(responseJson.title)
                // this.setState({
                  
                //   loading: false,popupVisible:true
                // })
                // Keyboard.dismiss();
              }else if(responseJson.code == 200){
                console.log("Success getViaLatLong");
                    this.setState({ 
                      kr:responseJson.meta.DistanceViaLatLong
                    })
              }
          
        })
            .catch((error) => {
              console.log("ayays",error)
              return 0;
            
        });
        
}



    initialArr = [
      {
        id: 1,
        category: "normal",
        text: "Brugernavn",
        color: "black",
        note: "Familien: "+await AsyncStorage.getItem('name'),
        image: "https://images.unsplash.com/photo-1511283402428-355853756676?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=1350&q=80"
      },
      {
        id: 2,
        category: "normal",
        text: "Info",
        color: "black",
        note: "Dit navn, adresse og kontaktoplysninger",
        image: "https://images.unsplash.com/photo-1543886151-3bc2b944c718?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=1350&q=80"
      },
      {
        id: 3,
        category: "profile",
        color: "pink",
        text: "Profilbillede",
        note: "Dit akutelle billede på din profil",
        image: await AsyncStorage.getItem('image_url')
      },
      {
        id: 4,
        category: "normal",
        color: "black",
        text: "Familiebeskrivelse",
        note: "Fokus på allergier",
        image: "https://images.unsplash.com/photo-1543886151-3bc2b944c718?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=1350&q=80"
      },
      {
        id: 5,
        category: "normal",
        color: "black",
        text: "Kode",
        note: "maajs22",
        image: "https://images.unsplash.com/photo-1543886151-3bc2b944c718?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=1350&q=80"
      },
      {
        id: 6,
        category: "normal",
        color: "black",
        text: "Dine betalinger",
        note: "Overblik over dine betaliner for Familieplus",
        image: "https://images.unsplash.com/photo-1543886151-3bc2b944c718?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=1350&q=80"
      },
      
    ];
  render(){
    const resizeMode = 'cover';
    return (
  
      <View style={styles.container}>
       
      <ImageBackground
         style={{
           backgroundColor: '#ccc',
           flex: 1,
           resizeMode,
           position: 'absolute',
           width: '120%', 
           height: '100%',
           justifyContent: 'center',
           
         }}
         source={require('../../../assets/images/slice81-min.png')}
         />
       
       <View style={{flex: 1, flexDirection: 'row',justifyContent:'space-between', margin:15,marginTop:Dimensions.get('screen').height/50}}>
           <View>
            {/* <TouchableOpacity onPress={this.back.bind(this)} loading={true}>
           <Ionicons name={Platform.OS === "ios" ? "ios-arrow-back" : "md-arrow-back"}  size={30}></Ionicons>
         </TouchableOpacity> */}
         </View>
         <View>
          <Text  style={{ fontSize: 30 ,alignSelf:'center',color:'#2db9b9',fontFamily:'KGPrimaryWhimsy'}}>Dine oplysninger</Text>
          </View>
          <View>
          {/* <TouchableOpacity onPress={this.back.bind(this)} loading={true}>
            <Image source={require('../../../assets/images/slice50-min.png')} style={{width: 20,height: 20,}}></Image>
         </TouchableOpacity> */}
         </View>
      </View>
      <View style={styles.container3}>
      <Container style={{backgroundColor:'transparent'}} >
      <Content>
      {this.initialArr.map((prop, key) => {
        console.log(key);
         return (
           
              <List style={{backgroundColor:'white',borderColor:'black',borderWidth:.5,borderRadius: 15,marginBottom:3,margin:8}} key={prop.id}>
                  <ListItem button onPress={this.back}>
                   <Body style={{flexDirection:'column'}}>
         <Text style={{fontSize:25,marginRight:15,fontFamily:'KGPrimaryWhimsy'}}>{prop.text}</Text>
                    <Text note style={{fontSize:12,marginRight:15}}>{prop.note}</Text>
                    </Body>
                    {prop.category === "clock" ?
                    <View >
                    <Right>
                     <Thumbnail square small source={require('../../../assets/images/slice60-min.png')} style={{marginRight:5,padding:2,resizeMode:'cover'}}/>
                    </Right>
                    </View>
                     
                    :   null
                    }
                    {prop.category === "profile" ?
                    <View style={{flexDirection:'row'}}>
                    <TouchableOpacity onPress={this.messages.bind(this)} loading={true}>
                    <Thumbnail square small source={require('../../../assets/icons/user_image.png')} style={{marginRight:5,padding:2,resizeMode:'cover'}}/>
                   </TouchableOpacity>
                   <View >
                      <Right>
                      <Icon name="ios-arrow-down" />
                      </Right>
                      </View>
                   </View>
                     :null}
                     {prop.category === "normal" ?
                      <View >
                      <Right>
                      <Icon name="ios-arrow-forward" />
                      </Right>
                      </View>
                     :null}
                  </ListItem>
                  {/* <Icon ios='ios-arrow-forward' android='md-arrow-dropright' iconRight/> */}
             </List>           
         );
         
      })}
            
        </Content>
         
       </Container>
      
      </View>
            
  </View>
   
    );
  }
  }
   

const styles = StyleSheet.create({
  container: {
    flex: 1,
    //alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: 'transparent'
  },
  container2: {
    flex: 1,
    //alignItems: 'center',
    //justifyContent: 'flex-start',
    
    backgroundColor: 'transparent'
    //bottom: Dimensions.get('screen').height /5
  },
  textstyle:{
    fontFamily:'KGPrimaryWhimsy',
    fontSize:40
  },
  container3: {
    position:'absolute',
    backgroundColor: 'transparent',
    flex: 1,
    height:hp('75%'),
    width:wp('100%'),
    top:hp('10%'),
    //justifyContent: "flex-end",
    //alignItems: "center"
  },
  dropdown: {
    height: Dimensions.get('screen').width /7,
    padding: 5,
    position:'relative',
    margin:5,
    borderWidth: .2,
    borderRadius: 12,
    textShadowColor: 'black',
    borderColor: 'black',
    marginBottom: 10,
    backgroundColor: 'white',
    opacity: 0.9,
    fontFamily:'KGPrimaryWhimsy',
    fontSize:20,
    alignContent:'center'
  },
  wrpper: {
    opacity: 0.9,
    fontSize:20,
    fontFamily:'KGPrimaryWhimsy',
    flexDirection: 'row', justifyContent: 'space-around',   
  },
  button: {
    backgroundColor: '#54969c',
    borderColor: 'white',
    borderWidth: .5,
    borderRadius: 12,
    color: 'white',
    overflow: 'hidden',
    padding: 12,
    textAlign:'center',
    width: 300,
    height: 48,
    opacity: 0.9,
    fontSize:20,
    fontFamily:'KGPrimaryWhimsy'
  },
});

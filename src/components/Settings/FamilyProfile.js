import React, { PureComponent } from 'react';
import {StyleSheet,View,ImageBackground,Image,TouchableOpacity,Platform,Dimensions,Text,AsyncStorage,TextInput,Button,ScrollView,
  TouchableWithoutFeedback,Keyboard,
  KeyboardAvoidingView } from 'react-native';
import BackButton from '../../BackButton';
import HeartBagBurger from '../../icons/HeartBagBurger';
import { Container, Thumbnail,Body,} from 'native-base';
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';
import Dialog, {SlideAnimation,DialogContent } from 'react-native-popup-dialog';
import Carousel, { Pagination } from 'react-native-snap-carousel';
import ProgressiveImage from '../ProgressiveImage';
import SwipeableRating from 'react-native-swipeable-rating';
import Modal from 'react-native-modalbox';
import { PH,SS } from 'react-native-dotenv'
const { width: viewportWidth } = Dimensions.get('window');
var screen = Dimensions.get('window');
import { connect } from 'react-redux';
import {loadReservation,minusreserveCount,deleteReservation} from '../actions/index'
const extraHeight = Platform.OS === 'ios' ? 50 : 100
const Fsize2 = Platform.OS === 'ios' ?25 : 18;
const Fsize3 = Platform.OS === 'ios' ?15 : 12;
const Fsize = Platform.OS === 'ios' ?22 : 16;
class FamilyProfile extends PureComponent {
  static navigationOptions = () => {
    return {
         headerLeft:<BackButton/>,
        headerTitle: (
        
            <Image
            resizeMode="contain"
            source={require('../../../assets/images/slice2.png')}
            style={{ width: 10, height:10 , flex:4,justifyContent: 'center', alignItems: 'center', padding: 25,bottom:10}}
          />
          ),
        headerRight: <HeartBagBurger/>
    };
};


    
   state = {
      username: '',
      password: '',
      ButtonStateHolder : true,
      loading: false,
      fontLoaded:false,
      preferences:'',
      userImage:null,
      userName:null,
      userRate:null,
      rating: 3,
      isOpen: false,
      isDisabled: false,
      swipeToClose: true,
      sliderValue: 0.3,
      fullscreen:false,
      itemData:null,
      activeSlide: 0,
    };


  onClose() {
    console.log('Modal just closed');
  }

  onOpen() {
    console.log('Modal just opened');
  }

  onClosingState(state) {
    console.log('the open/close of the swipeToClose just changed');
  }

   


  async componentDidMount() {
    let userProductList=await AsyncStorage.getItem('userProductList');
    if(userProductList!==null){
      this.setState({itemData:JSON.parse(userProductList)})
    }
    this.setState({userImage : await AsyncStorage.getItem('image_url'),userName : await AsyncStorage.getItem('name')})
    this.getUserProducts(await AsyncStorage.getItem('user_id'))
    }

    getUserProducts = async (id) =>{
      console.log(id)
      let data = new FormData();
      data.append('user_id', id);
      fetch(PH+'/api/v1/users/products', {
        method: 'POST',
        headers: {
          'Accept':'application/json',
          'Content-Type': 'multipart/form-data',
          'Authorization': 'Bearer '+await AsyncStorage.getItem('token')
        },
        body:data
        }).then((response) => response.json())
            .then(async (responseJson) => {
                this.setState({itemData:responseJson.meta.product})
                await AsyncStorage.setItem('userProductList', JSON.stringify(responseJson.meta.product) )
        })
            .catch((error) => {
              console.log("ayay")
        });
    }
    handleRating = (rating) => {
      this.setState({rating});
    }
    back = async ()=>{
    this.props.navigation.navigate('Home');
    }
    submit =()=>{
      //this.props.navigation.navigate('Home');
    }
    messages =()=>{
      this.props.navigation.navigate('ApproveChat');
    }
    next =()=>{
      this.props.navigation.navigate('DinKonto');
    }
    ratingCompleted(rating) {
      console.log("Rating is: " + rating)
    }

    renderImage = ({ item }) => (
      // console.log("ITEM",item)
      <TouchableOpacity  onPress={()=>this.setState({fullscreen:true})}>
        <View style={{flex: 1,
        justifyContent: 'center',
        width: wp('50%'),
        zIndex:10,
        top:this.state.fullscreen==true?30:0,
        height:this.state.fullscreen==true?400:200}}>
          <ProgressiveImage   
              source={{  uri: item.images[0] }}
              thumbnailSource={require('../../../assets/images/slice3.png')}
              style={{marginLeft:2,width:this.state.fullscreen==true?viewportWidth:160,height:this.state.fullscreen==true?400:200}}
              resizeMode={this.state.fullscreen==true?'contain':'cover'}/> 
            {this.state.fullscreen==true? <Text style={{position:'absolute',fontSize:Fsize2 ,alignSelf:'center',color:'#2db9b9',fontFamily:'KGPrimaryWhimsy',zIndex:99,bottom:10}}>{item.name}</Text>:null}
           
        </View>
      </TouchableOpacity>
    );
  render(){
    const resizeMode = 'cover';
    const { activeSlide } = this.state;  
    console.log(this.state.userImage)
    return (
     
      <View style={styles.container}>
    
      <ImageBackground
         style={{
           backgroundColor: '#ccc',
           flex: 1,
           resizeMode,
           position: 'absolute',
           width: '100%', 
           height: '100%',
           justifyContent: 'center',
           popupVisible:false
           
         }}
         source={require('../../../assets/images/slice81-min.png')}
         />
      
       <View style={{flex: 1, flexDirection: 'row',justifyContent:'space-between',top:10}}>
       <Dialog
                      visible={this.state.fullscreen}
                  >
              <DialogContent style={{width:wp('90%'),height:hp('70%'),alignItems:'center'}}>
              <TouchableOpacity style={{width:50,height:48,right:-5,top:7,position:'absolute',zIndex:100}} onPress={() => {this.setState({fullscreen:false})}}>
              <Image  resizeMode="contain" style={{width:30,height:30}} source={require('../../../assets/icons/close.png')}/>
              </TouchableOpacity>
              {this.state.itemData!==null?(
              <View style={{marginTop:10}}>
            
          
              <Carousel
                ref={c => {
                  this.slider1Ref = c;
                }}
                data={this.state.itemData}
                renderItem={this.renderImage}
                sliderWidth={viewportWidth}
                itemWidth={viewportWidth}
                inactiveSlideScale={1}
                inactiveSlideOpacity={1}
                firstItem={0}
                loop={false}
                autoplay={false}
                autoplayDelay={500}
                autoplayInterval={3000}
                onSnapToItem={index => this.setState({ activeSlide: index })}
              />
              <Pagination
                dotsLength={parseInt(this.state.itemData.length)}
                activeDotIndex={activeSlide}
                containerStyle={{ flex: 1,
                  position: 'absolute',
                  alignSelf: 'center',
                  justifyContent:'flex-end',
                  paddingVertical: 8,
                  marginTop: hp('60%'),zIndex:999}}
                  
                dotColor="rgba(128, 128, 128, 0.92)"
                dotStyle={{ width: 8,
                  height: 8,
                  borderRadius: 4,
                  marginHorizontal: 0}}
                inactiveDotColor="grey"
                inactiveDotOpacity={0.4}
                inactiveDotScale={0.6}
                carouselRef={this.slider1Ref}
                tappableDots={!!this.slider1Ref}
              />

             </View>
                          ):null}
              </DialogContent>
            </Dialog>
       <Dialog
          visible={this.state.popupVisible}
          onTouchOutside={() => {
            this.setState({ popupVisible: false });
          }}
        >
          
          <DialogContent style={{width:wp('90%')}}>
          {/* <Image  resizeMode="contain" style={{width:wp('90%') ,height:hp('20%'),alignSelf:'center'}} source={require('../../../assets/images/slice70-min.png')}/> */}
          <Text adjustsFontSizeToFit={true} style={{color:'black',fontSize:20,alignSelf:'center',justifyContent:'center',top:3}}>funktion stadig under udvikling..</Text>
          </DialogContent>
        </Dialog>
           <View>
            {/* <TouchableOpacity onPress={this.back.bind(this)} loading={true}>
           <Ionicons name={Platform.OS === "ios" ? "ios-arrow-back" : "md-arrow-back"}  size={30}></Ionicons>
         </TouchableOpacity> */}
         </View>
         <View>
          <Text  style={{ fontSize: 25 ,alignSelf:'center',color:'#2db9b9',fontFamily:'KGPrimaryWhimsy'}}>FAMILIEN <Text  style={{ fontSize: 25,color:'#ffb3b3',fontFamily:'KGPrimaryWhimsy',textTransform: 'uppercase'}}> {this.state.userName}</Text></Text>
          </View>
          <View>
          {/* <TouchableOpacity onPress={this.back.bind(this)} loading={true}>
            <Image source={require('../../../assets/images/slice50-min.png')} style={{width: 20,height: 20,}}></Image>
         </TouchableOpacity> */}
         </View>
         
      </View>     
      <View style={styles.container3}>
      
      <Container style={{backgroundColor:'transparent'}} >
        <View style={{flex:1,flexDirection:'column',alignContent:'center',alignItems:'center',bottom:10}}>
      <Body >
        {this.state.userImage==null?(
        <Thumbnail  source={require('../../../assets/icons/user_image.png')} />):
        <Thumbnail  source={{uri:this.state.userImage}} />      
        }
        </Body>
        <Body  style={{top:5}} >
        <SwipeableRating
          rating={this.state.rating}
          size={28}
          maxRating={6}
          gap={2}
          color='#e8b14a'
          emptyColor='#e8b14a'
          // onPress={() => this.refs.modal4.open()}
          allowHalves={true}
          xOffset={30}
        />
       
        </Body>
        <Body style={{width:wp('75%'),alignItems:'center',top:15}}>
        <Text  style={{  textAlignVertical: "center",textAlign: "center",fontSize: Fsize3 ,alignSelf:'center',color:'grey'}}>Vi er en røgfrifamilie. Vi er et dyrefrithjem.
Vi bruger parfumefri vaske og skyllemidde</Text>    
        </Body>
        <Body style={{width:wp('75%'),alignItems:'center',top:15,marginBottom:10}}>
        <TouchableOpacity onPress={() =>this.props.navigation.navigate('DinKonto')}>
          <Text style={styles.button}>Gå til min konto</Text>
        </TouchableOpacity>
        </Body>
        <Body style={{width:wp('75%'),alignItems:'center',top:15}}>
        <TouchableOpacity onPress={() =>this.props.navigation.navigate('FamilieGratis')}>
          <Text style={styles.button}>Prøv Familieplus gratis</Text>
        </TouchableOpacity>
        </Body>
        </View>
        <View style={{flex: 1, flexDirection: 'row', top: hp('5%'),zIndex:2000,justifyContent:'space-around',alignContent:'center',alignSelf:'center'}}>
              <View style={{marginLeft:-wp('5%')}}>
                <TouchableOpacity onPress={() =>this.props.navigation.navigate('Shop')}>
                  <Image  resizeMode="contain" style={{width:wp('45%') ,height:hp('25%'),alignSelf:'center'}} source={require('../../../assets/images/slice72-min.png')}/>
                  <Image  resizeMode="contain" style={{width:wp('18%') ,height:hp('12%'),alignSelf:'center',position:'absolute',alignSelf:'center',top:hp('8%')}} source={require('../../../assets/images/slice65-min.png')}/>
                  <Text style={{fontFamily:'KGPrimaryWhimsy',fontSize: 15,position:'absolute',alignSelf:'center',top:hp('20%')}}>Opret børneting</Text>          
                </TouchableOpacity>
              </View>

              <View style={{top:hp('16.5%'),marginLeft:-wp('3%')}}>
                <TouchableOpacity onPress={() =>this.togglePicker()}>
                  <Image  resizeMode="contain" style={{width:wp('8%') ,height:hp('8%'),alignSelf:'center'}} source={require('../../../assets/images/slice34-min.png')}/> 
                 
                </TouchableOpacity>
              </View>
              <View >
            {this.state.itemData!==null?(
          
              <View style={{width:wp('40%') ,height:hp('25%'),alignSelf:'center',top:12}} >
                  <Carousel
                ref={c => {
                  this.slider1Ref = c;
                }}
                data={this.state.itemData}
                renderItem={this.renderImage}
                sliderWidth={viewportWidth}
                itemWidth={viewportWidth}
                inactiveSlideScale={1}
                inactiveSlideOpacity={1}
                firstItem={0}
                loop={true}
                autoplay={true}
                autoplayDelay={500}
                autoplayInterval={3000}
                onSnapToItem={index => this.setState({ activeSlide: index })}
              />
                  {/* <SliderBox
             images={this.state.images}
             ImageComponent={ProgressiveImage}
            //  sliderBoxHeight={200}
             onCurrentImagePressed={index => this.setState({imagefullurl:this.state.images[index],fullscreen:true})}
             dotColor="#d18089"
             inactiveDotColor="#90A4AE"
             paginationBoxVerticalPadding={20}
             autoplay
             circleLoop
            //  parentWidth={wp('40%')}
             resizeMethod={'resize'}
             resizeMode={'contain'}
             paginationBoxStyle={{
               position: "absolute",
               bottom: 0,
               padding: 0,
               alignItems: "center",
               alignSelf: "center",
               justifyContent: "center",
               paddingVertical: 10
             }}
             dotStyle={{
               width: 10,
               height: 10,
               borderRadius: 5,
               marginHorizontal: 0,
               padding: 0,
               margin: 0,
               backgroundColor: "rgba(128, 128, 128, 0.92)"
             }}
             ImageComponentStyle={{borderRadius: 15, width: wp('40%'), marginTop: 2, marginBottom:10,marginLeft:30}}
             imageLoadingColor="#2196F3"
           /> */}
   
                  </View>
                      
            ):null}
                {/* <TouchableOpacity onPress={() =>this.Dress()}>
                  <Image  resizeMode="contain" style={{width:wp('40%') ,height:hp('25%'),alignSelf:'center'}} source={require('../../../assets/images/boots.png')}/>           
                </TouchableOpacity> */}
                
              </View>
              <View  style={{position:'absolute',top:hp('22%'),marginLeft:wp('50%'),flexDirection:'row'}}>
              <View>
                {/* <TouchableOpacity onPress={() =>this.props.navigation.navigate('EditProduct',{'item':this.state.itemData[this.state.activeSlide]})}> */}
                <TouchableOpacity onPress={() => this.setState({ popupVisible: true })}>
               
                  <Image  resizeMode="contain" style={{width:wp('15%') ,height:hp('10%'),alignSelf:'center'}} source={require('../../../assets/images/slice30-min.png')}/>           
                </TouchableOpacity>
                
              </View>
              <View style={{marginLeft:wp('5%')}}>
                <TouchableOpacity onPress={() =>this.setState({ popupVisible: true })}>
                  <Image  resizeMode="contain" style={{width:wp('15%') ,height:hp('10%'),alignSelf:'center'}} source={require('../../../assets/images/slice31-min.png')}/>           
                </TouchableOpacity>
                
              </View>
              </View>
            </View>
         
        <View  style={{position:'absolute',top:hp('55%'),marginLeft:wp('3%')}}>              
                  <Image  resizeMode="contain" style={{width:wp('95%') ,height:hp('25%'),alignSelf:'center'}} source={require('../../../assets/images/slice70-min.png')}/> 
              </View>
         
       </Container>
     
      </View>
       
        
     
        <Modal style={styles.modal4} position={"bottom"} ref={"modal4"}>
        {/* <TouchableWithoutFeedback onPress={()=>Keyboard.dismiss()}>  */}
      
        <Container style={{backgroundColor:'transparent'}} >
        <View style={{flex:1,flexDirection:'column',alignContent:'center',alignItems:'center',bottom:10}}>
      <Body >
      <Thumbnail  source={require('../../../assets/icons/user_image.png')} />        
        </Body>
        <Body  style={{top:5}} >
        <SwipeableRating
          rating={this.state.rating}
          size={28}
          maxRating={6}
          gap={2}
          color='#e8b14a'
          emptyColor='#e8b14a'
          onPress={() => this.refs.modal4.open()}
          allowHalves={true}
          xOffset={30}
        />
       
        </Body>
        </View>
       </Container>
       </Modal>
        
        {/* <KeyboardAvoidingView keyboardShouldPersistTaps={'always'}  extraScrollHeight={extraHeight} scrollEnabled={false} enableOnAndroid={true}>
        <TextInput multiline rounded style={{fontSize:20,position:'absolute',justifyContent:'flex-start',alignSelf:'center',backgroundColor:'white',height:200,width:wp('80%'),borderColor: '#bfbfbf',
          borderWidth: .5,
          borderRadius: 12,shadowColor: '#000',
          shadowOffset: { width: 0, height: 2 },paddingLeft:20,paddingRight:20,
          shadowOpacity: 0.5,
          shadowRadius: 1.5}} 
          // onChangeText={text => this.setState({messages:text})}
          autoCorrect={true}
        //   onEndEditing={(e) => 
        //   {
        //       )
        //   }
        // }
        
          ref={input => { this.textInput = input }}
          />
          <Button style={{alignSelf:'center',position:'absolute',marginTop:50}} title="SUBMIT"/>
           </KeyboardAvoidingView>
           </View>
           </TouchableWithoutFeedback> */}
      

        
       
  </View>
  
    );
  }
  }
  const mapStateToProps = (state) =>{
    return{
      reservationList:state.count.reservationList,
      reserveCount:state.count.reservationBadgeCount
    }
  }

  const mapDispatchToProps = (dispatch) =>{
    return{
      loadReservation:(data)=>dispatch(loadReservation(data)),
      minusreserve:(count)=>dispatch(minusreserveCount(count)),
      deleteReservation:(data)=>dispatch(deleteReservation(data))
    }
  }
  
export default connect(mapStateToProps,mapDispatchToProps)(FamilyProfile)   

const styles = StyleSheet.create({
  container: {
    flex: 1,
    //alignItems: 'center',
    flexDirection:'column',
    justifyContent: 'center',
    backgroundColor: 'transparent'
  },
  container2: {
    flex: 1,
    //alignItems: 'center',
    //justifyContent: 'flex-start',
    
    backgroundColor: 'transparent'
    //bottom: Dimensions.get('screen').height /5
  },
  textstyle:{
    fontFamily:'KGPrimaryWhimsy',
    fontSize:40
  },
  container3: {
    position:'relative',
    backgroundColor: 'transparent',
    height:hp('75%'),
    width:wp('100%'),
    //alignItems: "center"
  },
  dropdown: {
    height: Dimensions.get('screen').width /7,
    padding: 5,
    position:'relative',
    margin:5,
    borderWidth: .2,
    borderRadius: 12,
    textShadowColor: 'black',
    borderColor: 'black',
    marginBottom: 10,
    backgroundColor: 'white',
    opacity: 0.9,
    fontFamily:'KGPrimaryWhimsy',
    fontSize:20,
    alignContent:'center'
  },
  wrpper: {
    opacity: 0.9,
    fontSize:20,
    fontFamily:'KGPrimaryWhimsy',
    flexDirection: 'row', justifyContent: 'space-around',   
  },
  button: {
    backgroundColor: 'white',
    borderColor: 'black',
    borderWidth: .5,
    borderRadius: 12,
    color: '#54969c',
    overflow: 'hidden',
    padding: 12,
    textAlign:'center',
    width: 300,
    height: 44,
    opacity: 0.9,
    fontFamily:'KGPrimaryWhimsy',
    fontSize:Fsize
  },
  wrapper: {
    paddingTop: 50,
    flex: 1,
    position:'absolute',
    zIndex:999
  },
  modal4: {
    height: hp('80%')
  },

  btn: {
    margin: 10,
    backgroundColor: "#3B5998",
    color: "white",
    padding: 10
  },

  btnModal: {
    position: "absolute",
    top: 0,
    right: 0,
    width: 50,
    height: 50,
    backgroundColor: "transparent"
  },

  text: {
    color: "black",
    fontSize: 22
  }
});

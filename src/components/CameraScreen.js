import React, { Component } from "react";
import {
    View,
    Text,
    StyleSheet,
    ScrollView,
    Image,TouchableOpacity,
    Platform,
    AsyncStorage,
    Alert
} from "react-native";
import Modal from 'react-native-modal';
import { Camera} from 'expo-camera'
import * as Permissions from 'expo-permissions'
import * as ImagePicker from 'expo-image-picker';
import Constants from 'expo-constants'
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';
import { Container, Content, Header, Item, Icon, Input, Button } from 'native-base'
import Ionicons from 'react-native-vector-icons/Ionicons'
import PinchZoom from './PinchZoom';
import { PH } from 'react-native-dotenv'
import ImageResizer from 'react-native-image-resizer';
import * as ImageManipulator from "expo-image-manipulator";
import * as FileSystem from 'expo-file-system';
const MAX_ZOOM = 7; // iOS only
const ZOOM_F = Platform.OS === 'ios' ? 0.007 : 0.08;
import Dialog, {SlideAnimation,DialogContent } from 'react-native-popup-dialog';
import {connect} from 'react-redux'
import {getHasProducts} from '../components/actions/index'
 
 class CameraScreen extends Component {
 
    state = {
        hasCameraPermission: null,
        type: Camera.Constants.Type.back,
        hasAccesstoRoll:null,
        image: null,
        imageArray:[],
        flash:'off',
        zoom:0.0,
        notAllowed:false,
        onView:false,
        showDialogMessage:false,
        dialog_message:''
    }

    async componentDidMount() {
      this.willFocusListener = this.props.navigation.addListener('willFocus', () => {
        this.componentWillFocus();
      });
      this.didBlurListener = this.props.navigation.addListener('didBlur', () => {
          this.componentDidBlur();
         
      });
        const { status } = await Permissions.askAsync(Permissions.CAMERA);
      
         
        this.checkProduct();
        this.setState({ hasCameraPermission: status === 'granted',onView:true })    
      //   console.log(this.props.notAllowed)
      
        // console.log(this.props);
        this.getPermissionAsync()
    }

    componentWillUnmount() {
      this.didFocusListener.remove();
      this.didBlurListener.remove();
  }
  componentWillFocus() {
    this.setState({onView:true})     
}

componentDidBlur() {
  this.setState({onView:false,image: null,
    imageArray:[]})
}
    async snapPhoto() {       
      console.log('Button Pressed');
      if (this.camera) {
         console.log('Taking photo');
         const options = { quality: 1, base64: false, fixOrientation: false, 
         exif: true,};
         await this.camera.takePictureAsync(options).then(photo => {
          //  photo.exif.Orientation = photo.exif.Orientation*2;            
             console.log(photo);
            //  this.rotate90andFlip(photo)
             this.setState({
              image: photo.uri
            });  
            // this.props.onUpdate(photo.uri);          
             });     
       }
      }
     getPermissionAsync = async () => {
    if (Constants.platform.ios) {
      const { status } = await Permissions.askAsync(Permissions.CAMERA_ROLL);
      this.setState({ hasAccesstoRoll: status === 'granted' })
      if (status !== 'granted') {
        this.setState({
            showDialogMessage:true,
            dialog_message:'Sorry, we need camera roll permissions to make this work!'
        })
        //alert('Sorry, we need camera roll permissions to make this work!');
      }
    }else{
      const { status } = await Permissions.askAsync(Permissions.CAMERA_ROLL);
      this.setState({ hasAccesstoRoll: status === 'granted' })
      if (status !== 'granted') {
        this.setState({
            showDialogMessage:true,
            dialog_message:'Sorry, we need camera roll permissions to make this work!'
        })
        //alert('Sorry, we need camera roll permissions to make this work!');
      }else{
        const { status } = await Permissions.askAsync(Permissions.CAMERA_ROLL);
        this.setState({ hasAccesstoRoll: status === 'granted' })
      }
      // alert(status)
    }
   
  }

  checkProduct = async () =>{
    console.log("checking")
    let hasProduct=await AsyncStorage.getItem('hasProduct');
    console.log("bus",hasProduct);
      if(hasProduct!==null){
        console.log("sulod");
        this.props.getHasProducts(false)
      }else{
        fetch(PH+'/api/v1/users/hasproduct', {
          method: 'GET',
          headers: {
            'Accept':'application/json',
            'Content-Type': 'multipart/form-data',
            'Authorization': 'Bearer '+await AsyncStorage.getItem('token')
          },
          }).then((response) => response.json())
              .then(async (responseJson) => {
                console.log("ayay",responseJson.meta.productexist)
                if(responseJson.meta.productexist === true){
                  // this.setState({hasproduct:true})
                  await AsyncStorage.setItem('hasProduct', JSON.stringify(true) )
                  this.props.getHasProducts(false)
                }else{
                  this.props.getHasProducts(true)
                }
          })
              .catch((error) => {
                  console.log(error)
          });
      }
   
  }


  toggleModalAllowed = () => {
    
       
      this.props.getHasProducts(false)
    
  };

  // checkProduct = async () =>{
  //   console.log("token ni",await AsyncStorage.getItem('token'))
  //   fetch('https://free4kids.design4web.dk/api/v1/users/hasproduct', {
  //     method: 'GET',
  //     headers: {
  //       'Accept':'application/json',
  //       'Content-Type': 'multipart/form-data',
  //       'Authorization': 'Bearer '+await AsyncStorage.getItem('token')
  //     },
  //     }).then((response) => response.json())
  //         .then((responseJson) => {
  //           console.log(responseJson)
  //           if(responseJson.meta.productexist === true){
  //             this.setState({notAllowed:false})
  //           }else{
  //             this.setState({notAllowed:true})
  //           }
  //     })
  //         .catch((error) => {
  //             console.log(error)
  //     });
  // }
  onFlashTrue(){
    this.setState({flash:'on'})
  }
  onFlashFalse(){
    console.log("ayay")
    this.setState({flash:'off'})
  }
  onBack(){
    this.props.onBack({back:true});     
  }
  onCancel(){
    this.setState({image:null})
  }
 async onDone(){
    const manipResult = await ImageManipulator.manipulateAsync(
      this.state.image,
      [{ resize: { width: 500, height: 600 } }],
      { compress: 1, format: ImageManipulator.SaveFormat.PNG }
    );
    let fileInfo = await FileSystem.getInfoAsync(manipResult.uri);
    console.log("size",fileInfo.size)
    let orig = await FileSystem.getInfoAsync(this.state.image);
    console.log("orig",orig.size)
    
    var joined = this.state.imageArray.concat(manipResult.uri);
    // this.props.onUpdate(joined)
    this.props.navigation.navigate('CreateProduct',{ 'pictures':joined });
    // this.props.onBack({back:true});
  }
 async addMore(){
    const manipResult = await ImageManipulator.manipulateAsync(
      this.state.image,
      [{ resize: { width: 500, height: 600 } }],
      { compress: 1, format: ImageManipulator.SaveFormat.PNG }
    );
    let fileInfo = await FileSystem.getInfoAsync(manipResult.uri);
    console.log("size",fileInfo.size)
    let orig = await FileSystem.getInfoAsync(this.state.image);
    console.log("orig",orig.size)
    var joined = this.state.imageArray.concat(manipResult.uri);
    this.setState({imageArray: joined,image:null})
  }
  getImageFromPhone = async () => {
    console.log("ayay")
    this.getPermissionAsync()
    let result = await ImagePicker.launchImageLibraryAsync({
      mediaTypes: ImagePicker.MediaTypeOptions.Images,
      allowsEditing: false,
      aspect: [6, 8],
      quality:1
    });

    // let rotate= this.rotate90andFlip(result)
    console.log(result)
    if (result.cancelled==false) {
      this.setState({
        image: result.uri
      });  
      // this.props.onUpdate(result.uri); 
    }
  };

  _onPinchStart = () => {
    this._prevPinch = 1
  }

  _onDoubleTap = () => {
    this.setState({type:
      this.state.type === Camera.Constants.Type.back
      ? Camera.Constants.Type.front
      : Camera.Constants.Type.back
    })
  }

  _onPinchEnd = () => {
    this._prevPinch = 1
  }

  _onPinchProgress = (p) => {
    let p2 = p - this._prevPinch
    if(p2 > 0 && p2 > ZOOM_F) {
      this._prevPinch = p
      this.setState({zoom: Math.min(this.state.zoom + ZOOM_F, 1)}, () => {
      })
    }
    else if (p2 < 0 && p2 < -ZOOM_F) {
      this._prevPinch = p
      this.setState({zoom: Math.max(this.state.zoom - ZOOM_F, 0)}, () => {
      })
    }
  }


    render() {
      console.log("onView:",this.state.onView)
        const { hasCameraPermission } = this.state
        if (hasCameraPermission === null) {
            return <View />
        }
        else if (hasCameraPermission === false) {
            return <Text> No access to camera</Text>
        }
        else if(this.state.onView==true) {
            return (
                <View style={styles.container}>
                    <PinchZoom 
                    onPinchEnd={this._onPinchEnd}
                    onPinchStart={this._onPinchStart}
                    onPinchProgress={this._onPinchProgress}
                    doubleTap={this._onDoubleTap}>
                  
                 {this.state.image!==null&&this.state.onView==true?( 
                  <View style={styles.container}>
                     
                        <Image
                        resizeMode="contain"
                          style={{
                            width: wp('100%'),
                            height: hp('72%'),
                          }}
                          source={{ uri: this.state.image }}
                        />
                        <Text></Text>
                    <View style={{flexDirection:'row',alignContent:'space-between'}}>
                    <TouchableOpacity onPress={()=>this.onCancel()}>
                    <Text style={styles.button2}>Tag om</Text>
                    </TouchableOpacity>
                      <TouchableOpacity onPress={this.addMore.bind(this)}>
                      <Text style={styles.button}>Tilføj mere</Text>
                      </TouchableOpacity>
                      <TouchableOpacity onPress={this.onDone.bind(this)}>
                      <Text style={styles.button3}>Færdig</Text>
                      </TouchableOpacity>
                    </View>
                     
                    </View>
                  
                 ): 
                 
                 <Camera zoom={this.state.zoom}  autoFocus="on" flashMode={this.state.flash}  style={{ flex: 1, justifyContent: 'space-between' , width: wp('100%'),height:hp('90%')}} type={this.state.type}   ref={ (ref) => {this.camera = ref} }>
  
                
                 
                 <Header searchBar rounded
                     style={{
                         position: 'absolute', backgroundColor: 'transparent',
                         left: 0, top: 0, right: 0, zIndex: 0, alignItems: 'center',opacity:0
                     }}
                 >
                    
                 </Header>       
                 {this.state.flash=="on"?(
                   <TouchableOpacity  style={{position:'absolute' ,right:10,top:10,zIndex:100}}  onPress={() => {
                    this.onFlashFalse()    
                }}>
                         <Ionicons name={Platform.OS === "ios" ? "ios-flash" : "md-flash"} style={{color: 'white', fontSize: 25,}}
                        ></Ionicons>
                        </TouchableOpacity>
                  ):
                  <TouchableOpacity  style={{position:'absolute' ,right:10,top:10,zIndex:100}}  onPress={() => {
                    this.onFlashTrue()    
                }}>
                  <Ionicons name={Platform.OS === "ios" ? "ios-flash-off" : "md-flash-off"} style={{color: 'white', fontSize: 25,}}></Ionicons>
                    </TouchableOpacity>}
                  
                 <View style={{ flexDirection: 'row', justifyContent: 'space-between', paddingHorizontal: 0, marginBottom: 15, alignItems: 'flex-end' ,marginLeft:10 ,marginRight:10}}>
                   
                 <TouchableOpacity
                  style={{
                    alignSelf: 'flex-end',
                    alignItems: 'center',
                    backgroundColor: 'transparent',
                    opacity:0               
                  }}
                  onPress={()=>this.getImageFromPhone()}>
                  <Ionicons
                      name="ios-photos"
                      style={{ color: "#fff", fontSize: 20}}
                  />
                </TouchableOpacity>
                   <TouchableOpacity   onPress={this.snapPhoto.bind(this)}>
                     <View style={{ alignItems: 'center' }}>
                         <Ionicons name={Platform.OS === "ios" ? "ios-radio-button-off" : "ios-radio-button-off"}
                       
                             style={{ color: 'white', fontSize: 60 }}
                         ></Ionicons>
                        
                     </View>
                     </TouchableOpacity>
                     <TouchableOpacity    onPress={()=>this.getImageFromPhone()}>
                  <Ionicons
                      name="ios-photos"
                      style={{ color: "#fff", fontSize: 30}}
                  />
                     </TouchableOpacity>

                 </View>
               
             </Camera>}
             </PinchZoom>
             <Modal isVisible={this.props.hasproducts} style={{height:hp('50%')}} onBackdropPress={()=>this.toggleModalAllowed()} animationOut="fadeOutLeft" onSwipeStart={()=>console.log("ayay")}>
                 <TouchableOpacity style={{position:'relative',alignSelf:'flex-end',height:60,width:60,zIndex:99,right:25,top:-85}} onPress={()=>this.toggleModalAllowed()}>
                 </TouchableOpacity>
                 <TouchableOpacity style={{position:'relative',alignSelf:'center',height:60,width:80,zIndex:99,top:60}} onPress={()=>console.log("Reported")}>
                    
                 </TouchableOpacity>
                 <Image
                   style={{
                     zIndex:10,
                     resizeMode:'contain',
                     position: 'absolute',
                     alignSelf:'center',
                    width:wp('70%'),
                    height:wp('70%'),
                     justifyContent: 'center',
                     
                   }}
                   source={require('../../assets/icons/slice2.png')}
                   />
                   
                 </Modal>
                 
                <Dialog
                    visible={this.state.showDialogMessage}
                    onTouchOutside={() => { this.setState({ showDialogMessage: false }); }}
                    onHardwareBackPress={() => {this.setState({ showDialogMessage: false });}}
                    dialogStyle={{backgroundColor:'white',width:wp('90%'),alignItems:'center',alignContent:'center'}}>
                    <DialogContent>
                        <View styles={{alignItems:'center', }}>
                            <Text  style={{fontSize:28, color:'#eca6a1', padding: 12, textAlign:'center', fontFamily:'KGPrimaryWhimsy',}}>{this.state.dialog_message}</Text>
                        </View>
                    </DialogContent>
                </Dialog>
                 
                </View>
            )
        }else{
          return <View />
      }
    }
}

const mapStateToProps = (state) =>{
  console.log("buang",state.count.hasProducts);
  return{
    hasproducts: state.count.hasProducts
  }
}
const mapDispatchToProps = (dispatch) =>{
  return{
    
    getHasProducts:(data)=>dispatch(getHasProducts(data)),
  
  }
}

export default connect(mapStateToProps,mapDispatchToProps)(CameraScreen);
const styles = StyleSheet.create({
    container: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center'
    },
    button: {
      backgroundColor: '#54969c',
      borderColor: 'white',
      borderWidth: .5,
      borderRadius: 12,
      color: 'white',
      overflow: 'hidden',
      padding: 12,
      textAlign:'center',
      width: wp('31%'),
      height: 44,
      opacity: 0.9,
    },
    button2: {
      backgroundColor: '#d4675f',
      borderColor: 'white',
      borderWidth: .5,
      borderRadius: 12,
      color: 'white',
      overflow: 'hidden',
      padding: 12,
      textAlign:'center',
      width: wp('31%'),
      height: 44,
      opacity: 0.9,
    },
    button3: {
      backgroundColor: '#266336',
      borderColor: 'white',
      borderWidth: .5,
      borderRadius: 12,
      color: 'white',
      overflow: 'hidden',
      padding: 12,
      textAlign:'center',
      width: wp('31%'),
      height: 44,
      opacity: 0.9,
    },
});


import React, { Component } from 'react';
import { StyleSheet } from 'react-native';
import { withNavigation } from 'react-navigation';
import { TouchableOpacity } from 'react-native-gesture-handler';
import { Container, Content, List, ListItem, Thumbnail,Icon,Button,InputGroup,Input,Body,Right,Badge,Spinner } from 'native-base';
class BackButton extends Component {
 render() {
   return (
     <TouchableOpacity
       style={styles.backButton}
       onPress={() => {
         this.props.navigation.pop();
       }}
     >
       <Icon
         name= 'ios-arrow-back'
         size={20}
         style={{fontSize: 23, color: 'grey'}}
       />
     </TouchableOpacity>
   );
 }
}
const styles = StyleSheet.create({
 backButton: {
   height: 44,
   width: 44,
   justifyContent: 'center',
   alignItems: 'center',
 },
});
export default withNavigation(BackButton);
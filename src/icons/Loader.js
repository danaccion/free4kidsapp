import React, { Component } from 'react';
import {
  StyleSheet,
  View,
  Modal,
  ActivityIndicator,
  Text
} from 'react-native';
import AnimatedLoader from "react-native-animated-loader";
import { Container, Header, Content, Spinner } from 'native-base';

const Loader = props => {
  const {
    loading,
    ...attributes
  } = props;

  return (
   
        <AnimatedLoader
              loop={true}
              visible={loading}
              // overlayColor="rgba(255,255,255,0.75)"
              source={require("../../assets/images/loading2.json")}
              animationStyle={styles.lottie}
              speed={.5}
              
            />
       
  )
}

const styles = StyleSheet.create({
  modalBackground: {
    flex: 1,
    alignItems: 'center',
    flexDirection: 'column',
    justifyContent: 'space-around',
    backgroundColor: '#00000040'
  },
  lottie: {
    width: 200,
    height: 200
  },
  activityIndicatorWrapper: {
    backgroundColor: '#2db9c1',
    height: 100,
    width: 100,
    borderRadius: 10,
    display: 'flex',
    alignItems: 'center',
    // justifyContent: 'space-around'
  }
});

export default Loader;
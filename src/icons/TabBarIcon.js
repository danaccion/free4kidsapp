import React, { useState, useEffect } from 'react';

import {Image,View} from 'react-native'
import {Badge} from 'react-native-elements'
import { Ionicons } from '@expo/vector-icons';

import { connect } from 'react-redux';

 function TabBarIcon(props) {
 
  if(props.name=="shop"){
    return (
   
      <Image resizeMode="contain"
       style={{width:120,height:35,tintColor: props.focused ? '#e87289' : 'grey'}}
       source={require('../../assets/images/slice63-min.png')}
       />   
 );
  }else if(props.name=="giveaway"){
    return (
   
      <Image resizeMode="contain"
       style={{width:120,height:35}}
       source={require('../../assets/images/slice61-min.png')}
       />   
 );
  }else if(props.name=="approve"){
    return (
      <View  style={{ flexDirection: 'row', flex: 4 }}>
      <Image resizeMode="contain"
       style={{width:120,height:35, tintColor: props.focused ? '#e87289' : 'grey'}}
       source={require('../../assets/images/slice60-min.png')}
       />   
        {props.approvalCount!==0?
        <Badge
        status="warning"
        value={props.approvalCount}
        containerStyle={{ position: 'absolute', top: 2, right:35 }}
      />:null}
       </View>
 );
  }else if(props.name=="activity"){
    return (
      <View  style={{ flexDirection: 'row', flex: 4 }}>
      <Image resizeMode="contain"
       style={{width:120,height:35,tintColor: props.focused ? '#e87289' : 'grey'}}
       source={require('../../assets/images/slice59-min.png')}
       />   
         {/* <Badge
          status="warning"
          value={"2"}
          containerStyle={{ position: 'absolute', top: 2, right:35 }}
        /> */}
       </View>  
 );
  }else if(props.name=="reserve"){
    return (
      <View  style={{ flexDirection: 'row', flex: 4 }}>
      <Image resizeMode="contain"
       style={{width:120,height:35,tintColor: props.focused ? '#e87289' : 'grey'}}
       source={require('../../assets/images/slice62-min.png')}
       /> 
       {props.reservationCount!==0?
        <Badge
        status="warning"
        value={props.reservationCount}
        containerStyle={{ position: 'absolute', top: 2, right:35 }}
      />:null}
       
       </View>  
 );
  }else{
    return (
   
      <Image resizeMode="contain"
       style={{width:120,height:35}}
       source={require('../../assets/images/slice62-min.png')}
       />   
 );
  }
 
 
 
}
const mapStateToProps = (state) =>{
  return{
    reservationCount : state.count.reservationBadgeCount,
    approvalCount :  state.count.approvalBadgeCount,
  }
}

const mapDispatchToProps = (dispatch) =>{
  return{
    reserve:(count)=>dispatch(reserveCount(count))
  }
}

export default connect(mapStateToProps,mapDispatchToProps)(TabBarIcon)

import React, {Component} from 'react';
import { withNavigation } from 'react-navigation';
import { TouchableOpacity } from "react-native-gesture-handler";
import { Image,Text,View } from 'react-native';
import {Badge} from 'react-native-elements'
import Icon from 'react-native-vector-icons/SimpleLineIcons';
import {connect} from 'react-redux'
import {heartBagNotifCount} from '../components/actions/index'
import * as Animatable from 'react-native-animatable';
// import { Provider } from 'react-native-paper/lib/typescript/src/core/settings';
class HeartBagBurger extends Component{
  
  
    render() {
      
    return (
        <TouchableOpacity
        style={{
            width: 44,
            height: 44,
            marginLeft: 20,
            top:5
        }}
        onPress={()=>{
            this.props.navigation.navigate('Overview');
        }}>
       {this.props.heartBagNotif!==0?
               <Animatable.Image animation="tada" easing="ease-in" iterationCount="infinite"  
               source={require('../../assets/images/slice51-min.png')}
               style={{ width: 30, height: 30}}
             ></Animatable.Image>
            : <Image
            source={require('../../assets/images/slice51-min.png')}
            style={{ width: 30, height: 30}}
          ></Image>}
         
       
        
         {/* {this.props.heartBagNotif!==0?
                <Badge
                status="warning"
                value={this.props.heartBagNotif}
                containerStyle={{ position: 'absolute', top: 2, left:17 }}
            />:null} */}
        </TouchableOpacity>
        
    )
    };
}
const mapStateToProps = (state) =>{
    return{
      heartBagNotif : state.count.heartBagNotifCount,
    }
  }
  
  const mapDispatchToProps = (dispatch) =>{
    return{
        heartBagNotifCount:(count)=>dispatch(heartBagNotifCount(count))
    }
  }

export default connect(mapStateToProps,mapDispatchToProps)(withNavigation(HeartBagBurger));
import React, {Component} from 'react';
import { withNavigation } from 'react-navigation';
import { TouchableOpacity } from "react-native-gesture-handler";
import { Image,AsyncStorage } from 'react-native';

import Icon from 'react-native-vector-icons/SimpleLineIcons';
class HamburgerIcon extends Component{
    componentDidMount(){
        this.getImage()
    }
    async getImage (){
        this.setState({image:await AsyncStorage.getItem('image_url')})
    }
    state = {
        image:null
    }
    render() {
        console.log(this.state.image)
    return (
        <TouchableOpacity
        style={{
            width: 44,
            height: 44,
            marginLeft: 20,
            top:5
        }}
        onPress={()=>{
            this.props.navigation.navigate('Setting');
            // this.props.navigation.navigate.openDrawer();
        }}>
            <Image
          source={{uri:this.state.image}}
          style={{ width: 30, height: 30,borderRadius:30/2}}
        ></Image>
         <Image
            source={require('../../assets/images/img-gear.png')}
          style={{ width: 25, height: 25, position:'absolute',left:12,top:8}}
        ></Image>
        </TouchableOpacity>
    )
    };
}
export default withNavigation(HamburgerIcon);
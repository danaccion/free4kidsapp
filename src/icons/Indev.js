import React, { Component } from 'react';
import {
  StyleSheet,
  View,
  Modal,
  ActivityIndicator,
  Text,
  Platform
} from 'react-native';
const Fsize = Platform.OS === 'ios' ?15 : 13;
import { Container, Header, Content, Spinner } from 'native-base';

const Indev = props => {
  const {
    loading,
    ...attributes
  } = props;

  return (
    <Modal
      transparent={true}
      animationType={'none'}
      visible={loading}
      onRequestClose={() => {console.log('close modal')}}>
      <View style={styles.modalBackground}>
       
        <Content>
          <Text style={{color:'white',fontSize:Fsize,fontWeight:'400',bottom:5}}>IN DEVELOPMENT..</Text>
        </Content>
      
      </View>
    </Modal>
  )
}

const styles = StyleSheet.create({
  modalBackground: {
    flex: 1,
    alignItems: 'center',
    flexDirection: 'column',
    justifyContent: 'space-around',
    backgroundColor: '#00000090'
  },
  activityIndicatorWrapper: {
    backgroundColor: '#2db9c1',
    height: 100,
    width: 130,
    borderRadius: 10,
    display: 'flex',
    alignItems: 'center',
    // justifyContent: 'space-around'
  }
});

export default Indev;


var horizontalTabs=[
        {   
            id:2,
            type:'dress',
            description:'Beklædning',
            image:require('../../assets/images/slice43-min.png')
        },
        {
            id:3,
            type:'toys',
            description:'Legetøj',
            image:require('../../assets/images/slice44-min.png')
        },
        {  
            id:4,
            type:'beds',
            description:'Børneværelset',
            image:require('../../assets/images/slice45-min.png')
        },
    {
            id:6,
            type:'bike',
            description:'Cykler',
            image:require('../../assets/images/slice46-min.png')
    },
    {
        id:5,
        type:'Udstyr',
        description:'Udstyr',
        image:require('../../assets/images/image004.png')
    },
    {
        id:7,
        type:'Sport & fritid',
        description:'Sport & fritid',
        image:require('../../assets/images/image005.png')
    },  
    {
        id:8,
        type:'box',
        description:'Rodekassen',
        image:require('../../assets/images/slice48-min.png')
    },  
   
]

export {horizontalTabs};
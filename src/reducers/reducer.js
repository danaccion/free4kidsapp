import {
    DELETE_RESERVATION_PRODUCT,
    RESERVE_PRODUCT,
    MINUS_RESERVE_PRODUCT, 
    APPROVAL_COUNT,
    MINUS_APPROVAL_COUNT,
    LOAD_RESERVATION,
    LOAD_APPROVAL,
    DELETE_APPROVAL_PRODUCT,
    ADD_PRODUCT_DATA,
    LOAD_PRODUCTS,
    LOAD_BASKET_ITEMS,
    HAS_PRODUCTS,
    HEART_BAGNOTIF,
    ALL_NOTIFICATIONS,
    MESSAGES_NOTIF
    
} from '../components/actions/types';



const initialState = {
    reservationList: null,
    approvalList: null,
    reservationBadgeCount: 0,
    approvalBadgeCount: 0,
    basketBagdeCount:0,
    basketItems:null,
    addProductData:[],
    allProducts:null,
    hasProducts:false,
    onrefresh:true,
    category_id:null,
    heartBagNotifCount:0,
    allnotifications:null,
    messages_notif:null
}

 

const countReducer = (state = initialState, action) => {
    switch(action.type){
        case MESSAGES_NOTIF: 
        return {
            ...state,
            messages_notif:action.data
        };
        case ALL_NOTIFICATIONS: 
        return {
            ...state,
            allnotifications:action.data
        };
        case HEART_BAGNOTIF: 
        return {
            ...state,
            heartBagNotifCount:state.heartBagNotifCount+action.data
        };
        case HAS_PRODUCTS: 
        return {
            ...state,
            hasProducts:action.data
        };
        case LOAD_BASKET_ITEMS: 
        return {
            ...state,
            basketItems:action.data
        };
        case LOAD_PRODUCTS: 
        return {
            ...state,
            allProducts:action.data.data,
            onrefresh:false,
            category_id:action.data.category_id
        };

        case ADD_PRODUCT_DATA: 
        return {
            ...state,
            addProductData:state.addProductData.concate({
                field:action.data.field,
                data:action.data.description
            }),

        };

        case RESERVE_PRODUCT: 
        return {
            ...state,
            reservationBadgeCount:state.reservationBadgeCount+action.data,

        };

        case MINUS_RESERVE_PRODUCT: 
        return{
            ...state,
            reservationBadgeCount:state.reservationBadgeCount!==0?state.reservationBadgeCount-action.data:0
        };

        case APPROVAL_COUNT: 
        return {
            ...state,
            approvalBadgeCount:state.approvalBadgeCount+action.data
        };
        case DELETE_RESERVATION_PRODUCT: 
        return {
            ...state,
            reservationList:state.reservationList.filter((item)=>
            item.id!==action.data)
            
        };
        case DELETE_APPROVAL_PRODUCT: 
        return {
            ...state,
            approvalList:state.approvalList.filter((item)=>
            item.id!==action.data)
            
        };
        case MINUS_APPROVAL_COUNT:
        return{
            ...state,
            approvalBadgeCount:state.approvalBadgeCount!==0?state.approvalBadgeCount-action.data:0
        };

        case LOAD_RESERVATION: 
        return{
            ...state,
            reservationList:action.data
        };

        case LOAD_APPROVAL: 
        return{
            ...state,
            approvalList:action.data
        };


        default:
            return state;
    }
}
export default countReducer;
import React, {Component} from 'react';
import {StyleSheet, Text, View} from 'react-native';
import Icon from 'react-native-vector-icons/SimpleLineIcons';
import BackButton from './BackButton'
export default class BlueScreen extends Component {
    static navigationOptions = () => {
        return {
            headerLeft: <BackButton/>
        };
 };
    render() {
    return (
        <View style={styles.container}>
        <Text style={styles.title}>Blue Screen</Text>
        </View>
    );
    }
 }
  const styles = StyleSheet.create({
    container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: 'blue',
    },
    title: {
    fontSize: 20,
    textAlign: 'center',
    margin: 10,
    fontFamily:'KGPrimaryWhimsy',
    }
 });
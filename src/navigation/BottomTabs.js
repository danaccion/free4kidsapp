import * as React from 'react';
import { createAppContainer,withNavigationFocus,withNavigation } from "react-navigation";
import { createStackNavigator } from 'react-navigation-stack';
import {
  Platform,
  Text
} from "react-native";
import { createBottomTabNavigator } from 'react-navigation-tabs';
import Icon from 'react-native-vector-icons/SimpleLineIcons';
import TabBarIcon from '../icons/TabBarIcon';
import GreenScreen from "../GreenScreen";
import RedScreen from "../RedScreen";
import Giveaways from "../components/Giveaways/Giveaways";
import Srequest from "../components/Giveaways/BurgerComponents/Srequest";
import Clocations from "../components/Giveaways/BurgerComponents/Clocations";
import Donate from "../components/Giveaways/BurgerComponents/Donate";
import SaveBirth from "../components/Giveaways/BurgerComponents/SaveBirth";
import ShowFollowers from "../components/Giveaways/BurgerComponents/ShowFollowers";
import Overview from '../components/Reservations/BasketOverview'
import Activity from '../components/Activity/Index'
import ActivityChat from '../components/Activity/Chat'
import Reservation from '../components/Reservations/Index'
import ReservationChat from '../components/Reservations/Chat'
import TotalOrder from '../components/Reservations/TotalOrder'
import BuyShipping from '../components/Reservations/BuyShipping'
import BasketOverview from '../components/Reservations/BasketOverview'
import Approve from '../components/Approve/Index'
import ApproveChat from '../components/Approve/Chat'
import ApproveShipping from '../components/Approve/ApproveShipping'
import Setting from '../components/Settings/FamilyProfile'
import DinKonto from '../components/Settings/DinKonto';
import Slet from '../components/Settings/Slet'
import FamilieGratis from '../components/Settings/FamilieGratis'
import Help from '../components/Settings/Hjælp'
import Notification from '../components/Settings/Notification'
import Bliv from '../components/Settings/Bliv'
import Dine from '../components/Settings/Dine'
import CreateProduct from '../components/Shop/CreateProduct'
import ProductProfile from '../components/Shop/ProductProfile'
import StripeWebView from '../components/Shop/StripeWebView'
import ProductDetails from '../components/Shop/ProductDetails'
import UserProfile from '../components/Shop/UserProfile'
import DeactivateProduct from '../components/Approve/DeactivateProduct'
import EditComponent from '../components/EditProduct';
import CameraScreen from '../components/CameraScreen';
import {Provider} from 'react-redux';
import configureStore from '../store/index';

const store = configureStore();


const Fsize = Platform.OS === 'ios' ?10 : 8;
const activeTintLabelColor = '#e87289';
const inactiveTintLabelColor = '#808080';


    const ShopTab = createStackNavigator({
      Home: {screen:Giveaways},
      Srequest:{screen:Srequest},
      Clocations: { screen: Clocations },
      Donate: { screen: Donate },
      SaveBirth: { screen: SaveBirth },
      ShowFollowers: { screen: ShowFollowers },
      Overview: { screen: Overview },
      StripeWebView : {screen:StripeWebView},
      Setting: { screen: Setting },
      Slet: { screen: Slet },
      FamilieGratis: { screen: FamilieGratis },
      DinKonto: { screen: DinKonto },
      Help:  { screen: Help },
      Notification:  { screen: Notification },
      Bliv:  { screen: Bliv },
      Dine:  { screen: Dine },
      ProductDetails:{screen: ProductDetails},
      UserProfile: { screen: UserProfile },
      EditProduct: { screen: EditComponent },
      
    });

    ShopTab.navigationOptions = {
        // tabBarOnPress,
        tabBarLabel: ({ focused }) => ( 
        <Text adjustsFontSizeToFit numberOfLines={1}  style={{ fontSize: Fsize, alignSelf:'center',color:focused?activeTintLabelColor:'grey' }}> Shop </Text>
        ),
        tabBarIcon: ({ focused }) => (
          <TabBarIcon
            focused={focused}
            name='shop'
          />
        ),
        
        tabBarOptions: {
          activeTintColor: activeTintLabelColor,
          activeTintLabelColor:activeTintLabelColor,
          style: {
            backgroundColor: 'white',
          },
        },
       
      };
    const ReservationTab = createStackNavigator({
        Reservation: Reservation,
        TotalOrder: TotalOrder,
        ReservationChat: ReservationChat,
        BuyShipping: BuyShipping ,
        BasketOverview: BasketOverview,
        
    });
    ReservationTab.navigationOptions = {
        tabBarLabel:({ focused }) => ( 
        <Text adjustsFontSizeToFit numberOfLines={1}  style={{ fontSize: Fsize, alignSelf:'center',color:focused?activeTintLabelColor:'grey' }}> Reservationer </Text>
        ),
        tabBarIcon: ({ focused }) => (
          <TabBarIcon focused={focused} name='reserve' notif={12}/>
        ),
        tabBarOptions: {
          
          activeTintColor: activeTintLabelColor,
          labelStyle: {
            fontSize: 12,
          },
          style: {
            backgroundColor: 'white',
          },
        }
      };
    const HomeTab = createStackNavigator({
      
      Shop: CameraScreen,
      ProductProfile: { screen: ProductProfile },
      CreateProduct:{ screen: CreateProduct }
    });
    HomeTab.navigationOptions = {
        tabBarLabel:({ focused }) => ( 
        <Text adjustsFontSizeToFit numberOfLines={1}  style={{ fontSize: Fsize, alignSelf:'center',color:focused?activeTintLabelColor:'grey' }}> Giv Videre </Text>
        ),
        tabBarIcon: ({ focused }) => (
          <TabBarIcon focused={focused} name='giveaway'  />
        ),
        tabBarOptions: {
          activeTintColor: '#e87289',
          
          labelStyle: {
            fontSize: 7,
          },
          style: {
            backgroundColor: 'white',
          },
        }
      };
    const ApproveTab = createStackNavigator({
        Approve: Approve,
        ApproveChat: ApproveChat,
        ApproveShipping:ApproveShipping,
        DeactivateProduct:DeactivateProduct
    });
    ApproveTab.navigationOptions = {
        tabBarLabel:({ focused }) => ( 
        <Text adjustsFontSizeToFit numberOfLines={1}  style={{ fontSize: Fsize, alignSelf:'center',color:focused?activeTintLabelColor:'grey' }}> Godkend </Text>
        ),
        tabBarIcon: ({ focused ,tintColor}) => (
          // console.log("onfocus",focused)
          <TabBarIcon focused={focused} name='approve' />
        ),
        tabBarOptions: {
          activeTintColor: '#e87289',
          
          labelStyle: {
            fontSize: 12,
          },
          style: {
            backgroundColor: 'white',
          },
        }
      };
    const ActivityTab = createStackNavigator({
        Activity: Activity,
        ActivityChat: ActivityChat
    });
    ActivityTab.navigationOptions = {
        tabBarLabel:({ focused }) => ( 
        <Text adjustsFontSizeToFit numberOfLines={1}  style={{ fontSize: Fsize, alignSelf:'center',color:focused?activeTintLabelColor:'grey' }}> Notifikationer </Text>
        ),
        tabBarIcon: ({ focused,tintColor}) => (
          <TabBarIcon focused={focused} name='activity' />
        ),
        tabBarOptions: {
          activeTintColor: '#e87289',
          
          labelStyle: {
            fontSize: 20,
          },
          style: {
            backgroundColor: 'white',
          },
        }
      };

    const SettingTab = createStackNavigator({
      Slet: { screen: Slet },
      Help:  { screen: Help },
      Notification:  { screen: Notification },
      Bliv:  { screen: Bliv },
      Dine:  { screen: Dine },
      DinKonto:{ screen: DinKonto},
      FamilieGratis:{ screen: FamilieGratis},
    });
    // SettingTab.navigationOptions = {
    //     tabBarLabel: 'Aktivitet',
    //     tabBarIcon: ({ focused }) => (
    //       <TabBarIcon focused={focused} name='profile' />
    //     ),
    //   };

    const Tabs = createBottomTabNavigator({
        Home: ShopTab,
        Reservation: ReservationTab,
        Shop: HomeTab,
        Approve: ApproveTab,
        Activity: ActivityTab
    }, {
     
    
        // defaultNavigationOptions: ({ navigation }) => ({
        //   tabBarOnPress:(focused, scene)=>{
        //     // const {route, index} = scene;
        //     // if (route.index === 0) { // inside 1st screen of stacknavigator
        //     //   ReduxStore.dispatch(someAction());
          
        //     //   // Scroll to top
        //     //   const navigationInRoute = route.routes[0];
        //     //   if (!!navigationInRoute && !!navigationInRoute.params && !!navigationInRoute.params.scrollToTop) {
        //     //     navigationInRoute.params.scrollToTop();
        //     //   }
          
        //     // }
        //     // jumpToIndex(1); // Exit
        //     // console.log(scene)
        //      if(navigation.state.routeName=="Home"){
        //       const count=focused.navigation.state.routes[0].params.click++
        //       focused.navigation.state.routes[0].params.toTop(count)
        //      }
        //     if(focused){
        //       focused.navigation.navigate(navigation.state.routeName)
        //     }
        //   }
          
        // })
    });
    let RootNavigation = createAppContainer(withNavigation(Tabs));

    export default class BottomTabs extends React.Component {
      render() {
        return (
          <Provider store={store}>
            <RootNavigation/>
          </Provider>
        );
      }
    }
     
import React,{Component} from 'react';
import {
 createAppContainer,withNavigation
} from 'react-navigation';
import { createStackNavigator } from 'react-navigation-stack';
import { createDrawerNavigator } from 'react-navigation-drawer';
import { ScrollView } from 'react-native-gesture-handler';
import { SafeAreaView } from 'react-navigation';
import { Text,View,AsyncStorage,TouchableOpacity,Image} from 'react-native';
import Profile from '../components/Settings/FamilyProfile';
import DinKonto from '../components/Settings/DinKonto';
import DefaultScreen from '../DefaultScreen';
import styles from '../styles/SideMenu.style';
import BottomTabs from './BottomTabs';
// import Loader from '../../icons/addLoading';
import Overview from '../components/OverviewBasket/Overview'
import { Container, Content, List, ListItem, Thumbnail,Icon,Button,InputGroup,Input,Body,Right,Badge } from 'native-base';
import {widthPercentageToDP as wp, heightPercentageToDP as hp} from 'react-native-responsive-screen';


class HamburgerIcon extends Component{
    componentDidMount(){
        this.getImage()
    }
    async getImage (){
        this.setState({image:await AsyncStorage.getItem('image_url')})
    }
    state = {
        image:null
    }
    render() {
        console.log('OPPS',this.state.image)
    return (
        
            <Image
          source={{uri:this.state.image}}
          style={{ width: 30, height: 30,borderRadius:30/2}}
        ></Image>
    )
    };
}

const HamburgerNavigation = createDrawerNavigator(
    {
        Tabs: BottomTabs,
    },
    {
        initialRouteName: 'Tabs',
        contentComponent: props => {
            return (
                <View style={styles.container}>
                <ScrollView>
                    <SafeAreaView
                    forceInset={{ top: 'always', horizontal: 'never' }}
                >
                    <Body button style={{flexDirection:'row',alignSelf:'flex-start',marginLeft:20,marginBottom:10}}>
                    <View style={{marginRight:20}}>
                    <HamburgerIcon />
                    </View>
                    <TouchableOpacity onPress={() => {
                        props.navigation.navigate('Settings');
                        // props.navigation.closeDrawer();
                        }}>
                    <Text style={{ fontSize: 20 ,alignSelf:'center',color:'#2db9b9',fontFamily:'KGPrimaryWhimsy'}} >
                        Familieprofil
                    </Text>
                    </TouchableOpacity>
                    </Body>
                   
                    <Body button style={{flexDirection:'row',alignSelf:'flex-start',marginLeft:20,marginBottom:10}}>
                    <Thumbnail small source={require('../../assets/images/slice66-min.png')} style={{marginRight:10}}  />
                    <TouchableOpacity onPress={async()=> {
                         fetch('https://free4kids.design4web.dk/auth/logout', {
                            method: 'GET',
                            headers: {
                              'Accept':'application/json',
                               'Content-Type': 'application/json',
                              'Authorization': 'Bearer '+await AsyncStorage.getItem('token')
                            },
                            }).then((response) => response.json())
                                .then((responseJson) => {
                                    AsyncStorage.getAllKeys()
                                    .then(keys => AsyncStorage.multiRemove(keys))
                                    .then(() => console.log('success'));
                                    AsyncStorage.setItem('isLoggedIn','0');
                                    props.navigation.closeDrawer();
                                    props.navigation.navigate('Auth');
                            })
                                .catch((error) => {
                                    props.navigation.closeDrawer();
                                   props.navigation.navigate('Auth');
                            });
                        }}>
                    <Text style={{ fontSize: 20 ,alignSelf:'center',color:'#2db9b9',fontFamily:'KGPrimaryWhimsy'}}>
                        Log Out
                    </Text>
                    </TouchableOpacity>
                    </Body>
                    </SafeAreaView>
                </ScrollView>
               
                </View>
            )
        }
    }
 );
 const Stack = createStackNavigator(
    {
        Drawer: {
            screen: HamburgerNavigation,
            navigationOptions: {
                header: null,
            },
        },
        
        // Profile: { screen: Profile },
        DinKonto: { screen: DinKonto },
        DefaultScreen: {
            screen: DefaultScreen,
        }
    }
 );
export default createAppContainer(withNavigation(Stack));
import React, {Component} from 'react';
import {StyleSheet, Text, View} from 'react-native';
import Icon from 'react-native-vector-icons/SimpleLineIcons';
import BackButton from './BackButton'
export default class RedScreen extends Component {
    static navigationOptions = () => {
        return {
            headerLeft: <BackButton/>
        };
 };
    render() {
    return (
        <View style={styles.container}>
        <Text style={styles.title}>Red Screen</Text>
        </View>
    );
    }
 }
  const styles = StyleSheet.create({
    container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: 'red',
    },
    title: {
    fontSize: 20,
    textAlign: 'center',
    margin: 10,
    }
 });
import {createStore, combineReducers} from 'redux';
import badgeCountReducer from '../reducers/reducer';

const rootReducer = combineReducers({
    count: badgeCountReducer
})

const configureStore = () => createStore(rootReducer);

export default configureStore; 

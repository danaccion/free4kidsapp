const io = require('./index.js').io
const {VERIFY_USER,USER_CONNECTED,PRIVATE_MESSAGE} = require('../src/components/chatEvents/events.js')
const {createUser,createMessage,createChat} = require('../src/components/chatEvents/factories')
let connectedUsers = {}
module.exports = function(socket){
        console.log("socket id: "+socket.id)


        socket.on(VERIFY_USER,(nickname, callback)=>{
            if(isUser(connectedUsers,nickname)){
                callback({isUSer:true,user:null})
            }else{
                callback({isUSer:false,user:createUser({name:nickname,socketId:socket.id})})
            }
        } )

        socket.on(USER_CONNECTED,(user)=>{
            user.socketId=socket.id
            connectedUsers= addUser(connectedUsers,user)
            socket.user=user
            io.emit(USER_CONNECTED,connectedUsers)
            console.log(connectedUsers)

        })

        


    function addUser(userList,user){
        let newList =Object.assign({},userList)
        newList[user.name]=user
        return newList
    }

    function removeUser(userList,username){
        let newList =Object.assign({},userList)
        delete newList[username]
        return newList
    }

    function isUser(userList,username){
         
        return username in userList
    }


}
import React, {Component} from 'react';
import HamburgerNav from './src/navigation/HamburgerNav'
import Constants from 'expo-constants'
import * as Location from 'expo-location';
import {YellowBox } from 'react-native';
import LoadingScreen from './src/components/LoadingScreen'
import Login from './src/components/Auth/Login'
import ForgotPassword from './src/components/Auth/ForgotPassword'
import Register from './src/components/Auth/Register';
import HamburgerIcon from './src/icons/HamburgerIcon';
import { createStackNavigator } from 'react-navigation-stack';
import {createAppContainer, createSwitchNavigator } from 'react-navigation';
import { createDrawerNavigator } from 'react-navigation-drawer';
import { Asset } from 'expo-asset';
import * as Permissions from 'expo-permissions';
import { Notifications } from 'expo';
import {Provider} from 'react-redux';
import store from './src/store'
import * as Network from 'expo-network';
Text.defaultProps = Text.defaultProps || {};
Text.defaultProps.allowFontScaling = false;
TextInput.defaultProps = TextInput.defaultProps || {};
TextInput.defaultProps.allowFontScaling = false;
import {
  StyleSheet,Text,
  View,
  ActivityIndicator,
  AsyncStorage,
  StatusBar,
  Button,
  Image,
  TouchableHighlight,
  ImageBackground,
  TextInput,
  
  
} from 'react-native';
import * as Font from 'expo-font';
async function loadbackground() {
  await Promise.all([
    Asset.loadAsync([
      require('./assets/images/slice81-min.png')
    ]),
  ]);
}
const sleep = ms => new Promise(resolve => setTimeout(resolve, ms))

function cacheImages(images) {
  return images.map(image => {
    if (typeof image === 'string') {
      return Image.prefetch(image);
    } else {
      return Asset.fromModule(image).downloadAsync();
    }
  });
}

function cacheFonts(fonts) {
  return fonts.map(font => Font.loadAsync(font));
}

const Application = createStackNavigator({
  Home: { screen: HamburgerNav },
},
{
  headerMode: 'none',
  navigationOptions: {
      headerVisible: false,
  }
});

class App extends Component {
  
  
    state={
          fontLoaded:false,
          loaded:false,
          isReady: false,
          hasLocationPermission:null,
          popupVisible:false
        }
  async componentDidMount() {
   
    
  //  this.connect();
    // loadbackground()
    this._loadResourcesAsync()
    
    await AsyncStorage.getItem('isLoggedIn');
    
   
    
  }

  
  getPushNotificationPermissions = async () => {
    const { status: existingStatus } = await Permissions.getAsync(Permissions.NOTIFICATIONS);
    let finalStatus = existingStatus;
    
    // only ask if permissions have not already been determined, because
    // iOS won't necessarily prompt the user a second time.
    if (existingStatus !== 'granted') {
      // Android remote notification permissions are granted during the app
      // install, so this will only ask on iOS
      const { status } = await Permissions.askAsync(Permissions.NOTIFICATIONS);
      finalStatus = status;
    }

    // Stop here if the user did not grant permissions
    if (finalStatus !== 'granted') {
      return;
    }
    console.log(finalStatus)

    // Get the token that uniquely identifies this device
    const token = await Notifications.getExpoPushTokenAsync();
    console.log("Notification Token: ", await Notifications.getExpoPushTokenAsync());
    this.sendPushNotification(token);
  }

  async getNotif(){
    console.log("fetching resewrvation")
    let user_id= await AsyncStorage.getItem('user_id') 
      let data = new FormData();
      data.append('user_id', user_id);
      fetch(PH+'/api/v1/users/reservation', {
        method: 'POST',
        headers: {
          'Accept':'application/json',
          'Content-Type': 'multipart/form-data',
          'Authorization': 'Bearer '+await AsyncStorage.getItem('token')
        },
        body:data
        }).then((response) => response.json())
            .then((responseJson) => {
             
                console.log(responseJson)
                if(responseJson.meta.reservation.length>0){
                  this.setState({reserveData:responseJson.meta.reservation,refresh:false,refreshing:false})
                }
                else {
                  console.log("ayay")
                  this.setState({noData:true,reserveData:null,refreshing:false})
                }
        })
            .catch((error) => {
             console.log(error)
        });
  }



  async _loadResourcesAsync() {
    await Promise.all([
      Asset.loadAsync([require('./assets/images/slice80-min.png'),require('./assets/images/slice81-min.png'),require('./assets/images/slice49-min.png'),require('./assets/images/slice62-min.png'),require('./assets/images/slice51-min.png'),
      require('./assets/images/slice63-min.png'),require('./assets/images/slice60-min.png'),
      require('./assets/images/image001.png'),
      require('./assets/images/image004.png'),
      require('./assets/images/image005.png'),
      require('./assets/images/image008.png'),
      require('./assets/images/image010.png'),
      require('./assets/images/image011.png'),
      require('./assets/images/image013.png'),
      require('./assets/images/slice50-min.png'),
      require('./assets/images/slice52-min.png'),
      require('./assets/images/slice59-min.png'),
      require('./assets/images/slice70-min.png'),
      require('./assets/images/slice84-bil-min.png'),
      require('./assets/images/slice84-bil-min_flipped.png'),
      require('./assets/images/slice78-min.png'),
      require('./assets/images/slice73-min.png'),
      require('./assets/images/slice32-min.png'),
      require('./assets/images/slice33-min.png'),
      require('./assets/images/slice15_crop.png'),
      require('./assets/images/slice30-min.png'),
      require('./assets/images/slice82-ny-min.png'),
      require('./assets/images/slice66-min.png'),
      require('./assets/images/slice14.png'),
      require('./assets/images/slice14-inverted.png'),
      require('./assets/images/slice5-inverted.png'),
      require('./assets/images/slice12.png'),
      require('./assets/images/slice34-min.png'),
      require('./assets/images/slice31-min.png'),
      require('./assets/images/slice3.png'),
      require('./assets/images/slice2.png'),
      require('./assets/images/slice43-min.png'),
      require('./assets/images/slice44-min.png'),
      require('./assets/images/slice48-min.png'),
      require('./assets/images/slice42-min.png'),
      require('./assets/images/slice45-min.png'),
      require('./assets/images/slice72-min.png'),
      require('./assets/images/slice65-min.png'),
      require('./assets/icons/user_image.png'),
      require('./assets/images/image011.png'),
      require('./assets/images/slice46-min.png'),
      require('./assets/images/slice71-min.png'),
      require('./assets/icons/slice16.png'),
      require('./assets/images/page1.png'),
      require('./assets/images/page2.png'),
      require('./assets/images/page3.png'),
      require('./assets/images/page3.png'),
      require('./assets/icons/slice5.png'),
      require('./assets/icons/close.png'),
      require('./assets/icons/slice12.png'),
      require('./assets/icons/slice14.png'),
      require('./assets/images/img-inf.png'),
      require('./assets/girls_info/slice6.png'),
      require('./assets/girls_info/slice4.png'),
      require('./assets/girls_info/slice5.png'),
      require('./assets/girls_info/slice3.png'),
      require('./assets/girls_info/slice2.png'),
      require('./assets/girls_info/slice1.png'),
      require('./assets/icons/slice2.png'),
      require('./assets/icons/slice19.png'),
      require('./assets/images/slice61-min.png'),
      require('./assets/images/slice14-inverted.png'),
      require('./assets/icons/nogo.png'),
      require('./assets/icons/ok.png'),
    ]),
      Font.loadAsync({
        'KGPrimaryWhimsy':require('./assets/fonts/KGPrimaryWhimsy.ttf')
      }),
    ]);
    this.setState({ fontLoaded: true});
    this._loadData()
    
  }
  
  
  getPermissionAsync = async () => {
    // this.setState({refresh:true})
    
    if (Constants.platform.ios) {
      const { status } = await Permissions.askAsync(Permissions.LOCATION);
      // let loca = await Location.getCurrentPositionAsync();
      
      if (status !== 'granted') {
        alert('Sorry, we need Location permissions to serve you better!');
        const { status } = await Permissions.askAsync(Permissions.LOCATION);
      }else{
        // console.log("loca",loca,'status',status)
        let location = await Location.getCurrentPositionAsync({});
        AsyncStorage.setItem('user_location',location);
        this.setState({ hasLocationPermission: status === 'granted' })
      }
    }else{
      const { status } = await Permissions.askAsync(Permissions.LOCATION);
      this.setState({ hasLocationPermission: status === 'granted' })
      if (status !== 'granted') {
        alert('Sorry, we need Location permissions to serve you better!');
      }else{
      // console.log("loca",loca,'status',status)
      let location = await Location.getCurrentPositionAsync({});
      AsyncStorage.setItem('user_location',location);
      this.setState({ hasLocationPermission: status === 'granted' })
      
    }
   
    
    }
    await  this.checkProduct();
    this.getAllData();
 
   
  }
  
 render() {
   //Hides the logs //
  // console.log = function () {};
  YellowBox.ignoreWarnings([""]);
    return (
    <LoadingScreen/>
    );
 }
 _loadData= async() => {
  await sleep(1000)
  const isLoggedIn = await AsyncStorage.getItem('isLoggedIn');
  console.log(isLoggedIn);
  let isConnected = await Network.getNetworkStateAsync();
  console.log("is Connected",isConnected)
  this.props.navigation.navigate(isLoggedIn!=='1'?'Auth':'App');
}
}
const AuthStack = createStackNavigator({ 
  SignIn: Login,
  ForgotPassword: ForgotPassword,
  SignUp: Register,
 });
 
export default createAppContainer(createSwitchNavigator(
  {
    AuthLoading: App,
    App: Application,
    Auth: AuthStack,
    // Forsiden: Forsiden,
    // FamProfil: Oprettelse,
    // Indstillinger: Indstillinger,
    // Slet: Slet,
    // Hjaelp: Hjaelp,
    // Opgrader: Opgrader,
    // Notifikationer: Notify,
    // Dine_oplysninger: Dine_oplysninger,
    // Slet: Slet,
   // Giveaway: Giveaway,
  },
  {
    initialRouteName: 'AuthLoading',
  }
));